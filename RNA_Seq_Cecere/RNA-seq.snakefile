# Copyright (C) 2020-2023 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Snakefile to analyse RNA-seq data.
"""
import sys
major, minor = sys.version_info[:2]
if major < 3 or (major == 3 and minor < 6):
    sys.exit("Need at least python 3.6\n")


# TODO (04/10/2022):
# * normalize spike-in counts by their length (RPKM)
# * use scikit-learn to have a correction factor for transcript RPKM
# TODO first (04/10/2022):
# * output normalizations by total spike-ins (currently normalization is hard-coded to use protein_coding): raw from featureCounts / spike and RPKM (M would be "by million spike-in reads")
# * output slope and intercept of spike-in response in a file (and on the plot?)
# * find example config file activating spike-in stuff

# TODO: plot spike-in vs spike-in between libraries to detect anormal spike-ins: should be a straight line

# TODO: Add rules to take into account spike-ins.
# The size factor should be determined within the dynamic range of the spike-ins
# Use 1 RPKM for LDD
# See /Genomes/spike-ins/ERCC_RNA_Spike-In_Control_Mixes.pdf pp. 13-14
# Then use size factor to compute normalized RPKM and then fold changes like for small RNA IP

#TODO: same boxplots as small_RNA-seq
#TODO: for each contrast,  scatterplots with highlight of differentially expressed genes (with p-value colour code)
# and same scale for different biotypes
# boxplots of categories of genes (only protein-coding)
# Prepare scripts to compare with small RNA (fold vs fold)
#TODO: try to use bedtools intersect to do the counting (only reads strictly included in feature)
# bli@naples:/extra2/Documents/Mhe/bli/RNA_Seq_analyses$ mkfifo /tmp/gene_id
# bli@naples:/extra2/Documents/Mhe/bli/RNA_Seq_analyses$ mkfifo /tmp/count
# bli@naples:/extra2/Documents/Mhe/bli/RNA_Seq_analyses$ head -6 /tmp/WT_RT_1_protein_coding_counts_F1S.txt | tee >(cut -f9 | gtf2attr gene_id > /tmp/gene_id) | cut -f7,10 > /tmp/count & paste /tmp/gene_id /tmp/count
# "WBGene00022277"	-	170
# "WBGene00022276"	+	551
# "WBGene00022276"	+	522
# "WBGene00022276"	+	550
# "WBGene00022276"	+	550
# "WBGene00022276"	+	550


import os
OPJ = os.path.join
from distutils.util import strtobool
from glob import glob
from pickle import load
from yaml import safe_load as yload
from collections import defaultdict
from itertools import combinations
from functools import partial
from gzip import open as gzopen
from sys import stderr

import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)


warnings.formatwarning = formatwarning


def debug_wildcards(wildcards):
    print(wildcards)
    return []

from cytoolz import valmap
import matplotlib as mpl
# To be able to run the script without a defined $DISPLAY
mpl.use("PDF")
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
# https://stackoverflow.com/a/42768093/1878788
# from matplotlib.backends.backend_pgf import FigureCanvasPgf
# mpl.backend_bases.register_backend('pdf', FigureCanvasPgf)
# TEX_PARAMS = {
#     "text.usetex": True,            # use LaTeX to write all text
#     "pgf.rcfonts": False,           # Ignore Matplotlibrc
#     "pgf.texsystem": "lualatex",  # hoping to avoid memory issues
#     "pgf.preamble": [
#         r'\usepackage{color}'     # xcolor for colours
#     ]
# }
# mpl.rcParams.update(TEX_PARAMS)
mpl.rcParams["font.sans-serif"] = [
    "Arial", "Liberation Sans", "Bitstream Vera Sans"]
mpl.rcParams["font.family"] = "sans-serif"

import numpy as np
import pandas as pd
# To catch errors when plotting KDE
from scipy.linalg import LinAlgError
from scipy.stats import pearsonr, shapiro
from sklearn.linear_model import LinearRegression
import seaborn as sns
# To merge bigwig files
import pyBigWig
from mappy import fastx_read

from rpy2.robjects import Formula, StrVector
from idconvert import gene_ids_data_dir
from libdeseq import do_deseq2
from libhts import aligner2min_mapq
from libhts import status_setter, plot_lfc_distribution, plot_MA
from libhts import median_ratio_to_pseudo_ref_size_factors, size_factor_correlations
from libhts import (
    plot_paired_scatters,
    plot_norm_correlations,
    plot_counts_distribution,
    plot_boxplots)
from libworkflows import wc_applied, ensure_relative, cleanup_and_backup
from libworkflows import get_chrom_sizes, column_converter, make_id_list_getter
from libworkflows import strip_split, plot_text, save_plot, test_na_file, SHELL_FUNCTIONS, warn_context
from libworkflows import feature_orientation2stranded
from libworkflows import sum_by_family
from libworkflows import read_htseq_counts, sum_htseq_counts
from libworkflows import read_intersect_counts, sum_intersect_counts
from libworkflows import read_feature_counts, sum_feature_counts
from smincludes import rules as irules
from smwrappers import wrappers_dir

NO_DATA_ERRS = [
    "Empty 'DataFrame': no numeric data to plot",
    "no numeric data to plot"]
BAD_DATA_ERRS = [
    "`dataset` input should have multiple elements.",
    "array must not contain infs or NaNs",
    "zero-size array to reduction operation fmax which has no identity"]


alignment_settings = {"bowtie2": "", "hisat2": "", "crac": "-k 20 --stranded --use-x-in-cigar"}

# Possible feature ID conversions
ID_TYPES = ["name", "cosmid"]

# TODO: make this part of the configuration
LFC_RANGE = {
    "spike_ins" : (-10, 10),
    "protein_coding" : (-10, 10),
    "DNA_transposons_rmsk" : (-10, 10),
    "RNA_transposons_rmsk" : (-10, 10),
    "satellites_rmsk" : (-10, 10),
    "simple_repeats_rmsk" : (-10, 10),
    "DNA_transposons_rmsk_families" : (-10, 10),
    "RNA_transposons_rmsk_families" : (-10, 10),
    "satellites_rmsk_families" : (-10, 10),
    "simple_repeats_rmsk_families" : (-10, 10),
    "pseudogene" : (-10, 10),
    "all_rmsk" : (-10, 10),
    "all_rmsk_families" : (-10, 10),
    "alltypes" : (-10, 10)}
# Cutoffs in log fold change
LFC_CUTOFFS = [0.5, 1, 2]
UP_STATUSES = [f"up{cutoff}" for cutoff in LFC_CUTOFFS]
DOWN_STATUSES = [f"down{cutoff}" for cutoff in LFC_CUTOFFS]
#status2colour = make_status2colour(DOWN_STATUSES, UP_STATUSES)
#STATUSES = list(reversed(DOWN_STATUSES)) + ["down", "NS", "up"] + UP_STATUSES
#STATUS2COLOUR = dict(zip(STATUSES, sns.color_palette("coolwarm", len(STATUSES))))

# Or use --configfile instead
#configfile:
#    "RNA-seq.config.json"


spikein_dict = config["spikein_dict"]
spikein_microliters = spikein_dict["microliters"]
spikein_dilution_factor = spikein_dict["dilution_factor"]
# To get the expected number of attomoles of a given spike-in,
# multiply this by the value found in column 4 of Genomes/spike-ins/ERCC92_concentrations.txt
spikein_microliter_equivalent = spikein_microliters / spikein_dilution_factor
if spikein_microliter_equivalent:
    spikein_concentrations = pd.read_table(
        spikein_dict["concentrations"],
        index_col="ERCC ID",
        usecols=["ERCC ID", "concentration in Mix %s (attomoles/ul)" % spikein_dict["mix"]])
    from scipy.constants import Avogadro
    # attomole: 10^{-18} mole
    molecules_per_attomole = Avogadro / 10**18
    spikein_expected_counts = spikein_concentrations * spikein_microliter_equivalent * molecules_per_attomole
    spikein_expected_counts.columns = ["expected_counts"]

# http://sailfish.readthedocs.io/en/master/library_type.html
LIB_TYPE = config["lib_type"]
ORIENTATIONS = config["orientations"]
# key: library name
# value: dictionary
#   key: replicate number
#   value: path to raw data
lib2raw = config["lib2raw"]
LIBS = list(lib2raw.keys())
REPS = config["replicates"]
#COND_PAIRS = config["cond_pairs"]
#CONTRASTS = [f"{cond1}_vs_{cond2}" for [cond1, cond2] in COND_PAIRS]
DE_COND_PAIRS = config["de_cond_pairs"]
msg = "\n".join([
    "Some contrats do not use known library names.",
    "Contrasts:"
    ", ".join([f"({cond}, {ref})" for (cond, ref) in DE_COND_PAIRS])])
assert all([cond in LIBS and ref in LIBS for (cond, ref) in DE_COND_PAIRS]), msg
IP_COND_PAIRS = config["ip_cond_pairs"]
msg = "\n".join([
    "Some contrats do not use known library names.",
    "Contrasts:"
    ", ".join([f"({cond}, {ref})" for (cond, ref) in IP_COND_PAIRS])])
assert all([cond in LIBS and ref in LIBS for (cond, ref) in IP_COND_PAIRS]), ""
COND_PAIRS = DE_COND_PAIRS + IP_COND_PAIRS
DE_CONTRASTS = [f"{cond1}_vs_{cond2}" for [cond1, cond2] in DE_COND_PAIRS]
IP_CONTRASTS = [f"{cond1}_vs_{cond2}" for [cond1, cond2] in IP_COND_PAIRS]
contrasts_dict = {"de" : DE_CONTRASTS, "ip" : IP_CONTRASTS}
CONTRASTS = DE_CONTRASTS + IP_CONTRASTS
CONTRAST2PAIR = dict(zip(CONTRASTS, COND_PAIRS))
CONDITIONS = [{
    "lib" : lib,
    "rep" : rep} for rep in REPS for lib in LIBS]
# We use this for various things in order to have always the same library order:
COND_NAMES = ["_".join((
    cond["lib"],
    cond["rep"])) for cond in CONDITIONS]
# COND_COLUMNS = pd.DataFrame(CONDITIONS).assign(
#     cond_name=pd.Series(COND_NAMES).values).set_index("cond_name")


BIOTYPES = config["biotypes"]

RMSK_BIOTYPES = [
    "DNA_transposons_rmsk",
    "RNA_transposons_rmsk",
    "satellites_rmsk",
    "simple_repeats_rmsk"]
RMSK_FAMILIES_BIOTYPES = [
    "DNA_transposons_rmsk_families",
    "RNA_transposons_rmsk_families",
    "satellites_rmsk_families",
    "simple_repeats_rmsk_families"]

BIOTYPES_TO_JOIN = {
    "all_rmsk": [biotype for biotype in BIOTYPES if biotype in RMSK_BIOTYPES],
    "all_rmsk_families": [biotype for biotype in BIOTYPES if biotype in RMSK_FAMILIES_BIOTYPES],
    # We only count "protein_coding", not "protein_codin_{5UTR,CDS,3UTR}"
    "alltypes": [biotype for biotype in BIOTYPES if not biotype.startswith("protein_coding_")]}
JOINED_BIOTYPES = list(BIOTYPES_TO_JOIN.keys())
DE_BIOTYPES = [biotype for biotype in LFC_RANGE.keys() if biotype in BIOTYPES + JOINED_BIOTYPES]
if "spike_ins" in BIOTYPES:
    PAIR_SCATTER_BIOTYPES = ["spike_ins"]
else:
    PAIR_SCATTER_BIOTYPES = []
ID_LISTS = config.get("boxplot_gene_lists", [])
#ID_LISTS = [
#    "germline_specific", "histone",
#    "csr1_prot_si_supertargets_48hph",
#    "spermatogenic_Ortiz_2014", "oogenic_Ortiz_2014"]
aligner = config["aligner"]
########################
# Genome configuration #
########################
# config["genome_dict"] can be either the path to a genome configuration file
# or a dict
if isinstance(config["genome_dict"], (str, bytes)):
    print(f"loading {config['genome_dict']}", file=stderr)
    with open(config["genome_dict"]) as fh:
        genome_dict = yload(fh)
else:
    genome_dict = config["genome_dict"]
genome = genome_dict["name"]
chrom_sizes = get_chrom_sizes(genome_dict["size"])
chrom_sizes.update(valmap(int, genome_dict.get("extra_chromosomes", {})))
genomelen = sum(chrom_sizes.values())
genome_db = genome_dict["db"][aligner]
# bed file binning the genome in 10nt bins
genome_binned = genome_dict["binned"]
annot_dir = genome_dict["annot_dir"]
# What are the difference between
# OPJ(convert_dir, "wormid2name.pickle") and genome_dict["converter"]?
# /!\ gene_ids_data_dir contains more conversion dicts,
# but is not influenced by genome preparation customization,
# like splitting of miRNAs into 3p and 5p.
convert_dir = genome_dict.get("convert_dir", gene_ids_data_dir)
# For wormid2name, load in priority the one
# that might contain custom gene names, like for splitted miRNAs
with open(
        genome_dict.get(
            "converter",
            OPJ(convert_dir, "wormid2name.pickle")),
        "rb") as dict_file:
    wormid2name = load(dict_file)
gene_lists_dir = genome_dict["gene_lists_dir"]
avail_id_lists = set(glob(OPJ(gene_lists_dir, "*_ids.txt")))
#COUNTERS = config["counters"]
# htseq_count is very slow
# bedtools intersect eats all the RAM when there are a lot of rRNAs:
# https://github.com/arq5x/bedtools2/issues/565
COUNTERS = ["feature_count"]


upload_on_err = strtobool(str(config.get("upload_on_err", "True")))
#output_dir = config["output_dir"]
#workdir: config["output_dir"]
output_dir = os.path.abspath(".")
transcriptomes_dir = OPJ("merged_transcriptomes")
log_dir = OPJ(f"logs_{genome}")
data_dir = OPJ("data")

#NORM_TYPES = config["norm_types"]
if spikein_microliter_equivalent:
    SIZE_FACTORS = ["protein_coding", "miRNA", "median_ratio_to_pseudo_ref", "spike_ins"]
    NORM_TYPES = ["spike_ins", "protein_coding", "median_ratio_to_pseudo_ref"]
else:
    SIZE_FACTORS = ["protein_coding", "miRNA", "median_ratio_to_pseudo_ref"]
    NORM_TYPES = ["protein_coding", "median_ratio_to_pseudo_ref"]
    assert "spike_ins" not in BIOTYPES
assert set(SIZE_FACTORS).issubset(set(BIOTYPES) | {"median_ratio_to_pseudo_ref"})
assert set(NORM_TYPES).issubset(set(SIZE_FACTORS))

# PolyU tail min and max length
minU, maxU = 1, 5

# Seems to interfere with temp files deletion
wildcard_constraints:
    rep="\d+",
    orientation="|".join(ORIENTATIONS),
    biotype="|".join(BIOTYPES + JOINED_BIOTYPES),
    fold_type="|".join(["mean_log2_RPM_fold", "log2FoldChange", "lfcMLE"]),

#ruleorder: join_all_counts > gather_counts

# Define functions to be used in shell portions
shell.prefix(SHELL_FUNCTIONS)


def specific_input(aligner):
    if aligner == "hisat2":
        files = []
        # files = [
        #     expand(
        #         OPJ(aligner, f"mapped_{genome}", "ballgown",
        #             "{lib}_{rep}_on_%s.gtf" % genome),
        #         lib=LIBS, rep=REPS),
        #     OPJ(transcriptomes_dir, aligner, f"mapped_{genome}", "stringtie", "compare.stats"),
        #     OPJ(transcriptomes_dir, aligner, f"mapped_{genome}", "stringtie", "merged.gtf"),]
    elif aligner == "bowtie2":
        files = []
    elif aligner == "crac":
        files = []
    else:
        raise NotImplementedError("%s is not yet handeled." % aligner)
    return files

counts_files = [
    expand(
        OPJ(aligner, f"mapped_{genome}", "{counter}",
            "{lib}_mean_{mapped_type}", "{lib}_mean_{biotype}_{orientation}_TPM.txt"),
        counter=COUNTERS, lib=LIBS, mapped_type=[f"on_{genome}"],
        biotype=["alltypes"], orientation=ORIENTATIONS),
    expand(
        OPJ(aligner, f"mapped_{genome}", "{counter}",
            "all_{mapped_type}", "{biotype}_{orientation}_TPM.txt"),
        counter=COUNTERS, mapped_type=[f"on_{genome}", f"unique_on_{genome}"],
        biotype=["alltypes", "protein_coding"], orientation=ORIENTATIONS),
    expand(
        OPJ(aligner, f"mapped_{genome}", "{counter}",
            "all_{mapped_type}", "{biotype}_{orientation}_RPK.txt"),
        counter=COUNTERS, mapped_type=[f"on_{genome}", f"unique_on_{genome}"],
        biotype=BIOTYPES + JOINED_BIOTYPES, orientation=ORIENTATIONS),
    expand(
        OPJ(aligner, f"mapped_{genome}", "{counter}",
            "all_{mapped_type}", "{biotype}_{orientation}_counts.txt"),
        counter=COUNTERS, mapped_type=[f"on_{genome}", f"unique_on_{genome}"],
        biotype=BIOTYPES + JOINED_BIOTYPES, orientation=ORIENTATIONS),
    # expand(
    #     OPJ(aligner, f"mapped_{genome}", "{counter}",
    #         "summaries", f"{{lib}}_{{rep}}_on_{genome}_{{orientation}}_counts.txt"),
    #     counter=COUNTERS, lib=LIBS, rep=REPS, orientation=ORIENTATIONS),
    # expand(
    #     OPJ(aligner, f"mapped_{genome}", "{counter}",
    #         f"{{lib}}_{{rep}}_on_{genome}", "{biotype}_{orientation}_counts.txt"),
    #     counter=COUNTERS, lib=LIBS, rep=REPS, biotype=BIOTYPES, orientation=ORIENTATIONS),
    # expand(
    #     OPJ(aligner, f"mapped_{genome}", "{counter}",
    #         f"{{lib}}_{{rep}}_on_{genome}", "{biotype}_{orientation}_counts_gene_names.txt"),
    #     counter=COUNTERS, lib=LIBS, rep=REPS, biotype=BIOTYPES, orientation=ORIENTATIONS),]
    # Just a test:
    # expand(
    #     OPJ(aligner, f"mapped_{genome}", "{counter}",
    #         "summaries", "{lib}_{rep}_{mapped_type}_{orientation}_counts.txt"),
    #     counter=COUNTERS, lib=LIBS, rep=REPS,
    #     mapped_type=[f"on_{genome}", f"polyU_on_{genome}"], orientation=ORIENTATIONS),
    # expand(
    #     OPJ(aligner, f"mapped_{genome}", "{counter}",
    #         "{lib}_{rep}_{mapped_type}", "{lib}_{rep}_{biotype}_{orientation}_counts.txt"),
    #     counter=COUNTERS, lib=LIBS, rep=REPS,
    #     mapped_type=[f"on_{genome}", f"polyU_on_{genome}"], biotype=BIOTYPES, orientation=ORIENTATIONS),
    # expand(
    #     OPJ(aligner, f"mapped_{genome}", "{counter}",
    #         "{lib}_{rep}_{mapped_type}", "{lib}_{rep}_{biotype}_{orientation}_counts_gene_names.txt"),
    #     counter=COUNTERS, lib=LIBS, rep=REPS,
    #     mapped_type=[f"on_{genome}", f"polyU_on_{genome}"], biotype=BIOTYPES, orientation=ORIENTATIONS),]
    expand(
        OPJ(aligner, f"mapped_{genome}", "{counter}",
            "summaries", "{lib}_{rep}_{mapped_type}_{orientation}_counts.txt"),
        counter=COUNTERS, lib=LIBS, rep=REPS,
        mapped_type=[f"on_{genome}", f"unique_on_{genome}"], orientation=ORIENTATIONS),
    expand(
        OPJ(aligner, f"mapped_{genome}", "{counter}",
            "{lib}_{rep}_{mapped_type}", "{lib}_{rep}_{biotype}_{orientation}_counts.txt"),
        counter=COUNTERS, lib=LIBS, rep=REPS,
        mapped_type=[f"on_{genome}", f"unique_on_{genome}"], biotype=BIOTYPES, orientation=ORIENTATIONS),
    expand(
        OPJ(aligner, f"mapped_{genome}", "{counter}",
            "{lib}_{rep}_{mapped_type}", "{lib}_{rep}_{biotype}_{orientation}_counts_gene_names.txt"),
        counter=COUNTERS, lib=LIBS, rep=REPS,
        mapped_type=[f"on_{genome}", f"unique_on_{genome}"], biotype=BIOTYPES, orientation=ORIENTATIONS),]

# Actually not necessarily IP data
# TODO: generalize contrast types
if contrasts_dict["ip"]:
    ip_fold_boxplots = [
        expand(
            OPJ(aligner, f"mapped_{genome}", "{counter}", "fold_boxplots_{mapped_type}",
                "{contrast_type}_{orientation}_{biotype}_{fold_type}_{id_list}_boxplots.pdf"),
            #counter=COUNTERS, mapped_type=[f"on_{genome}", f"unique_on_{genome}"],
            counter=COUNTERS, mapped_type=[f"on_{genome}"],
            contrast_type=["ip"],
            orientation=ORIENTATIONS, biotype=DE_BIOTYPES,
            fold_type=["mean_log2_RPM_fold"], id_list=ID_LISTS),]
else:
    ip_fold_boxplots = []

if contrasts_dict["de"]:
    de_fold_boxplots = [
        expand(
            OPJ(aligner, f"mapped_{genome}", "{counter}", "fold_boxplots_{mapped_type}",
                "{contrast_type}_{orientation}_{biotype}_{fold_type}_{id_list}_boxplots.pdf"),
            counter=COUNTERS, mapped_type=[f"on_{genome}", f"unique_on_{genome}"],
            contrast_type=["de"],
            orientation=ORIENTATIONS, biotype=DE_BIOTYPES,
            fold_type=["log2FoldChange"], id_list=ID_LISTS),]
else:
    de_fold_boxplots = []

de_files = []
if len(REPS) > 1:
    de_files += expand(
        OPJ(aligner, f"mapped_{genome}", "{counter}",
            "deseq2_{mapped_type}", "{contrast}", "{orientation}_{biotype}", "{contrast}_counts_and_res.txt"),
        counter=COUNTERS, mapped_type=[f"on_{genome}", f"unique_on_{genome}"],
        contrast=DE_CONTRASTS, orientation=ORIENTATIONS, biotype=DE_BIOTYPES)
    de_files += expand(
        OPJ(aligner, f"mapped_{genome}", "{counter}",
            "deseq2_{mapped_type}", "{contrast}", "{orientation}_{biotype}", "{contrast}_{fold_type}_distribution.pdf"),
        counter=COUNTERS, mapped_type=[f"on_{genome}", f"unique_on_{genome}"],
        contrast=DE_CONTRASTS, orientation=ORIENTATIONS, biotype=DE_BIOTYPES,
        fold_type=["log2FoldChange"])
    de_files += expand(
        OPJ(aligner, f"mapped_{genome}", "{counter}",
            "deseq2_{mapped_type}", "{contrast}", "{orientation}_{biotype}", "{contrast}_MA_with_{id_list}.pdf"),
        counter=COUNTERS, mapped_type=[f"on_{genome}", f"unique_on_{genome}"],
        contrast=DE_CONTRASTS, orientation=ORIENTATIONS, biotype=DE_BIOTYPES,
        id_list=ID_LISTS + ["lfc_statuses"])
    de_files += de_fold_boxplots
else:
    warnings.warn("DESeq2 analyses will not be run because there are not enough replicates.\n")

bigwig_files = [
    # expand(
    #     OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}",
    #         "{lib}_{rep}_{orientation}_{mapped_type}_by_{norm_type}.bw"),
    #     lib=LIBS, rep=REPS, orientation=ORIENTATIONS,
    #     mapped_type=[f"on_{genome}", f"polyU_on_{genome}"], norm_type=NORM_TYPES),
    # expand(
    #     OPJ(aligner, f"mapped_{genome}", "{lib}_mean",
    #         "{lib}_mean_{orientation}_{mapped_type}_by_{norm_type}.bw"),
    #     lib=LIBS, orientation=ORIENTATIONS,
    #     mapped_type=[f"on_{genome}", f"polyU_on_{genome}"], norm_type=NORM_TYPES),]
    expand(
        OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}",
            "{lib}_{rep}_{orientation}_{mapped_type}_by_{norm_type}.bw"),
        lib=LIBS, rep=REPS, orientation=ORIENTATIONS,
        #mapped_type=[f"on_{genome}", f"unique_on_{genome}"], norm_type=NORM_TYPES),
        mapped_type=[f"on_{genome}"], norm_type=NORM_TYPES),
    expand(
        OPJ(aligner, f"mapped_{genome}", "{lib}_mean",
            "{lib}_mean_{orientation}_{mapped_type}_by_{norm_type}.bw"),
        lib=LIBS, orientation=ORIENTATIONS,
        #mapped_type=[f"on_{genome}", f"unique_on_{genome}"], norm_type=NORM_TYPES),]
        mapped_type=[f"on_{genome}"], norm_type=NORM_TYPES),]

if spikein_microliter_equivalent:
    spikein_files = [
        expand(OPJ(aligner, f"mapped_{genome}", "{counter}",
            f"all_on_{genome}", "{lib}_{rep}_spike_ins_{orientation}_TPM_response.pdf"),
            counter=COUNTERS, orientation=ORIENTATIONS, lib=LIBS, rep=REPS),]
else:
    spikein_files = []

replicates_comparisons = []
if len(REPS) > 1:
    ## TODO: debug:
    # Something to do with partial expand on quantification type?
    # Missing input files for rule compare_replicates:
    # results_44hph/hisat2/mapped_C_elegans/{counter}/all_{mapped_type}/{biotype}_{orientation}_RPM.txt
    # MissingInputException in line 1646 of /home/bli/src/bioinfo_utils/RNA-seq/RNA-seq.snakefile:
    # Missing input files for rule differential_expression:
    # results_44hph_vs_38hph/hisat2/mapped_C_elegans/{counter}/all_{mapped_type}/{biotype}_{orientation}_counts.txt
    # see https://bitbucket.org/snakemake/snakemake/issues/956/placeholders-not-properly-matched-with
    replicates_comparisons.extend(
        expand(
            OPJ(aligner, f"mapped_{genome}", "{counter}",
                "all_{mapped_type}", "replicates_comparison",
                "{lib}", "{biotype}_{orientation}_{quantif_type}_correlations.txt"),
            counter=COUNTERS, mapped_type=[f"on_{genome}", f"unique_on_{genome}"],
            lib=LIBS, biotype=["alltypes"],
            orientation=ORIENTATIONS, quantif_type=["TPM"]))
    replicates_comparisons.extend(
        expand(
            OPJ(aligner, f"mapped_{genome}", "{counter}",
                "all_{mapped_type}", "replicates_comparison",
                "{lib}", "{biotype}_{orientation}_{quantif_type}_correlations.txt"),
            counter=COUNTERS, mapped_type=[f"on_{genome}", f"unique_on_{genome}"],
            lib=LIBS, biotype=BIOTYPES + ["alltypes"],
            orientation=ORIENTATIONS, quantif_type=["counts", "RPM"]))
else:
    warnings.warn("Replicates comparisons will not be run because there are not enough replicates.\n")

localrules: all, link_raw_data

rule all:
    input:
        specific_input(aligner),
        expand(
            OPJ(aligner, f"mapped_{genome}", f"{{lib}}_{{rep}}_on_{genome}_samtools_stats.txt"),
            lib=LIBS, rep=REPS),
        expand(
            OPJ(aligner, f"mapped_{genome}", f"{{lib}}_{{rep}}_on_{genome}_coverage.txt"),
            lib=LIBS, rep=REPS),
        bigwig_files,
        spikein_files,
        # Uses too much memory when there are many libraries
        # expand(OPJ(aligner, f"mapped_{genome}", "{counter}",
        #     f"all_on_{genome}", "scatter_pairs/{biotype}_{orientation}_{quantif_type}_scatter_pairs.pdf"),
        #     counter=COUNTERS, biotype=["alltypes"], orientation=ORIENTATIONS, quantif_type=["TPM"]),
        # expand(OPJ(aligner, f"mapped_{genome}", "{counter}",
        #     f"all_on_{genome}", "scatter_pairs/{biotype}_{orientation}_{quantif_type}_scatter_pairs.pdf"),
        #     counter=COUNTERS, biotype=PAIR_SCATTER_BIOTYPES, orientation=ORIENTATIONS, quantif_type=["counts", "RPK"]),
        expand(
            OPJ(aligner, f"mapped_{genome}", "{counter}",
                "summaries", "all_{mapped_type}_{orientation}_{biotype}_norm_correlations.pdf"),
            counter=COUNTERS, mapped_type=[f"on_{genome}"], orientation=ORIENTATIONS, biotype=BIOTYPES + ["alltypes"]),
        expand(
            OPJ(aligner, f"mapped_{genome}", "{counter}",
                "summaries", "all_{mapped_type}_{orientation}_{biotype}_norm_counts_distrib.pdf"),
            counter=COUNTERS, mapped_type=[f"on_{genome}"], orientation=ORIENTATIONS, biotype=BIOTYPES + ["alltypes"]),
        de_files,
        ip_fold_boxplots,
        counts_files,
        replicates_comparisons,
        ##


include: ensure_relative(irules["link_raw_data"], workflow.basedir)
#include: "../snakemake_wrappers/includes/link_raw_data.rules"


# def mapping_command(aligner):
#     """This function returns the shell commands to run given the *aligner*."""
#     if aligner == "hisat2":
#         shell_commands = """
# cmd="hisat2 -p {threads} --dta --seed 123 -t --mm -x %s -U {input.fastq} --no-unal --un-gz {output.nomap_fastq} -S {output.sam}"
# echo ${{cmd}} 1> {log.log}
# eval ${{cmd}} 1>> {log.log} 2> {log.err}
# """ % genome_db
#     elif aligner == "bowtie2":
#         shell_commands = """
# cmd="bowtie2 -p {threads} --seed 123 -t --mm -x %s -U {input.fastq} --no-unal --un-gz {output.nomap_fastq} -S {output.sam}"
# echo ${{cmd}} 1> {log.log}
# eval ${{cmd}} 1>> {log.log} 2> {log.err}
# """ % genome_db
#     else:
#         raise NotImplementedError("%s is not yet handled." % aligner)
#     return shell_commands

# Could this be useful?
# https://stackoverflow.com/a/5328139/1878788
#def debug_output():
#    exc_type, exc_value, tb = sys.exc_info()
#    if tb is not None:
#        print(tb.tb_frame.f_locals)
#    return ""

rule map_on_genome:
    input:
        fastq = OPJ(data_dir, "{lib}_{rep}.fastq.gz"),
    output:
        # temp because it uses a lot of space
        sam = temp(OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}_on_%s.sam" % genome)),
        #sam = temp(OPJ(aligner, "mapped_%s" % genome, "{lib}_{rep}_on_%s.sam" % genome)),
        nomap_fastq = OPJ(aligner, f"not_mapped_{genome}", "{lib}_{rep}_unmapped_on_%s.fastq.gz" % genome),
    params:
        aligner = aligner,
        index = genome_db,
        settings = alignment_settings[aligner],
    message:
        "Mapping {wildcards.lib}_{wildcards.rep} on %s." % genome
    log:
        log = OPJ(log_dir, "map_on_genome_{lib}_{rep}.log"),
        err = OPJ(log_dir, "map_on_genome_{lib}_{rep}.err")
    resources:
        io=5
    threads:
        8
    #shell:
    #    mapping_command(aligner)
    wrapper:
        f"file://{wrappers_dir}/map_on_genome"


rule extract_nomap_polyU:
    f"""
    Extract from the non-mappers those that end with a polyU tail
    of length from {minU} to {maxU}."""
    input:
        nomap_fastq = OPJ(aligner, f"not_mapped_{genome}", "{lib}_{rep}_unmapped_on_%s.fastq.gz" % genome),
    output:
        nomap_polyU = OPJ(aligner, f"not_mapped_{genome}", "{lib}_{rep}_unmapped_on_%s_polyU.fastq.gz" % genome),
        nomap_polyU_stats = OPJ(aligner, f"not_mapped_{genome}", "{lib}_{rep}_unmapped_on_%s_polyU_stats.txt" % genome),
    run:
        stats = defaultdict(int)
        with gzopen(output.nomap_polyU, "wb") as fq_out:
            for (name, seq, qual) in fastx_read(input.nomap_fastq):
                trimmed = seq.rstrip("T")
                nb_T = len(seq) - len(trimmed)
                stats[nb_T] += 1
                if minU <= nb_T <= maxU:
                    # Find number of ending Ts
                    fq_out.write(f"@{name}\n{trimmed}\n+\n{qual:.{len(trimmed)}}\n".encode("utf-8"))
        with open(output.nomap_polyU_stats, "w") as stats_file:
            tot = 0
            for (nb_T, stat) in sorted(stats.items()):
                stats_file.write(f"{nb_T}\t{stat}\n")
                tot += stat
            stats_file.write(f"tot\t{tot}\n")


rule remap_on_genome:
    input:
        fastq = rules.extract_nomap_polyU.output.nomap_polyU,
    output:
        # temp because it might use a lot of space
        sam = temp(OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}_polyU_on_%s.sam" % genome)),
        nomap_fastq = OPJ(aligner, f"not_mapped_{genome}", "{lib}_{rep}_polyU_unmapped_on_%s.fastq.gz" % genome),
    params:
        aligner = aligner,
        index = genome_db,
        settings = alignment_settings[aligner],
    message:
        "Remapping {wildcards.lib}_{wildcards.rep} polyU-trimmed unmapped reads on %s." % genome
    log:
        log = OPJ(log_dir, "remap_on_genome_{lib}_{rep}.log"),
        err = OPJ(log_dir, "remap_on_genome_{lib}_{rep}.err")
    resources:
        io=5
    threads:
        8
    #shell:
    #    mapping_command(aligner)
    wrapper:
        f"file://{wrappers_dir}/map_on_genome"


def source_sam(wildcards):
    mapped_type = wildcards.mapped_type
    if mapped_type == f"on_{genome}":
        ## debug
        # Why rules.map_on_genome.output.sam doesn't get properly expanded?
        #return rules.map_on_genome.output.sam
        return OPJ(aligner, f"mapped_{genome}", f"{wildcards.lib}_{wildcards.rep}_on_%s.sam" % genome)
        ##
    elif mapped_type == f"polyU_on_{genome}":
        return rules.remap_on_genome.output.sam
    elif mapped_type == f"unique_on_{genome}":
        raise NotImplementedError(f"{mapped_type} only handled from count level on.")
    else:
        raise NotImplementedError(f"{mapped_type} not handled (yet?)")


rule sam2indexedbam:
    input:
        #debug_wildcards,
        # sam = OPJ(aligner, f"mapped_{genome}", f"{{lib}}_{{rep}}_on_{genome}.sam"),
        sam = source_sam,
    output:
        # sorted_bam = protected(OPJ(aligner, f"mapped_{genome}", f"{{lib}}_{{rep}}_on_{genome}_sorted.bam")),
        # index = protected(OPJ(aligner, f"mapped_{genome}", f"{{lib}}_{{rep}}_on_{genome}_sorted.bam.bai")),
        sorted_bam = protected(OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}_{mapped_type}_sorted.bam")),
        index = protected(OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}_{mapped_type}_sorted.bam.bai")),
    message:
        "Sorting and indexing sam file for {wildcards.lib}_{wildcards.rep}_{wildcards.mapped_type}."
    log:
        log = OPJ(log_dir, "sam2indexedbam_{lib}_{rep}_{mapped_type}.log"),
        err = OPJ(log_dir, "sam2indexedbam_{lib}_{rep}_{mapped_type}.err"),
    resources:
        mem_mb=4100,
        io=45
    threads:
        8
    wrapper:
        f"file://{wrappers_dir}/sam2indexedbam"


rule compute_mapping_stats:
    input:
        sorted_bam = rules.sam2indexedbam.output.sorted_bam,
        #sorted_bam = OPJ(aligner, f"mapped_{genome}", f"{{lib}}_{{rep}}_on_{genome}_sorted.bam"),
    output:
        stats = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}_{mapped_type}_samtools_stats.txt"),
    shell:
        """samtools stats {input.sorted_bam} > {output.stats}"""


rule get_nb_mapped:
    """Extracts the number of mapped reads from samtools stats results."""
    input:
        stats = rules.compute_mapping_stats.output.stats,
        #stats = OPJ(aligner, f"mapped_{genome}", f"{{lib}}_{{rep}}_on_{genome}_samtools_stats.txt"),
    output:
        nb_mapped = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}_{mapped_type}_nb_mapped.txt"),
    shell:
        """samstats2mapped {input.stats} {output.nb_mapped}"""


rule compute_coverage:
    input:
        sorted_bam = rules.sam2indexedbam.output.sorted_bam,
        #sorted_bam = OPJ(aligner, f"mapped_{genome}", f"{{lib}}_{{rep}}_on_{genome}_sorted.bam"),
    output:
        coverage = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}_{mapped_type}_coverage.txt"),
    params:
        genomelen = genomelen,
    shell:
        """
        bases=$(samtools depth {input.sorted_bam} | awk '{{sum += $3}} END {{print sum}}') || error_exit "samtools depth failed"
        python3 -c "print(${{bases}} / {params.genomelen})" > {output.coverage}
        """


rule assemble_transcripts:
    input:
        sorted_bam = OPJ(aligner, f"mapped_{genome}", f"{{lib}}_{{rep}}_on_{genome}_sorted.bam"),
    output:
        gtf = OPJ(aligner, f"mapped_{genome}", "stringtie", f"{{lib}}_{{rep}}_on_{genome}.gtf"),
    params:
        annot = OPJ(annot_dir, "genes.gtf"),
    message:
        "Assembling transcripts for {wildcards.lib}_{wildcards.rep} with stringtie."
    log:
        log = OPJ(log_dir, "assemble_transcripts_{lib}_{rep}.log"),
        err = OPJ(log_dir, "assemble_transcripts_{lib}_{rep}.err")
    resources:
        io=5
    threads:
        4
    shell:
        """
        cmd="stringtie -p {threads} -G {params.annot} -o {output} -l {wildcards.lib}_{wildcards.rep} {input}"
        echo ${{cmd}} 1> {log.log}
        eval ${{cmd}} 1>> {log.log} 2> {log.err}
        """


rule merge_transcripts:
    input:
        expand(OPJ(aligner, f"mapped_{genome}", "stringtie", f"{{lib}}_{{rep}}_on_{genome}.gtf"), lib=LIBS, rep=REPS),
    output:
        merged = OPJ(transcriptomes_dir, aligner, f"mapped_{genome}", "stringtie", "merged.gtf"),
    params:
        annot = OPJ(annot_dir, "genes.gtf"),
    message:
        "Merging transcripts with stringtie."
    log:
        log = OPJ(log_dir, "merge_transcripts.log"),
        err = OPJ(log_dir, "merge_transcripts.err")
    resources:
        io=5
    shell:
        """
        cmd="stringtie --merge -G {params.annot} -o {output} {input}"
        echo ${{cmd}} 1> {log.log}
        eval ${{cmd}} 1>> {log.log} 2> {log.err}
        """


rule gffcompare:
    input:
        merged = OPJ(transcriptomes_dir, aligner, f"mapped_{genome}", "stringtie", "merged.gtf"),
    output:
        stats = OPJ(transcriptomes_dir, aligner, f"mapped_{genome}", "stringtie", "compare.stats"),
    params:
        compare = OPJ(transcriptomes_dir, aligner, f"mapped_{genome}", "stringtie", "compare"),
        annot = OPJ(annot_dir, "genes.gtf"),
    message:
        "Merging transcripts with stringtie."
    log:
        log = OPJ(log_dir, "gffcompare.log"),
        err = OPJ(log_dir, "gffcompare.err")
    resources:
        io=5
    shell:
        """
        cmd="gffcompare -r {params.annot} -o {params.compare} {input.merged}"
        echo ${{cmd}} 1> {log.log}
        eval ${{cmd}} 1>> {log.log} 2> {log.err}
        """


rule quantify_transcripts:
    input:
        merged = OPJ(transcriptomes_dir, aligner, f"mapped_{genome}", "stringtie", "merged.gtf"),
        sorted_bam = OPJ(aligner, f"mapped_{genome}", f"{{lib}}_{{rep}}_on_{genome}_sorted.bam"),
    output:
        gtf = OPJ(aligner, f"mapped_{genome}", "ballgown", f"{{lib}}_{{rep}}_on_{genome}.gtf"),
    message:
        "Quantifying transcripts for {wildcards.lib}_{wildcards.rep} with stringtie."
    log:
        OPJ(log_dir, "quantify_transcripts_{lib}_{rep}.log")
    threads:
        4
    resources:
        io=5
    shell:
        """
        cmd="stringtie -e -B -p {threads} -G {input.merged} -o {output} {input.sorted_bam}"
        echo ${{cmd}} 1> {log}
        eval ${{cmd}} 1>> {log}
        """


# def htseq_orientation2stranded(wildcards):
#     orientation = wildcards.orientation
#     if orientation == "fwd":
#         if LIB_TYPE[-2:] == "SF":
#             return "yes"
#         elif LIB_TYPE[-2:] == "SR":
#             return "reverse"
#         else:
#             raise ValueError(f"{LIB_TYPE} library type not compatible with strand-aware read counting.")
#     elif orientation == "rev":
#         if LIB_TYPE[-2:] == "SF":
#             return "reverse"
#         elif LIB_TYPE[-2:] == "SR":
#             return "yes"
#         else:
#             raise ValueError(f"{LIB_TYPE} library type not compatible with strand-aware read counting.")
#     elif orientation == "all":
#         return "no"
#     else:
#         exit("Orientation is to be among \"fwd\", \"rev\" and \"all\".")


def biotype2annot(wildcards):
    if wildcards.biotype.endswith("_rmsk_families"):
        biotype = wildcards.biotype[:-9]
    else:
        biotype = wildcards.biotype
    return OPJ(annot_dir, f"{biotype}.gtf")


def source_sorted_bam(wildcards):
    mapped_type = wildcards.mapped_type
    if mapped_type.startswith("unique_"):
        real_mapped_type = mapped_type[7:]
    else:
        real_mapped_type = mapped_type
    return OPJ(
        aligner, f"mapped_{genome}",
        f"{wildcards.lib}_{wildcards.rep}_{real_mapped_type}_sorted.bam")


def source_index(wildcards):
    mapped_type = wildcards.mapped_type
    if mapped_type.startswith("unique_"):
        real_mapped_type = mapped_type[7:]
    else:
        real_mapped_type = mapped_type
    return OPJ(
        aligner, f"mapped_{genome}",
        f"{wildcards.lib}_{wildcards.rep}_{real_mapped_type}_sorted.bam.bai")


rule feature_count_reads:
    input:
        sorted_bam = source_sorted_bam,
        # TODO: remove this?
        index = source_index,
        #sorted_bam = rules.sam2indexedbam.output.sorted_bam,
        #index = rules.sam2indexedbam.output.index,
    output:
        counts = OPJ(aligner, f"mapped_{genome}", "feature_count", "{lib}_{rep}_{mapped_type}", "{lib}_{rep}_{biotype}_{orientation}_counts.txt"),
        counts_converted = OPJ(aligner, f"mapped_{genome}", "feature_count", "{lib}_{rep}_{mapped_type}", "{lib}_{rep}_{biotype}_{orientation}_counts_gene_names.txt"),
    #wildcard_constraints:
    #    mapped_type = "|".join([f"on_{genome}", f"polyU_on_{genome}"])
    params:
        min_mapq = partial(aligner2min_mapq, aligner),
        stranded = feature_orientation2stranded(LIB_TYPE),
        annot = biotype2annot,
        # pickled dictionary that associates gene ids to gene names
        converter = genome_dict["converter"]
    message:
        "Counting {wildcards.orientation} {wildcards.biotype} reads for {wildcards.lib}_{wildcards.rep} {wildcards.mapped_type} with featureCounts."
    benchmark:
        OPJ(log_dir, "feature_count_reads", "{lib}_{rep}_{mapped_type}_{biotype}_{orientation}_benchmark.txt")
    log:
        log = OPJ(log_dir, "feature_count_reads", "{lib}_{rep}_{mapped_type}_{biotype}_{orientation}.log"),
        err = OPJ(log_dir, "feature_count_reads", "{lib}_{rep}_{mapped_type}_{biotype}_{orientation}.err"),
    shell:
        """
        tmpdir=$(TMPDIR=/var/tmp mktemp -dt "feature_{wildcards.lib}_{wildcards.rep}_{wildcards.mapped_type}_{wildcards.biotype}_{wildcards.orientation}.XXXXXXXXXX")
        cmd="featureCounts {params.min_mapq} -a {params.annot} -o {output.counts} -t transcript -g "gene_id" -O -M --primary -s {params.stranded} --fracOverlap 0 --tmpDir ${{tmpdir}} {input.sorted_bam}"
        featureCounts -v 2> {log.log}
        echo ${{cmd}} 1>> {log.log}
        eval ${{cmd}} 1>> {log.log} 2> {log.err} || error_exit "featureCounts failed"
        rm -rf ${{tmpdir}}
        cat {output.counts} | wormid2name > {output.counts_converted}
        # cat {output.counts} | id2name.py {params.converter} > {output.counts_converted}
        """


# rule feature_count_unique_reads:
#     input:
#         sorted_bam = source_sorted_bam,
#         index = source_index,
#         #sorted_bam = rules.sam2indexedbam.output.sorted_bam,
#         #index = rules.sam2indexedbam.output.index,
#     output:
#         counts = OPJ(aligner, f"mapped_{genome}", "feature_count", "{lib}_{rep}_{mapped_type}", "{lib}_{rep}_{biotype}_{orientation}_counts.txt"),
#         counts_converted = OPJ(aligner, f"mapped_{genome}", "feature_count", "{lib}_{rep}_{mapped_type}", "{lib}_{rep}_{biotype}_{orientation}_counts_gene_names.txt"),
#     #wildcard_constraints:
#     #    mapped_type = f"unique_on_{genome}"
#     params:
#         min_mapq = partial(aligner2min_mapq, aligner),
#         stranded = feature_orientation2stranded(LIB_TYPE),
#         annot = biotype2annot,
#         # pickled dictionary that associates gene ids to gene names
#         converter = genome_dict["converter"]
#     message:
#         "Counting {wildcards.orientation} {wildcards.biotype} reads for {wildcards.lib}_{wildcards.rep} {wildcards.mapped_type} with featureCounts."
#     benchmark:
#         OPJ(log_dir, "feature_count_reads", "{lib}_{rep}_{mapped_type}_{biotype}_{orientation}_benchmark.txt")
#     log:
#         log = OPJ(log_dir, "feature_count_reads", "{lib}_{rep}_{mapped_type}_{biotype}_{orientation}.log"),
#         err = OPJ(log_dir, "feature_count_reads", "{lib}_{rep}_{mapped_type}_{biotype}_{orientation}.err"),
#     shell:
#         """
#         tmpdir=$(mktemp -dt "feature_{wildcards.lib}_{wildcards.rep}_{wildcards.mapped_type}_{wildcards.biotype}_{wildcards.orientation}.XXXXXXXXXX")
#         cmd="featureCounts {params.min_mapq} -a {params.annot} -o {output.counts} -t transcript -g "gene_id" -O -M --primary -s {params.stranded} --fracOverlap 0 --tmpDir ${{tmpdir}} {input.sorted_bam}"
#         featureCounts -v 2> {log.log}
#         echo ${{cmd}} 1>> {log.log}
#         eval ${{cmd}} 1>> {log.log} 2> {log.err} || error_exit "featureCounts failed"
#         rm -rf ${{tmpdir}}
#         cat {output.counts} | id2name.py {params.converter} > {output.counts_converted}
#         """


# def biotype2merged(wildcards):
#     if wildcards.biotype.endswith("_rmsk_families"):
#         biotype = wildcards.biotype[:-9]
#     else:
#         biotype = wildcards.biotype
#     return OPJ(annot_dir, f"{biotype}_merged.bed")
#
#
# def intersect_orientation2stranded(wildcards):
#     orientation = wildcards.orientation
#     if orientation == "fwd":
#         if LIB_TYPE[-2:] == "SF":
#             return "-s"
#         elif LIB_TYPE[-2:] == "SR":
#             return "-S"
#         else:
#             raise ValueError(f"{LIB_TYPE} library type not compatible with strand-aware read counting.")
#     elif orientation == "rev":
#         if LIB_TYPE[-2:] == "SF":
#             return "-S"
#         elif LIB_TYPE[-2:] == "SR":
#             return "-s"
#         else:
#             raise ValueError(f"{LIB_TYPE} library type not compatible with strand-aware read counting.")
#     elif orientation == "all":
#         return ""
#     else:
#         exit("Orientation is to be among \"fwd\", \"rev\" and \"all\".")
#
#
# rule intersect_count_reads:
#     input:
#         sorted_bam = OPJ(aligner, f"mapped_{genome}", f"{{lib}}_{{rep}}_on_{genome}_sorted.bam"),
#         bai = OPJ(aligner, f"mapped_{genome}", f"{{lib}}_{{rep}}_on_{genome}_sorted.bam.bai"),
#     output:
#         counts = OPJ(aligner, f"mapped_{genome}", "intersect_count", f"{{lib}}_{{rep}}_on_{genome}", "{lib}_{rep}_{biotype}_{orientation}_counts.txt"),
#         counts_converted = OPJ(aligner, f"mapped_{genome}", "intersect_count", f"{{lib}}_{{rep}}_on_{genome}", "{lib}_{rep}_{biotype}_{orientation}_counts_gene_names.txt"),
#     params:
#         stranded = intersect_orientation2stranded,
#         annot = biotype2merged,
#     message:
#         "Counting {wildcards.orientation} {wildcards.biotype} reads for {wildcards.lib}_{wildcards.rep} with bedtools intersect."
#     benchmark:
#         OPJ(log_dir, "intersect_count_reads", "{lib}_{rep}_{biotype}_{orientation}_benchmark.txt")
#     log:
#         log = OPJ(log_dir, "intersect_count_reads", "{lib}_{rep}_{biotype}_{orientation}.log"),
#         err = OPJ(log_dir, "intersect_count_reads", "{lib}_{rep}_{biotype}_{orientation}.err"),
#     threads: 4  # to limit memory usage, actually
#     wrapper:
#         f"file://{wrappers_dir}/intersect_count_reads"


rule summarize_counts:
    """For a given library, write a summary of the read counts for the various biotypes."""
    input:
        biotype_counts_files = expand(OPJ(aligner, f"mapped_{genome}", "{{counter}}", "{{lib}}_{{rep}}_{{mapped_type}}", "{{lib}}_{{rep}}_{biotype}_{{orientation}}_counts.txt"), biotype=BIOTYPES),
    output:
        summary = OPJ(aligner, f"mapped_{genome}", "{counter}", "summaries", "{lib}_{rep}_{mapped_type}_{orientation}_counts.txt")
    run:
        if wildcards.counter == "htseq_count":
            sum_counter = sum_htseq_counts
        elif wildcards.counter == "intersect_count":
            sum_counter = sum_intersect_counts
        elif wildcards.counter == "feature_count":
            sum_counter = sum_feature_counts
        else:
            raise NotImplementedError(f"{wildcards.counter} not handled (yet?)")
        with open(output.summary, "w") as summary_file:
            header = "\t".join(BIOTYPES)
            summary_file.write("%s\n" % header)
            sums = "\t".join((str(sum_counter(counts_file)) for counts_file in input.biotype_counts_files))
            summary_file.write("%s\n" % sums)


rule gather_read_counts_summaries:
    """Gather read count summaries across libraries: lib_rep -> all."""
    input:
        summary_tables = expand(OPJ(
            aligner, f"mapped_{genome}", "{{counter}}", "summaries",
            "{name}_{{mapped_type}}_{{orientation}}_counts.txt"), name=COND_NAMES),
    output:
        summary_table = OPJ(
            aligner, f"mapped_{genome}", "{counter}", "summaries",
            "all_{mapped_type}_{orientation}_counts.txt"),
    run:
        #summary_files = (OPJ(
        summary_files = [OPJ(
            aligner, f"mapped_{genome}", wildcards.counter, "summaries",
            f"{cond_name}_{wildcards.mapped_type}_{wildcards.orientation}_counts.txt") for cond_name in COND_NAMES]
                #orientation=wildcards.orientation)) for cond_name in COND_NAMES)
        try:
            summaries = pd.concat((pd.read_table(summary_file).T.astype(int) for summary_file in summary_files), axis=1)
        except ValueError:
            warnings.warn("Some data may be missing. Using floats instead of ints.\n")
            summaries = pd.concat((pd.read_table(summary_file).T.astype(float) for summary_file in summary_files), axis=1)
        summaries.columns = COND_NAMES
        summaries.to_csv(output.summary_table, na_rep="NA", sep="\t")


rule gather_counts:
    """For a given biotype, gather counts from all libraries in one table."""
    input:
        counts_tables = expand(
            OPJ(aligner, f"mapped_{genome}", "{{counter}}",
                "{cond_name}_{{mapped_type}}",
                "{cond_name}_{{biotype}}_{{orientation}}_counts.txt"),
            cond_name=COND_NAMES),
    output:
        counts_table = OPJ(
            aligner, f"mapped_{genome}", "{counter}",
            "all_{mapped_type}", "{biotype}_{orientation}_counts.txt"),
    wildcard_constraints:
        # Avoid ambiguity with join_all_counts
        biotype = "|".join(BIOTYPES)
    run:
        # Gathering the counts data
        ############################
        #counts_files = (OPJ(
        counts_files = [OPJ(
            aligner,
            f"mapped_{genome}",
            wildcards.counter,
            f"{cond_name}_{wildcards.mapped_type}",
            f"{cond_name}_{wildcards.biotype}_{wildcards.orientation}_counts.txt") for cond_name in COND_NAMES]
            #"{biotype}_{orientation}_counts.txt".format(biotype=wildcards.biotype, orientation=wildcards.orientation)) for cond_name in COND_NAMES)
        if wildcards.counter == "htseq_count":
            counts_data = pd.concat(
                map(read_htseq_counts, counts_files),
                axis=1).fillna(0).astype(int)
        elif wildcards.counter == "intersect_count":
            counts_data = pd.concat(
                map(read_intersect_counts, counts_files),
                axis=1).fillna(0).astype(int)
        elif wildcards.counter == "feature_count":
            counts_data = pd.concat(
                map(read_feature_counts, counts_files),
                axis=1).fillna(0).astype(int)
        else:
            raise NotImplementedError(f"{wildcards.counter} not handled (yet?)")
        counts_data.columns = COND_NAMES
        # Simple_repeat|Simple_repeat|(TTTTTTG)n:1
        # Simple_repeat|Simple_repeat|(TTTTTTG)n:2
        # Simple_repeat|Simple_repeat|(TTTTTTG)n:3
        # Simple_repeat|Simple_repeat|(TTTTTTG)n:4
        # -> Simple_repeat|Simple_repeat|(TTTTTTG)n
        if wildcards.biotype.endswith("_rmsk_families"):
            counts_data = sum_by_family(counts_data)
        counts_data.index.names = ["gene"]
        counts_data.to_csv(output.counts_table, na_rep="NA", sep="\t")


# TODO: plot log(pseudocounts) vs log(concentration) as in figs sup8 and sup9 of Risso et al. 2014
# Colour-code the spike-in length
# Also do the plot with RPK instead of pseudocounts
rule compute_RPK:
    """For a given biotype, compute the corresponding RPK value (reads per kilobase)."""
    input:
        # TODO: Why wildcards seems to be None?
        #counts_data = rules.gather_counts.output.counts_table,
        counts_data = OPJ(aligner, f"mapped_{genome}", "{counter}",
            "all_{mapped_type}", "{biotype}_{orientation}_counts.txt"),
    output:
        rpk_file = OPJ(aligner, f"mapped_{genome}", "{counter}",
            "all_{mapped_type}", "{biotype}_{orientation}_RPK.txt"),
    params:
        feature_lengths_file = OPJ(annot_dir, "union_exon_lengths.txt"),
    # run:
    #     counts_data = pd.read_table(input.counts_data, index_col="gene")
    #     feature_lengths = pd.read_table(params.feature_lengths_file, index_col="gene")
    #     common = counts_data.index.intersection(feature_lengths.index)
    #     rpk = 1000 * counts_data.loc[common].div(feature_lengths.loc[common]["union_exon_len"], axis="index")
    #     rpk.to_csv(output.rpk_file, sep="\t")
    wrapper:
        f"file://{wrappers_dir}/compute_RPK"


rule compute_sum_million_RPK:
    input:
        rpk_file = rules.compute_RPK.output.rpk_file,
    output:
        sum_rpk_file = OPJ(aligner, f"mapped_{genome}", "{counter}",
            "all_{mapped_type}", "{biotype}_{orientation}_sum_million_RPK.txt"),
    run:
        sum_rpk = pd.read_table(
            input.rpk_file,
            index_col=0).sum()
        (sum_rpk / 1000000).to_csv(output.sum_rpk_file, sep="\t")


# Compute TPM using total number of mappers divided by genome length
# (not sum of RPK across biotypes: some mappers may not be counted)
# No, doesn't work: mappers / genome length not really comparable
# Needs to be done on all_types
rule compute_TPM:
    """For a given biotype, compute the corresponding TPM value (reads per kilobase per million mappers)."""
    input:
        rpk_file = rules.compute_RPK.output.rpk_file
    output:
        tpm_file = OPJ(aligner, f"mapped_{genome}", "{counter}",
            "all_{mapped_type}", "{biotype}_{orientation}_TPM.txt"),
    # The sum must be done over all counted features
    wildcard_constraints:
        biotype = "|".join(["alltypes", "protein_coding"])
    # run:
    #     rpk = pd.read_table(input.rpk_file, index_col="gene")
    #     tpm = 1000000 * rpk / rpk.sum()
    #     tpm.to_csv(output.tpm_file, sep="\t")
    wrapper:
        f"file://{wrappers_dir}/compute_TPM"


# Useful to compute translation efficiency in the Ribo-seq pipeline
rule compute_mean_TPM:
    input:
        all_tmp_file = rules.compute_TPM.output.tpm_file
    output:
        tpm_file = OPJ(aligner, f"mapped_{genome}", "{counter}",
            "{lib}_mean_{mapped_type}", "{lib}_mean_{biotype}_{orientation}_TPM.txt"),
    run:
        tpm = pd.read_table(
            input.all_tmp_file, index_col="gene",
            usecols=["gene", *[f"{wildcards.lib}_{rep}" for rep in REPS]])
        tpm_mean = tpm.mean(axis=1)
        # This is a Series
        tpm_mean.name = wildcards.lib
        tpm_mean.to_csv(output.tpm_file, sep="\t", header=True)


rule compute_RPM:
    input:
        counts_data = OPJ(aligner, f"mapped_{genome}", "{counter}",
            "all_{mapped_type}", "{biotype}_{orientation}_counts.txt"),
        #summary_table = rules.gather_read_counts_summaries.output.summary_table,
        summary_table = OPJ(
            aligner, f"mapped_{genome}", "{counter}", "summaries",
            "all_{mapped_type}_fwd_counts.txt"),
    output:
        rpm_file = OPJ(aligner, f"mapped_{genome}", "{counter}",
            "all_{mapped_type}", "{biotype}_{orientation}_RPM.txt"),
    run:
        # Reading column counts from {input.counts_table}
        counts_data = pd.read_table(
            input.counts_data,
            index_col="gene")
        # Reading number of protein_coding fwd mappers from {input.summary_table}
        norm = pd.read_table(input.summary_table, index_col=0).loc["protein_coding"]
        # Computing counts per million protein_coding fwd mappers
        RPM = 1000000 * counts_data / norm
        RPM.to_csv(output.rpm_file, sep="\t")


def source_quantif(wildcards):
    """Determines from which rule the gathered counts should be sourced."""
    if wildcards.quantif_type == "counts":
        # return rules.gather_counts.output.counts_table
        return OPJ(
            aligner, f"mapped_{genome}", f"{wildcards.counter}",
            f"all_{wildcards.mapped_type}",
            f"{wildcards.biotype}_{wildcards.orientation}_counts.txt")
    elif wildcards.quantif_type == "RPK":
        # return rules.compute_RPK.output.rpk_file
        return OPJ(aligner, f"mapped_{genome}", f"{wildcards.counter}",
            f"all_{wildcards.mapped_type}",
            f"{wildcards.biotype}_{wildcards.orientation}_RPK.txt")
    elif wildcards.quantif_type == "TPM":
        # return rules.compute_TPM.output.tpm_file
        return OPJ(aligner, f"mapped_{genome}", f"{wildcards.counter}",
            f"all_{wildcards.mapped_type}",
            f"{wildcards.biotype}_{wildcards.orientation}_TPM.txt")
    elif wildcards.quantif_type == "RPM":
        #return rules.compute_RPM.output.rpm_file
        return OPJ(aligner, f"mapped_{genome}", f"{wildcards.counter}",
            f"all_{wildcards.mapped_type}",
            f"{wildcards.biotype}_{wildcards.orientation}_RPM.txt")
    else:
        raise NotImplementedError("%s is not yet handeled." % wildcards.quantif_type)


rule compare_replicates:
    """This rule will compute comparison statistics between the counts obtained from replicates of a condition."""
    input:
        # AttributeError: 'NoneType' object has no attribute 'keys'
        # counts_data = rules.gather_counts.output.counts_table,
        #counts_data = OPJ(aligner, f"mapped_{genome}", "{counter}",
        #    f"all_on_{genome}", "{biotype}_{orientation}_counts.txt"),
        data_file = source_quantif,
        # MissingInputException in line 1174 of /home/bli/src/bioinfo_utils/RNA-seq/RNA-seq.snakefile:
        # Missing input files for rule compare_replicates:
        # results_prg1_KO_48hph/hisat2/mapped_C_elegans/{counter}/all_{mapped_type}/{biotype}_{orientation}_TPM.txt
    output:
        corr_file = OPJ(aligner, f"mapped_{genome}", "{counter}",
            "all_{mapped_type}", "replicates_comparison",
            "{lib}", "{biotype}_{orientation}_{quantif_type}_correlations.txt"),
    run:
        column_list = [f"{wildcards.lib}_{rep}" for rep in REPS]
        data = pd.read_table(
            input.data_file,
            usecols=["gene", *column_list],
            index_col="gene")
        log_data = np.log10(data)
        log_data[log_data == -np.inf] = np.nan
        with open(output.corr_file, "w") as corr_file:
            # Add normality test and/or log-transform the data
            normality = {}
            log_normality = {}
            for rep in data.columns:
                try:
                    _, normality[rep] = shapiro(data[rep])
                except ValueError as err:
                    if str(err) == "Data must be at least length 3.":
                        normality[rep] = np.NAN
                    else:
                        raise
                try:
                    _, log_normality[rep] = shapiro(log_data[rep].dropna())
                except ValueError as err:
                    if str(err) == "Data must be at least length 3.":
                        log_normality[rep] = np.NAN
                    else:
                        raise
            # TODO: Make a double loop and put the results in a table?
            for (rep_a, rep_b) in combinations(data.columns, 2):
                # https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.stats.pearsonr.html
                # Strictly speaking, Pearson’s correlation requires that each dataset be normally distributed.
                if np.isnan(normality[rep_a]) or normality[rep_a] < 0.05:
                    if np.isnan(log_normality[rep_a]) or log_normality[rep_a] < 0.05:
                        warn_a = "(**)"
                    else:
                        warn_a = "(*)"
                else:
                    warn_a = ""
                if np.isnan(normality[rep_b]) or normality[rep_b] < 0.05:
                    if np.isnan(log_normality[rep_b]) or log_normality[rep_b] < 0.05:
                        warn_b = "(**)"
                    else:
                        warn_b = "(*)"
                else:
                    warn_b = ""
                try:
                    (r_val, p_val) = pearsonr(data[rep_a], data[rep_b])
                    corr_file.write(f"{rep_a}{warn_a}_vs_{rep_b}{warn_b}\t{r_val} ({p_val})\n")
                except ValueError as err:
                    if str(err) == "x and y must have length at least 2.":
                        corr_file.write(f"Only {len(data[[rep_a, rep_b]])} points in data.\n")
                    else:
                        raise
                filtered = log_data[[rep_a, rep_b]].dropna()
                try:
                    (log_r_val, log_p_val) = pearsonr(filtered[rep_a], filtered[rep_b])
                    corr_file.write(f"log10_{rep_a}{warn_a}_vs_log10_{rep_b}{warn_b}\t{log_r_val} ({log_p_val})\n")
                except ValueError as err:
                    if str(err) == "x and y must have length at least 2.":
                        corr_file.write(f"Only {len(filtered)} points in log10 data.\n")
                    else:
                        raise
            corr_file.write("(*): not normal\n(**): log10 not normal\n")


rule plot_biotype_scatter_pairs:
    input:
        data_file = source_quantif,
    output:
        plot_file = OPJ(aligner, f"mapped_{genome}", "{counter}",
            "all_{mapped_type}", "scatter_pairs",
            "{biotype}_{orientation}_{quantif_type}_scatter_pairs.pdf"),
    run:
        data = pd.read_table(input.data_file, index_col="gene")
        save_plot(
            output.plot_file, plot_paired_scatters,
            data, log_log=False,
            title="{wildcards.biotype} {wildcards.orientation} {wildcards.quantif_type}")


def plot_spikein_response(data, lib):
    fig, axis = plt.subplots()
    #axis.set(xscale="log", yscale="log")
    #sns.regplot("expected_counts", lib, data, ax=axis)
    try:
        sns.regplot(
            data=np.log10(data[["expected_counts", lib]]).replace(
                [np.inf, -np.inf], np.nan).dropna(),
            x="expected_counts", y=lib,
            ax=axis)
        axis.set_xlabel("log10(expected count)")
        axis.set_ylabel("log10(TPM)")
    except ValueError:
        pass
    #sns.regplot("expected_counts", lib, data, ax=axis, robust=True, ci=None)
    #axis = sns.lmplot("expected_counts", lib, data)
    #plt.xscale("log")
    #plt.yscale("log")
    # axis.set_xscale('log')
    # axis.set_yscale('log')


def do_linear_regression(data, x_col, y_col, y_min=0, fit_intercept=True, transform=None):
    """Performs the linear regression for the dependance of y_col on x_col.
    Returns the filtered and transformed data, the slope and intercept, and the
    relative squared y-distance distances of the points with respect to the values
    predicted by the regression line.
    It is better to transform RNA-seq data before performing linear regression.
    For instance using transform="log10".
    """
    if transform is not None:
        data = getattr(np, transform)(
            data[[x_col, y_col]].dropna().query(f"`{y_col}` >= {y_min}")).replace(
                [np.inf, -np.inf], np.nan).dropna()
    else:
        data = data[[x_col, y_col]].dropna().query(f"`{y_col}` >= {y_min}")
    lm = LinearRegression(fit_intercept=fit_intercept)
    X = data.drop(y_col, axis=1)
    lm.fit(X, data[y_col])
    [slope] = lm.coef_
    intercept = lm.intercept_
    predicted = lm.predict(X)
    # squared_diffs_lm = np.power(data[y_col] - predicted_lm, 2)
    # https://en.wikipedia.org/wiki/Residual_sum_of_squares
    # RSS = np.power((data[y_col] - predicted), 2).sum()
    rel_squared_diffs = np.power((data[y_col] - predicted), 2) / predicted
    # I confirmed in one example this gives the same values:
    # predicted_manual = intercept + (slope * data[x_col])
    # squared_diffs_manual = np.power(data[y_col] - predicted_manual, 2)
    #return data, slope, intercept, squared_diffs_lm
    return data, slope, intercept, rel_squared_diffs


rule plot_spikein_responses:
    """Plot log2(TPM) vs log2(spikein_expected_counts), for each library."""
    input:
        tpm_file = OPJ(aligner, f"mapped_{genome}", "{counter}",
            f"all_on_{genome}", "alltypes_{orientation}_TPM.txt"),
    output:
        response_plots = expand(OPJ(aligner, f"mapped_{genome}", "{{counter}}",
            f"all_on_{genome}", "{lib}_{rep}_spike_ins_{{orientation}}_TPM_response.pdf"), lib=LIBS, rep=REPS),
        spikein_slope_files = OPJ(
            aligner, f"mapped_{genome}", "{counter}", f"all_on_{genome}",
            "spike_ins_{orientation}_TPM_response_slope_and_intercept.txt"),
    run:
        tpm_table = pd.read_table(input.tpm_file, index_col="gene")
        common = tpm_table.index.intersection(spikein_expected_counts.index)
        data = pd.concat(
            [tpm_table.loc[common], spikein_expected_counts],
            axis=1).replace([np.nan], 0)
        # pp = PdfPages(output.response_plot)
        # for lib in data.columns[:-1]:
        #     save_plot(
        #         pp,
        #         plot_spikein_response, data, lib,
        #         title="spike-ins TPM response")
        # pp.close()
        slopes = {}
        intercepts = {}
        rel_sq_diffs_list = []
        for libname in data.columns[:-1]:
            filename = OPJ(
                aligner, f"mapped_{genome}", f"{wildcards.counter}",
                f"all_on_{genome}",
                f"{libname}_spike_ins_{wildcards.orientation}_TPM_response.pdf")
            save_plot(
                filename,
                plot_spikein_response, data, libname,
                title=f"{libname} spike-ins TPM response")
            # TODO: gather squared_diffs across libraries and find the most stable spike-ins
            # Then use those to compute slope again and use it for normalization
            # TODO (04/10/2022): save regline_slope and regline_intercept somewhere.
            (
                # Not the data transformed by LinearRegression
                # Just pre-processed data
                transformed_data,
                regline_slope,
                regline_intercept,
                rel_squared_diffs) = do_linear_regression(
                    # intercept supposed to be 0 for spikein response
                    data, "expected_counts", libname, y_min=1, fit_intercept=False, transform="log10")
            rel_sq_diffs_list.append(rel_squared_diffs)
            slopes[libname] = regline_slope
            intercepts[libname] = regline_intercept
        reg_params = pd.concat(
            [pd.Series(slopes), pd.Series(intercepts)],
            axis=1)
        reg_params.columns = ["slope", "intercept"]
        reg_params.to_csv(output.spikein_slope_files, sep="\t")
        # TODO (28/10/2022): The following looks like old work in progress:
        all_rel_sq_diffs = pd.concat(rel_sq_diffs_list, axis=1).dropna()
        # print(all_rel_sq_diffs)
        #all_rel_sq_diffs.assign(p_normal=normaltest(all_rel_sq_diffs, axis=1, nan_policy="omit").pvalue)
        chi_squared = all_rel_sq_diffs.sum(axis=1)
        # print(chi_squared)
        #plt.scatter(x=chi2.cdf(chi_squared.as_matrix(), len(data.columns) - 2), y=chi_squared.as_matrix())
        # Cooks distances using statsmodels
        # import numpy as np
        # import statsmodels.api as sm
        # import statsmodels
        # from statsmodels.stats.outliers_influence import OLSInfluence
        # y = np.random.normal(0,1, 100)
        # x = np.random.normal(0,1, 100)
        # results = sm.OLS(y,x).fit()
        # res = OLSInfluence(results).cooks_distance


rule compute_median_ratio_to_pseudo_ref_size_factors:
    input:
        counts_table = rules.gather_counts.output.counts_table,
    output:
        median_ratios_file = OPJ(aligner, f"mapped_{genome}", "{counter}", "all_{mapped_type}", "{biotype}_{orientation}_median_ratios_to_pseudo_ref.txt"),
    run:
        counts_data = pd.read_table(
            input.counts_table,
            index_col=0,
            na_filter=False)
        # http://stackoverflow.com/a/21320592/1878788
        #median_ratios = pd.DataFrame(median_ratio_to_pseudo_ref_size_factors(counts_data)).T
        #median_ratios.index.names = ["median_ratio_to_pseudo_ref"]
        # Easier to grep when not transposed, actually:
        median_ratios = median_ratio_to_pseudo_ref_size_factors(counts_data)
        median_ratios.to_csv(output.median_ratios_file, sep="\t")


def source_normalizer(wildcards):
    if wildcards.norm_type == "median_ratio_to_pseudo_ref":
        return OPJ(
            aligner, f"mapped_{genome}", COUNTERS[0],
            f"all_{wildcards.mapped_type}",
            "protein_coding_fwd_median_ratios_to_pseudo_ref.txt")
    elif wildcards.norm_type in BIOTYPES:
        return OPJ(
            aligner, f"mapped_{genome}", COUNTERS[0],
            f"all_{wildcards.mapped_type}",
            f"{wildcards.norm_type}_fwd_sum_million_RPK.txt")
    else:
        raise NotImplementedError(f"{wildcards.norm_type} normalization not implemented yet.")


# Warning: The normalization is done based on a particular count using the first counter
rule make_normalized_bigwig:
    input:
        bam = rules.sam2indexedbam.output.sorted_bam,
        # Take the column out of summaries, and write it in similar format than deseq-style values
        size_factor_file = source_normalizer,
        #size_factor_file = OPJ(aligner, f"mapped_{genome}", COUNTERS[0], f"all_on_{genome}", "protein_coding_fwd_median_ratios_to_pseudo_ref.txt"),
        #summary_table = OPJ(aligner, f"mapped_{genome}", COUNTERS[0], "summaries", f"all_on_{genome}_fwd_counts.txt"),
    output:
        bigwig_norm = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "{lib}_{rep}_{orientation}_{mapped_type}_by_{norm_type}.bw"),
    params:
        genome_binned = genome_binned,
    #    orient_filter = genomecov_filter,
    threads: 12  # to limit memory usage, actually
    log:
        log = OPJ(log_dir, "make_normalized_bigwig", "{lib}_{rep}_{mapped_type}_{orientation}_by_{norm_type}.log"),
        err = OPJ(log_dir, "make_normalized_bigwig", "{lib}_{rep}_{mapped_type}_{orientation}_by_{norm_type}.err"),
    shell:
        # LIB_TYPE[-1] should be "F" or "R" (or "U", treated as "F" in practice)
        """
        bam2bigwig.sh {input.bam} {params.genome_binned} \\
            {wildcards.lib}_{wildcards.rep} {wildcards.orientation} %s \\
            {input.size_factor_file} {output.bigwig_norm} \\
            > {log.log} 2> {log.err} \\
            || error_exit "bam2bigwig.sh failed"
        """ % LIB_TYPE[-1]


rule merge_bigwig_reps:
    """This rule merges bigwig files by computing a mean across replicates."""
    input:
        expand(OPJ(aligner, f"mapped_{genome}", "{{lib}}_{rep}", "{{lib}}_{rep}_{{orientation}}_{{mapped_type}}_by_{{norm_type}}.bw"), rep=REPS),
    output:
        bw = OPJ(aligner, f"mapped_{genome}", "{lib}_mean", "{lib}_mean_{orientation}_{mapped_type}_by_{norm_type}.bw"),
    log:
        warnings = OPJ(log_dir, "merge_bigwig_reps", "{lib}_mean_{orientation}_{mapped_type}_by_{norm_type}.warnings"),
    threads: 12  # to limit memory usage, actually
    run:
        with warn_context(log.warnings) as warn:
            try:
                bws = [pyBigWig.open(bw_filename) for bw_filename in input]
                #for bw_filename in input:
                #    bws.append(pyBigWig.open(bw_filename))
            except RuntimeError as e:
                warn(str(e))
                warn("Generating empty file.\n")
                # Make the file empty
                open(output.bw, "w").close()
            else:
                bw_out = pyBigWig.open(output.bw, "w")
                bw_out.addHeader(list(chrom_sizes.items()))
                for (chrom, chrom_len) in chrom_sizes.items():
                    try:
                        assert all([bw.chroms()[chrom] == chrom_len for bw in bws])
                    except KeyError as e:
                        warn(str(e))
                        warn(f"Chromosome {chrom} might be missing from one of the input files.\n")
                        for filename, bw in zip(input, bws):
                            msg = " ".join([f"{filename}:", *list(bw.chroms().keys())])
                            warn(f"{msg}:\n")
                        #raise
                        warn(f"The bigwig files without {chrom} will be skipped.\n")
                    to_use = [bw for bw in bws if chrom in bw.chroms()]
                    if to_use:
                        means = np.nanmean(np.vstack([bw.values(chrom, 0, chrom_len, numpy=True) for bw in to_use]), axis=0)
                    else:
                        means = np.zeros(chrom_len)
                    # bin size is 10
                    bw_out.addEntries(chrom, 0, values=np.nan_to_num(means[0::10]), span=10, step=10)
                bw_out.close()
                for bw in bws:
                    bw.close()


@wc_applied
def source_counts_to_join(wildcards):
    """
    Determines which elementary biotype counts files should be joined to make the desired "joined" biotype.
    """
    return expand(
        OPJ(aligner, f"mapped_{genome}", "{{counter}}", "all_{{mapped_type}}", "{biotype}_{{orientation}}_counts.txt"),
        biotype=BIOTYPES_TO_JOIN[wildcards.biotype])


rule join_all_counts:
    """Concat counts for all biotypes into all"""
    input:
        counts_tables = source_counts_to_join,
    output:
        counts_table = OPJ(aligner, f"mapped_{genome}", "{counter}", "all_{mapped_type}", "{biotype}_{orientation}_counts.txt"),
    wildcard_constraints:
        biotype = "|".join(JOINED_BIOTYPES)
    run:
        counts_data = pd.concat((pd.read_table(table, index_col="gene") for table in input.counts_tables))
        if len(counts_data.index.unique()) != len(counts_data.index):
            dups = counts_data.loc[counts_data.index.duplicated(keep=False)]
        assert len(counts_data.index.unique()) == len(counts_data.index), f"Some genes appear several times in the counts table:\n{dups.to_markdown()}\n"
        counts_data.index.names = ["gene"]
        counts_data.to_csv(output.counts_table, sep="\t")


@wc_applied
def source_counts(wildcards):
    """Determines from which rule the gathered counts should be sourced."""
    if wildcards.biotype in JOINED_BIOTYPES:
        ## debug
        return rules.join_all_counts.output.counts_table
        #return OPJ(aligner, f"mapped_{genome}", f"{wildcards.counter}", f"all_{wildcards.mapped_type}", f"alltypes_{wildcards.orientation}_counts.txt")
        ##
    else:
        # "Directly" from the counts gathered across libraries
        ## debug
        # Why rules.gather_counts.output.counts_table doesn't ger properly expanded?
        # open_curly = rules.gather_counts.output.counts_table.count("{")
        # if open_curly:
        #     print(rules.gather_counts.output.counts_table)
        #     print(wildcards)
        #     print(OPJ(
        #         aligner, f"mapped_{genome}", f"{wildcards.counter}",
        #         f"all_{wildcards.mapped_type}", f"{wildcards.biotype}_{wildcards.orientation}_counts.txt"))
        #     sys.exit(1)
        return rules.gather_counts.output.counts_table
        #return OPJ(
        #    aligner, f"mapped_{genome}", f"{wildcards.counter}",
        #    f"all_{wildcards.mapped_type}", f"{wildcards.biotype}_{wildcards.orientation}_counts.txt")
        ##


rule test_size_factor:
    input:
        counts_table = source_counts,
        summary_table = rules.gather_read_counts_summaries.output.summary_table,
    output:
        norm_correlations = OPJ(aligner, f"mapped_{genome}", "{counter}", "summaries", "all_{mapped_type}_{orientation}_{biotype}_norm_correlations.pdf"),
        norm_counts_distrib_plot = OPJ(aligner, f"mapped_{genome}", "{counter}", "summaries", "all_{mapped_type}_{orientation}_{biotype}_norm_counts_distrib.pdf"),
    params:
        size_factor_types = SIZE_FACTORS,
        norm_corr_plot_title = lambda wildcards : f"Correlation across samples between normalized {wildcards.orientation}_{wildcards.biotype}_{wildcards.mapped_type} counts and size factors.",
        counts_distrib_plot_title = lambda wildcards : f"Normalized {wildcards.orientation}_{wildcards.biotype}_{wildcards.mapped_type} counts distributions\n(size factor: {{}})",
    log:
        warnings = OPJ(log_dir, "test_size_factor", "{counter}_{orientation}_{biotype}_{mapped_type}.warnings"),
    threads: 8  # to limit memory usage, actually
    run:
        with warn_context(log.warnings) as warn:
            counts_data = pd.read_table(
                input.counts_table,
                index_col=0,
                na_filter=False)
            summaries = pd.read_table(input.summary_table, index_col=0)
            correlations = pd.concat((size_factor_correlations(counts_data, summaries, normalizer) for normalizer in params.size_factor_types), axis=1)
            correlations.columns = params.size_factor_types
            save_plot(
                output.norm_correlations, plot_norm_correlations, correlations,
                title=params.norm_corr_plot_title)
            #correlations.plot.kde()
            # TODO: Normalizations:
            # counts_data / summaries.loc[normalizer]
            # Get an idea of read counts distribution:
            # The filter amounts to counts_data.mean(axis=1) > 4
            #np.log10(counts_data[counts_data.sum(axis=1) > 4 * len(counts_data.columns)] + 1).plot.kde()
            #np.log10(counts_data[counts_data.prod(axis=1) > 0]).plot.kde()
            # assert len(counts_data) > 1, "Counts data with only one row cannot have its distribution estimated using KDE."
            if len(counts_data) > 1:
                pp = PdfPages(output.norm_counts_distrib_plot)
                for normalizer in params.size_factor_types:
                    if normalizer == "median_ratio_to_pseudo_ref":
                        size_factors = median_ratio_to_pseudo_ref_size_factors(counts_data)
                    else:
                        size_factors = summaries.loc[normalizer]
                    by_norm = counts_data / size_factors
                    data = np.log10(by_norm[counts_data.prod(axis=1) > 0])
                    if np.isfinite(data).any().any():
                        warn(f"Data contains invalid values: {str(data)}")
                    try:
                        xlabel = "log10(normalized counts)"
                        save_plot(pp, plot_counts_distribution, data, xlabel,
                            format="pdf",
                            title=params.counts_distrib_plot_title.format(normalizer))
                    except TypeError as e:
                        if str(e) in NO_DATA_ERRS:
                            warn("\n".join([
                                "Got TypeError:",
                                f"{str(e)}",
                                f"No data to plot for {normalizer}\n"]))
                        else:
                            raise
                    except LinAlgError as e:
                        if str(e) == "singular matrix":
                            warn("\n".join([
                                "Got LinAlgError:", f"{str(e)}",
                                f"Data cannot be plotted for {normalizer}",
                                f"{data}\n"]))
                        else:
                            raise
                    except ValueError as e:
                        if str(e) in BAD_DATA_ERRS:
                            warn("\n".join([
                                "Got ValueError:", f"{str(e)}",
                                f"Data cannot be plotted for {normalizer}",
                                f"{data}\n"]))
                        else:
                            raise
                    # xlabel = "log10(normalized counts)"
                    # if len(data) < 2:
                    #     msg = "\n".join([
                    #         "It seems that normalization led to data loss.",
                    #         "Cannot use KDE to estimate distribution."])
                    #     assert len(by_norm) > 1, msg
                    #     msg = "".join([
                    #         f"Only {len(by_norm[counts_data.prod(axis=1) > 0])} rows have no zeros",
                    #         "and can be log-transformed."])
                    #     warnings.warn(
                    #         msg + "\nSkipping %s_%s" % (wildcards.orientation, wildcards.biotype))
                    # else:
                    #     try:
                    #         save_plot(pp, plot_counts_distribution, data, xlabel,
                    #             format="pdf",
                    #             title="Normalized %s_%s counts distributions\n(size factor: %s)" % (wildcards.orientation, wildcards.biotype, normalizer))
                    #     except np.linalg.linalg.LinAlgError as e:
                    #         msg = "".join([
                    #             "There seems to be a problem with the data.\n",
                    #             "The data matrix has %d lines and %d columns.\n" % (len(data), len(data.columns))])
                    #         warnings.warn(msg + "\nSkipping %s_%s" % (wildcards.orientation, wildcards.biotype))
                pp.close()
            else:
                # Make the file empty
                warn(f"Not enough data to make this file:\n{output.norm_counts_distrib_plot}\n")
                plot_text(output.norm_counts_distrib_plot, "Not enough data.", params.counts_distrib_plot_title)


# TODO: Deal with 0-counts cases:
# Error in job differential_expression while creating output files mapping_gonad_met2set25/hisat2/mapped_C_elegans/feature_count/deseq2/met2set25_vs_WT/rev_miRNA_deseq2.txt, mapping_gonad_met2set25/hisat2/mapped_C_elegans/feature_count/deseq2/met2set25_vs_WT/rev_miRNA_up_genes.txt, mapping_gonad_met2set25/hisat2/mapped_C_elegans/feature_count/deseq2/met2set25_vs_WT/rev_miRNA_down_genes.txt, mapping_gonad_met2set25/hisat2/mapped_C_elegans/feature_count/deseq2/met2set25_vs_WT/rev_miRNA_counts_and_res.txt.
# RuleException:
# RRuntimeError in line 609 of /pasteur/homes/bli/src/RNA_Seq_Cecere/RNA-seq.snakefile:
# Error in DESeqDataSet(se, design = design, ignoreRank) : 
#   all samples have 0 counts for all genes. check the counting script.
# 
#   File "/pasteur/homes/bli/src/RNA_Seq_Cecere/RNA-seq.snakefile", line 609, in __rule_differential_expression
#   File "/home/bli/src/bioinfo_utils/libhts/libhts/libhts.py", line 33, in do_deseq2
#   File "/home/bli/.local/lib/python3.6/site-packages/rpy2/robjects/functions.py", line 178, in __call__
#   File "/home/bli/.local/lib/python3.6/site-packages/rpy2/robjects/functions.py", line 106, in __call__
#   File "/home/bli/lib/python3.6/concurrent/futures/thread.py", line 55, in run
# Exiting because a job execution failed. Look above for error message
# 
#
# TODO: Also deal with these cases (https://bioinformatics.stackexchange.com/questions/909/running-differential-expression-analyses-on-count-matrices-with-many-zeroes)
# /home/bli/.local/lib/python3.6/site-packages/rpy2/rinterface/__init__.py:186: RRuntimeWarning: estimating size factors
# 
#   warnings.warn(x, RRuntimeWarning)
# /home/bli/.local/lib/python3.6/site-packages/rpy2/rinterface/__init__.py:186: RRuntimeWarning: estimating dispersions
# 
#   warnings.warn(x, RRuntimeWarning)
# /home/bli/.local/lib/python3.6/site-packages/rpy2/rinterface/__init__.py:186: RRuntimeWarning: gene-wise dispersion estimates
# 
#   warnings.warn(x, RRuntimeWarning)
# /home/bli/.local/lib/python3.6/site-packages/rpy2/rinterface/__init__.py:186: RRuntimeWarning: mean-dispersion relationship
# 
#   warnings.warn(x, RRuntimeWarning)
# /home/bli/.local/lib/python3.6/site-packages/rpy2/rinterface/__init__.py:186: RRuntimeWarning: -- note: fitType='parametric', but the dispersion trend was not well captured by the
#    function: y = a/x + b, and a local regression fit was automatically substituted.
#    specify fitType='local' or 'mean' to avoid this message next time.
# 
#   warnings.warn(x, RRuntimeWarning)
# /home/bli/.local/lib/python3.6/site-packages/rpy2/rinterface/__init__.py:186: RRuntimeWarning: Error in lfproc(x, y, weights = weights, cens = cens, base = base, geth = geth,  : 
#   newsplit: out of vertex space
# 
#   warnings.warn(x, RRuntimeWarning)
# /home/bli/.local/lib/python3.6/site-packages/rpy2/rinterface/__init__.py:186: RRuntimeWarning: In addition: 
#   warnings.warn(x, RRuntimeWarning)
# /home/bli/.local/lib/python3.6/site-packages/rpy2/rinterface/__init__.py:186: RRuntimeWarning: There were 17 warnings (use warnings() to see them)
#   warnings.warn(x, RRuntimeWarning)
# /home/bli/.local/lib/python3.6/site-packages/rpy2/rinterface/__init__.py:186: RRuntimeWarning: 
# 
#   warnings.warn(x, RRuntimeWarning)
# Error in job differential_expression while creating output files mapping_gonad_met2set25/hisat2/mapped_C_elegans/feature_count/deseq2/met2set25_vs_WT/all_snRNA_deseq2.txt, mapping_gonad_met2set25/hisat2/mapped_C_elegans/feature_count/deseq2/met2set25_vs_WT/all_snRNA_up_genes.txt, mapping_gonad_met2set25/hisat2/mapped_C_elegans/feature_count/deseq2/met2set25_vs_WT/all_snRNA_down_genes.txt, mapping_gonad_met2set25/hisat2/mapped_C_elegans/feature_count/deseq2/met2set25_vs_WT/all_snRNA_counts_and_res.txt.
# RuleException:
# RRuntimeError in line 609 of /pasteur/homes/bli/src/RNA_Seq_Cecere/RNA-seq.snakefile:
# Error in lfproc(x, y, weights = weights, cens = cens, base = base, geth = geth,  : 
#   newsplit: out of vertex space
# 
#   File "/pasteur/homes/bli/src/RNA_Seq_Cecere/RNA-seq.snakefile", line 609, in __rule_differential_expression
#   File "/home/bli/src/bioinfo_utils/libhts/libhts/libhts.py", line 34, in do_deseq2
#   File "/home/bli/.local/lib/python3.6/site-packages/rpy2/robjects/functions.py", line 178, in __call__
#   File "/home/bli/.local/lib/python3.6/site-packages/rpy2/robjects/functions.py", line 106, in __call__
#   File "/home/bli/lib/python3.6/concurrent/futures/thread.py", line 55, in run
# Exiting because a job execution failed. Look above for error message


rule compute_RPM_folds:
    input:
        rpm_file = OPJ(aligner, f"mapped_{genome}", "{counter}",
            "all_{mapped_type}", "{biotype}_{orientation}_RPM.txt"),
        summary_table = rules.gather_read_counts_summaries.output.summary_table,
    output:
        fold_results = OPJ(
            aligner, f"mapped_{genome}", "{counter}", "RPM_folds_{mapped_type}",
            "{contrast}", "{orientation}_{biotype}", "{contrast}_RPM_folds.txt"),
    run:
        (cond, ref) = CONTRAST2PAIR[wildcards.contrast]
        column_list = expand("{lib}_{rep}", lib=[ref, cond], rep=REPS)
        RPM = pd.read_table(
            input.rpm_file,
            usecols=["gene", *column_list],
            index_col="gene")
        # Computing RPM log2 fold changes
        # Add 0.25 as RPM "pseudo-count" (edgeR does this ? https://www.biostars.org/p/102568/#102604)
        lfc = np.log2(pd.DataFrame(
            {f"log2({cond}_{rep}/{ref}_{rep})" : (RPM[f"{cond}_{rep}"] + 0.25) / (RPM[f"{ref}_{rep}"] + 0.25) for rep in REPS}))
        # Compute the mean
        lfc = lfc.assign(mean_log2_RPM_fold=lfc.mean(axis=1))
        # Converting gene IDs
        ######################
        with open(OPJ(convert_dir, "wormid2cosmid.pickle"), "rb") as dict_file:
            lfc = lfc.assign(cosmid=lfc.apply(column_converter(load(dict_file)), axis=1))
        #with open(OPJ(convert_dir, "wormid2name.pickle"), "rb") as dict_file:
            #lfc = lfc.assign(name=lfc.apply(column_converter(load(dict_file)), axis=1))
        lfc = lfc.assign(name=lfc.apply(column_converter(wormid2name), axis=1))
        pd.concat((RPM, lfc), axis=1).to_csv(output.fold_results, sep="\t")


from rpy2.robjects import Formula, StrVector
#from rpy2.rinterface import RRuntimeError
rule differential_expression:
    input:
        counts_table = source_counts,
        summary_table = rules.gather_read_counts_summaries.output.summary_table,
    output:
        deseq_results = OPJ(
            aligner, f"mapped_{genome}", "{counter}", "deseq2_{mapped_type}",
            "{contrast}", "{orientation}_{biotype}", "{contrast}_deseq2.txt"),
        up_genes = OPJ(
            aligner, f"mapped_{genome}", "{counter}", "deseq2_{mapped_type}",
            "{contrast}", "{orientation}_{biotype}", "{contrast}_up_genes.txt"),
        down_genes = OPJ(
            aligner, f"mapped_{genome}", "{counter}", "deseq2_{mapped_type}",
            "{contrast}", "{orientation}_{biotype}", "{contrast}_down_genes.txt"),
        counts_and_res = OPJ(
            aligner, f"mapped_{genome}", "{counter}", "deseq2_{mapped_type}",
            "{contrast}", "{orientation}_{biotype}", "{contrast}_counts_and_res.txt"),
    log:
        warnings = OPJ(log_dir, "differential_expression", "{counter}_{contrast}_{orientation}_{biotype}_{mapped_type}.warnings"),
    threads: 4  # to limit memory usage, actually
    run:
        with warn_context(log.warnings) as warn:
            counts_data = pd.read_table(input.counts_table, index_col="gene")
            summaries = pd.read_table(input.summary_table, index_col=0)
            # Running DESeq2
            #################
            formula = Formula("~ lib")
            (cond, ref) = CONTRAST2PAIR[wildcards.contrast]
            if not any(counts_data[f"{ref}_{rep}"].any() for rep in REPS):
                warn(
                    "Reference data is all zero.\nSkipping %s_%s_%s" % (
                        wildcards.contrast, wildcards.orientation, wildcards.biotype))
                for outfile in output:
                    shell(f"echo 'NA' > {outfile}")
            else:
                contrast = StrVector(["lib", cond, ref])
                try:
                    res, size_factors = do_deseq2(COND_NAMES, CONDITIONS, counts_data, formula=formula, contrast=contrast)
                #except RRuntimeError as e:
                except RuntimeError as e:
                    warn(
                        "Probably not enough usable data points to perform DESeq2 analyses:\n%s\nSkipping %s_%s_%s" % (
                            str(e), wildcards.contrast, wildcards.orientation, wildcards.biotype))
                    for outfile in output:
                        shell(f"echo 'NA' > {outfile}")
                else:
                    # Determining fold-change category
                    ###################################
                    set_de_status = status_setter(LFC_CUTOFFS, "log2FoldChange")
                    #counts_and_res = add_tags_column(pd.concat((counts_and_res, res), axis=1).assign(status=res.apply(set_de_status, axis=1)), input.tags_table, "small_type")
                    res = res.assign(status=res.apply(set_de_status, axis=1))
                    # Converting gene IDs
                    ######################
                    with open(OPJ(convert_dir, "wormid2cosmid.pickle"), "rb") as dict_file:
                        res = res.assign(cosmid=res.apply(column_converter(load(dict_file)), axis=1))
                    #with open(OPJ(convert_dir, "wormid2name.pickle"), "rb") as dict_file:
                        #res = res.assign(name=res.apply(column_converter(load(dict_file)), axis=1))
                    res = res.assign(name=res.apply(column_converter(wormid2name), axis=1))
                    # Just to see if column_converter works also with named column, and not just index:
                    # with open(OPJ(convert_dir, "cosmid2name.pickle"), "rb") as dict_file:
                    #     res = res.assign(name=res.apply(column_converter(load(dict_file), "cosmid"), axis=1))
                    ##########################################
                    # res.to_csv(output.deseq_results, sep="\t", na_rep="NA", decimal=",")
                    res.to_csv(output.deseq_results, sep="\t", na_rep="NA")
                    # Joining counts and DESeq2 results in a same table and determining up- or down- regulation status
                    counts_and_res = counts_data
                    for normalizer in SIZE_FACTORS:
                        if normalizer == "median_ratio_to_pseudo_ref":
                            ## Adapted from DESeq paper (doi:10.1186/gb-2010-11-10-r106) but
                            ## add pseudo-count to compute the geometric mean, then remove it
                            #pseudo_ref = (counts_data + 1).apply(gmean, axis=1) - 1
                            #def median_ratio_to_pseudo_ref(col):
                            #    return (col / pseudo_ref).median()
                            #size_factors = counts_data.apply(median_ratio_to_pseudo_ref, axis=0)
                            size_factors = median_ratio_to_pseudo_ref_size_factors(counts_data)
                        else:
                            #raise NotImplementedError(f"{normalizer} normalization not implemented")
                            size_factors = summaries.loc[normalizer]
                        by_norm = counts_data / size_factors
                        by_norm.columns = by_norm.columns.map(lambda s: "%s_by_%s" % (s, normalizer))
                        counts_and_res = pd.concat((counts_and_res, by_norm), axis=1)
                    #counts_and_res = add_tags_column(pd.concat((counts_and_res, res), axis=1).assign(status=res.apply(set_de_status, axis=1)), input.tags_table, "small_type")
                    counts_and_res = pd.concat((counts_and_res, res), axis=1)
                    counts_and_res.to_csv(output.counts_and_res, sep="\t", na_rep="NA")
                    # Saving lists of genes gaining or loosing siRNAs
                    # diff_expressed = res.query("padj < 0.05")
                    # up_genes = list(diff_expressed.query("log2FoldChange >= 1").index)
                    # down_genes = list(diff_expressed.query("log2FoldChange <= -1").index)
                    up_genes = list(counts_and_res.query(f"status in {UP_STATUSES}").index)
                    down_genes = list(counts_and_res.query(f"status in {DOWN_STATUSES}").index)
                    #up_genes = list(counts_and_res.query("status == 'up'").index)
                    #down_genes = list(counts_and_res.query("status == 'down'").index)
                    with open(output.up_genes, "w") as up_file:
                        if up_genes:
                            up_file.write("%s\n" % "\n".join(up_genes))
                        else:
                            up_file.truncate(0)
                    with open(output.down_genes, "w") as down_file:
                        if down_genes:
                            down_file.write("%s\n" % "\n".join(down_genes))
                        else:
                            down_file.truncate(0)


rule make_lfc_distrib_plot:
    input:
        deseq_results = rules.differential_expression.output.deseq_results,
    output:
        lfc_plot = OPJ(aligner, f"mapped_{genome}", "{counter}", "deseq2_{mapped_type}", "{contrast}", "{orientation}_{biotype}", "{contrast}_{fold_type}_distribution.pdf"),
    run:
        if test_na_file(input.deseq_results):
            warnings.warn(
                "No DESeq2 results for %s_%s_%s. Making dummy output." % (
                    wildcards.contrast, wildcards.orientation, wildcards.biotype))
            for outfile in output:
                shell(f"echo 'NA' > {outfile}")
        else:
            res = pd.read_table(input.deseq_results, index_col=0)
            save_plot(
                output.lfc_plot, plot_lfc_distribution,
                res, wildcards.contrast, wildcards.fold_type,
                title="log fold-change distribution for %s, %s_%s_%s" % (
                    wildcards.contrast,
                    wildcards.orientation,
                    wildcards.biotype,
                    wildcards.mapped_type))


def set_lfc_range(wildcards):
    return LFC_RANGE[wildcards.biotype]


# takes wildcards, gene list name or path
# returns list of wormbase ids
get_id_list = make_id_list_getter(gene_lists_dir, avail_id_lists)
rule make_MA_plot:
    input:
        deseq_results = rules.differential_expression.output.deseq_results,
    output:
        MA_plot = OPJ(aligner, f"mapped_{genome}", "{counter}", "deseq2_{mapped_type}", "{contrast}", "{orientation}_{biotype}", "{contrast}_MA_with_{id_list}.pdf"),
    params:
        lfc_range = set_lfc_range,
        id_list = get_id_list,
    threads: 8  # to limit memory usage, actually
    run:
        if test_na_file(input.deseq_results):
            warnings.warn(
                "No DESeq2 results for %s_%s_%s. Making dummy output." % (
                    wildcards.contrast, wildcards.orientation, wildcards.biotype))
            for outfile in output:
                shell(f"echo 'NA' > {outfile}")
        else:
            res = pd.read_table(input.deseq_results, index_col=0)
            if params.id_list is None:
                grouping = "status"
                group2colour = None
            else:
                grouping = params.id_list
                group2colour = (wildcards.id_list, sns.xkcd_rgb["red"])
            save_plot(
                output.MA_plot, plot_MA, res,
                grouping=grouping,
                group2colour=group2colour,
                lfc_range = params.lfc_range,
                fold_type = "log2FoldChange",
                title="MA-plot for %s, %s_%s_%s" % (
                    wildcards.contrast,
                    wildcards.orientation,
                    wildcards.biotype, wildcards.mapped_type),
                square=True)


def source_fold_data(wildcards):
    fold_type = wildcards.fold_type
    if fold_type in {"log2FoldChange", "lfcMLE"}:
        if hasattr(wildcards, "contrast_type"):
            return [filename_template.format(**wildcards) for filename_template in expand(
                OPJ(aligner, f"mapped_{genome}",
                    "{{counter}}", "deseq2_{{mapped_type}}", "{contrast}",
                    "{{orientation}}_{{biotype}}", "{contrast}_counts_and_res.txt"),
                # OPJ("hisat2/mapped_on_C_elegans/feature_count/deseq2_on_C_elegans/{contrast}/rev_DNA_transposons_rmsk_families/{contrast}_counts_and_res.txt"),
                contrast=contrasts_dict[wildcards.contrast_type])]
        else:
            return rules.differential_expression.output.counts_and_res
    elif fold_type == "mean_log2_RPM_fold":
        if hasattr(wildcards, "contrast_type"):
            return [filename_template.format(**wildcards) for filename_template in expand(
                OPJ(aligner, f"mapped_{genome}",
                    "{{counter}}", "RPM_folds_{{mapped_type}}", "{contrast}",
                    "{{orientation}}_{{biotype}}", "{contrast}_RPM_folds.txt"),
                contrast=contrasts_dict[wildcards.contrast_type])]
        else:
            return rules.compute_RPM_folds.output.fold_results
    else:
        raise NotImplementedError("Unknown fold type: %s" % fold_type)


def set_id_lists(wildcards):
    gene_list = wildcards.id_list
    if gene_list == "all_gene_lists":
        return all_id_lists
    else:
        return {gene_list : get_id_list(gene_list)}


rule make_contrast_lfc_boxplots:
    input:
        data = source_fold_data,
    output:
        boxplots = OPJ(aligner, f"mapped_{genome}", "{counter}", "fold_boxplots_{mapped_type}",
            "{contrast_type}_{orientation}_{biotype}_{fold_type}_{id_list}_boxplots.pdf"),
    log:
        warnings = OPJ(
            log_dir, "make_contrast_lfc_boxplots", aligner,
            f"mapped_{genome}", "{counter}", "fold_boxplots_{mapped_type}",
            "{contrast_type}_{orientation}_{biotype}_{fold_type}_{id_list}.warnings"),
    params:
        id_lists = set_id_lists,
    run:
        with warn_context(log.warnings) as warn:
            # source_data should generate the file names with the contrasts 
            # in the order defined in contrasts_dict[wildcards.contrast_type]
            # (otherwise, the zip will associate the wrong contrast names and files).
            # print("Reading fold changes from:", *input.data, sep="\n")
            lfcs_dict = {}
            for (contrast, filename) in zip(contrasts_dict[wildcards.contrast_type], input.data):
                # Input files may come from a failed DESeq2 analysis
                if test_na_file(filename):
                    warn(
                        "No %s results for %s_%s_%s_%s. Making dummy output." % (
                            wildcards.fold_type,
                            wildcards.mapped_type,
                            contrast,
                            wildcards.orientation,
                            wildcards.biotype))
                    # Should we make an empty lfc_data DataFrame instead?
                    # Make the file empty
                    open(output.boxplots, "w").close()
                    break
                try:
                    lfc_data = pd.read_table(filename, index_col="gene")
                except TypeError as err:
                    # if str(err) == "unsupported operand type(s) for -: 'str' and 'int'":
                    #     warn(str(err))
                    #     warn("Generating empty file.\n")
                    #     # Make the file empty
                    #     open(output.boxplots, "w").close()
                    #     break
                    warn(
                        "Unexpected TypeError:\n{str(err)}\n"
                        "This occurred while parsing {filename}.")
                    raise
                #print(type(lfc_data))
                for (list_name, id_list) in params.id_lists.items():
                    try:
                        selected_rows = lfc_data.loc[lfc_data.index.intersection(id_list)]
                    except TypeError as err:
                        print(params.id_lists)
                        print(type(id_list))
                        print(lfc_data.index.intersection(id_list))
                        raise
                    #print("selection has type:", type(selected_rows))
                    #print(selected_rows)
                    selected_data = selected_rows[wildcards.fold_type]
                    #print("column has type:", type(selected_data))
                    #print(selected_data)
                    lfcs_dict[f"{contrast}_{list_name}"] = selected_data
            else:
                # No break encountered in the loop
                lfcs = pd.DataFrame(lfcs_dict)
                # lfcs = pd.DataFrame(
                #     {f"{contrast}_{list_name}" : pd.read_table(filename, index_col="gene").loc[
                #         set(id_list)][wildcard.fold_type] for (
                #             contrast, filename) in zip(contrasts_dict[wildcards.contrast_type], input.data) for (
                #                 list_name, id_list) in params.id_lists.items()})
                title = f"{wildcards.orientation} {wildcards.biotype} {wildcards.mapped_type} {wildcards.fold_type} folds for {wildcards.contrast_type} contrasts"
                try:
                    save_plot(
                        output.boxplots,
                        plot_boxplots,
                        lfcs,
                        wildcards.fold_type,
                        title=title)
                except TypeError as err:
                    if str(err) in NO_DATA_ERRS:
                        warn("\n".join([
                            "Got TypeError:",
                            f"{str(err)}",
                            f"No data to plot for {title}\n"]))
                        warn("Generating empty file.\n")
                        # Make the file empty
                        open(output.boxplots, "w").close()
                    else:
                        raise

#ax = res[res["status"] == "NS"].plot.scatter(x="baseMean", y="lfcMLE", s=2, logx=True, c="grey", label="NS")
#cutoffs = [-c for c in reversed(lfc_cutoffs)] + [0] + lfc_cutoffs
#for cutoff in lfc_cutoffs:
#    res[res["status"] == f"up{cutoff}"].plot.scatter(x="baseMean", y="lfcMLE", s=2, logx=True, c=colour, label=f"up{cutoff}", ax=ax)

# rule convert_wormids:
#     input:
#         deseq_results_in = rules.differential_expression.output.deseq_results,
#         counts_and_res_in = rules.differential_expression.output.counts_and_res,
#         up_genes_in = rules.differential_expression.output.up_genes,
#         down_genes_in = rules.differential_expression.output.down_genes,
#     output:
#         deseq_results_out = OPJ(aligner, f"mapped_{genome}", "{counter}", "deseq2_{mapped_type}", "{contrast}", "{orientation}_{biotype}_deseq2_{id_type}.txt"),
#         counts_and_res_out = OPJ(aligner, f"mapped_{genome}", "{counter}", "deseq2_{mapped_type}", "{contrast}", "{orientation}_{biotype}_counts_and_res_{id_type}.txt"),
#         up_genes_out = OPJ(aligner, f"mapped_{genome}", "{counter}", "deseq2_{mapped_type}", "{contrast}", "{orientation}_{biotype}_up_genes_{id_type}.txt"),
#         down_genes_out = OPJ(aligner, f"mapped_{genome}", "{counter}', "deseq2_{mapped_type}", "{contrast}", "{orientation}_{biotype}_down_genes_{id_type}.txt"),
#     run:
#         with open(OPJ(convert_dir, "wormid2{id_type}.pickle".format(id_type=wildcards.id_type)), "rb") as dict_file:
#             converter = load(dict_file)
#         for infile_name, outfile_name in zip(input, output):
#             with open(infile_name, "r") as infile, open(outfile_name, "w") as outfile:
#                 for fields in map(strip_split, infile):
#                     outfile.write("%s\n" % "\t".join((converter.get(field, field) for field in fields)))


onsuccess:
    print("RNA-seq analysis finished.")
    cleanup_and_backup(output_dir, config, delete=True)

onerror:
    shell(f"rm -rf {output_dir}_err")
    shell(f"cp -rp {output_dir} {output_dir}_err")
    if upload_on_err:
        cleanup_and_backup(output_dir + "_err", config)
    print("RNA-seq analysis failed.")
