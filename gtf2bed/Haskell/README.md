Goal
====

This program converts text passed to its standard input to bed format, using
the `gene_id` annotation to make the 4th column.

Example usage
=============

    gft2bed < genes.gtf > genes.bed

Compiling
=========

This program needs to be compiled and installed using the stack Haskell package
manager. See the `install.sh` script.
