# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

options(show.error.locations=TRUE)
library(deseq2.bli.utils)

######################
# Make some graphics #
######################

#' Generate graphics about a set of libraries
#'
#' @param dds A DESeq2 table
#' @param out_dir The directory in which to write the figures
#' @export
make_pca_and_heatmap <- function(dds, out_dir) {
    require(DESeq2)
    require("RColorBrewer")
    groups <- head(colnames(colData(dds)), -2)
    rld <- rlog(dds, blind=F)
    #dput(rld)
    #print(groups)
    #print(colnames(colData(rld)))
    stopifnot(all(groups %in% names(colData(rld))))
    sampleDists <- dist(t(assay(rld)))
    sampleDistMatrix <- as.matrix(sampleDists)
    #print("Making new row names")
    # http://stackoverflow.com/a/40789701/1878788
    new_rownames <- do.call(function (...) paste(..., sep = "-"), as.data.frame(colData(rld))[groups])
    rownames(sampleDistMatrix) <- new_rownames
    #rownames(sampleDistMatrix) <- do.call(function (...) paste(..., sep = "-"), colData(rld)[groups])
    #rownames(sampleDistMatrix) <- paste(rld$genotype, rld$treatment, rld$replicate, sep="-")
    colnames(sampleDistMatrix) <- colnames(rld)
    colors <- colorRampPalette(rev(RColorBrewer::brewer.pal(9, "Blues")))(255)
    #pca <- DESeq2::plotPCA(rld, intgroup=c("genotype", "treatment", "replicate"))
    #print("Making pca")
    pca <- DESeq2::plotPCA(rld, intgroup=groups)
    pdf(file.path(out_dir, "deseq2_overview_graphs.pdf"), paper="a4")
    print(pca)
    require(pheatmap)
    print("Making heatmap")
    pheatmap::pheatmap(
        sampleDistMatrix,
        clustering_distance_rows=sampleDists,
        clustering_distance_cols=sampleDists,
        col=colors)
    dev.off()
    print("Heatmap done")
}
#TODO: make boxplots grouping conditions, for a given list of genes

#TODO: Like supplement 5a (http://www.nature.com/nsmb/journal/v21/n4/fig_tab/nsmb.2801_SF5.html)
# Boxplots of fold changes for various selections of genes (filtered based on belonging to a list)

#' Making boxplots with a given experiment
#'
#' @param result_name The name of the results table
#' @param ref The reference condition
#' @param other The compared condition
#' @param sets_name The name of a list of sets
#' @export
make_subsample_boxplots <- function(result_name, ref, other, sets_name) {
    gene_sets <- get(
        paste0(sets_name, "_gene_sets"),
        envir=parent.frame()
        )
    ylab <- bquote("log2"~"("*.(other) ~ "/" ~ .(ref)*")")
    subsample_lfc_func <- deseq2.bli.utils::make_subsample_lfc_func(result_name)
    pdf(file.path(out_dir, paste(result_name, sets_name, "subsample_boxplots.pdf", sep="_")))
    par(
        mar=c(12.1, 5.1, 2.1, 2.1),
        cex.axis=0.75)
    try(
        boxplot(
            sapply(gene_sets, subsample_lfc_func),
            #ylab=expression("log"[2]~"(mutant"~"/"~"wild-type)"),
            ylab=ylab,
            las=2, pch=20, cex=0.5)
        )
    # add horizontal line
    abline(h=0, lty=3, col="blue")
    dev.off()
}

#' Making boxplots for various experiments
#'
#' @param result_names The names of the results tables
#' @param boxplot_name Base to use for the boxplot file name
#' @param wormids The list of genes on which to work
#' @export
make_boxplots <- function(result_names, biotype, boxplot_name, wormids=NULL) {
    ylab <- bquote("log2FoldChange")
    get_lfc <- deseq2.bli.utils::make_lfc_getter(biotype, wormids)
    pdf(file.path(out_dir, paste(boxplot_name, "boxplots.pdf", sep="_")))
    par(
        mar=c(12.1, 5.1, 2.1, 2.1),
        cex.axis=0.75)
    data <- sapply(result_names, get_lfc)
    #TODO: deal with empty data?
    #print(data)
    try(
        boxplot(
            data,
            #sapply(result_names, get_lfc),
            #ylab=expression("log"[2]~"(mutant"~"/"~"wild-type)"),
            ylab=ylab,
            las=2, pch=20, cex=0.5)
        )
    # add horizontal line
    abline(h=0, lty=3, col="blue")
    dev.off()
}

#' Generating scatterplot and volcano plot for an "experiment"
#'
#' @param exp_name The name of the experiment
#' @param p_size The size to use for the dots in the graphics (default: 0.2)
#' @export
scatter_and_volcano <- function(exp_name, p_size=0.2) {
    results_table <- get(
    paste0(exp_name, "_results"),
    envir=parent.frame()
    )
    par(mfrow=c(2,1), pty="s")
    with(results_table,
        plot(
            log2(mean_ref_counts),
            log2(mean_other_counts),
            pch=20, col="grey", cex=p_size, asp=1))
    with(subset(results_table,
                padj < 0.05 &
                (log2FoldChange < -1 |
                log2FoldChange > 1)
                ),
        points(log2(mean_ref_counts), log2(mean_other_counts), pch=20, col="black", cex=p_size))
    with(subset(results_table,
                gene %in% endo_22G_down_prg1_Tang_2016_wormids &
                padj < 0.05 &
                (log2FoldChange < -1 |
                log2FoldChange > 1)
                ),
        points(log2(mean_ref_counts), log2(mean_other_counts), pch=20, col="blue", cex=p_size))
    with(subset(results_table,
                gene %in% endo_22G_up_prg1_Tang_2016_wormids &
                padj < 0.05 &
                (log2FoldChange < -1 |
                log2FoldChange > 1)
                ),
        points(log2(mean_ref_counts), log2(mean_other_counts), pch=20, col="red", cex=p_size))
    legend("topleft", legend=c("22G_down", "22G_up"), pch=c(20, 20), col=c("blue", "red"), cex=0.7)

    with(results_table, plot(log2FoldChange, -log10(padj), pch=20, col="grey", cex=p_size))
    with(subset(results_table,
                padj < 0.05 &
                (log2FoldChange < -1 |
                log2FoldChange > 1)),
        points(log2FoldChange, -log10(padj), pch=20, col="black", cex=p_size))
    with(subset(results_table,
                gene %in% endo_22G_down_prg1_Tang_2016_wormids &
                padj < 0.05 &
                (log2FoldChange < -1 |
                log2FoldChange > 1)),
        points(log2FoldChange, -log10(padj), pch=20, col="blue", cex=p_size))
    with(subset(results_table,
                gene %in% endo_22G_up_prg1_Tang_2016_wormids &
                padj < 0.05 &
                (log2FoldChange < -1 |
                log2FoldChange > 1)),
        points(log2FoldChange, -log10(padj), pch=20, col="red", cex=p_size))
    legend("topright", legend=c("22G_down", "22G_up"), pch=c(20, 20), col=c("blue", "red"), cex=.7)
}


#TODO: adapt this to any number of replicates (put scatterplot for pairs of replicates in a mfrwo(n, n))
#' Generating scatterplots between two replicates
#'
#' @param exp_name The name of the experiment
#' @param cond The condition
#' @export
replicates_scatterplot <- function(exp_name, cond, sample_table) {
    counts <- get_counts(exp_name, cond, sample_table, count_type="counts")
    tau <- cor(
        counts[["1"]],
        counts[["2"]],
        method="kendall"
        )
    p_size <- 0.1
    xlab <- paste(paste0(cond, "_1"), "read counts")
    ylab <- paste(paste0(cond, "_2"), "read counts")
    plot(
        counts[["1"]],
        counts[["2"]],
        pch=20, cex=p_size,
        asp=1, log="xy",
        xlab=xlab,
        ylab=ylab
        )
    legend(x='bottomright', legend=paste('tau =', round(tau, 2)))
}


#' Generating expression volcano plot
#'
#' @param results_table The table to use
#' @param colours List of colours to use for, respectively:
#' \enumerate{
#'   \item default colour (applied to all points before applying the following colours)
#'   \item default colour for points with padj < 0.05
#'   \item colours to apply for points with padj < 0.05 and belonging to the lists given in the \emph{gene_sets} parameter.
#' }
#' @param gene_sets List of sets of gene identifiers.
#' The corresponding points will have the colours specified starting at the 3rd element of \emph{colours}.
#' Elements belonging to two sets will have the colour associated with the last set.
#' @export
expression_volcanoplot <- function(results_table, colours, gene_sets) {
    p_size <- 0.1
    with(
        results_table,
        plot(
            log2FoldChange,
            -log10(padj),
            pch=20, col=colours[[1]], cex=p_size
            )
        )
    with(
        subset(
            results_table,
            padj < 0.05),
        points(
            log2FoldChange,
            -log10(padj),
            pch=20, col=colours[[2]], cex=p_size
            )
        )
    for (i in seq_along(gene_sets)){
        with(
            subset(
                results_table,
                padj < 0.05 &
                gene %in% gene_sets[[i]]
        ),
        points(
            log2FoldChange,
            -log10(padj),
            pch=20, col=colours[[i+2]], cex=p_size
            )
        )
    }
    legend("topleft", legend=c("padj < 0.05", paste("padj < 0.05 and ", names(gene_sets))), pch=20, col=tail(colours, n=-1), cex=0.7)
}

#' Generating expression scatterplot
#'
#' @param results_table The table to use
#' @param ref_label How to name of the reference condition
#' @param other_label How to name of the compared condition
#' @param colours List of colours to use for, respectively:
#' \enumerate{
#'   \item default colour (applied to all points before applying the following colours)
#'   \item default colour for points with padj < 0.05
#'   \item colours to apply for points with padj < 0.05 and belonging to the lists given in the \emph{gene_sets} parameter.
#' }
#' @param gene_sets List of sets of gene identifiers.
#' The corresponding points will have the colours specified starting at the 3rd element of \emph{colours}.
#' Elements belonging to two sets will have the colour associated with the last set.
#' @export
expression_scatterplot <- function(results_table, ref_label, other_label, colours, gene_sets) {
    p_size <- 0.1
    xlab <- paste(ref_label, "normalized read counts")
    ylab <- paste(other_label, "normalized read counts")
    with(
        results_table,
        plot(
            mean_ref_counts,
            mean_other_counts,
            pch=20, col=colours[[1]], cex=p_size,
            asp=1, log="xy",
            xlab=xlab,
            ylab=ylab
            )
        )
    with(
        subset(
            results_table,
            padj < 0.05),
        points(
            mean_ref_counts,
            mean_other_counts,
            pch=20, col=colours[[2]], cex=p_size
            )
        )
    for (i in seq_along(gene_sets)){
        with(
            subset(
                results_table,
                padj < 0.05 &
                gene %in% gene_sets[[i]]
        ),
        points(
            mean_ref_counts,
            mean_other_counts,
            pch=20, col=colours[[i+2]], cex=p_size
            )
        )
    }
    legend("topleft", legend=c("padj < 0.05", paste("padj < 0.05 and ", names(gene_sets))), pch=20, col=tail(colours, n=-1), cex=0.7)
}




#' Generating scatterplots of TE expression
#'
#' @param results_table The table to use
#' @param ref_label How to name of the reference condition
#' @param other_label How to name of the compared condition
TE_scatterplot <- function(results_table, ref_label, other_label) {
    with(
        results_table,
        plot(
            mean_ref_counts,
            mean_other_counts,
            pch=20, col="grey", cex=p_size,
            asp=1, log="xy",
            xlab=paste(ref_label, "normalized read counts"),
            ylab=paste(other_label, "normalized read counts")
            )
        )
    with(
        subset(
            results_table,
            padj < 0.05),
        points(
            mean_ref_counts,
            mean_other_counts,
            pch=20, col="black", cex=p_size
            )
        )
    with(
        subset(
            results_table,
            padj < 0.05
            ),
         textxy(
            mean_ref_counts,
            mean_other_counts,
            labs=transposon_family, col="black", cex=0.5, offset=0.5
            )
         )
    with(
        subset(
            results_table,
            padj < 0.05 &
            transposon_family %in% endo_22G_down_prg1_TE_Tang_2016
        ),
        points(
            mean_ref_counts,
            mean_other_counts,
            pch=20, col="red", cex=p_size
            )
        )
    legend("topleft", legend=c("padj < 0.05", "padj < 0.05 and 22G depleted in prg-1 mutant (Tang 2016)"), pch=20, col=c("black", "red"), cex=0.7)
}

