#!/usr/bin/env python3
# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""This script parses a sorted bam file and uses annotations from a gtf file to
identify small RNA categories."""

# Doesn't work
# import pyximport
# pyximport.install(pyimport = True)
import argparse
import logging
import warnings
import os
from gzip import open as gzopen
from contextlib import contextmanager, ExitStack
from time import perf_counter
from collections import defaultdict
from functools import reduce
from operator import attrgetter, itemgetter
from multiprocessing import Pool, Manager, cpu_count
# from pybedtools import Attributes, BedTool, Interval
from pybedtools import BedTool, Interval
import pysam
import pandas as pd
# from pandas import eval as pd_eval
# Seems slightly faster than map from standard library
from cytoolz import map, partition_all
# from cytoolz.curried import get
# from cytoolz.curried import filter as cfilter
# from cytoolz.itertoolz import concat
# from paraccumulators import parallel_accumulators
from paraccumulators.forkiter import FuncApplier
# from blimisc import add_dicts, reverse_complement
# from blimisc import add_dicts
from libsmallrna import count_small
from libsmallrna import count_annots, count_first_bases
from libsmallrna import Composition, get_read_info, add_results
from libsmallrna import PI_MIN, PI_MAX, SI_MIN, SI_MAX
from libsmallrna import SI_TYPES, SIU_TYPES
from libsmallrna import has_pi_signature, has_endosi_signature
from libsmallrna import has_26G_signature, has_csr1_endosi_signature


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)


warnings.formatwarning = formatwarning

OPJ = os.path.join


def mapstring(iterable):
    """Converts elements of an iterable into strings."""
    return map(str, iterable)
# A = {"A"}
# C = {"C"}
# G = {"G"}
# T = {"T"}
# R = A | G
# Y = C | T
# S = G | C
# W = A | T
# K = G | T
# M = A | C
# B = C | G | T
# D = A | G | T
# H = A | C | T
# V = A | C | G
# N = {"A", "C", "G", "T"}
# DNA_COMPL_TABLE = str.maketrans("ACGTRYSWKMBDHVN", "TGCAYRWSMKVHDBN")


# def reverse_complement(seq):
#     """Faster than Bio.Seq.reverse_complement"""
#     return seq.translate(DNA_COMPL_TABLE)[::-1]


# first element of an iterable
# FST = itemgetter(0)
# FST = get(0)
# second element of an iterable
SND = itemgetter(1)
# SND = get(1)
# extract info from AlignedSegment
ALI2READ_INFO = attrgetter(
    "query_name",
    "query_sequence",
    "query_qualities",  # or qual ?
    "query_length")
FQ_TEMPLATE = "@%s\n%s\n+\n%s\n"
ALI2POS_INFO = attrgetter(
    "reference_name",
    "reference_start",
    "reference_end")


def make_gtf_info_getter(getter_type):
    """Generates a function that extracts gtf information."""
    if getter_type == "tabix":
        raise NotImplementedError("%s is not up-to-date" % getter_type)
        # def get_gtf_info(gtf):
        #     """Extracts information from gtf element parsed with pysam."""
        #     return (gtf.gene_biotype, gtf.strand, gtf.gene_id)
    elif getter_type == "pybedtools":
        def get_gtf_info(gtf):
            """Extracts information from pybedtools.Interval *gtf*."""
            attrs = gtf.attrs
            # return (attrs["gene_biotype"], gtf.strand, attrs["gene_id"])
            # start and end are in the bed-style 0-based half open system
            return (attrs["gene_biotype"],
                    gtf.strand, gtf.start, gtf.end,
                    attrs["gene_id"])
    else:
        raise NotImplementedError("%s not available" % getter_type)
    return get_gtf_info


def make_annotation_processor(getter_type):
    """Generates a function that processes annotations."""
    get_gtf_info = make_gtf_info_getter(getter_type)

    def process_annotations(annots, ali):
        """Determines which annotation in *annots* should be associated with the
        AlignedSegment *ali*."""
        (name, seq, qual, read_len, _, _, start, end, strand) = get_read_info(
            ali)

        # TODO: see if five prime (6th element from get_read_info) can be used
        # For piRNA, we allow up to 26 nt because uncompletely matured piRNAs
        # might exist and this might vary between libraries.
        def match_pi(annot):
            """Returns True if the annotation tuple *annot* matches a piRNA and
            is compatible with the AlignedSegment mapping coordinates."""
            if strand == "+":
                return (annot[0] == "piRNA"
                        and annot[1] == "+"
                        and annot[2] == start)
            else:
                return (annot[0] == "piRNA"
                        and annot[1] == "-"
                        and annot[3] == end)

        def match_mi(annot):
            """Returns True if the annotation tuple *annot* matches a miRNA and
            is compatible with the AlignedSegment mapping coordinates."""
            return (annot[0] == "miRNA"
                    and annot[1] == strand
                    and annot[2] == start
                    and annot[3] == end)

        # TODO: add match_tRF:
        # * start and end should fall inside a tRNA annotation
        # * the annotation should be same strand as the read
        def match_tRF(annot):
            """Returns True if the annotation tuple *annot* matches a tRNA and
            is compatible with the AlignedSegment mapping coordinates."""
            return (annot[0] == "tRNA"
                    and annot[1] == strand
                    and annot[2] <= start
                    and annot[3] >= end)

        # sense_only = cfilter(same_strand)
        # fastq = FQ_TEMPLATE % (name, seq, qual)
        # Simplify and collapse in a set, group by biotype
        # it seems that information is lost in the values
        # biotype2annots = dict(groupby(
        #     {get_gtf_info(gtf) for gtf in sorted(annots)}, key=FST))
        # biotype2annots = defaultdict(list)
        # Using set avoids repeated annotations (somehow they happen...)
        # It seems also slightly faster than using lists.
        # biotype2annots = defaultdict(set)
        # for biotype, annot_strand, annot_id in map(get_gtf_info, annots):
        # for annot in map(get_gtf_info, annots):
        #     biotype2annots[annot[0]].add(annot)
        #     # biotype2annots[annot[0]].append(annot)
        annot_set = {get_gtf_info(annot) for annot in annots}
        biotypes = {annot[0] for annot in annot_set}
        #
        # Determine prioritary annotation for this alignment
        # --------------------------------------------------
        #
        # For miRNA or piRNA, we ignore other annotation types, and we care
        # only about those annotations that are sense with respect to the read.
        # For miRNA, we also want the coordinates to match exactly.
        # For piRNA, we also want the coordinates to match exactly
        # and the read to be 21U (actually, we want to accept longer reads).
        # TODO (21/10/2021): Add a tRF category
        # with no constraints on read extremities coordinates.
        # Where in the priority order? After pi and mi, before si?
        #
        # If not a miRNA or piRNA, then it might be an RdRP-derived endosiRNA.
        # We want to keep the annotations for further analyses.
        #
        # If miRNA annotations are actually all antisense to the read,
        # continue to the following annotation categories
        if ("miRNA" in biotypes
                and any(map(match_mi, annot_set))):
            # If piRNA annotations are actually all antisense to the read,
            # or if the read is not a 21U, consider it an actual miRNA
            if (("piRNA" in biotypes)
                    and has_pi_signature(seq, read_len)
                    and any(map(match_pi, annot_set))):
                annot_name = "piRNA"
                signature = f"{PI_MIN}-{PI_MAX}U"
                annotations = set(filter(
                    match_pi, annot_set))
            else:
                annot_name = "miRNA"
                signature = "NA"
                annotations = set(filter(
                    match_mi, annot_set))
        elif ("tRNA" in biotypes
                and any(map(match_tRF, annot_set))):
            # If piRNA annotations are actually all antisense to the read,
            # or if the read is not a 21U, consider it an actual tRF
            if (("piRNA" in biotypes)
                    and has_pi_signature(seq, read_len)
                    and any(map(match_pi, annot_set))):
                annot_name = "piRNA"
                signature = f"{PI_MIN}-{PI_MAX}U"
                annotations = set(filter(
                    match_pi, annot_set))
            else:
                annot_name = "tRNA"
                signature = "NA"
                annotations = set(filter(
                    match_tRF, annot_set))
        elif has_pi_signature(seq, read_len):
            if ("piRNA" in biotypes
                    and any(map(match_pi, annot_set))):
                annot_name = "piRNA"
                signature = f"{PI_MIN}-{PI_MAX}U"
                annotations = set(filter(
                    match_pi, annot_set))
            else:
                # What do these aligned reads become?
                # TODO: be sure they get classified as "general" all_si
                annot_name = "_and_".join([
                    f"{PI_MIN}-{PI_MAX}U",
                    *sorted(biotypes)])
                signature = f"{PI_MIN}-{PI_MAX}U"
                annotations = annot_set
        # elif match_tRF():
        #     TODO
        # elif "piRNA" in biotype2annots:
        #     annotations = list(biotype2annots["piRNA"])
        #     annot = frozenset(
        #         set(biotype2annots.keys()) | {"anomalous_piRNA"})
        elif has_endosi_signature(seq, read_len):
            annot_name = "_and_".join([
                f"{SI_MIN}G",
                *sorted(biotypes)])
            signature = f"{SI_MIN}G"
            annotations = annot_set
        elif has_26G_signature(seq, read_len):
            annot_name = "_and_".join([
                f"{SI_MAX}G",
                *sorted(biotypes)])
            signature = f"{SI_MAX}G"
            annotations = annot_set
        elif has_csr1_endosi_signature(seq, read_len):
            annot_name = "_and_".join([
                f"csr1_{SI_MIN}G",
                *sorted(biotypes)])
            signature = f"csr1_{SI_MIN}G"
            annotations = annot_set
        else:
            # Can still be considered endo siRNA, if they are antisense
            # to certain annotation biotypes
            annot_name = "_and_".join([
                "no_signature",
                *sorted(biotypes)])
            signature = "no_signature"
            annotations = annot_set
        # maybe plug the read flow in composition analysis functions
        # compositions[annot].loc[read_len][seq[0]] += 1
        # It is faster using at:
        # compositions[annot].at[read_len, seq[0]] += 1
        return (annot_name, signature), (annotations, (name, seq, qual, read_len, strand))
    return process_annotations

# First: Count bona fide piRNA and miRNA
# (plus size and composition table for the miRNA)
# This count can be done at the read level
#
# To output a table for DESeq-like analyses:
# Either find a way to distinguish between various piRNA
# or counting for both of them

# Protein-oriented analysis
# -------------------------
# From (annotations, (name, seq, qual, read_len, strand) pairs, filter
# out from annotations those that are same_strand with respect to the read
# Reason 1: for protein_coding annotations, we are only interested in
# RDRP-generated small RNAs, which target an RNA
# Reason 2: for the other annotations, we consider that same_strand is likely
# some degradation product, and that the library preparation is good enough
# to make such cases negligible
#
# Eliminate reads that are antisense to some annotation other than
# protein_coding or satellites or simple repeats
# The reason is that the real target of the small RNA can be the other,
# and not the protein_coding
# (report the number of such cases as ambiguous real target)
# In other words, we exclude what is antisense to something other than
# CDS, UTR or pure_intron, to avoid counting false targets
#
# Then exclude (CDS and UTR) as ambiguous (report number of such cases)
# Then give counts on either:
#    CDS,
#    UTR,
#    pure_intron,
#    exon_junction (pure_intron and (CDS or UTR))
#
# For these categories, fill first-base composition table
# For these same reads, try to give per gene_id counts
# and write a table for DESeq-like analyses
# But what to do when there are several gene_id ?
# Exclude as ambiguous (report number of such cases).

# Transposon-oriented analysis
# ----------------------------
# Eliminate


# annotation names that seem to be frequent and that are not protein-coding
# COMMON_NO_TARGET = {"piRNA", "miRNA", "no_signature", f"{SI_MIN}G"}


def fq_writer(queue,
              fq_paths,
              ambig_type_path, ambig_id_path):
    """Takes information about fastq records to write in *queue*.
    The queue should give (type, fq) pairs where the type
    determines in which file to write the fastq string fq.
    Such a queue will receive information in *libsmallrna.count_small*
    via its *put* method and the information will be retrieved here
    via its *get* method.
    *fq_paths* should be a dictionary associating paths to
    fastq.gz files with small RNA types.
    The other two arguments are paths of files in which to write
    information when read classification is ambiguous."""
    # https://docs.python.org/3/library/contextlib.html#contextlib.ExitStack
    with ExitStack() as stack, \
            open(ambig_type_path, "w") as ambig_type_file, \
            open(ambig_id_path, "w") as ambig_id_file:
        dest_files = {
            f"{small_type}RNA": stack.enter_context(gzopen(fq_path, "wb"))
            for (small_type, fq_path) in fq_paths.items()}
        dest_files["ambiguous_type"] = ambig_type_file
        dest_files["ambiguous_id"] = ambig_id_file
        while True:
            (read_type, fastq) = queue.get()
            if read_type in dest_files:
                dest_files[read_type].write(fastq)
            else:
                assert read_type == "", f"Unexpected read type: {read_type}"
                print("No more fastq records to write.")
                for dest_file in dest_files.values():
                    dest_file.flush()
                break


@contextmanager
def annotation_context(annot_file, getter_type):
    """Yields a function to get annotations for an AlignedSegment."""
    if getter_type == "tabix":
        gtf_parser = pysam.ctabix.asGTF()
        gtf_file = pysam.TabixFile(annot_file, mode="r")
        fetch_annotations = gtf_file.fetch

        def get_annotations(ali):
            """Generates an annotation getter for *ali*."""
            # return fetch_annotations(*ALI2POS_INFO(ali), parser=gtf_parser)
            return fetch_annotations(
                ali.reference_name,
                ali.reference_start,
                ali.reference_end,
                parser=gtf_parser)
    elif getter_type == "pybedtools":
        gtf_file = open(annot_file, "r")
        # Does not work because somehow gets "consumed" after first usage
        # fetch_annotations = BedTool(gtf_file).all_hits
        # Much too slow
        # fetch_annotations = BedTool(gtf_file.readlines()).all_hits
        # https://daler.github.io/pybedtools/topical-low-level-ops.html
        fetch_annotations = BedTool(gtf_file).as_intervalfile().all_hits

        def get_annotations(ali):
            """Generates an annotation list for *ali*."""
            # return fetch_annotations(Interval(*ALI2POS_INFO(ali)))
            return fetch_annotations(Interval(
                ali.reference_name,
                ali.reference_start,
                ali.reference_end))
    else:
        raise NotImplementedError("%s not available" % getter_type)
    yield get_annotations
    gtf_file.close()


# To use with memory_profiler
# @profile
def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-b", "--bamfile",
        required=True,
        help="Sorted and indexed bam file containing the mapped reads."
        "A given read is expected to be aligned at only one location.")
    parser.add_argument(
        "-g", "--gtf",
        required=True,
        help="A sorted, bgzip-compressed gtf file."
        "A corresponding .tbi tabix index should exist.")
    parser.add_argument(
        "-a", "--annotation_getter",
        choices=["tabix", "pybedtools"],
        default="pybedtools",
        help="Method to use to access annotations from the gtf file.")
    parser.add_argument(
        "-r", "--reads_dir",
        required=True,
        help="Directory in which small RNA reads should be written.")
    parser.add_argument(
        "-o", "--out_dir",
        required=True,
        help="Directory in which results should be written.")
    parser.add_argument(
        "-l", "--logfile",
        help="File in which to write logs.")
    parser.add_argument(
        "-p", "--nprocs",
        type=int,
        default=max(1, cpu_count() - 2),
        help="Number of processes to use for the read annotation operations.")
    parser.add_argument(
        "-z", "--buffer_size",
        type=int,
        default=2500,
        help="Number to reads to give to a process in a batch.")
    parser.add_argument(
        "-u", "--remapped_polyU",
        help="Set this option to annotate remapped polyU small RNAs.",
        action="store_true")
    args = parser.parse_args()
    if not args.logfile:
        logfilename = "%s.log" % args.annotation_getter
    else:
        logfilename = args.logfile
    logging.basicConfig(
        filename=logfilename,
        level=logging.DEBUG)
    info = logging.info
    # debug = logging.debug
    # warning = logging.warning
    process_annotations = make_annotation_processor(args.annotation_getter)
    with annotation_context(args.gtf,
                            args.annotation_getter) as get_annotations:
        with pysam.AlignmentFile(args.bamfile, "rb") as bamfile:
            def generate_annotations():
                """Generates annotations for the alignments in *bamfile*."""
                last_t = perf_counter()
                nb_reads = 0
                for nb_reads, ali in enumerate(bamfile.fetch(), start=1):
                    if not nb_reads % 1000:
                        now = perf_counter()
                        info("%d alignments processed "
                             "(%.0f alignments / s)" % (
                                 nb_reads,
                                 1000.0 / (now - last_t)))
                        # if not nb_reads % 50000:
                        #     gc.collect()
                        last_t = perf_counter()
                    # Has side effects in that it modifies compositions
                    # yield process_annotations(
                    #     get_annotations(ali),
                    #     ali, compositions["all"])
                    yield process_annotations(get_annotations(ali), ali)
                global nb_mapped
                nb_mapped = nb_reads
                return
            annot_generator = generate_annotations()
            os.makedirs(args.out_dir, exist_ok=True)
            os.makedirs(args.reads_dir, exist_ok=True)

            # The annotations will be grouped in chunks of args.buffer_size
            # items (each item corresponding to annotation information gathered
            # for one alignment in the bam file).
            annot_stream = partition_all(
                args.buffer_size, annot_generator)
            # The functions in the list passed to the FuncApplier object
            # process chunks of annotations.
            # The FuncApplier object is a callable that replicates
            # the chunks for these functions to process, and puts the
            # corresponding results in a tuple.
            # stream_processor = FuncApplier([
            #     count_annots, count_pimis, count_sis, count_all_sis,
            #     count_first_bases])
            stream_processor = FuncApplier([
                count_annots, count_small,
                count_first_bases])
            small_types = [
                "pi", "mi", "tRF", "all_si",
                f"all_si_{SI_MIN}G", f"all_si_{SI_MAX}G", *SI_TYPES]
            # The same classification logic is applied to reads
            # that had a polyU tail, remapped after polyU removal,
            # but read and output file names are different.
            if args.remapped_polyU:
                # We don't really care about piu and miu,
                # but they are here for code simplicity.
                # One reason is that elements in small_types_u
                # should be in the same order as small_types
                # in order to be able to build the u2nonu dict.
                small_types_u = [
                    "piu", "miu", "tRFu", "all_siu",
                    f"all_siu_{SI_MIN}G", f"all_siu_{SI_MAX}G", *SIU_TYPES]
                # !! relies on the fact that dicts have stable order
                # via the construction of SIU_TYPES and SI_TYPES
                # from SMALL_TYPES_TREE
                u2nonu = dict(zip(small_types_u, small_types)).get
                # File paths dictionaries use u2nonu to have the base
                # small type names as keys, so that further code can
                # ignore the U vs. non-U distinction (see fq_writer).
                fq_paths = {u2nonu(small_type): OPJ(
                    args.reads_dir, f"{small_type}RNA.fastq.gz") for small_type in small_types_u}
                ambig_type_path = OPJ(args.out_dir, "ambig_typeU.txt")
                ambig_id_path = OPJ(args.out_dir, "ambig_idU.txt")
                nb_mapped_path = OPJ(args.reads_dir, "nb_mappedU.txt")
                annot_stats_path = OPJ(args.out_dir, "annot_statsU.txt")
                counts_paths = {u2nonu(small_type): OPJ(
                    args.out_dir, f"{small_type}_counts.txt") for small_type in small_types_u}
                first_base_composition_path = OPJ(
                    args.out_dir, "first_base_compositionU.txt")
            else:
                fq_paths = {small_type: OPJ(
                    args.reads_dir, f"{small_type}RNA.fastq.gz") for small_type in small_types}
                ambig_type_path = OPJ(args.out_dir, "ambig_type.txt")
                ambig_id_path = OPJ(args.out_dir, "ambig_id.txt")
                nb_mapped_path = OPJ(args.reads_dir, "nb_mapped.txt")
                annot_stats_path = OPJ(args.out_dir, "annot_stats.txt")
                counts_paths = {small_type: OPJ(
                    args.out_dir, f"{small_type}_counts.txt") for small_type in small_types}
                first_base_composition_path = OPJ(
                    args.out_dir, "first_base_composition.txt")
            with Manager() as mgr, Pool(processes=max(1, args.nprocs)) as pool:
                # This queue  will receive (read_type, fastq) pairs from
                # the annotation-processing functions.
                write_queue = mgr.Queue()
                # fq_writer takes (read_type, fastq) pairs from the queue
                # writes the fastq sequences in the appropriate files.
                writing = pool.apply_async(
                    fq_writer,
                    (write_queue,
                     fq_paths,
                     ambig_type_path, ambig_id_path))
                # The FuncApplier stream_processor will be called
                # on chunks of annotations taken from annot_stream.
                # This will happen in a distributed manner, each worker
                # in the pool dealing with the processing of one chunk.
                results_generator = pool.imap_unordered(
                    stream_processor,
                    ((annot_info, write_queue) for annot_info in annot_stream))
                # The results generated by results_generator are reduced
                # into final results in a tuple with as many elements as
                # there were functions in the FuncApplier object.
                # The results are grouped into tuples based on the functions
                # inside stream_processor that generate them
                (annot_stats,
                 counters_values,
                 nucl_counts) = reduce(
                     add_results,
                     results_generator,
                     (
                         defaultdict(int),
                         (defaultdict(int) for _ in small_types),
                         defaultdict(Composition)))
                counters = dict(zip(small_types, counters_values))
                with open(nb_mapped_path, "w") as counts_file:
                    counts_file.write("%d\n" % nb_mapped)
                print("total_ali:", nb_mapped)
                # Inform the queue that nothing more will come.
                write_queue.put(("", ""))
                # http://stackoverflow.com/a/42766899/1878788
                writing.get()

            with open(annot_stats_path, "w") as annot_stats_file:
                annot_stats_file.write("%s\n" % "\n".join(
                    mapstring(sorted(annot_stats.items(), key=SND))))

            for small_type in small_types:
                pd.DataFrame(
                    sorted(counters[small_type].items()),
                    columns=("gene", "count")).set_index("gene").to_csv(
                        counts_paths[small_type], sep="\t")

            with open(first_base_composition_path, "w") as composition_file:
                for read_len, nucl_count in sorted(nucl_counts.items()):
                    composition_file.write("%s\n" % "\t".join(mapstring([
                        read_len,
                        nucl_count.a,
                        nucl_count.c,
                        nucl_count.g,
                        nucl_count.t])))
    return 0


if __name__ == "__main__":
    exit(main())
