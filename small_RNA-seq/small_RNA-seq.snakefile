# Copyright (C) 2020-2023 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Snakefile to analyse small_RNA-seq data.

TODO: Some figures and summaries may be overridden when changing the mapper. The mapper name should be added to their path.

There are 2 types of counts. One resulting from small RNA annotations using a custom script, and one resulting from remapping and counting using featureCount.

The first one can contain "chimeric" annotations (possibly resulting from overlapping annotations?):
```
bli@naples:/extra2/Documents/Mhe/bli/small_RNA-seq_analyses$ head results_HRDE1_IP/bowtie2/mapped_C_elegans/annotation/hrde1_flag_HRDE1_IP_1_18-26_on_C_elegans/all_siu_counts.txt
gene	count
DNA?|DNA?|NDNAX1_CE:81	1
DNA?|DNA?|NDNAX2_CE:110_and_WBGene00005903	1
DNA?|DNA?|NDNAX2_CE:45	2
DNA?|DNA?|NDNAX3_CE:31	1
DNA|CMC-Chapaev|Chapaev-1_CE:45	1
DNA|CMC-Chapaev|Chapaev-1_CE:58_and_WBGene00005792	1
DNA|CMC-Chapaev|Chapaev-2_CE:40_and_WBGene00021886	1
DNA|CMC-Chapaev|Chapaev-2_CE:41_and_WBGene00021886	3
DNA|CMC-Chapaev|Chapaev-2_CE:42_and_WBGene00021886	5

Maybe it would be better to not use these counts.
```

"""
import sys
major, minor = sys.version_info[:2]
if major < 3 or (major == 3 and minor < 6):
    sys.exit("Need at least python 3.6\n")

# TODO (27/10/2021): isolate tRFs: sense to tRNA, then look at abundance distribution across sites on their remapping: Sites with narrow size distribution and high enough count are potentially interesting. Is it recurrent across libraries?
# TODO: add tRFs in pimi22G -> pimitRF22G
# Done (27/10/2021): add ri_si[u]_{22G|26G}: antisense to rRNA genes

# TODO: Try to find what the proportion of small reads map at unique position within repetitive elements.
# What is the distribution of unique reads among repetitive elements of a given family: is it evenly spread, or biased?
# Is it different if we use all small RNAs instead of just unique ones?
# For bowtie2, allow MAPQ >= 23 to get "not too ambiguously mapped reads"

# TODO: implement RPM folds from feature_counts results (and also for Ribo-seq pipeline)

# TODO: scatterplots log(IP_RPM) vs log(input_RPM) (or mutant vs WT), colouring with gene list
# TODO: heatmaps with rows (genes) ranked by expression in WT (mean reference level (wild types or inputs)), columns representing fold in mut vs WT
# Possibly remove the low expressed ones
# TODO: MA plot: fold vs "mean" count (to be defined)?

# TODO: extract most abundant piRNA reads (WT_48hph)
#bioawk -c fastx '{print $seq}' results_prg1_HS/bowtie2/mapped_C_elegans/reads/WT_RT_1_18-26_on_C_elegans/piRNA.fastq.gz | sort | uniq -c | sort -n | mawk '$1 >= 25 {print}' | wc -l
# -> 6857
# map them on the genome (without the piRNA loci), or on pseudo-genomes with specific features (histone genes...), tolerating a certain amount of mismatches (using bowtie1)
# Or: scan the genome (or "pseudo-genome") to find where there are matches with 0 to 3 mismatches at specific positions (13-21) (and perfect matches in the 5' zone)

# TODO: meta-profiles with IP and input on the same meta profile for Ortiz_oogenic and Ortiz_spermatogenic
# Make external script to generate meta-profiles on a custom list of libraries, and a given list of genes.

# TODO: boxplots with contrasts as series and a selection of gene lists
# Ex: 3 time points and oogenic and spermatogenic -> 6 boxes (custom program needed)
# Or simpler: one figure per gene list -> done for a full group of contrasts in make_gene_list_lfc_boxplots
# TODO: boxplots for all genes, for different small RNA categories (same as above but with all genes)

# Total number of "non-structural" (mapped - (fwd-(t,sn,sno,r-RNA))) to compute RPKM
# Quick-and-dirty: use gene span (TES - TSS) -> done for repeats
# Better: use length of union of exons for each gene -> done for genes
# Ideal: use transcriptome-based per-isoform computation
# Actually, we don't want a normalization by length. We deal with small RNAs, not transcripts
# TODO: make a bigwig of IP/input: is it possible?
# For RNA-seq?: use (oocyte, spermatogenic) intersected with "CSR-1-loaded" (=targets for different time point), csr1ADH_vs_WT_all_alltypes_up_genes_ids.txt (and intersected with non-spermatogenic) for meta-profiles


# Possibly filter out on RPKM
# Then, compute folds of RP(K)M IP/input (for a given experiment, i.-e. REP)
# and give list of genes sorted by such folds -> see /Gene_lists/csr1_prot_si_supertargets*

# Exploratory:
# Heatmap (fold)
# genes | experiment

# Then either:
# - take the genes (above log-fold threshold) common across replicates
# - look at irreproducible discovery rate (http://dx.doi.org/doi:10.1214%2F11-AOAS466)
# -> define CSR-1-loaded

# For metagene: see --metagene option of computeMatrix
# Retrieve gtf info after filtering out interfering genes based on merged bed

# TODO
# Locate potential targets of piRNA (using perfect complementarity), in a given annot_biotypes category for a given set of piRNA (the whole list, or for instance, up or down-regulated ones...).
# To do this: map all perfect matches, make a bed file, intersect with the annotation file for the given biotype, with antisense (note that this will automatically filter out self matches) -> done but only for all piRNAs
# Plot the count of endo-siRNA around those sites (from 100 upstream up to 100 downstream, using meta-TSS-like analysis) -> done


import os
OPJ = os.path.join
from distutils.util import strtobool
from glob import glob
from re import sub
from pickle import load
from yaml import safe_load as yload
from fileinput import input as finput
from sys import stderr
from subprocess import Popen, PIPE, CalledProcessError
# Garbage-collect (to be used before Popen)
# see comment on https://stackoverflow.com/a/13329386/1878788
from gc import collect

# Useful for functional style
from itertools import chain, combinations, product, repeat, starmap
from functools import partial, reduce
from operator import or_ as union
from cytoolz import concat, flip, merge_with, take_nth, valmap


def concat_lists(lists):
    return list(concat(lists))


def dont_merge(*values):
    """This function can be passed to *merge_with* so that
    merge fails when dicts share keys."""
    try:
        ((val,),) = values
    except ValueError:
        raise ValueError("The dictionaries are not supposed to share keys.")
    return val


# Useful data structures
from collections import OrderedDict as od
from collections import defaultdict, Counter


import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)


warnings.formatwarning = formatwarning


from snakemake.io import apply_wildcards
# from gatb import Bank
from mappy import fastx_read
# To parse SAM format
import pyBigWig

# To compute correlation coefficient
# from scipy.stats.stats import pearsonr
# To catch errors when plotting KDE
from scipy.linalg import LinAlgError
# For data processing and displaying
from sklearn import preprocessing
from sklearn.decomposition import PCA
import matplotlib as mpl
# To be able to run the script without a defined $DISPLAY
# https://github.com/mwaskom/seaborn/issues/1262
#mpl.use("agg")
mpl.use("PDF")
#mpl.rcParams["figure.figsize"] = 2, 4
mpl.rcParams["font.sans-serif"] = [
    "Arial", "Liberation Sans", "Bitstream Vera Sans"]
mpl.rcParams["font.family"] = "sans-serif"
#mpl.rcParams["figure.figsize"] = [16, 30]

from matplotlib import cm
from matplotlib.colors import Normalize

from matplotlib import numpy as np
from math import ceil, floor
from matplotlib.backends.backend_pdf import PdfPages
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
# import seaborn.apionly as sns
# import husl
# predefined seaborn graphical parameters
sns.set_context("talk")

from libsmallrna import (
    PI_MIN, PI_MAX, SI_MIN, SI_MAX,
    SI_SUFFIXES, SI_PREFIXES,
    SMALL_TYPES_TREE,
    type2RNA,
    types_under,
    SMALL_TYPES,
    SI_TYPES,
    SIU_TYPES,
    SISIU_TYPES,
    RMSK_SISIU_TYPES,
    JOINED_SMALL_TYPES)
# Do this outside the workflow
#from libhts import gtf_2_genes_exon_lengths, repeat_bed_2_lengths
from idconvert import gene_ids_data_dir
from libdeseq import do_deseq2
from libhts import aligner2min_mapq
from libhts import status_setter, make_empty_bigwig
from libhts import median_ratio_to_pseudo_ref_size_factors, size_factor_correlations
from libhts import plot_paired_scatters, plot_norm_correlations, plot_counts_distribution, plot_boxplots, plot_histo
from libworkflows import texscape, wc_applied, ensure_relative, cleanup_and_backup
from libworkflows import get_chrom_sizes, column_converter, make_id_list_getter
from libworkflows import read_int_from_file, strip_split, file_len, last_lines, plot_text, save_plot, SHELL_FUNCTIONS
from libworkflows import filter_combinator, read_feature_counts, sum_by_family, sum_feature_counts, sum_htseq_counts, warn_context
from smincludes import rules as irules
from smwrappers import wrappers_dir
strip = str.strip

NO_DATA_ERRS = [
    "Empty 'DataFrame': no numeric data to plot",
    "no numeric data to plot"]


alignment_settings = {"bowtie2": "-L 6 -i S,1,0.8 -N 0"}

# Positions in small RNA sequences for which to analyse nucleotide distribution
#POSITIONS = ["first", "last"]
POSITIONS = config["positions"]
# Orientations of small RNAs with respect to an annotated feature orientation.
# "fwd" and "rev" restrict feature quantification to sense or antisense reads.
ORIENTATIONS = config["orientations"]
# Codes for types of small RNAs
# Codes ending with "u" are for uridinylated endo siRNAs
# (recognized as not having mapped without first trimming a poly-T tail.
# "prot", "te" and "pseu" prefixes indicate the type of mRNA template that is inferred to have been used for endo siRNA synthesis by RdRP
# See small_RNA_seq_annotate.py and libsmallrna.pyx for more details


# small RNA types on which to run DESeq2
DE_TYPES = config["de_types"]
assert set(DE_TYPES) <= set(SMALL_TYPES + JOINED_SMALL_TYPES), "%s\n%s" % (", ".join(DE_TYPES), ", ".join(set(SMALL_TYPES + JOINED_SMALL_TYPES)))
# small RNA types on which to compute IP/input RPM folds
# prot_si does not include polyU siRNA, so the counts here may be lower than in pisimi
# siu includes SIU_TYPES, but not SI_TYPES, so the counts here may be lower than in pisimi
# pisimi includes all (SI_TYPES, SIU_TYPES, pi and mi (pi and mi will not be on the same genes as the siRNAs))
#IP_TYPES = ["pisimi", "siu", "prot_si"]
# TODO: update cross_HTS with pimi22G
# TODO: what kind of pimi22G ? -> pi, mi and si_22G, but not siu_22G
# TODO: add tRFs either in pimi22G or alone? If alone, check that label_order in plot_fold_heatmap is correct
IP_TYPES = [f"pimi{SI_MIN}G", f"pimi{SI_MIN}GtRF", f"siu_{SI_MIN}G", f"prot_si_{SI_MIN}G", f"ri_si_{SI_MIN}G",
            f"siu_{SI_MAX}G", f"prot_si_{SI_MAX}G", f"ri_si_{SI_MAX}G"]
assert set(IP_TYPES) <= set(
    SMALL_TYPES + [f"all_si_{SI_MIN}G", f"all_si_{SI_MAX}G"] + JOINED_SMALL_TYPES), ", ".join(IP_TYPES)
#IP_TYPES = config["ip_types"]
# Cutoffs in log fold change
LFC_CUTOFFS = [0.5, 1, 2]
UP_STATUSES = [f"up{cutoff}" for cutoff in LFC_CUTOFFS]
DOWN_STATUSES = [f"down{cutoff}" for cutoff in LFC_CUTOFFS]
#STANDARDS = ["zscore", "robust", "minmax", "unit"]
#STANDARDS = ["zscore", "robust", "minmax"]
# hexbin jointplot for principal components crashes on MemoryError for PCA without standardization
#STANDARDS = ["robust", "identity"]
STANDARDS = ["robust"]

COMPL = {"A" : "T", "C" : "G", "G" : "C", "T" : "A", "N" : "N"}

# Possible feature ID conversions
ID_TYPES = ["name", "cosmid"]

#########################
# Reading configuration #
#########################
# key: library name
# value: 3' adapter sequence
lib2adapt = config["lib2adapt"]
trim5 = config["trim5"]
trim3 = config["trim3"]
# key: library name
# value: path to raw data
lib2raw = config["lib2raw"]
LIBS = list(lib2raw.keys())
#REF=config["WT"]
#MUT=config["mutant"]
# Used to associate colours to libraries
# key: series name
# value: list of libraries
colour_series_dict = config["colour_series"]
genotype_series = colour_series_dict.get("genotype_series", [])
# Groups of libraries to put together on a same metagene profile
lib_groups = config["lib_groups"]
GROUP_TYPES = list(lib_groups.keys())
merged_groups = merge_with(concat_lists, *lib_groups.values())
ALL_LIB_GROUPS = list(merged_groups.keys())
#all_libs_in_group = concat_lists(merge_with(concat_lists, *lib_groups.values()).values())
REPS = config["replicates"]
#TREATS = config["treatments"]
# Conditions to compare using DESeq2
# Note: we don't want contrasts of the following type:
# - ({geno}, {geno}) where various treatments are taken into account
# - ({treat}, {treat}) where various genotypes are taken into account
# We also don't want contrasts where more than one factor vary (ex: prg1_RT vs WT_HS30)
#ALL_COND_PAIRS = [(MUT, REF)] + list(combinations(reversed(TREATS), 2)) + [("%s_%s" % (lib2, treat2), "%s_%s" % (lib1, treat1)) for ((lib1, treat1), (lib2, treat2)) in combinations(product(LIBS, TREATS), 2)]
#COND_PAIRS = [(lib2, lib1) for (lib1, lib2) in [
#    ["%s_%s" % (geno, treat) for (geno, treat) in product(genos, treats)] for (genos, treats) in chain(
#        product(combinations(LIBS, 1), combinations(TREATS, 2)),
#        product(combinations(LIBS, 2), combinations(TREATS, 1)))]]
#CONTRASTS = ["%s_vs_%s" % cond_pair for cond_pair in COND_PAIRS]
#CONTRAST2PAIR = dict(zip(CONTRASTS, COND_PAIRS))
DE_COND_PAIRS = config["de_cond_pairs"]
msg = "\n".join([
    "Some contrats do not use known library names.",
    "Contrasts:"
    ", ".join([f"({cond}, {ref})" for (cond, ref) in DE_COND_PAIRS])])
assert all([cond in LIBS and ref in LIBS for (cond, ref) in DE_COND_PAIRS]), msg
IP_COND_PAIRS = config["ip_cond_pairs"]
msg = "\n".join([
    "Some contrats do not use known library names.",
    "Contrasts:"
    ", ".join([f"({cond}, {ref})" for (cond, ref) in IP_COND_PAIRS])])
assert all([cond in LIBS and ref in LIBS for (cond, ref) in IP_COND_PAIRS]), ""
COND_PAIRS = DE_COND_PAIRS + IP_COND_PAIRS
DE_CONTRASTS = [f"{cond1}_vs_{cond2}" for [cond1, cond2] in DE_COND_PAIRS]
IP_CONTRASTS = [f"{cond1}_vs_{cond2}" for [cond1, cond2] in IP_COND_PAIRS]
contrasts_dict = {"de" : DE_CONTRASTS, "ip" : IP_CONTRASTS}
CONTRASTS = DE_CONTRASTS + IP_CONTRASTS
CONTRAST2PAIR = dict(zip(CONTRASTS, COND_PAIRS))
MIN_LEN = config["min_len"]
MAX_LEN = config["max_len"]
size_selected = "%s-%s" % (MIN_LEN, MAX_LEN)
# bli@naples:/pasteur/entites/Mhe/Genomes$ cat C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/miRNA.bed | mawk '{hist[$3-$2]++} END {for (l in hist) print l"\t"hist[l]}' | sort -n
# 17	1
# 18	2
# 19	4
# 20	18
# 21	57
# 22	172
# 23	138
# 24	45
# 25	11
# 26	3
# This should ideally come from genome configuration:
MI_MAX = 26
read_type_max_len = {
    size_selected: int(MAX_LEN),
    "pi": min(PI_MAX, int(MAX_LEN)),
    "si": min(SI_MAX, int(MAX_LEN)),
    "mi": min(MI_MAX, int(MAX_LEN)),
    "tRF": int(MAX_LEN)}
READ_TYPES_FOR_COMPOSITION = [
    size_selected, "nomap_siRNA", "all_siRNA",
    f"all_si_{SI_MIN}GRNA", f"all_si_{SI_MAX}GRNA",
    *type2RNA(SMALL_TYPES)]
READ_TYPES_FOR_MAPPING = [
    *READ_TYPES_FOR_COMPOSITION,
    "all_siuRNA",
    f"si_{SI_MIN}GRNA", f"si_{SI_MAX}GRNA",
    f"siu_{SI_MIN}GRNA", f"siu_{SI_MAX}GRNA"]
# Types of annotation features, as defined in the "gene_biotype"
# GTF attribute sections of the annotation files.
COUNT_BIOTYPES = config["count_biotypes"]
BOXPLOT_BIOTYPES = config.get("boxplot_biotypes", [])
ANNOT_BIOTYPES = config["annot_biotypes"]
assert "protein_coding_3UTR" not in set(ANNOT_BIOTYPES), "It seems having 3' or 5' UTR annotations makes 22G RNA disappear from those regions. Only use the more general protein_coding_UTR in annot_biotypes"

RMSK_BIOTYPES = [
    "DNA_transposons_rmsk",
    "RNA_transposons_rmsk",
    "satellites_rmsk",
    "simple_repeats_rmsk"]
RMSK_FAMILIES_BIOTYPES = [
    "DNA_transposons_rmsk_families",
    "RNA_transposons_rmsk_families",
    "satellites_rmsk_families",
    "simple_repeats_rmsk_families"]

BIOTYPES_TO_JOIN = {
    "all_rmsk": [biotype for biotype in COUNT_BIOTYPES + BOXPLOT_BIOTYPES if biotype in RMSK_BIOTYPES],
    "all_rmsk_families": [biotype for biotype in COUNT_BIOTYPES + BOXPLOT_BIOTYPES if biotype in RMSK_FAMILIES_BIOTYPES],
    # We only count "protein_coding", not "protein_codin_{5UTR,CDS,3UTR}"
    "alltypes": [biotype for biotype in COUNT_BIOTYPES + BOXPLOT_BIOTYPES if not biotype.startswith("protein_coding_")]}
# Filter out those with empty values
JOINED_BIOTYPES = [joined_biotype for (joined_biotype, biotypes) in BIOTYPES_TO_JOIN.items() if biotypes]

# cf Gu et al. (2009), supplement:
# -----
# To compare 22G-RNAs derived from a gene, transposon, or pseudogene between two
# samples, each sample was normalized using the total number of reads less structural RNAs, i.e.
# sense small RNA reads likely derived from degraded ncRNAs, tRNAs, snoRNAs, rRNAs,
# snRNAs, and scRNAs. Degradation products of structural RNAs map to the sense strand, with a
# poorly defined size profile and 1 st nucleotide distribution. At least 25 22G-RNA reads per million,
# nonstructural reads in one of the two samples was arbitrarily chosen as a cutoff for comparison
# analyses. A change of 2-fold or more between samples was chosen as an enrichment threshold.
# Because some 21U-RNAs or miRNAs overlap with protein coding genes, reads derived from
# miRNA loci within a window of ± 4 nt and all the known 21U-RNAs were filtered out prior to
# comparison analysis.
# -----
# And Germano Cecere, about scRNA:
# -----
# Its an old nomenclature and in anycase there is only one of this annotated scRNAs
# (small cytoplasmic RNA genes).
# https://www.ncbi.nlm.nih.gov/books/NBK19701/table/genestructure_table2/?report=objectonly
# Don't even pay attention to this
# -----
STRUCTURAL_BIOTYPES = ["tRNA", "snRNA", "snoRNA", "rRNA", "ncRNA"]
GENE_LISTS = config["gene_lists"]
BOXPLOT_GENE_LISTS = config["boxplot_gene_lists"]
#BOXPLOT_GENE_LISTS = [
#    "all_genes",
#    "replication_dependent_octamer_histone",
#    "piRNA_dependent_prot_si_down4",
#    "csr1_prot_si_supertargets_48hph",
#    "spermatogenic_Ortiz_2014", "oogenic_Ortiz_2014"]
aligner = config["aligner"]
########################
# Genome configuration #
########################
# config["genome_dict"] can be either the path to a genome configuration file
# or a dict
if isinstance(config["genome_dict"], (str, bytes)):
    print(f"loading {config['genome_dict']}", file=stderr)
    with open(config["genome_dict"]) as fh:
        genome_dict = yload(fh)
else:
    genome_dict = config["genome_dict"]
genome = genome_dict["name"]
chrom_sizes = get_chrom_sizes(genome_dict["size"])
chrom_sizes.update(valmap(int, genome_dict.get("extra_chromosomes", {})))
genomelen = sum(chrom_sizes.values())
genome_db = genome_dict["db"][aligner]
# bed file binning the genome in 10nt bins
genome_binned = genome_dict["binned"]
annot_dir = genome_dict["annot_dir"]
exon_lengths_file = OPJ(annot_dir, "union_exon_lengths.txt"),
# What are the difference between
# OPJ(convert_dir, "wormid2name.pickle") and genome_dict["converter"]?
# /!\ gene_ids_data_dir contains more conversion dicts,
# but is not influenced by genome preparation customization,
# like splitting of miRNAs into 3p and 5p.
convert_dir = genome_dict.get("convert_dir", gene_ids_data_dir)
# For wormid2name, load in priority the one
# that might contain custom gene names, like for splitted miRNAs
with open(
        genome_dict.get(
            "converter",
            OPJ(convert_dir, "wormid2name.pickle")),
        "rb") as dict_file:
    wormid2name = load(dict_file)

gene_lists_dir = genome_dict["gene_lists_dir"]
avail_id_lists = set(glob(OPJ(gene_lists_dir, "*_ids.txt")))
index = genome_db

upload_on_err = strtobool(str(config.get("upload_on_err", "True")))
#output_dir = config["output_dir"]
#workdir: config["output_dir"]
output_dir = os.path.abspath(".")
local_annot_dir = config.get("local_annot_dir", OPJ("annotations"))
log_dir = config.get("log_dir", OPJ("logs"))
data_dir = config.get("data_dir", OPJ("data"))
# To put the results of small_RNA_seq_annotate
mapping_dir = OPJ(aligner, f"mapped_{genome}")
reads_dir = OPJ(mapping_dir, "reads")
annot_counts_dir = OPJ(mapping_dir, "annotation")
# TODO: add tRFs?
# The order after pi, mi and tRF must match: this is used in make_read_counts_summary
ANNOT_COUNTS_TYPES = [
    "pi", "mi", "tRF", "all_si",
    f"all_si_{SI_MIN}G", f"all_si_{SI_MAX}G",
    *SI_TYPES]
ANNOT_COUNTS_TYPES_U = [
    "all_siu",
    f"all_siu_{SI_MIN}G", f"all_siu_{SI_MAX}G",
    *SIU_TYPES]
feature_counts_dir = OPJ(mapping_dir, "feature_count")
READ_TYPES_FOR_COUNTING = ["all_siRNA", f"{size_selected}_and_nomap_siRNA", f"prot_si_{SI_MIN}GRNA_and_prot_siu_{SI_MIN}GRNA", *type2RNA([f"prot_si_{SI_MIN}G", f"prot_si_{SI_MAX}G"])]
# Reads remapped and counted using featureCounts
REMAPPED_COUNTED = [
    f"{small_type}_{mapping_type}_{biotype}_{orientation}_transcript" for (
        small_type, mapping_type, biotype, orientation) in product(
            READ_TYPES_FOR_COUNTING,
            [f"on_{genome}", f"unique_on_{genome}"],
            #[f"on_{genome}"],
            COUNT_BIOTYPES + JOINED_BIOTYPES,
            ORIENTATIONS)]
# As of july 2020, exons are only available in genes.gtf
REMAPPED_COUNTED.extend([
    f"{small_type}_{mapping_type}_genes_{orientation}_exon" for (
        small_type, mapping_type, orientation) in product(
            READ_TYPES_FOR_COUNTING,
            [f"on_{genome}", f"unique_on_{genome}"],
            ORIENTATIONS)])
# Used to skip some genotype x treatment x replicate number combinations
# when some of them were not sequenced
forbidden = {frozenset(wc_comb.items()) for wc_comb in config["missing"]}
CONDITIONS = [{
    "lib" : lib,
    "rep" : rep} for rep in REPS for lib in LIBS]
# We use this for various things in order to have always the same library order:
COND_NAMES = ["_".join((
    cond["lib"],
    cond["rep"])) for cond in CONDITIONS]
COND_COLUMNS = pd.DataFrame(CONDITIONS).assign(
    cond_name=pd.Series(COND_NAMES).values).set_index("cond_name")
#SIZE_FACTORS = ["raw", "deduped", size_selected, "mapped", "siRNA", "miRNA"]
#SIZE_FACTORS = [size_selected, "mapped", "miRNA"]
# TESTED_SIZE_FACTORS = ["mapped", "non_structural", "siRNA", "miRNA", "median_ratio_to_pseudo_ref"]
TESTED_SIZE_FACTORS = ["mapped", "non_structural", "all_sisiuRNA", "piRNA", "miRNA", "median_ratio_to_pseudo_ref"]
#SIZE_FACTORS = ["mapped", "miRNA", "median_ratio_to_pseudo_ref"]
# "median_ratio_to_pseudo_ref" is a size factor adapted from
# the method described in the DESeq paper, but with addition
# and then substraction of a pseudocount, in order to deal with zero counts.
# This seems to perform well (see "test_size_factor" results).
DE_SIZE_FACTORS = ["non_structural", "median_ratio_to_pseudo_ref"]
SIZE_FACTORS = config.get("size_factors", ["non_structural"])
#NORMALIZER = "median_ratio_to_pseudo_ref"
# Do we really need distinct lists of normalizers?
RPM_NORMS = SIZE_FACTORS
RPM_FOLD_TYPES = [
    f"mean_log2_RPM_by_{norm}_fold"
    for norm in RPM_NORMS]

# For metagene analyses
#META_MARGIN = 300
META_MARGIN = 0
META_SCALE = 500
#UNSCALED_INSIDE = 500
UNSCALED_INSIDE = 0
#META_MIN_LEN = 1000
META_MIN_LEN = 2 * UNSCALED_INSIDE
MIN_DIST = 2 * META_MARGIN
READ_TYPES_FOR_METAPROFILES = [
    size_selected,
    f"all_si_{SI_MIN}GRNA", f"all_si_{SI_MAX}GRNA",
    f"si_{SI_MIN}GRNA", f"si_{SI_MAX}GRNA",
    f"siu_{SI_MIN}GRNA", f"siu_{SI_MAX}GRNA",
    "miRNA", "piRNA", "tRFRNA", "all_siRNA"]

# split = str.split
#
#
# def strip_split(text):
#     return split(strip(text), "\t")
#
#
# # http://stackoverflow.com/a/845069/1878788
# def file_len(fname):
#     p = Popen(
#         ['wc', '-l', fname],
#         stdout=PIPE,
#         stderr=PIPE)
#     (result, err) = p.communicate()
#     if p.returncode != 0:
#         raise IOError(err)
#     return int(result.strip().split()[0])


def add_dataframes(df1, df2):
    return df1.add(df2, fill_value=0)


def sum_dataframes(dfs):
    return reduce(add_dataframes, dfs)


def sum_counts(fname):
    collect()
    p = Popen(
        # ['awk', '$1 ~ /^piRNA$|^miRNA$|^pseudogene$|^satellites_rmsk$|^simple_repeats_rmsk$|^protein_coding_|^.NA_transposons_rmsk$/ {sum += $2} END {print sum}', fname],
        # slightly faster
        ['mawk', '$1 ~ /^piRNA$|^miRNA$|^pseudogene$|^satellites_rmsk$|^simple_repeats_rmsk$|^rRNA$|^protein_coding_|^.NA_transposons_rmsk$/ {sum += $2} END {print sum}', fname],
        stdout=PIPE,
        stderr=PIPE)
    (result, err) = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    try:
        return int(result.strip().split()[0])
    except IndexError:
        warnings.warn(f"No counts in {fname}\n")
        return 0

def sum_te_counts(fname):
    collect()
    p = Popen(
        ['awk', '$1 !~ /WBGene/ {sum += $2} END {print sum}', fname],
        stdout=PIPE,
        stderr=PIPE)
    (result, err) = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    return int(result.strip().split()[0])


import matplotlib.patheffects


class Scale(matplotlib.patheffects.RendererBase):
    def __init__(self, sx, sy=None):
        self._sx = sx
        self._sy = sy

    def draw_path(self, renderer, gc, tpath, affine, rgbFace):
        affine = affine.identity().scale(self._sx, self._sy) + affine
        renderer.draw_path(gc, tpath, affine, rgbFace)


def print_wc(wildcards):
    print(wildcards)
    return []


def check_wc(wildcards):
    #if hasattr(wildcards, "read_type"):
    #    if wildcards.read_type.endswith("_on_C"):
    #        print(wildcards)
    if any(attr.endswith("_on_C") for attr in vars(wildcards).values() if type(attr) == "str"):
        print(wildcards)
    return []


filtered_product = filter_combinator(product, forbidden)

# Limit risks of ambiguity by imposing replicates to be numbers
# and restricting possible forms of some other wildcards
wildcard_constraints:
    lib="|".join(LIBS),
    #treat="|".join(TREATS),
    rep="\d+",
    min_dist="\d+",
    min_len="\d+",
    #max_len="\d+",
    biotype="|".join(set(COUNT_BIOTYPES + ANNOT_BIOTYPES + JOINED_BIOTYPES + ["genes"])),
    id_list="|".join(GENE_LISTS),
    type_set="|".join(["all", "protein_coding", "protein_coding_TE"]),
    mapping_type="|".join([f"on_{genome}", f"unique_on_{genome}"]),
    #small_type="si|siu|sisiu|all_si|all_siu|all_sisiu|%s" % "|".join(SMALL_TYPES + JOINED_SMALL_TYPES),
    small_type="si|siu|sisiu|all_si|all_siu|all_sisiu|all_si_%sG|all_si_%sG|%s" % (SI_MIN, SI_MAX, "|".join(SMALL_TYPES + JOINED_SMALL_TYPES)),
    #small_type="si|siu|sisiu|all_si|all_siu|all_sisiu|%s" % "|".join(
    #    [f"all_si_{SI_MIN}G", "all_si_{SI_MAX}G"] + SMALL_TYPES + JOINED_SMALL_TYPES),
    mapped_type="|".join([size_selected, "nomap_siRNA"]),
    read_type="|".join([*REMAPPED_COUNTED, *READ_TYPES_FOR_MAPPING, *READ_TYPES_FOR_COUNTING, "trimmed", "nomap"]),
    infix="si|siu",
    suffix="|".join(SI_SUFFIXES),
    standard="zscore|robust|minmax|unit|identity",
    orientation="all|fwd|rev",
    contrast="|".join(CONTRASTS),
    norm="|".join(TESTED_SIZE_FACTORS),
    lib_group="|".join(ALL_LIB_GROUPS),
    group_type="|".join(GROUP_TYPES),
    fold_type="|".join(["log2FoldChange", "lfcMLE", *RPM_FOLD_TYPES]),
    feature_type="|".join(["transcript", "exon"])

#ruleorder: map_on_genome > sam2indexedbam > compute_coverage > remap_on_genome > resam2indexedbam > recompute_coverage

# Define functions to be used in shell portions
shell.prefix(SHELL_FUNCTIONS)


###################################
# Preparing the input of rule all #
###################################

bigwig_files = [
    # individual libraries
    expand(
        expand(
            OPJ(mapping_dir, "{lib}_{rep}",
                "{lib}_{rep}_{{read_type}}_on_%s_by_{{norm}}_{{orientation}}.bw" % genome),
            filtered_product, lib=LIBS, rep=REPS),
        read_type=READ_TYPES_FOR_MAPPING, norm=SIZE_FACTORS, orientation=["fwd", "rev", "all"]),
    # means of replicates
    expand(
        expand(
            OPJ(mapping_dir, "{lib}_mean",
                "{lib}_mean_{{read_type}}_on_%s_by_{{norm}}_{{orientation}}.bw" % genome),
            filtered_product, lib=LIBS),
        read_type=READ_TYPES_FOR_MAPPING, norm=SIZE_FACTORS, orientation=["fwd", "rev", "all"]),
    ]

meta_profiles = [
        #expand(OPJ(local_annot_dir, "transcripts_{type_set}", "merged_isolated_{min_dist}.bed"), type_set=["all", "protein_coding", "protein_coding_TE"], min_dist="0 5 10 25 50 100 250 500 1000 2500 5000 10000".split()),
        #expand(OPJ(local_annot_dir, "transcripts_{type_set}", "merged_isolated_{min_dist}_{biotype}_min_{min_len}.bed"), type_set=["all", "protein_coding", "protein_coding_TE"], min_dist="0 5 10 25 50 100 250 500 1000 2500 5000 10000".split(), biotype=["protein_coding"], min_len=[str(META_MIN_LEN)]),
        [expand(
            OPJ("figures", "mean_meta_profiles_meta_scale_{meta_scale}",
                "{read_type}_by_{norm}_{orientation}_on_{type_set}_merged_isolated_{min_dist}_{biotype}_min_{min_len}_{group_type}_{lib_group}_meta_profile.pdf"),
            meta_scale=[str(META_SCALE)],
            read_type=READ_TYPES_FOR_METAPROFILES,
            norm=SIZE_FACTORS, orientation=["all"], type_set=["protein_coding_TE"], min_dist=[str(MIN_DIST)],
            biotype=["protein_coding", "DNA_transposons_rmsk", "RNA_transposons_rmsk"], min_len=[str(META_MIN_LEN)],
            group_type=[group_type], lib_group=list(lib_group.keys())) for (group_type, lib_group) in lib_groups.items()],
        # Same as above ?
        # [expand(
        #     OPJ("figures", "mean_meta_profiles_meta_scale_{meta_scale}",
        #         "{read_type}_by_{norm}_{orientation}_on_{type_set}_merged_isolated_{min_dist}_{biotype}_min_{min_len}_{group_type}_{lib_group}_meta_profile.pdf"),
        #     meta_scale=[str(META_SCALE)], read_type=[size_selected, "siRNA", "siuRNA", "miRNA", "piRNA", "all_siRNA"],
        #     norm=SIZE_FACTORS, orientation=["all"], type_set=["protein_coding_TE"], min_dist=[str(MIN_DIST)],
        #     biotype=["protein_coding", "DNA_transposons_rmsk", "RNA_transposons_rmsk"], min_len=[str(META_MIN_LEN)],
        #     group_type=[group_type], lib_group=list(lib_group.keys())) for (group_type, lib_group) in lib_groups.items()],
        [expand(
            OPJ("figures", "mean_meta_profiles_meta_scale_{meta_scale}",
                "{read_type}_by_{norm}_{orientation}_on_{type_set}_merged_isolated_{min_dist}_{biotype}_min_{min_len}_{group_type}_{lib_group}_meta_profile.pdf"),
            meta_scale=[str(META_SCALE)],
            read_type=READ_TYPES_FOR_METAPROFILES,
            norm=SIZE_FACTORS, orientation=["all"], type_set=["protein_coding"], min_dist=[str(MIN_DIST)],
            biotype=["protein_coding"], min_len=[str(META_MIN_LEN)],
            group_type=[group_type], lib_group=list(lib_group.keys())) for (group_type, lib_group) in lib_groups.items()],
        [expand(
            OPJ("figures", "mean_meta_profiles_meta_scale_{meta_scale}",
                "{read_type}_by_{norm}_{orientation}_on_{type_set}_merged_isolated_{min_dist}_{id_list}_{group_type}_{lib_group}_meta_profile.pdf"),
            meta_scale=[str(META_SCALE)],
            read_type=READ_TYPES_FOR_METAPROFILES,
            norm=SIZE_FACTORS, orientation=["all"], type_set=["protein_coding_TE"], min_dist=["0"], id_list=GENE_LISTS,
            group_type=[group_type], lib_group=list(lib_group.keys())) for (group_type, lib_group) in lib_groups.items()],
        ## TODO: Resolve issue with bedtools
        # expand(
        #     OPJ("figures", "{lib}_{rep}",
        #         "{read_type}_by_{norm}_{orientation}_pi_targets_in_{biotype}_profile.pdf"),
        #     lib=LIBS, rep=REPS, read_type=READ_TYPES_FOR_METAPROFILES,
        #     norm=SIZE_FACTORS, orientation=["all"], biotype=["protein_coding"]),
    ]

read_graphs = [
    expand(
        expand(
            OPJ("figures", "{{lib}}_{{rep}}", "{read_type}_base_composition_from_{position}.pdf"),
            read_type=READ_TYPES_FOR_COMPOSITION, position=["start", "end"]),
        filtered_product, lib=LIBS, rep=REPS),
    expand(
        expand(
            OPJ("figures", "{{lib}}_{{rep}}", "{read_type}_base_logo_from_{position}.pdf"),
            read_type=READ_TYPES_FOR_COMPOSITION, position=["start", "end"]),
        filtered_product, lib=LIBS, rep=REPS),
    expand(
        expand(
            OPJ("figures", "{{lib}}_{{rep}}", "{read_type}_{position}_base_composition.pdf"),
            read_type=READ_TYPES_FOR_COMPOSITION, position=POSITIONS),
        filtered_product, lib=LIBS, rep=REPS),
    expand(
        expand(
            OPJ("figures", "{{lib}}_{{rep}}", "{read_type}_{position}_base_logo.pdf"),
            read_type=READ_TYPES_FOR_COMPOSITION, position=POSITIONS),
        filtered_product, lib=LIBS, rep=REPS),
    expand(
        expand(
            OPJ("figures", "{{lib}}_{{rep}}", "{read_type}_size_distribution.pdf"),
            read_type=["trimmed", "nomap"]),
        filtered_product, lib=LIBS, rep=REPS),
    expand(
        OPJ("figures", "{lib}_{rep}", f"{size_selected}_smallRNA_barchart.pdf"),
        filtered_product, lib=LIBS, rep=REPS),
    expand(
        OPJ("figures", "{lib}_{rep}", "nb_reads.pdf"),
        filtered_product, lib=LIBS, rep=REPS),
    ]

ip_fold_heatmaps = []
de_fold_heatmaps = []
ip_fold_boxplots = []
# Temporary, until used for boxplots:
remapped_folds = []
remapped_fold_boxplots = []
if contrasts_dict["ip"]:
    if IP_CONTRASTS:
        if BOXPLOT_BIOTYPES:
            remapped_fold_boxplots = expand(
                OPJ("figures", "{contrast}", "by_biotypes",
                    "{contrast}_{read_type}_{mapping_type}_{biotypes}_{orientation}_transcript_{fold_type}_{gene_list}_boxplots.pdf"),
                contrast=IP_CONTRASTS,
                read_type=READ_TYPES_FOR_COUNTING,
                mapping_type=[f"on_{genome}", f"unique_on_{genome}"],
                #mapping_type=[f"on_{genome}"],
                # TODO: Read this from config file
                biotypes=["_and_".join(BOXPLOT_BIOTYPES)],
                orientation=ORIENTATIONS,
                fold_type=RPM_FOLD_TYPES,
                gene_list=BOXPLOT_GENE_LISTS)
        #remapped_folds = expand(
        #    OPJ(mapping_dir, f"RPM_folds_{size_selected}", "all", "remapped", "{counted_type}_mean_log2_RPM_fold.txt"),
        #    counted_type=REMAPPED_COUNTED)
        remapped_folds = expand(
            OPJ(mapping_dir, "RPM_by_{norm}_folds_%s" % size_selected, "all", "remapped", "{counted_type}_mean_log2_RPM_by_{norm}_fold.txt"),
            counted_type=REMAPPED_COUNTED, norm=RPM_NORMS)
    # Should be generated as a dependency of the "all" contrast:
    ## TODO: check that this is generated
    # remapped_folds += expand(
    #     OPJ(mapping_dir, f"RPM_folds_{size_selected}", "{contrast}", "remapped",
    #         "{contrast}_{read_type}_{mapping_type}_{biotype}_{orientation}_transcript_RPM_folds.txt"),
    #     contrast=IP_CONTRASTS,
    #     read_type=READ_TYPES_FOR_COUNTING,
    #     mapping_type=[f"on_{genome}", f"unique_on_{genome}"],
    #     biotype=["alltypes"],
    #     orientation=ORIENTATIONS)
    ##
    # snakemake -n OK
    # expand(
    #     OPJ(mapping_dir, f"RPM_folds_{size_selected}", "all",
    #         "{read_type}_{mapping_type}_{biotype}_{orientation}_transcript_mean_log2_RPM_fold.txt"),
    #     read_type=READ_TYPES_FOR_COUNTING, mapping_type=[f"on_{genome}"], biotype=COUNT_BIOTYPES, orientation=ORIENTATIONS),
    # snakemake -n OK
    # expand(
    #     OPJ(mapping_dir, f"RPM_folds_{size_selected}", "{contrast}",
    #         "{contrast}_{small_type}_{mapping_type}_{biotype}_{orientation}_transcript_RPM_folds.txt"),
    #     contrast=IP_CONTRASTS, small_type=READ_TYPES_FOR_COUNTING, mapping_type=[f"on_{genome}"], biotype=COUNT_BIOTYPES, orientation=ORIENTATIONS),
    ip_fold_heatmaps = expand(
        OPJ("figures", "fold_heatmaps", "{small_type}_{fold_type}_heatmap.pdf"),
        small_type=IP_TYPES, fold_type=RPM_FOLD_TYPES)
    ip_fold_boxplots = expand(
        OPJ("figures", "all_{contrast_type}",
            "{contrast_type}_{small_type}_{fold_type}_{gene_list}_boxplots.pdf"),
        contrast_type=["ip"], small_type=IP_TYPES, fold_type=RPM_FOLD_TYPES,
        gene_list=BOXPLOT_GENE_LISTS)
    # Subdivided by biotype (to enable distinction between 5'UTR, CDS and 3'UTR)
    ## TODO: activate this?
    # ip_fold_boxplots = expand(
    #     OPJ("figures", "all_{contrast_type}",
    #         "{contrast_type}_{small_type}_{fold_type}_{gene_list}_{biotype}_boxplots.pdf"),
    #     contrast_type=["ip"], small_type=IP_TYPES, fold_type=["mean_log2_RPM_fold"],
    #     gene_list=BOXPLOT_GENE_LISTS, biotype=["protein_coding_5UTR", "protein_coding_CDS", "protein_coding_3UTR"])
    ##
if contrasts_dict["de"]:
    de_fold_heatmaps = expand(
        OPJ("figures", "fold_heatmaps", "{small_type}_{fold_type}_heatmap.pdf"),
        small_type=DE_TYPES, fold_type=["log2FoldChange"])

exploratory_graphs = [
    ip_fold_heatmaps,
    de_fold_heatmaps,
    # Large figures, not very readable
    # expand(
    #     OPJ(mapping_dir, f"deseq2_{size_selected}", "{contrast}", "{contrast}_{small_type}_by_{norm}_pairplots.pdf"),
    #     contrast=DE_CONTRASTS, small_type=DE_TYPES, norm=DE_SIZE_FACTORS),
    ## TODO: debug PCA
    #expand(
    #    OPJ("figures", "{small_type}_{standard}_PCA.pdf"),
    #    small_type=["pisimi"], standard=STANDARDS),
    #expand(
    #    OPJ("figures", "{small_type}_{standard}_PC1_PC2_distrib.pdf"),
    #    small_type=["pisimi"], standard=STANDARDS),
    ##
    #expand(
    #    OPJ("figures", "{small_type}_clustermap.pdf"),
    #    small_type=SMALL_TYPES),
    #expand(
    #    OPJ("figures", "{small_type}_zscore_clustermap.pdf"),
    #    small_type=SMALL_TYPES),
    #expand(
    #    OPJ("figures", "{contrast}", "{small_type}_zscore_clustermap.pdf"),
    #    contrast=CONTRASTS, small_type=DE_TYPES),
    ]

if DE_CONTRASTS:
    de_fold_boxplots = expand(
        OPJ("figures", "{contrast}",
            "{contrast}_{small_type}_{fold_type}_{gene_list}_boxplots.pdf"),
        contrast=DE_CONTRASTS, small_type=DE_TYPES, fold_type=["log2FoldChange", *RPM_FOLD_TYPES],
        gene_list=["all_gene_lists"])
else:
    de_fold_boxplots = []
if IP_CONTRASTS:
    # 08/06/2023: expand by norm in both dir and file name
    all_contrasts_folds = []
    for norm in RPM_NORMS:
        all_contrasts_folds.extend([
            OPJ(mapping_dir, f"RPM_by_{norm}_folds_{size_selected}", "all", f"pimi{SI_MIN}G_mean_log2_RPM_by_{norm}_fold.txt"),
            OPJ(mapping_dir, f"RPM_by_{norm}_folds_{size_selected}", "all", f"pimi{SI_MIN}GtRF_mean_log2_RPM_by_{norm}_fold.txt"),
            # To have RPM (folds) for transgenes (which are not in prot_si category)
            OPJ(mapping_dir, f"RPM_by_{norm}_folds_{size_selected}", "all", f"all_si_{SI_MIN}G_mean_log2_RPM_by_{norm}_fold.txt")])
    ip_fold_boxplots_by_contrast = expand(
        OPJ("figures", "{contrast}",
            "{contrast}_{small_type}_{fold_type}_{gene_list}_boxplots.pdf"),
        contrast=IP_CONTRASTS, small_type=IP_TYPES, fold_type=RPM_FOLD_TYPES,
        gene_list=["all_gene_lists"])
else:
    all_contrasts_folds = []
    ip_fold_boxplots_by_contrast = []

fold_boxplots = [de_fold_boxplots, ip_fold_boxplots_by_contrast, ip_fold_boxplots]

localrules: all, link_raw_data

rule all:
    input:
        expand(
            OPJ(mapping_dir, "{lib}_{rep}", "{read_type}_on_%s_coverage.txt" % genome),
            filtered_product, lib=LIBS, rep=REPS, read_type=[size_selected]),
        bigwig_files,
        meta_profiles,
        # snakemake -n OK
        # expand(
        #     expand(
        #         OPJ(feature_counts_dir, "summaries",
        #             "{{lib}}_{{rep}}_{read_type}_on_%s_{orientation}_transcript_counts.txt" % (genome)),
        #         read_type=READ_TYPES_FOR_MAPPING, orientation=ORIENTATIONS),
        #     filtered_product, lib=LIBS, rep=REPS),
        # TODO
        # snakemake -n OK
        # expand(
        #     expand(
        #         OPJ(feature_counts_dir,
        #             "{{lib}}_{{rep}}_{small_type}_counts.txt"),
        #         small_type=REMAPPED_COUNTED),
        #     filtered_product, lib=LIBS, rep=REPS),
        # snakemake -n not OK: summaries contains all biotypes
        # expand(
        #     expand(
        #         OPJ(feature_counts_dir, "summaries",
        #             "{{lib}}_{{rep}}_{small_type}_counts.txt"),
        #         small_type=REMAPPED_COUNTED),
        #     filtered_product, lib=LIBS, rep=REPS),
        # REMAPPED_COUNTED = [f"{small_type}_on_{genome}_{biotype}_{orientation}_transcript" for (small_type, biotype, orientation) in product(READ_TYPES_FOR_MAPPING, set(COUNT_BIOTYPES + ANNOT_BIOTYPES), ORIENTATIONS)]
        expand(
            expand(
                OPJ(feature_counts_dir, "summaries",
                    "{{lib}}_{{rep}}_%s_and_nomap_siRNA_on_%s_{orientation}_transcript_counts.txt" % (size_selected, genome)),
                orientation=ORIENTATIONS),
            filtered_product, lib=LIBS, rep=REPS),
        expand(
            expand(
                OPJ(feature_counts_dir, "summaries",
                    "{{lib}}_{{rep}}_prot_si_%sGRNA_and_prot_siu_%sGRNA_on_%s_{orientation}_transcript_counts.txt" % (SI_MIN, SI_MIN, genome)),
                orientation=ORIENTATIONS),
            filtered_product, lib=LIBS, rep=REPS),
        # Try to get counts on individual exons
        # Use biotype "genes" because genes.gtf has "exon" feature type
        expand(
            expand(OPJ(feature_counts_dir,
                "{{lib}}_{{rep}}_prot_si_%sGRNA_and_prot_siu_%sGRNA_on_%s_genes_{orientation}_exon_counts.txt" % (SI_MIN, SI_MIN, genome)),
                orientation=ORIENTATIONS),
            filtered_product, lib=LIBS, rep=REPS),
        read_graphs,
        #expand(OPJ(feature_counts_dir, "summaries", "{lib}_{rep}_nb_non_structural.txt"), filtered_product, lib=LIBS, rep=REPS),
        #OPJ(mapping_dir, f"RPM_folds_{size_selected}", "all", "pisimi_mean_log2_RPM_fold.txt"),
        all_contrasts_folds,
        # Not looking ad deseq2 results any more
        #expand(OPJ(mapping_dir, f"deseq2_{size_selected}", "all", "pisimi_{fold_type}.txt"), fold_type=["log2FoldChange"]),
        #expand(OPJ(mapping_dir, "RPM_folds_%s" % size_selected, "{contrast}", "{contrast}_{small_type}_RPM_folds.txt"), contrast=IP_CONTRASTS, small_type=DE_TYPES),
        exploratory_graphs,
        remapped_folds,
        fold_boxplots,
        remapped_fold_boxplots,
        #expand(OPJ("figures", "{small_type}_unit_clustermap.pdf"), small_type=SMALL_TYPES),
        #expand(OPJ(
        #    feature_counts_dir,
        #    "all_{read_type}_{mapping_type}_{biotype}_{orientation}_transcript_counts.txt"), read_type=READ_TYPES_FOR_COUNTING, mapping_type=[f"on_{genome}"], biotype=ANNOT_BIOTYPES, orientation=ORIENTATIONS),
        # expand(OPJ(
        #     feature_counts_dir,
        #     "all_{small_type}_{mapping_type}_{biotype}_{orientation}_transcript_counts.txt"),
        #     small_type=READ_TYPES_FOR_MAPPING, mapping_type=[f"on_{genome}"], biotype=set(COUNT_BIOTYPES + ANNOT_BIOTYPES), orientation=ORIENTATIONS),
        # Note (14/02/2022): no pi here: Why ? -> Let's add pi while we're adding missing tRF
        #expand(
        #    OPJ(annot_counts_dir, f"all_{size_selected}_on_{genome}", "{small_type}_RPM.txt"),
        #    small_type=["pi", "mi", "tRF", *SI_TYPES, *SIU_TYPES, f"pimi{SI_MIN}G", f"pimi{SI_MIN}GtRF"]),
        expand(
            OPJ(annot_counts_dir, f"all_{size_selected}_on_{genome}", "{small_type}_RPM_by_{norm}.txt"),
            small_type=["pi", "mi", "tRF", *SI_TYPES, *SIU_TYPES, f"pimi{SI_MIN}G", f"pimi{SI_MIN}GtRF"],
            norm=RPM_NORMS),
        # piRNA and satel_siu raise ValueError: `dataset` input should have multiple elements when plotting
        # simrep_siu raise TypeError: Empty 'DataFrame': no numeric data to plot
        expand(
            OPJ("figures", "{small_type}_norm_correlations.pdf"),
            small_type=["mi", *SI_TYPES, *SIU_TYPES, f"pimi{SI_MIN}G", f"pimi{SI_MIN}GtRF"]),
        expand(
            OPJ("figures", "{small_type}_norm_counts_distrib.pdf"),
            small_type=["mi", *SI_TYPES, *SIU_TYPES, f"pimi{SI_MIN}G", f"pimi{SI_MIN}GtRF"]),

#absolute = "/pasteur/homes/bli/src/bioinfo_utils/snakemake_wrappers/includes/link_raw_data.rules"
#relative_include_path = "../snakemake_wrappers/includes/link_raw_data.snakefile"
#absolute_include_path = os.path.join(workflow.basedir, relative_include_path)
#assert os.path.exists(absolute_include_path)
#include: relative_include_path
include: ensure_relative(irules["link_raw_data"], workflow.basedir)

if int(trim3) + int(trim5) == 0 or config.get("nodedup", False):
    rule trim_and_dedup:
        input:
            rules.link_raw_data.output,
            #OPJ(data_dir, "{lib}_{rep}.fastq.gz"),
        params:
            adapter = lambda wildcards: lib2adapt[wildcards.lib],
            trim5 = trim5,
            trim3 = trim3,
        output:
            trimmed = OPJ(data_dir, "trimmed", "{lib}_{rep}_trimmed.fastq.gz"),
            nb_raw =  OPJ(data_dir, "trimmed", "{lib}_{rep}_nb_raw.txt"),
            nb_trimmed =  OPJ(data_dir, "trimmed", "{lib}_{rep}_nb_trimmed.txt"),
            nb_deduped =  OPJ(data_dir, "trimmed", "{lib}_{rep}_nb_deduped.txt"),
        threads: 2
        #resources:
        #    mem_mb=1049300
        message:
            "Trimming adaptor from raw data, removing random 5' {trim5}-mers and 3' {trim3}-mers for {wildcards.lib}_{wildcards.rep} (no deduplication)."
        benchmark:
            OPJ(log_dir, "trim_and_dedup", "{lib}_{rep}_benchmark.txt")
        log:
            cutadapt = OPJ(log_dir, "cutadapt", "{lib}_{rep}.log"),
            trim_and_dedup = OPJ(log_dir, "trim_and_dedup", "{lib}_{rep}.log"),
        shell:
            """
            zcat {input} \\
                | tee >(count_fastq_reads {output.nb_raw}) \\
                | cutadapt -a {params.adapter} --discard-untrimmed - 2> {log.cutadapt} \\
                | tee >(count_fastq_reads {output.nb_trimmed}) \\
                | trim_random_nt {params.trim5} {params.trim3}  2>> {log.cutadapt} \\
                | gzip > {output.trimmed} \\
                2> {log.trim_and_dedup}
            cp {output.nb_trimmed} {output.nb_deduped}
            """
else:
    # Extra step needed: deduplication before removal of UMIs
    rule trim_and_dedup:
        input:
            rules.link_raw_data.output,
            #OPJ(data_dir, "{lib}_{rep}.fastq.gz"),
        params:
            adapter = lambda wildcards: lib2adapt[wildcards.lib],
            trim5 = trim5,
            trim3 = trim3,
        output:
            trimmed = OPJ(data_dir, "trimmed", "{lib}_{rep}_trimmed.fastq.gz"),
            nb_raw =  OPJ(data_dir, "trimmed", "{lib}_{rep}_nb_raw.txt"),
            nb_trimmed =  OPJ(data_dir, "trimmed", "{lib}_{rep}_nb_trimmed.txt"),
            nb_deduped =  OPJ(data_dir, "trimmed", "{lib}_{rep}_nb_deduped.txt"),
        threads: 2
        #resources:
        #    mem_mb=1049300
        message:
            "Trimming adaptor from raw data, deduplicating reads, removing random 5' {trim5}-mers and 3' {trim3}-mers for {wildcards.lib}_{wildcards.rep}."
        benchmark:
            OPJ(log_dir, "trim_and_dedup", "{lib}_{rep}_benchmark.txt")
        log:
            cutadapt = OPJ(log_dir, "cutadapt", "{lib}_{rep}.log"),
            trim_and_dedup = OPJ(log_dir, "trim_and_dedup", "{lib}_{rep}.log"),
        shell:
            """
            zcat {input} \\
                | tee >(count_fastq_reads {output.nb_raw}) \\
                | cutadapt -a {params.adapter} --discard-untrimmed - 2> {log.cutadapt} \\
                | tee >(count_fastq_reads {output.nb_trimmed}) \\
                | dedup \\
                | tee >(count_fastq_reads {output.nb_deduped}) \\
                | trim_random_nt {params.trim5} {params.trim3}  2>> {log.cutadapt} \\
                | gzip > {output.trimmed} \\
                2> {log.trim_and_dedup}
            """


def awk_size_filter(wildcards):
    """Returns the bioawk filter to select reads of size from MIN_LEN to MAX_LEN."""
    return "%s <= length($seq) && length($seq) <= %s" % (MIN_LEN, MAX_LEN)


rule select_size_range:
    """Select (and count) reads in the correct size range."""
    input:
        rules.trim_and_dedup.output.trimmed
    output:
        selected = OPJ(data_dir, "trimmed", "{lib}_{rep}_%s.fastq.gz" % size_selected),
        nb_selected = OPJ(data_dir, "trimmed", "{lib}_{rep}_nb_%s.txt" % size_selected),
    params:
        awk_filter = awk_size_filter,
    message:
        "Selecting reads size %s for {wildcards.lib}_{wildcards.rep}." % size_selected
    shell:
        """
        bioawk -c fastx '{params.awk_filter} {{print "@"$name" "$4"\\n"$seq"\\n+\\n"$qual}}' {input} \\
            | tee >(count_fastq_reads {output.nb_selected}) \\
            | gzip > {output.selected}
        """


# TODO: update this
# TODO: add tRFs?
@wc_applied
def source_fastq(wildcards):
    """Determine the fastq file corresponding to a given read type."""
    read_type = wildcards.read_type
    if read_type == "raw":
        return rules.link_raw_data.output
    elif read_type == "trimmed":
        return rules.trim_and_dedup.output.trimmed
    elif read_type == size_selected:
        return rules.select_size_range.output.selected
    elif read_type == "nomap":
        return rules.map_on_genome.output.nomap_fastq
    elif read_type == "nomap_siRNA":
        return rules.extract_nomap_siRNAs.output.nomap_si
    # [:-3]: remove "RNA" from "{small_type}RNA"
    elif read_type[:-3] in annotate_read_output:
        return annotate_read_output[read_type[:-3]]
    elif read_type == f"si_{SI_MIN}GRNA":
        return OPJ(
            reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome),
            f"si_{SI_MIN}GRNA.fastq.gz"),
    elif read_type == f"si_{SI_MAX}GRNA":
        return OPJ(
            reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome),
            f"si_{SI_MAX}GRNA.fastq.gz"),
    elif read_type == f"siu_{SI_MIN}GRNA":
        return OPJ(
            reads_dir, "{lib}_{rep}_%s_on_%s" % ("nomap_siRNA", genome),
            f"siu_{SI_MIN}GRNA.fastq.gz"),
    elif read_type == f"siu_{SI_MAX}GRNA":
        return OPJ(
            reads_dir, "{lib}_{rep}_%s_on_%s" % ("nomap_siRNA", genome),
            f"siu_{SI_MAX}GRNA.fastq.gz"),
    elif read_type[:-3] in annotate_read_output_U:
        return annotate_read_output_U[read_type[:-3]]
    elif read_type == f"sisiu_{SI_MIN}GRNA":
        return OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), f"sisiu_{SI_MIN}GRNA.fastq.gz"),
    elif read_type == f"sisiu_{SI_MAX}GRNA":
        return OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), f"sisiu_{SI_MAX}GRNA.fastq.gz"),
    elif read_type == f"all_sisiu_{SI_MIN}GRNA":
        return OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), f"all_sisiu_{SI_MIN}GRNA.fastq.gz"),
    elif read_type == f"all_sisiu_{SI_MAX}GRNA":
        return OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), f"all_sisiu_{SI_MAX}GRNA.fastq.gz"),
    else:
        raise NotImplementedError("Unknown read type: %s" % read_type)




rule map_on_genome:
    input:
        #rules.select_size_range.output.selected,
        #fastq = source_fastq,
        fastq = rules.select_size_range.output.selected,
    output:
        sam = temp(OPJ(mapping_dir, "{lib}_{rep}", "%s_on_%s.sam" % (size_selected, genome))),
        nomap_fastq = OPJ(aligner, f"not_mapped_{genome}", "{lib}_{rep}_%s_unmapped_on_%s.fastq.gz" % (size_selected, genome)),
    params:
        aligner = aligner,
        index = genome_db,
        settings = alignment_settings[aligner],
    message:
        "Mapping {wildcards.lib}_{wildcards.rep}_%s on C. elegans genome." % size_selected
    benchmark:
        OPJ(log_dir, "map_on_genome", "{lib}_{rep}_%s_benchmark.txt" % size_selected)
    log:
        log = OPJ(log_dir, "map_on_genome", "{lib}_{rep}_%s.log" % size_selected),
        err = OPJ(log_dir, "map_on_genome", "{lib}_{rep}_%s.err" % size_selected)
    threads:
        8
    resources:
        mem_mb=700
    wrapper:
        f"file://{wrappers_dir}/map_on_genome"


rule extract_nomap_siRNAs:
    """Extract from the non-mappers those that end in poly-T, and could be mappable after T-tail trimming."""
    input:
        OPJ(aligner, f"not_mapped_{genome}", "{lib}_{rep}_%s_unmapped_on_%s.fastq.gz" % (size_selected, genome)), 
        #rules.map_on_genome.output.nomap_fastq,
    output:
        nomap_si = OPJ(aligner, f"not_mapped_{genome}", "{lib}_{rep}_%s_unmapped_siRNA.fastq.gz" % size_selected),
        nb_nomap_si = OPJ(aligner, f"not_mapped_{genome}", "{lib}_{rep}_%s_unmapped_siRNA.txt" % size_selected),
    message:
        "Extracting G.{{%d,%d}}T+ reads from unmapped {wildcards.lib}_{wildcards.rep}_%s." % (SI_MIN - 2, SI_MAX - 1, size_selected)
    shell:
        """
        # -E for extended regexps (allows {{21,25}} and +)
        # -B 1 for the @* fastq header
        # -A 2 for the + and qualities lines
        zcat {input} | grep -E -B 1 -A 2 "^G[ACGTN]{{%d,%d}}T+$" \\
            | grep -v "^--$" | trim-t-tail-from-fastq \\
            | tee >(count_fastq_reads {output.nb_nomap_si}) \\
            | gzip > {output.nomap_si}
        """ % (SI_MIN - 2, SI_MAX - 1)


rule remap_on_genome:
    """Remap small reads after first categorization following initial mapping."""
    input:
        #rules.select_size_range.output.selected,
        #fastq = wc_applied(source_fastq),
        fastq = source_fastq,
    output:
        sam = temp(OPJ(mapping_dir, "{lib}_{rep}", "{read_type}_on_%s.sam" % genome)),
        nomap_fastq = OPJ(aligner, f"not_mapped_{genome}", "{lib}_{rep}_{read_type}_unmapped_on_%s.fastq.gz" % genome),
    # Here we don't map size_selected
    wildcard_constraints:
        read_type = "|".join(set(READ_TYPES_FOR_MAPPING + READ_TYPES_FOR_COUNTING) - {size_selected})
    params:
        aligner = aligner,
        index = genome_db,
        settings = alignment_settings[aligner],
    message:
        "Mapping {wildcards.lib}_{wildcards.rep}_{wildcards.read_type} on C. elegans genome."
    benchmark:
        OPJ(log_dir, "remap_on_genome", "{lib}_{rep}_{read_type}_benchmark.txt")
    log:
        log = OPJ(log_dir, "remap_on_genome", "{lib}_{rep}_{read_type}.log"),
        err = OPJ(log_dir, "remap_on_genome", "{lib}_{rep}_{read_type}.err")
    threads:
        8
    resources:
        mem_mb=700
    wrapper:
        f"file://{wrappers_dir}/map_on_genome"


def source_sam(wildcards):
    if hasattr(wildcards, "read_type"):
        return OPJ(
            mapping_dir,
            f"{wildcards.lib}_{wildcards.rep}",
            f"{wildcards.read_type}_on_{genome}.sam")
    else:
        return OPJ(
            mapping_dir,
            f"{wildcards.lib}_{wildcards.rep}",
            f"{size_selected}_on_{genome}.sam")


rule sam2indexedbam:
    input:
        #check_wc,
        sam = source_sam,
    output:
        sorted_bam = OPJ(mapping_dir, "{lib}_{rep}", "{read_type}_on_%s_sorted.bam" % genome),
        index = OPJ(mapping_dir, "{lib}_{rep}", "{read_type}_on_%s_sorted.bam.bai" % genome),
    message:
        "Sorting and indexing sam file for {wildcards.lib}_{wildcards.rep}_{wildcards.read_type}."
    benchmark:
        OPJ(log_dir, "sam2indexedbam", "{lib}_{rep}_{read_type}_benchmark.txt"),
    log:
        log = OPJ(log_dir, "sam2indexedbam", "{lib}_{rep}_{read_type}.log"),
        err = OPJ(log_dir, "sam2indexedbam", "{lib}_{rep}_{read_type}.err"),
    threads:
        8
    resources:
        mem_mb=4100
    wrapper:
        f"file://{wrappers_dir}/sam2indexedbam"


rule compute_coverage:
    input:
        sorted_bam = rules.sam2indexedbam.output.sorted_bam,
    output:
        coverage = OPJ(mapping_dir, "{lib}_{rep}", "{read_type}_on_%s_coverage.txt" % genome),
    params:
        genomelen = genomelen,
    shell:
        """
        bases=$(samtools depth {input.sorted_bam} | awk '{{sum += $3}} END {{print sum}}') || error_exit "samtools depth failed"
        python3 -c "print(${{bases}} / {params.genomelen})" > {output.coverage}
        """


def feature_orientation2stranded(wildcards):
    orientation = wildcards.orientation
    if orientation == "fwd":
        return 1
    elif orientation == "rev":
        return 2
    elif orientation == "all":
        return 0
    else:
        exit("Orientation is to be among \"fwd\", \"rev\" and \"all\".")


def source_bams(wildcards):
    """We can count from several bam files with feature_count."""
    read_types = wildcards.read_type.split("_and_")
    sorted_bams = [OPJ(
        mapping_dir, f"{wildcards.lib}_{wildcards.rep}",
        f"{read_type}_on_{genome}_sorted.bam") for read_type in read_types]
    return sorted_bams


rule feature_count_reads:
    input:
        source_bams,
    output:
        counts = OPJ(
            feature_counts_dir,
            "{lib}_{rep}_{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}_counts.txt"),
    params:
        min_mapq = partial(aligner2min_mapq, aligner),
        stranded = feature_orientation2stranded,
        annot = lambda wildcards: OPJ(annot_dir, f"{wildcards.biotype}.gtf"),
        gene_id_name = genome_dict.get("gene_id_name", "gene_id"),
        tmpdir_prefix = lambda wildcards: f"feature_{wildcards.lib}_{wildcards.rep}_{wildcards.read_type}_{wildcards.mapping_type}_{wildcards.biotype}_{wildcards.orientation}_{wildcards.feature_type}.XXXXXXXXXX",
    threads: 8
    resources:
        mem_mb=138
    message:
        "Counting {wildcards.orientation} {wildcards.biotype} {wildcards.feature_type} reads for {wildcards.lib}_{wildcards.rep}_{wildcards.read_type}_{wildcards.mapping_type} with featureCounts."
    benchmark:
        OPJ(log_dir, "feature_count_reads", "{lib}_{rep}_{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}_benchmark.txt"),
    log:
        log = OPJ(log_dir, "feature_count_reads", "{lib}_{rep}_{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}.log"),
        err = OPJ(log_dir, "feature_count_reads", "{lib}_{rep}_{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}.err"),
    wrapper:
        f"file://{wrappers_dir}/feature_count_reads"


rule summarize_feature_counts:
    """For a given library, compute the total counts for each biotype and write this in a summary table."""
    input:
        expand(
            OPJ(feature_counts_dir,
                "{{lib}}_{{rep}}_{{read_type}}_{{mapping_type}}_{biotype}_{{orientation}}_{{feature_type}}_counts.txt"),
            biotype=[biotype for biotype in COUNT_BIOTYPES if not biotype.endswith("_rmsk_families")]),
    output:
        summary = OPJ(mapping_dir, "feature_count", "summaries",
            "{lib}_{rep}_{read_type}_{mapping_type}_{orientation}_{feature_type}_counts.txt")
    run:
        with open(output.summary, "w") as summary_file:
            header = "\t".join(COUNT_BIOTYPES)
            summary_file.write("#biotypes\t%s\n" % header)
            sums = "\t".join((str(sum_feature_counts(counts_file, nb_bams=len(wildcards.read_type.split("_and_")))) for counts_file in input))
            summary_file.write(f"%s_%s_%s\t%s\n" % (wildcards.lib, wildcards.rep, wildcards.orientation, sums))


def source_feature_counts_to_gather(wildcards):
    if wildcards.biotype.endswith("_rmsk_families"):
        return [OPJ(feature_counts_dir, f"{cond_name}_{wildcards.read_type}_{wildcards.mapping_type}_{wildcards.biotype[:-9]}_{wildcards.orientation}_{wildcards.feature_type}_counts.txt") for cond_name in COND_NAMES]
    else:
        return [OPJ(feature_counts_dir, f"{cond_name}_{wildcards.read_type}_{wildcards.mapping_type}_{wildcards.biotype}_{wildcards.orientation}_{wildcards.feature_type}_counts.txt") for cond_name in COND_NAMES]


# TODO: update consumers with mapping_type
rule gather_feature_counts:
    """For a given biotype and read type, gather counts from all libraries in one table."""
    input:
        counts_tables = source_feature_counts_to_gather,
        # counts_tables = expand(
        #     OPJ(feature_counts_dir,
        #         "{cond_name}_{{read_type}}_{{mapping_type}}_{{biotype}}_{{orientation}}_{{feature_type}}_counts.txt"),
        #     cond_name=COND_NAMES),
    output:
        counts_table = OPJ(
            feature_counts_dir,
            "all_{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}_counts.txt"),
    wildcard_constraints:
        biotype = "|".join(COUNT_BIOTYPES + BOXPLOT_BIOTYPES + ["genes"])
    run:
        # Gathering the counts data
        ############################
        #counts_files = (OPJ(
        #    feature_counts_dir,
        #    f"{cond_name}_{wildcards.read_type}_{wildcards.mapping_type}_{wildcards.biotype}_{wildcards.orientation}_{wildcards.feature_type}_counts.txt") for cond_name in COND_NAMES)
        counts_data = pd.concat(
            (read_feature_counts(
                counts_table,
                nb_bams=len(wildcards.read_type.split("_and_"))).sum(axis=1) for counts_table in input.counts_tables),
            axis=1).fillna(0).astype(int)
        ## TODO
        ## debug
        try:
            counts_data.columns = COND_NAMES
        except ValueError as e:
            print(counts_data.columns)
            print(COND_NAMES)
            raise
        ##
        # Simple_repeat|Simple_repeat|(TTTTTTG)n:1
        # Simple_repeat|Simple_repeat|(TTTTTTG)n:2
        # Simple_repeat|Simple_repeat|(TTTTTTG)n:3
        # Simple_repeat|Simple_repeat|(TTTTTTG)n:4
        # -> Simple_repeat|Simple_repeat|(TTTTTTG)n
        if wildcards.biotype.endswith("_rmsk_families"):
            counts_data = sum_by_family(counts_data)
        counts_data.index.names = ["gene"]
        counts_data.to_csv(output.counts_table, sep="\t")


@wc_applied
def source_feature_counts_to_join(wildcards):
    """
    Determines which elementary biotype counts files should be joined to make the desired "joined" biotype.
    """
    ## debug
    #print("dest biotype:", wildcards.biotype)
    #print("source biotypes:", ",".join(BIOTYPES_TO_JOIN[wildcards.biotype]))
    ##
    #if BIOTYPES_TO_JOIN[wildcards.biotype]:
    return expand(
        OPJ(feature_counts_dir,
            "all_{{read_type}}_{{mapping_type}}_{biotype}_{{orientation}}_{{feature_type}}_counts.txt"),
        biotype=BIOTYPES_TO_JOIN[wildcards.biotype])
    #return []


rule join_all_feature_counts:
    """Concat counts for all biotypes into all"""
    input:
        counts_tables = source_feature_counts_to_join,
    output:
        counts_table = OPJ(
            feature_counts_dir,
            "all_{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}_counts.txt"),
    wildcard_constraints:
        biotype = "|".join(JOINED_BIOTYPES + ["genes"])
    run:
        ## debug:
        print("Input:", ", ".join(input))
        ##
        counts_data = pd.concat((pd.read_table(table, index_col="gene") for table in input.counts_tables))
        dups = counts_data.index.duplicated(keep='first')
        non_unique = counts_data.index[dups]
        assert len(counts_data.index.unique()) == len(counts_data.index), f"Some genes appear several times in the counts table:\n{non_unique}."
        counts_data.index.names = ["gene"]
        counts_data.to_csv(output.counts_table, sep="\t")


rule gather_annotations:
    input:
        expand(OPJ(annot_dir, "{biotype}.gtf"), biotype=ANNOT_BIOTYPES),
    output:
        merged_gtf = OPJ(local_annot_dir, "all_annotations.gtf"),
        #merged_gtf_gz = OPJ(local_annot_dir, "all_annotations.gtf.gz"),
        #index = OPJ(local_annot_dir, "all_annotations.gtf.gz.tbi"),
        #merged_bed = OPJ(local_annot_dir, "all_annotations.bed"),
    message:
        "Gathering annotations for {}".format(", ".join(ANNOT_BIOTYPES))
    shell:
        """
        # -m is for merging
        LC_ALL=C sort -k1,1 -k4,4n -m {input} > {output.merged_gtf}
        """
    # 31/01/2019: For some reason, tabix complains
    # [E::hts_idx_push] unsorted positions
    # tbx_index_build failed: results_csr1_ADH_WAGO1_IP/annotations/all_annotations.gtf.gz
    # The .gz and .gz.tbi are no longer used by the annotation script, anyway.
    # shell:
    #     """
    #     # -m is for merging
    #     LC_ALL=C sort -k1,1 -k4,4n -m {input} | tee {output.merged_gtf} | bgzip > {output.merged_gtf_gz}
    #     tabix -p gff {output.merged_gtf_gz}
    #     """
    #     #ensembl_gtf2bed.py {output.merged_gtf} > {output.merged_bed}


#mapping without mismatch

# use (normalized by nb_trimmed?) counts in composition graphs instead of %
# identify piRNAs, miRNAs based on the annotations (sense)
# identify 22 among the rest (we expect antisense to protein-coding genes (and TEs), with G at their first position
# on the unmapped reads (and also in the mapped), look for 22(U+), trim the U and re-map (CSR-1 pathway)
# Quantify normalized 22 by target length, compare between libraries
# Repeat composition analyses for separate small RNA types
# Normalize by miRNA and by mappers
# piechart for small RNA types / histograms of normalized counts (piecharts are wrong, used bars instead)

# endo-si are everyting that did not map as piRNA or miRNA
# special case among anti-sense to coding: 22G
# other special case among anti-sense to coding: 22G(U+)
#see https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2776052/figure/F2/
# Or use same categories as for RNA-seq annotation
# TODO:
# Plot correlation between read counts (miRNA, piRNA...) in libraries, with correlation coefficient
# For gene-mapping: normalize by gene length and sort by this RPKM-like measure
# Find protein coding genes with increased or reduced antisense-mapping 22G (DESeq or similar)

#Gu et al 2009, supplement:
#
# To compare 22G-RNAs derived from a gene, transposon, or pseudogene between two
# samples, each sample was normalized using the total number of reads less structural RNAs, i.e.
# sense small RNA reads likely derived from degraded ncRNAs, tRNAs, snoRNAs, rRNAs,
# snRNAs, and scRNAs. Degradation products of structural RNAs map to the sense strand, with a
# poorly defined size profile and 1 st nucleotide distribution. At least 25 22G-RNA reads per million,
# nonstructural reads in one of the two samples was arbitrarily chosen as a cutoff for comparison
# analyses. A change of 2-fold or more between samples was chosen as an enrichment threshold.
# Because some 21U-RNAs or miRNAs overlap with protein coding genes, reads derived from
# miRNA loci within a window of ± 4 nt and all the known 21U-RNAs were filtered out prior to
# comparison analysis.


# rule small_RNA_seq_annotate:
#     """Identify in details the type and source of the small RNAs. Generate
#     fastq files and count files. To separate processing are done, the second being
#     for T-tail trimmed potential endo siRNAs."""
#     input:
#         sorted_bam = OPJ(mapping_dir, "{lib}_{rep}", f"{size_selected}_on_{genome}_sorted.bam"),
#         index = OPJ(mapping_dir, "{lib}_{rep}", f"{size_selected}_on_{genome}_sorted.bam.bai"),
#         remapped_sorted_bam = OPJ(mapping_dir, "{lib}_{rep}", f"nomap_siRNA_on_{genome}_sorted.bam"),
#         remapped_index = OPJ(mapping_dir, "{lib}_{rep}", f"nomap_siRNA_on_{genome}_sorted.bam.bai"),
#         merged_gtf = rules.gather_annotations.output.merged_gtf,
#     output:
#         # for normalization
#         nb_mapped = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "nb_mapped.txt"),
#         nb_mappedU = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "nb_mappedU.txt"),
#         # reads
#         ########
#         prot_si = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "prot_siRNA.fastq.gz"),
#         te_si = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "te_siRNA.fastq.gz"),
#         pseu_si = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "pseu_siRNA.fastq.gz"),
#         satel_si = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "satel_siRNA.fastq.gz"),
#         simrep_si = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "simrep_siRNA.fastq.gz"),
#         pi = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "piRNA.fastq.gz"),
#         mi = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "miRNA.fastq.gz"),
#         all_si = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "all_siRNA.fastq.gz"),
#         prot_siu = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "prot_siuRNA.fastq.gz"),
#         te_siu = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "te_siuRNA.fastq.gz"),
#         pseu_siu = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "pseu_siuRNA.fastq.gz"),
#         satel_siu = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "satel_siuRNA.fastq.gz"),
#         simrep_siu = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "simrep_siuRNA.fastq.gz"),
#         all_siu = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "all_siuRNA.fastq.gz"),
#         # stats
#         ########
#         all_annots = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "annot_stats.txt"),
#         ambig_type = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "ambig_type.txt"),
#         ambig_typeU = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "ambig_typeU.txt"),
#         ambig_id = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "ambig_id.txt"),
#         # counts
#         #########
#         prot_si_counts = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "prot_si_counts.txt"),
#         te_si_counts = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "te_si_counts.txt"),
#         pseu_si_counts = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "pseu_si_counts.txt"),
#         satel_si_counts = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "satel_si_counts.txt"),
#         simrep_si_counts = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "simrep_si_counts.txt"),
#         pi_counts = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "pi_counts.txt"),
#         mi_counts = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "mi_counts.txt"),
#         all_si_counts = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "all_si_counts.txt"),
#         prot_siu_counts = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "prot_siu_counts.txt"),
#         te_siu_counts = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "te_siu_counts.txt"),
#         pseu_siu_counts = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "pseu_siu_counts.txt"),
#         satel_siu_counts = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "satel_siu_counts.txt"),
#         simrep_siu_counts = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "simrep_siu_counts.txt"),
#         all_siu_counts = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "all_siu_counts.txt"),
#     params:
#         reads_out_dir = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome)),
#         readsU_out_dir = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome)),
#         counts_out_dir = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome)),
#         countsU_out_dir = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome)),
#     message:
#         "Annotating small RNA reads for {wildcards.lib}_{wildcards.rep}_%s." % size_selected
#     benchmark:
#         OPJ(log_dir, "small_RNA_seq_annotate", "{lib}_{rep}_%s_benchmark.txt" % size_selected)
#     log:
#         mapped_log = OPJ(log_dir, "small_RNA_seq_annotate", "{lib}_{rep}_%s.log" % size_selected),
#         nomap_siRNA_log = OPJ(log_dir, "small_RNA_seq_annotate", "{lib}_{rep}_nomap_siRNA.log")
#     threads: 12
#     shell:
#         """
#         small_RNA_seq_annotate.py -p {threads} -b {input.sorted_bam} -g {input.merged_gtf} \\
#             -r {params.reads_out_dir} -o {params.counts_out_dir} -l {log.mapped_log}
#         small_RNA_seq_annotate.py -p {threads} -b {input.remapped_sorted_bam} -u -g {input.merged_gtf} \\
#             -r {params.readsU_out_dir} -o {params.countsU_out_dir} -l {log.nomap_siRNA_log}
#         """


# TODO: This should be updated in order to match the actual output of small_RNA_seq_annotate.py
annotate_read_output = {
    small_type: OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), f"{small_type}RNA.fastq.gz")
    for small_type in ANNOT_COUNTS_TYPES}
annotate_count_output = {
    small_type: OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), f"{small_type}_counts.txt")
    for small_type in ANNOT_COUNTS_TYPES}


# TODO: We could probably hard-code the values of mapped_type in the two annotate rules.
rule small_RNA_seq_annotate:
    """Identify in details the type and source of the small RNAs.
    Generate fastq files and count files."""
    input:
        sorted_bam = OPJ(mapping_dir, "{lib}_{rep}", f"{size_selected}_on_{genome}_sorted.bam"),
        index = OPJ(mapping_dir, "{lib}_{rep}", f"{size_selected}_on_{genome}_sorted.bam.bai"),
        merged_gtf = rules.gather_annotations.output.merged_gtf,
    output:
        # for normalization
        nb_mapped = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "nb_mapped.txt"),
        # reads
        ########
        read_files = list(annotate_read_output.values()),
        # stats
        ########
        all_annots = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "annot_stats.txt"),
        ambig_type = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "ambig_type.txt"),
        ambig_id = OPJ(annot_counts_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "ambig_id.txt"),
        # counts
        #########
        count_files = list(annotate_count_output.values()),
    # wildcard_constraints:
    #     mapped_type=f"{size_selected}"
    params:
        reads_out_dir = lambda wildcards: OPJ(reads_dir, f"{wildcards.lib}_{wildcards.rep}_{size_selected}_on_{genome}"),
        counts_out_dir = lambda wildcards: OPJ(annot_counts_dir, f"{wildcards.lib}_{wildcards.rep}_{size_selected}_on_{genome}"),
    message:
        "Annotating small RNA reads for {wildcards.lib}_{wildcards.rep}_{size_selected}."
    benchmark:
        OPJ(log_dir, "small_RNA_seq_annotate", "{lib}_{rep}_%s_benchmark.txt" % size_selected)
    log:
        mapped_log = OPJ(log_dir, "small_RNA_seq_annotate", "{lib}_{rep}_%s.log" % size_selected),
    threads: 12
    resources:
        mem_mb=19150
    shell:
        """
        niceload --noswap small_RNA_seq_annotate.py -p {threads} -b {input.sorted_bam} -g {input.merged_gtf} \\
            -r {params.reads_out_dir} -o {params.counts_out_dir} -l {log.mapped_log}
        """


# Here we ignore the theoretical existence of miu and piu
annotate_read_output_U = {
    small_type: OPJ(reads_dir, "{lib}_{rep}_nomap_siRNA_on_%s" % genome, f"{small_type}RNA.fastq.gz")
    for small_type in ANNOT_COUNTS_TYPES_U}
annotate_count_output_U = {
    small_type: OPJ(annot_counts_dir, "{lib}_{rep}_nomap_siRNA_on_%s" % genome, f"{small_type}_counts.txt")
    for small_type in ANNOT_COUNTS_TYPES_U}


rule small_RNA_seq_annotate_U:
    """Identify in details the type and source of the small RNAs.
    Generate fastq files and count files."""
    input:
        sorted_bam = OPJ(mapping_dir, "{lib}_{rep}", "nomap_siRNA_on_%s_sorted.bam" % genome),
        index = OPJ(mapping_dir, "{lib}_{rep}", "nomap_siRNA_on_%s_sorted.bam.bai" % genome),
        merged_gtf = rules.gather_annotations.output.merged_gtf,
    output:
        # for normalization
        nb_mapped = OPJ(reads_dir, "{lib}_{rep}_nomap_siRNA_on_%s" % genome, "nb_mappedU.txt"),
        # reads
        ########
        read_files = list(annotate_read_output_U.values()),
        # stats
        ########
        all_annots = OPJ(annot_counts_dir, "{lib}_{rep}_nomap_siRNA_on_%s" % genome, "annot_statsU.txt"),
        ambig_type = OPJ(annot_counts_dir, "{lib}_{rep}_nomap_siRNA_on_%s" % genome, "ambig_typeU.txt"),
        ambig_id = OPJ(annot_counts_dir, "{lib}_{rep}_nomap_siRNA_on_%s" % genome, "ambig_idU.txt"),
        # counts
        #########
        count_files = list(annotate_count_output_U.values()),
    # wildcard_constraints:
    #     mapped_type=f"nomap_siRNA"
    params:
        reads_out_dir = lambda wildcards: OPJ(reads_dir, f"{wildcards.lib}_{wildcards.rep}_nomap_siRNA_on_{genome}"),
        counts_out_dir = lambda wildcards: OPJ(annot_counts_dir, f"{wildcards.lib}_{wildcards.rep}_nomap_siRNA_on_{genome}"),
    message:
        "Annotating small RNA reads for {wildcards.lib}_{wildcards.rep}_nomap_siRNA."
    benchmark:
        OPJ(log_dir, "small_RNA_seq_annotate_U", "{lib}_{rep}_nomap_siRNA_benchmark.txt")
    log:
        mapped_log = OPJ(log_dir, "small_RNA_seq_annotate_U", "{lib}_{rep}_nomap_siRNA.log"),
    threads: 12
    resources:
        mem_mb=14700
    shell:
        """
        niceload --noswap small_RNA_seq_annotate.py -p {threads} -b {input.sorted_bam} -u -g {input.merged_gtf} \\
            -r {params.reads_out_dir} -o {params.counts_out_dir} -l {log.mapped_log}
        """


rule count_non_structural_mappers:
    input:
        nb_mapped = rules.small_RNA_seq_annotate.output.nb_mapped,
        nb_mappedU = rules.small_RNA_seq_annotate_U.output.nb_mapped,
        summary = OPJ(
            feature_counts_dir, "summaries",
            "{lib}_{rep}_%s_and_nomap_siRNA_on_%s_fwd_transcript_counts.txt" % (size_selected, genome)),
    output:
        nb_non_structural = OPJ(
            feature_counts_dir, "summaries",
            "{lib}_{rep}_nb_non_structural.txt"),
    run:
        nb_mapped = read_int_from_file(input.nb_mapped)
        nb_mappedU = read_int_from_file(input.nb_mappedU)
        structural = pd.read_table(input.summary, index_col=0).loc[:, STRUCTURAL_BIOTYPES].sum(axis=1)[0]
        with open(output.nb_non_structural, "w") as out_file:
            out_file.write("%d\n" % ((nb_mapped + nb_mappedU) - structural))


rule plot_read_numbers:
    input:
        nb_raw = rules.trim_and_dedup.output.nb_raw,
        nb_deduped = rules.trim_and_dedup.output.nb_deduped,
        nb_trimmed = rules.trim_and_dedup.output.nb_trimmed,
        nb_selected = rules.select_size_range.output.nb_selected,
        nb_mapped = rules.small_RNA_seq_annotate.output.nb_mapped,
        nb_mappedU = rules.small_RNA_seq_annotate_U.output.nb_mapped,
    output:
        plot = OPJ("figures", "{lib}_{rep}", "nb_reads.pdf"),
    message:
        "Plotting read numbers for {wildcards.lib}_{wildcards.rep}."
    # Currently not used
    #log:
    #    log = OPJ(log_dir, "plot_read_numbers", "{lib}_{rep}.log"),
    #    err = OPJ(log_dir, "plot_read_numbers", "{lib}_{rep}.err")
    benchmark:
        OPJ(log_dir, "plot_read_numbers", "{lib}_{rep}_benchmark.txt"),
    resources:
        mem_mb=2400
    run:
        name = f"{wildcards.lib}_{wildcards.rep}"
        summary = pd.Series({
            "nb_raw" : read_int_from_file(input.nb_raw),
            "nb_deduped" : read_int_from_file(input.nb_deduped),
            "nb_trimmed" : read_int_from_file(input.nb_trimmed),
            "nb_selected" : read_int_from_file(input.nb_selected),
            "nb_mapped" : read_int_from_file(input.nb_mapped),
            "nb_mappedU" : read_int_from_file(input.nb_mappedU)})
        # Chose column order:
        summary = summary[[
            "nb_raw", "nb_deduped", "nb_trimmed", "nb_selected",
            "nb_mapped", "nb_mappedU"]]
        save_plot(output.plot, plot_barchart, summary, title="%s reads" % name)


# rule join_si_reads:
#     input:
#         prot_si = rules.small_RNA_seq_annotate.output.prot_si,
#         prot_siu = rules.small_RNA_seq_annotate.output.prot_siu,
#         te_si = rules.small_RNA_seq_annotate.output.prot_si,
#         te_siu = rules.small_RNA_seq_annotate.output.prot_siu,
#         pseu_si = rules.small_RNA_seq_annotate.output.pseu_si,
#         pseu_siu = rules.small_RNA_seq_annotate.output.pseu_siu,
#         satel_si = rules.small_RNA_seq_annotate.output.satel_si,
#         satel_siu = rules.small_RNA_seq_annotate.output.satel_siu,
#         simrep_si = rules.small_RNA_seq_annotate.output.simrep_si,
#         simrep_siu = rules.small_RNA_seq_annotate.output.simrep_siu,
#         all_si = rules.small_RNA_seq_annotate.output.all_si,
#         all_siu = rules.small_RNA_seq_annotate.output.all_siu,
#     output:
#         si = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "siRNA.fastq.gz"),
#         siu = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "siuRNA.fastq.gz"),
#         sisiu = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "sisiuRNA.fastq.gz"),
#         prot_sisiu = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "prot_sisiuRNA.fastq.gz"),
#         te_sisiu = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "te_sisiuRNA.fastq.gz"),
#         pseu_sisiu = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "pseu_sisiuRNA.fastq.gz"),
#         satel_sisiu = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "satel_sisiuRNA.fastq.gz"),
#         simrep_sisiu = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "simrep_sisiuRNA.fastq.gz"),
#         all_sisiu = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "all_sisiuRNA.fastq.gz"),
#     shell:
#         """
#         cat {input.prot_si} {input.te_si} {input.pseu_si} {input.satel_si} {input.simrep_si} > {output.si}
#         cat {input.prot_siu} {input.te_siu} {input.pseu_siu} {input.satel_siu} {input.simrep_siu} > {output.siu}
#         cat {output.si} {output.siu} > {output.sisiu}
#         cat {input.prot_si} {input.prot_siu} > {output.prot_sisiu}
#         cat {input.te_si} {input.te_siu} > {output.te_sisiu}
#         cat {input.pseu_si} {input.pseu_siu} > {output.pseu_sisiu}
#         cat {input.satel_si} {input.satel_siu} > {output.satel_sisiu}
#         cat {input.simrep_si} {input.simrep_siu} > {output.simrep_sisiu}
#         cat {input.all_si} {input.all_siu} > {output.all_sisiu}
#         """


def read_types_to_join(wildcards):
    # if wildcards.mapped_type == size_selected:
    #     assert wildcards.infix == "si"
    #     small_types = list(types_under(["si", f"{wildcards.suffix}"]))
    # elif wildcards.mapped_type == "nomap_siRNA":
    #     assert wildcards.infix == "siu"
    #     small_types = list(types_under(["siu", f"{wildcards.suffix}"]))
    # else:
    #     raise NotImplementedError("Unknown read type: %s" % wildcards.mapped_type)
    if wildcards.infix == "si":
        assert wildcards.mapped_type == size_selected
    elif wildcards.infix == "siu":
        assert wildcards.mapped_type == "nomap_siRNA"
    else:
        raise NotImplementedError(f"Unknown small read type: {wildcards.infix}_{wildcards.suffix}")
    small_types = list(types_under([f"{wildcards.infix}", f"{wildcards.suffix}"]))
    return [
        OPJ(
            reads_dir, f"{wildcards.lib}_{wildcards.rep}_{wildcards.mapped_type}_on_%s" % genome,
            f"{small_type}RNA.fastq.gz")
        for small_type in small_types]


rule join_si_reads:
    input:
        read_types_to_join,
    output:
        si = OPJ(reads_dir, "{lib}_{rep}_{mapped_type}_on_%s" % genome, "{infix}_{suffix}RNA.fastq.gz"),
    shell:
        """
        cat {input} > {output.si}
        """


# sisiu come from size_selected and nomap_siRNA
# they are stored in size_selected
rule join_all_sisiu_reads:
    input:
        all_si = annotate_read_output["all_si"],
        all_siu = annotate_read_output_U["all_siu"],
    output:
        all_sisiu = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "all_sisiuRNA.fastq.gz"),
    shell:
        """
        cat {input.all_si} {input.all_siu} > {output.all_sisiu}
        """


# sisiu come from size_selected and nomap_siRNA
# they are stored in size_selected
rule join_sisiu_reads:
    input:
        si = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "si_{suffix}RNA.fastq.gz"),
        siu = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % ("nomap_siRNA", genome), "siu_{suffix}RNA.fastq.gz"),
    output:
        sisiu = OPJ(reads_dir, "{lib}_{rep}_%s_on_%s" % (size_selected, genome), "sisiu_{suffix}RNA.fastq.gz"),
    shell:
        """
        cat {input.si} {input.siu} > {output.sisiu}
        """


# import re
# from statistics import mean
# def log2data(line):
#     return list(map(
#         int,
#         re.sub(
#             r"^INFO:root:(\d+) alignments processed \((\d+) alignments / s\)",
#             r"\1\t\2", line).strip().split()))
#
# with open(logfile) as f:
#     speeds = list(map(log2data, f))
#     spds = list(zip(*speeds))[1]
#     df = pd.pd.DataFrame(speeds).set_index(0)


rule gather_small_RNA_counts:
    """For a given small_type, gather counts from all libraries in one table."""
    input:
        counts_tables = expand(OPJ(
            annot_counts_dir,
            "{name}_{{mapped_type}}_on_%s" % genome,
            "{{small_type}}_counts.txt"), name=COND_NAMES),
    output:
        counts_table = OPJ(
            annot_counts_dir,
            "all_{mapped_type}_on_%s" % genome, "{small_type}_counts.txt"),
    log:
        log = OPJ(log_dir, "gather_small_RNA_counts", "{mapped_type}_{small_type}.log"),
    run:
        with open(log.log, "w") as logfile:
            # Gathering the counts data
            ############################
            counts_files = (OPJ(
                annot_counts_dir,
                f"{cond_name}_{wildcards.mapped_type}_on_{genome}",
                f"{wildcards.small_type}_counts.txt") for cond_name in COND_NAMES)
            if wildcards.small_type == "pi":
                drop = ["piRNA"]
            elif wildcards.small_type == "mi":
                drop = ["miRNA"]
            elif wildcards.small_type == "tRF":
                drop = ["tRFRNA"]
            elif wildcards.small_type in {
                    *{f"prot_si_{suffix}" for suffix in SI_SUFFIXES},
                    *{f"prot_siu_{suffix}" for suffix in SI_SUFFIXES}}:
                drop = ["protein_coding_CDS", "protein_coding_UTR", "protein_coding_exon_junction", "protein_coding_pure_intron"]
            elif wildcards.small_type in {
                    *{f"te_si_{suffix}" for suffix in SI_SUFFIXES},
                    *{f"te_siu_{suffix}" for suffix in SI_SUFFIXES}}:
                drop = ["DNA_transposons_rmsk", "RNA_transposons_rmsk"]
            elif wildcards.small_type in {
                    *{f"pseu_si_{suffix}" for suffix in SI_SUFFIXES},
                    *{f"pseu_siu_{suffix}" for suffix in SI_SUFFIXES}}:
                drop = ["pseudogene"]
            elif wildcards.small_type in {
                    *{f"satel_si_{suffix}" for suffix in SI_SUFFIXES},
                    *{f"satel_siu_{suffix}" for suffix in SI_SUFFIXES}}:
                drop = ["satellites_rmsk"]
            elif wildcards.small_type in {
                    *{f"simrep_si_{suffix}" for suffix in SI_SUFFIXES},
                    *{f"simrep_siu_{suffix}" for suffix in SI_SUFFIXES}}:
                drop = ["simple_repeats_rmsk"]
            elif wildcards.small_type in {
                    *{f"ri_si_{suffix}" for suffix in SI_SUFFIXES},
                    *{f"ri_siu_{suffix}" for suffix in SI_SUFFIXES}}:
                drop = ["rRNA"]
            elif wildcards.small_type in {
                    *{f"all_si_{suffix}" for suffix in SI_SUFFIXES},
                    *{f"all_siu_{suffix}" for suffix in SI_SUFFIXES}}:
                drop = [f"all_si_{suffix}RNA" for suffix in SI_SUFFIXES]
            elif wildcards.small_type in {"all_si", "all_siu"}:
                drop = ["all_siRNA"]
            else:
                # Unknown read type: all_si_22G
                raise NotImplementedError("Unknown read type: %s" % wildcards.small_type)
            logfile.write(f"Will drop {drop} for {wildcards.small_type}.\n")
            counts_data = pd.concat(
                (pd.read_table(
                    counts_file,
                    index_col=0,
                    na_filter=False).drop(drop, errors="ignore") for counts_file in counts_files),
                axis=1).fillna(0).astype(int)
            counts_data.columns = COND_NAMES
            # Simple_repeat|Simple_repeat|(TTTTTTG)n:1
            # Simple_repeat|Simple_repeat|(TTTTTTG)n:2
            # Simple_repeat|Simple_repeat|(TTTTTTG)n:3
            # Simple_repeat|Simple_repeat|(TTTTTTG)n:4
            # -> Simple_repeat|Simple_repeat|(TTTTTTG)n
            if wildcards.small_type in RMSK_SISIU_TYPES:
                counts_data = sum_by_family(counts_data)
            counts_data.index.names = ["gene"]
            counts_data.to_csv(output.counts_table, sep="\t")


def counts_to_join(wildcards):
    small_types = list(types_under([f"{wildcards.infix}", f"{wildcards.suffix}"]))
    return [
        OPJ(
            annot_counts_dir,
            f"all_{wildcards.mapped_type}_on_{genome}",
            f"{small_type}_counts.txt")
        for small_type in small_types]


# TODO: drop duplicates or sum counts when duplicate row indices?
# (prot_si_22G, te_si_22G, pseu_si_22G, satel_si_22G and simrep_si_22G) into si_22G
# or
# (prot_si_26G, te_si_26G, pseu_si_26G, satel_si_26G and simrep_si_26G) into si_26G
rule join_si_counts:
    """concat SI_TYPES (prot_si, te_si, pseu_si, satel_si and simrep_si) into si"""
    input:
        counts_tables = counts_to_join,
    output:
        counts_table = OPJ(
            annot_counts_dir,
            "all_{mapped_type}_on_%s" % genome, "{infix}_{suffix}_counts.txt"),
    run:
        counts_data = pd.concat((
            pd.read_table(table, index_col="gene") for table in input.counts_tables))
        counts_data.index.names = ["gene"]
        counts_data.to_csv(output.counts_table, sep="\t")


rule join_sisiu_counts:
    """sum si and siu into sisiu"""
    input:
        si_counts_table = OPJ(annot_counts_dir, f"all_{size_selected}_on_{genome}", "si_{suffix}_counts.txt"),
        siu_counts_table = OPJ(annot_counts_dir, f"all_nomap_siRNA_on_{genome}", "siu_{suffix}_counts.txt"),
    output:
        counts_table = OPJ(
            annot_counts_dir,
            f"all_{size_selected}_on_{genome}", "sisiu_{suffix}_counts.txt"),
    run:
        si_counts_data = pd.read_table(input.si_counts_table, index_col="gene")
        siu_counts_data = pd.read_table(input.siu_counts_table, index_col="gene")
        counts_data = add_dataframes(si_counts_data, siu_counts_data)
        counts_data.index.names = ["gene"]
        counts_data.to_csv(output.counts_table, sep="\t")


rule join_all_sisiu_counts:
    """sum all_si and all_siu into all_sisiu"""
    input:
        all_si_counts_table = OPJ(
            annot_counts_dir,
            f"all_{size_selected}_on_{genome}", "all_si_counts.txt"),
        all_siu_counts_table = OPJ(
            annot_counts_dir,
            f"all_nomap_siRNA_on_{genome}", "all_siu_counts.txt"),
    output:
        counts_table = OPJ(
            annot_counts_dir,
            f"all_{size_selected}_on_{genome}", "all_sisiu_counts.txt"),
    run:
        all_si_counts_data = pd.read_table(input.all_si_counts_table, index_col="gene")
        all_siu_counts_data = pd.read_table(input.all_siu_counts_table, index_col="gene")
        counts_data = add_dataframes(all_si_counts_data, all_siu_counts_data)
        counts_data.index.names = ["gene"]
        counts_data.to_csv(output.counts_table, sep="\t")


#TODO: add tRF, then change category name
rule join_pimi22GtRF_counts:
    f"""concat si_{SI_MIN}G with mi and pi into pimi{SI_MIN}G (a.k.a pisimi) and then with tRF into pimi{SI_MIN}GtRF (a.k.a pisimitRF)"""
    input:
        pi_counts_table = OPJ(
            annot_counts_dir,
            f"all_{size_selected}_on_{genome}", "pi_counts.txt"),
        si_22G_counts_table = OPJ(
            annot_counts_dir,
            f"all_{size_selected}_on_{genome}", f"si_{SI_MIN}G_counts.txt"),
        mi_counts_table = OPJ(
            annot_counts_dir,
            f"all_{size_selected}_on_{genome}", "mi_counts.txt"),
        tRF_counts_table = OPJ(
            annot_counts_dir,
            f"all_{size_selected}_on_{genome}", "tRF_counts.txt"),
    output:
        counts_table = OPJ(
            annot_counts_dir,
            f"all_{size_selected}_on_{genome}", "pisimi_counts.txt"),
        # Also generate pisimitRF_counts.txt
        counts_table_plus_tRF = OPJ(
            annot_counts_dir,
            f"all_{size_selected}_on_{genome}", "pisimitRF_counts.txt"),
    run:
        pi_counts_data = pd.read_table(input.pi_counts_table, index_col="gene")
        si_22G_counts_data = pd.read_table(input.si_22G_counts_table, index_col="gene")
        mi_counts_data = pd.read_table(input.mi_counts_table, index_col="gene")
        counts_data = pd.concat([pi_counts_data, si_22G_counts_data, mi_counts_data])
        counts_data.index.names = ["gene"]
        counts_data.to_csv(output.counts_table, sep="\t")
        tRF_counts_data = pd.read_table(input.tRF_counts_table, index_col="gene")
        pisimitRF_counts_data = pd.concat([counts_data, tRF_counts_data])
        pisimitRF_counts_data.index.names = ["gene"]
        pisimitRF_counts_data.to_csv(output.counts_table_plus_tRF, sep="\t")


@wc_applied
def source_small_RNA_counts(wildcards):
    """Determines from which rule the gathered small counts should be sourced."""
    # TODO: add {small_type}_{mapping_type}_{biotype}_{orientation}_{feature_type} category to use the featurecounts results
    if hasattr(wildcards, "biotype"):
        if wildcards.biotype in JOINED_BIOTYPES:
            return rules.join_all_feature_counts.output.counts_table
        else:
            return rules.gather_feature_counts.output.counts_table
        #return OPJ(
        #    feature_counts_dir,
        #    f"all_{wildcards.remapped_counted}_{wildcards.feature_type}_counts.txt")
    # if wildcards.small_type == "pisimi":
    #     # all_si and also pi and mi
    #     return rules.join_pisimi_counts.output.counts_table
    if wildcards.small_type == f"pimi{SI_MIN}G":
        # si_22G and also pi and mi
        return rules.join_pimi22GtRF_counts.output.counts_table
    elif wildcards.small_type == f"pimi{SI_MIN}GtRF":
        # si_22G and also pi and mi and tRF
        return rules.join_pimi22GtRF_counts.output.counts_table_plus_tRF
    elif wildcards.small_type in {f"sisiu_{suffix}" for suffix in SI_SUFFIXES}:
        # si and siu
        return rules.join_sisiu_counts.output.counts_table
    elif wildcards.small_type in {f"siu_{suffix}" for suffix in SI_SUFFIXES}:
        # prot_siu, te_siu, pseu_siu, satel_siu and simrep_siu
        # return rules.join_siu_counts.output.counts_table
        return OPJ(
            annot_counts_dir,
            f"all_nomap_siRNA_on_{genome}", f"{wildcards.small_type}_counts.txt")
    elif wildcards.small_type in {f"si_{suffix}" for suffix in SI_SUFFIXES}:
        # prot_si, te_si, pseu_si, satel_si and simrep_si
        # return rules.join_si_counts.output.counts_table
        return OPJ(
            annot_counts_dir,
            f"all_{size_selected}_on_{genome}", f"{wildcards.small_type}_counts.txt")
    elif wildcards.small_type == "all_sisiu":
        # all_si and all_siu (no annotation constraint apart from not (mi or pi))
        return rules.join_all_sisiu_counts.output.counts_table
    elif wildcards.small_type in annotate_count_output:
        return OPJ(
            annot_counts_dir,
            f"all_{size_selected}_on_{genome}", f"{wildcards.small_type}_counts.txt")
    elif wildcards.small_type in annotate_count_output_U:
        return OPJ(
            annot_counts_dir,
            f"all_nomap_siRNA_on_{genome}", f"{wildcards.small_type}_counts.txt")
    else:
        # "Directly" from the counts gathered across libraries
        #return rules.gather_small_RNA_counts.output.counts_table
        raise NotImplementedError("Unknown small read type: %s" % wildcards.small_type)


def make_tag_association(dfs, tag):
    """Associates a tag "tag" to the union of the indices of dataframes *dfs*."""
    idx = reduce(union, (df.index for df in dfs))
    if len(idx):
        return pd.DataFrame(list(zip(idx, repeat(tag)))).set_index(0)
    return pd.DataFrame()


# Could we use biotype instead?
# Maybe not, because biotypes can overlap
# (protein_coding and protein_coding_CDS, for instance).
rule associate_small_type:
    """This rule uses the small RNA count matrices
    to associate a "small RNA type" to each gene identifier."""
    input:
        si_counts_tables = expand(
            OPJ(annot_counts_dir, f"all_{size_selected}_on_{genome}", "{prefix}_si_{suffix}_counts.txt"),
            prefix=SI_PREFIXES,
            suffix=SI_SUFFIXES),
        siu_counts_tables = expand(
            OPJ(annot_counts_dir, f"all_nomap_siRNA_on_{genome}", "{prefix}_siu_{suffix}_counts.txt"),
            prefix=SI_PREFIXES,
            suffix=SI_SUFFIXES),
        pi_counts_table = OPJ(
            annot_counts_dir, f"all_{size_selected}_on_{genome}", "pi_counts.txt"),
        mi_counts_table = OPJ(
            annot_counts_dir, f"all_{size_selected}_on_{genome}", "mi_counts.txt"),
        tRF_counts_table = OPJ(
            annot_counts_dir, f"all_{size_selected}_on_{genome}", "tRF_counts.txt"),
        all_sisiu_counts_table = rules.join_all_sisiu_counts.output.counts_table,
    output:
        tags_table = OPJ(annot_counts_dir, f"all_{size_selected}_on_{genome}", "id2tags.txt"),
    run:
        def sisiu_tables(prefix):
            """Return the list of the paths to all the "primary"
            endo siRNA counts antisense to biotype *prefix*."""
            si_tables = [
                OPJ(annot_counts_dir, f"all_{size_selected}_on_{genome}", f"{prefix}_si_{suffix}_counts.txt")
                for suffix in SI_SUFFIXES]
            siu_tables = [
                OPJ(annot_counts_dir, f"all_nomap_siRNA_on_{genome}", f"{prefix}_siu_{suffix}_counts.txt")
                for suffix in SI_SUFFIXES]
            return [*si_tables, *siu_tables]
        sisiu_tags_table = pd.concat(make_tag_association(
            (pd.read_table(table, index_col="gene") for table in sisiu_tables(prefix)),
            "%s_sisiu" % prefix) for prefix in SI_PREFIXES)
        pi_tags_table = make_tag_association(
            (pd.read_table(input.pi_counts_table, index_col="gene"),), "pi")
        mi_tags_table = make_tag_association(
            (pd.read_table(input.mi_counts_table, index_col="gene"),), "mi")
        tRF_tags_table = make_tag_association(
            (pd.read_table(input.tRF_counts_table, index_col="gene"),), "tRF")
        # TODO: add check for intersection of pi, mi, and tRF?
        assert len(pi_tags_table.index.intersection(mi_tags_table.index)) == 0, "Some genes have both pi and mi."
        assert len(pi_tags_table.index.intersection(sisiu_tags_table.index)) == 0, "Some genes have both pi and sisiu."
        assert len(mi_tags_table.index.intersection(sisiu_tags_table.index)) == 0, "Some genes have both mi and sisiu."
        assert len(tRF_tags_table.index.intersection(sisiu_tags_table.index)) == 0, "Some genes have both tRF and sisiu."
        tags_table = pd.concat((sisiu_tags_table, pi_tags_table, mi_tags_table, tRF_tags_table))
        # Add the "all_sisiu" tag to those not already tagged
        all_sisiu_tags_table = make_tag_association(
            (pd.read_table(input.all_sisiu_counts_table, index_col="gene"),), "all_sisiu")
        untagged = all_sisiu_tags_table.index.difference(tags_table.index)
        tags_table = pd.concat((tags_table, all_sisiu_tags_table.loc[untagged]))
        tags_table.index.names = ["gene"]
        tags_table.columns = ["small_type"]
        tags_table.to_csv(output.tags_table, sep="\t")


# TODO: check what this does with index column name (can it loose "gene"?)
def add_tags_column(data, tags_table, tag_name, logfile=None):
    """Adds a column *tag_name* to *data* based on the DataFrame *tag_table*
    associating tags to row names."""
    # Why "inner" ?
    df = pd.concat((data, pd.read_table(tags_table, index_col=0)), join="inner", axis=1)
    if logfile is not None:
        logfile.write(f"Index in data is: {data.index.name}\n")
        logfile.write(f"Index in df is: {df.index.name}\n")
    df.columns = (*data.columns, tag_name)
    return df


# TODO: update this
# Generate report with
# - raw
# - trimmed
# - deduplicated
# - size-selected
# - mapped
# - ambiguous
# - si, pi, mi, tRF
# - all_si ((22G-26G)(T*) that are not mi or tRF or pi)
rule make_read_counts_summary:
    input:
        nb_raw = rules.trim_and_dedup.output.nb_raw,
        nb_deduped = rules.trim_and_dedup.output.nb_deduped,
        nb_trimmed = rules.trim_and_dedup.output.nb_trimmed,
        nb_size_selected = rules.select_size_range.output.nb_selected,
        nb_nomap_si = rules.extract_nomap_siRNAs.output.nb_nomap_si,
        nb_mapped = rules.small_RNA_seq_annotate.output.nb_mapped,
        nb_non_structural = rules.count_non_structural_mappers.output.nb_non_structural,
        ambig_type = rules.small_RNA_seq_annotate.output.ambig_type,
        ambig_typeU = rules.small_RNA_seq_annotate_U.output.ambig_type,
        annot_counts = [annotate_count_output[small_type] for small_type in ANNOT_COUNTS_TYPES],
        annot_counts_U = [annotate_count_output_U[small_type] for small_type in ANNOT_COUNTS_TYPES_U],
        # prot_si_counts = rules.small_RNA_seq_annotate.output.prot_si_counts,
        # prot_siu_counts = rules.small_RNA_seq_annotate_U.output.prot_siu_counts,
        # te_si_counts = rules.small_RNA_seq_annotate.output.te_si_counts,
        # te_siu_counts = rules.small_RNA_seq_annotate_U.output.te_siu_counts,
        # pseu_si_counts = rules.small_RNA_seq_annotate.output.pseu_si_counts,
        # pseu_siu_counts = rules.small_RNA_seq_annotate_U.output.pseu_siu_counts,
        # satel_si_counts = rules.small_RNA_seq_annotate.output.satel_si_counts,
        # satel_siu_counts = rules.small_RNA_seq_annotate_U.output.satel_siu_counts,
        # simrep_si_counts = rules.small_RNA_seq_annotate.output.simrep_si_counts,
        # simrep_siu_counts = rules.small_RNA_seq_annotate_U.output.simrep_siu_counts,
        # all_si_counts = rules.small_RNA_seq_annotate.output.all_si_counts,
        # all_siu_counts = rules.small_RNA_seq_annotate_U.output.all_siu_counts,
        # pi_counts = rules.small_RNA_seq_annotate.output.pi_counts,
        # mi_counts = rules.small_RNA_seq_annotate.output.mi_counts,
    output:
        summary = OPJ(
            aligner, "summaries",
            "{lib}_{rep}_%s_on_%s_read_counts.txt" % (size_selected, genome)),
    log:
        warnings = OPJ(log_dir, "make_read_counts_summary", "{lib}_{rep}.warnings"),
    benchmark:
        OPJ(log_dir, "make_read_counts_summary", "{lib}_{rep}_benchmark.txt"),
    #threads: 8  # to limit memory usage, actually
    run:
        with warn_context(log.warnings) as warn, open(output.summary, "w") as summary_file:
            try:
                annot_counts_files = {
                    small_type: apply_wildcards(path_template, wildcards)
                    for (small_type, path_template)
                    in merge_with(dont_merge, annotate_count_output, annotate_count_output_U).items()}
            except ValueError as err:
                print(*annotate_count_output.keys(), sep=", ")
                print(*annotate_count_output_U.keys(), sep=", ")
                raise
            # annot_counts_files = valmap(
            #     flip(apply_wildcards, wildcards),
            #     merge_with(dont_merge, annotate_count_output, annotate_count_output_U))
            # TODO: add all_sisiuRNA
            summary_file.write("%s\n" % "\t".join([
                "raw", "trimmed", "deduped", "%s" % size_selected, "nomap_siRNA",
                "mapped", "non_structural", "ambig_type",
                *type2RNA(SISIU_TYPES),
                "all_sisiuRNA", "piRNA", "miRNA", "tRFRNA"]))
            summary_file.write("%d\t" % read_int_from_file(input.nb_raw))
            summary_file.write("%d\t" % read_int_from_file(input.nb_trimmed))
            summary_file.write("%d\t" % read_int_from_file(input.nb_deduped))
            summary_file.write("%d\t" % read_int_from_file(input.nb_size_selected))
            summary_file.write("%d\t" % read_int_from_file(input.nb_nomap_si))
            summary_file.write("%d\t" % read_int_from_file(input.nb_mapped))
            summary_file.write("%d\t" % read_int_from_file(input.nb_non_structural))
            summary_file.write(str(file_len(input.ambig_type) + file_len(input.ambig_typeU)))
            summary_file.write("\t")
            # SISIU_TYPES is used for headers, but counts come from SI_TYPES and SIU_TYPES
            assert len(SISIU_TYPES) == len(SI_TYPES)
            assert len(SISIU_TYPES) == len(SIU_TYPES)
            for (si_type, siu_type) in zip(SI_TYPES, SIU_TYPES):
                summary_file.write(str(sum_counts(annot_counts_files[si_type]) + sum_counts(annot_counts_files[siu_type])))
                summary_file.write("\t")
            collect()
            p = Popen(
                ['awk', '$1 == "all_siRNA" {print $2}', annot_counts_files["all_si"]],
                stdout=PIPE,
                stderr=PIPE)
            (result, err) = p.communicate()
            if p.returncode != 0:
                raise IOError(err)
            collect()
            try:
                nb_all_si = result.strip().split()[0]
            except IndexError as err:
                warn(f"No entry for all_siRNA in {annot_counts_files['all_si']}?\n")
                raise
                # nb_all_si = "0"
            # We look for "all_siRNA", not "all_siuRNA" because
            # that's how they are labelled by small_RNA_seq_annotate.py
            pu = Popen(
                ['awk', '$1 == "all_siRNA" {print $2}', annot_counts_files["all_siu"]],
                stdout=PIPE,
                stderr=PIPE)
            (resultu, erru) = pu.communicate()
            if pu.returncode != 0:
                raise IOError(erru)
            try:
                nb_all_siu = resultu.strip().split()[0]
            except IndexError as err:
                warn(f"No entry for all_siRNA in {annot_counts_files['all_siu']}?\n")
                raise
                # nb_all_siu = "0"
            summary_file.write(str(int(nb_all_si) + int(nb_all_siu)))
            summary_file.write("\t")
            summary_file.write(str(sum_counts(annot_counts_files["pi"])))
            summary_file.write("\t")
            summary_file.write(str(sum_counts(annot_counts_files["mi"])))
            summary_file.write("\t")
            summary_file.write(str(sum_counts(annot_counts_files["tRF"])))
            summary_file.write("\n")


rule compute_median_ratio_to_pseudo_ref_size_factors:
    input:
        counts_table = source_small_RNA_counts,
    output:
        median_ratios_file = OPJ(
            annot_counts_dir, "all_{mapped_type}_on_%s" % genome,
            "{small_type}_median_ratios_to_pseudo_ref.txt"),
    run:
        counts_data = pd.read_table(
            input.counts_table,
            index_col=0,
            na_filter=False)
        # http://stackoverflow.com/a/21320592/1878788
        #median_ratios = pd.DataFrame(median_ratio_to_pseudo_ref_size_factors(counts_data)).T
        #median_ratios.index.names = ["median_ratio_to_pseudo_ref"]
        # Easier to grep when not transposed, actually:
        median_ratios = median_ratio_to_pseudo_ref_size_factors(counts_data)
        median_ratios.to_csv(output.median_ratios_file, sep="\t")


# TODO: add DESeq2 style size factors ?
rule gather_read_counts_summaries:
    input:
        summary_tables = expand(OPJ(
            aligner, "summaries",
            "{name}_%s_on_%s_read_counts.txt" % (size_selected, genome)), name=COND_NAMES),
    output:
        summary_table = OPJ(
            aligner, "summaries",
            "all_%s_on_%s_read_counts.txt" % (size_selected, genome)),
    run:
        summary_files = (OPJ(
            aligner, "summaries",
            f"{cond_name}_{size_selected}_on_{genome}_read_counts.txt") for cond_name in COND_NAMES)
        summaries = pd.concat((pd.read_table(summary_file).T.astype(int) for summary_file in summary_files), axis=1)
        summaries.columns = COND_NAMES
        summaries.to_csv(output.summary_table, sep="\t")


# Moved to script compute_genes_exon_lengths.py
#rule compute_genes_exon_lengths:
#    """To each gene ID, associate a length obtained by taking the union of all exons belonging to transcripts of this gene."""
#    input:
#        genes_gtf = OPJ(annot_dir, "genes.gtf"),
#        dte_bed = OPJ(annot_dir, "DNA_transposons_rmsk.bed"),
#        rte_bed = OPJ(annot_dir, "RNA_transposons_rmsk.bed"),
#        satel_bed = OPJ(annot_dir, "satellites_rmsk.bed"),
#        simrep_bed = OPJ(annot_dir, "simple_repeats_rmsk.bed"),
#
#    output:
#        exon_lengths = OPJ(annot_dir, "union_exon_lengths.txt"),
#    run:
#        pd.concat((
#            gtf_2_genes_exon_lengths(input.genes_gtf),
#            repeat_bed_2_lengths(input.dte_bed),
#            repeat_bed_2_lengths(input.rte_bed),
#            repeat_bed_2_lengths(input.satel_bed),
#            repeat_bed_2_lengths(input.simrep_bed))).to_csv(output.exon_lengths, sep="\t")       


if False:
    rule compute_RPM:
        input:
            counts_table = source_small_RNA_counts,
            summary_table = rules.gather_read_counts_summaries.output.summary_table,
            tags_table = rules.associate_small_type.output.tags_table,
        output:
            RPM_table = OPJ(
                annot_counts_dir,
                "all_{mapped_type}_on_%s" % genome, "{small_type}_RPM.txt"),
        log:
            log = OPJ(log_dir, "compute_RPM_{mapped_type}", "{small_type}.log"),
        benchmark:
            OPJ(log_dir, "compute_RPM_{mapped_type}", "{small_type}_benchmark.txt"),
        run:
            with open(log.log, "w") as logfile:
                logfile.write(f"Reading column counts from {input.counts_table}\n")
                counts_data = pd.read_table(
                    input.counts_table,
                    index_col="gene")
                logfile.write(f"Reading number of non-structural mappers from {input.summary_table}\n")
                norm = pd.read_table(input.summary_table, index_col=0).loc["non_structural"]
                logfile.write(str(norm))
                logfile.write("Computing counts per million non-structural mappers\n")
                RPM = 1000000 * counts_data / norm
                add_tags_column(RPM, input.tags_table, "small_type").to_csv(output.RPM_table, sep="\t")


rule compute_RPM:
    input:
        counts_table = source_small_RNA_counts,
        summary_table = rules.gather_read_counts_summaries.output.summary_table,
        tags_table = rules.associate_small_type.output.tags_table,
    output:
        RPM_table = OPJ(
            annot_counts_dir,
            "all_{mapped_type}_on_%s" % genome, "{small_type}_RPM_by_{norm}.txt"),
    log:
        log = OPJ(log_dir, "compute_RPM_by_{norm}_{mapped_type}", "{small_type}.log"),
    benchmark:
        OPJ(log_dir, "compute_RPM_by_{norm}_{mapped_type}", "{small_type}_benchmark.txt"),
    run:
        with open(log.log, "w") as logfile:
            logfile.write(f"Reading column counts from {input.counts_table}\n")
            counts_data = pd.read_table(
                input.counts_table,
                index_col="gene")
            logfile.write(f"Reading number of {wildcards.norm} from {input.summary_table}\n")
            norm = pd.read_table(input.summary_table, index_col=0).loc[wildcards.norm]
            logfile.write(str(norm))
            logfile.write(f"Computing counts per million {wildcards.norm}\n")
            RPM = 1000000 * counts_data / norm
            add_tags_column(RPM, input.tags_table, "small_type").to_csv(output.RPM_table, sep="\t")


if False:
    rule compute_RPM_folds:
        input:
            counts_table = source_small_RNA_counts,
            #exon_lengths = rules.compute_genes_exon_lengths.output.exon_lengths,
            summary_table = rules.gather_read_counts_summaries.output.summary_table,
            tags_table = rules.associate_small_type.output.tags_table,
        output:
            fold_results = OPJ(
                mapping_dir, f"RPM_folds_{size_selected}",
                "{contrast}", "{contrast}_{small_type}_RPM_folds.txt"),
        log:
            log = OPJ(log_dir, "compute_RPM_folds", "{contrast}_{small_type}.log"),
        benchmark:
            OPJ(log_dir, "compute_RPM_folds", "{contrast}_{small_type}_benchmark.txt"),
        run:
            with open(log.log, "w") as logfile:
                (cond, ref) = CONTRAST2PAIR[wildcards.contrast]
                logfile.write(f"cond: {cond}, ref: {ref}\n")
                column_list = expand("{lib}_{rep}", lib=[ref, cond], rep=REPS)
                logfile.write(f"Reading columns {column_list} from {input.counts_table}\n")
                counts_data = pd.read_table(
                    input.counts_table,
                    usecols=["gene", *column_list],
                    index_col="gene")
                assert counts_data.index.name == "gene", f"Wrong index: {counts_data.index.name}"
                # logfile.write(f"Columns in counts_data are: {counts_data.columns}")
                # logfile.write(f"Index in counts_data is: {counts_data.index.name}")
                logfile.write(f"Found {len(counts_data.index)} genes\n")
                #logfile.write(f"Reading exon lengths from {exon_lengths_file}\n")
                #exon_lengths = pd.read_table(
                #    input.exon_lengths_file,
                #    index_col="gene")
                #logfile.write(f"Found {len(exon_lengths.index)} genes\n")
                #logfile.write("Determining set of common genes\n")
                #common = counts_data.index.intersection(exon_lengths.index)
                #logfile.write(f"Found {len(common)} common genes\n")
                #logfile.write("Computing counts per kilobase\n")
                #RPK = 1000 * counts_data.loc[common].div(exon_lengths.loc[common]["union_exon_len"], axis="index")
                logfile.write(f"Reading number of non-structural mappers from {input.summary_table}\n")
                norm = pd.read_table(input.summary_table, index_col=0).loc["non_structural"][column_list]
                logfile.write(str(norm))
                logfile.write("Computing counts per million non-structural mappers\n")
                RPM = 1000000 * counts_data / norm
                # logfile.write(f"Columns in RPM are: {RPM.columns}\n")
                # logfile.write(f"Index in RPM is: {RPM.index.name}\n")
                assert RPM.index.name == "gene", f"Wrong index: {RPM.index.name}"
                logfile.write("Computing RPM log2 fold changes\n")
                # Maybe not a very sensible way to compute the mean folds: it pairs replicates, but the pairing is arbitrary
                # TODO: Should we compute the fold of the means instead?
                # -> No: the pairing is not necessarily arbitrary.
                # The condition pairs may come from the same biological line, for instance.
                # Add 0.25 as RPM "pseudo-count" (edgeR does this ? https://www.biostars.org/p/102568/#102604)
                lfc = np.log2(pd.DataFrame(
                    {f"log2({cond}_{rep}/{ref}_{rep})" : (RPM[f"{cond}_{rep}"] + 0.25) / (RPM[f"{ref}_{rep}"] + 0.25) for rep in REPS}))
                # Compute the mean
                lfc = lfc.assign(mean_log2_RPM_fold=lfc.mean(axis=1))
                # Converting gene IDs
                ######################
                with open(OPJ(convert_dir, "wormid2cosmid.pickle"), "rb") as dict_file:
                    lfc = lfc.assign(cosmid=lfc.apply(column_converter(load(dict_file)), axis=1))
                #with open(OPJ(convert_dir, "wormid2name.pickle"), "rb") as dict_file:
                #    lfc = lfc.assign(name=lfc.apply(column_converter(load(dict_file)), axis=1))
                lfc = lfc.assign(name=lfc.apply(column_converter(wormid2name), axis=1))
                # logfile.write(f"Columns in lfc are: {lfc.columns}\n")
                # logfile.write(f"Index in lfc is: {lfc.index.name}\n")
                assert lfc.index.name == "gene", f"Wrong index: {lfc.index.name}"
                logfile.write(f"Adding small read type info from {input.tags_table}\n")
                #pd.concat((counts_data.loc[common], RPM, add_tags_column(lfc, input.tags_table, "small_type")), axis=1).to_csv(output.fold_results, sep="\t")
                lfc_with_tags = add_tags_column(lfc, input.tags_table, "small_type", logfile)
                logfile.write(f"Columns in lfc_with_tags are: {lfc_with_tags.columns}\n")
                logfile.write(f"Index in lfc_with_tags is: {lfc_with_tags.index.name}\n")
                lfc_idx = lfc.index
                RPM_idx = RPM.index
                lfc_with_tags_idx = lfc_with_tags.index
                lfc_xor_RPM = lfc_idx.symmetric_difference(RPM_idx)
                lfc_xor_lfc_with_tags = lfc_idx.symmetric_difference(lfc_with_tags_idx)
                RPM_xor_lfc_with_tags = RPM_idx.symmetric_difference(lfc_with_tags_idx)
                logfile.write(f"Index difference:\nlfc_xor_RPM: {lfc_xor_RPM}\nlfc_xor_lfc_with_tags: {lfc_xor_lfc_with_tags}\nRPM_xor_lfc_with_tags: {RPM_xor_lfc_with_tags}\n")
                with_tags = pd.concat((RPM, lfc_with_tags), axis=1)
                logfile.write(f"Columns in with_tags are: {with_tags.columns}\n")
                logfile.write(f"Index in with_tags is: {with_tags.index.name}\n")
                logfile.write(f"Then writing to {output.fold_results}\n")
                with_tags.to_csv(output.fold_results, sep="\t")

rule compute_RPM_folds:
    input:
        counts_table = source_small_RNA_counts,
        #exon_lengths = rules.compute_genes_exon_lengths.output.exon_lengths,
        summary_table = rules.gather_read_counts_summaries.output.summary_table,
        tags_table = rules.associate_small_type.output.tags_table,
    output:
        fold_results = OPJ(
            mapping_dir, "RPM_by_{norm}_folds_%s" % size_selected,
            "{contrast}", "{contrast}_{small_type}_RPM_by_{norm}_folds.txt"),
    log:
        log = OPJ(log_dir, "compute_RPM_by_{norm}_folds", "{contrast}_{small_type}.log"),
    benchmark:
        OPJ(log_dir, "compute_RPM_by_{norm}_folds", "{contrast}_{small_type}_benchmark.txt"),
    run:
        with open(log.log, "w") as logfile:
            (cond, ref) = CONTRAST2PAIR[wildcards.contrast]
            logfile.write(f"cond: {cond}, ref: {ref}\n")
            column_list = expand("{lib}_{rep}", lib=[ref, cond], rep=REPS)
            logfile.write(f"Reading columns {column_list} from {input.counts_table}\n")
            counts_data = pd.read_table(
                input.counts_table,
                usecols=["gene", *column_list],
                index_col="gene")
            assert counts_data.index.name == "gene", f"Wrong index: {counts_data.index.name}"
            # logfile.write(f"Columns in counts_data are: {counts_data.columns}")
            # logfile.write(f"Index in counts_data is: {counts_data.index.name}")
            logfile.write(f"Found {len(counts_data.index)} genes\n")
            #logfile.write(f"Reading exon lengths from {exon_lengths_file}\n")
            #exon_lengths = pd.read_table(
            #    input.exon_lengths_file,
            #    index_col="gene")
            #logfile.write(f"Found {len(exon_lengths.index)} genes\n")
            #logfile.write("Determining set of common genes\n")
            #common = counts_data.index.intersection(exon_lengths.index)
            #logfile.write(f"Found {len(common)} common genes\n")
            #logfile.write("Computing counts per kilobase\n")
            #RPK = 1000 * counts_data.loc[common].div(exon_lengths.loc[common]["union_exon_len"], axis="index")
            logfile.write(f"Reading number of {wildcards.norm} from {input.summary_table}\n")
            norm = pd.read_table(input.summary_table, index_col=0).loc[wildcards.norm][column_list]
            logfile.write(str(norm))
            logfile.write(f"Computing counts per million {wildcards.norm}\n")
            RPM = 1000000 * counts_data / norm
            # logfile.write(f"Columns in RPM are: {RPM.columns}\n")
            # logfile.write(f"Index in RPM is: {RPM.index.name}\n")
            assert RPM.index.name == "gene", f"Wrong index: {RPM.index.name}"
            logfile.write("Computing RPM log2 fold changes\n")
            # Maybe not a very sensible way to compute the mean folds: it pairs replicates, but the pairing is arbitrary
            # TODO: Should we compute the fold of the means instead?
            # -> No: the pairing is not necessarily arbitrary.
            # The condition pairs may come from the same biological line, for instance.
            # Add 0.25 as RPM "pseudo-count" (edgeR does this ? https://www.biostars.org/p/102568/#102604)
            lfc = np.log2(pd.DataFrame(
                {f"log2({cond}_{rep}/{ref}_{rep})" : (RPM[f"{cond}_{rep}"] + 0.25) / (RPM[f"{ref}_{rep}"] + 0.25) for rep in REPS}))
            # Compute the mean
            lfc = lfc.assign(mean_log2_RPM_fold=lfc.mean(axis=1))
            # Converting gene IDs
            ######################
            with open(OPJ(convert_dir, "wormid2cosmid.pickle"), "rb") as dict_file:
                lfc = lfc.assign(cosmid=lfc.apply(column_converter(load(dict_file)), axis=1))
            #with open(OPJ(convert_dir, "wormid2name.pickle"), "rb") as dict_file:
            #    lfc = lfc.assign(name=lfc.apply(column_converter(load(dict_file)), axis=1))
            lfc = lfc.assign(name=lfc.apply(column_converter(wormid2name), axis=1))
            # logfile.write(f"Columns in lfc are: {lfc.columns}\n")
            # logfile.write(f"Index in lfc is: {lfc.index.name}\n")
            assert lfc.index.name == "gene", f"Wrong index: {lfc.index.name}"
            logfile.write(f"Adding small read type info from {input.tags_table}\n")
            #pd.concat((counts_data.loc[common], RPM, add_tags_column(lfc, input.tags_table, "small_type")), axis=1).to_csv(output.fold_results, sep="\t")
            lfc_with_tags = add_tags_column(lfc, input.tags_table, "small_type", logfile)
            logfile.write(f"Columns in lfc_with_tags are: {lfc_with_tags.columns}\n")
            logfile.write(f"Index in lfc_with_tags is: {lfc_with_tags.index.name}\n")
            lfc_idx = lfc.index
            RPM_idx = RPM.index
            lfc_with_tags_idx = lfc_with_tags.index
            lfc_xor_RPM = lfc_idx.symmetric_difference(RPM_idx)
            lfc_xor_lfc_with_tags = lfc_idx.symmetric_difference(lfc_with_tags_idx)
            RPM_xor_lfc_with_tags = RPM_idx.symmetric_difference(lfc_with_tags_idx)
            logfile.write(f"Index difference:\nlfc_xor_RPM: {lfc_xor_RPM}\nlfc_xor_lfc_with_tags: {lfc_xor_lfc_with_tags}\nRPM_xor_lfc_with_tags: {RPM_xor_lfc_with_tags}\n")
            with_tags = pd.concat((RPM, lfc_with_tags), axis=1)
            logfile.write(f"Columns in with_tags are: {with_tags.columns}\n")
            logfile.write(f"Index in with_tags is: {with_tags.index.name}\n")
            logfile.write(f"Then writing to {output.fold_results}\n")
            with_tags.to_csv(output.fold_results, sep="\t")


if False:
    rule gather_RPM_folds:
        """Gathers RPM folds across contrasts."""
        input:
            fold_results = expand(OPJ(
                mapping_dir, f"RPM_folds_{size_selected}",
                "{contrast}", "{contrast}_{{small_type}}_RPM_folds.txt"), contrast=IP_CONTRASTS),
        output:
            all_folds = OPJ(
                mapping_dir, f"RPM_folds_{size_selected}", "all", "{small_type}_mean_log2_RPM_fold.txt"),
        # wildcard_constraints:
        #     small_type="si|siu|sisiu|all_si|all_siu|all_sisiu|%s"% "|".join(SMALL_TYPES + JOINED_SMALL_TYPES),
        log:
            log = OPJ(log_dir, "gather_RPM_folds", "{small_type}.log"),
        benchmark:
            OPJ(log_dir, "gather_RPM_folds", "{small_type}_benchmark.txt"),
        run:
            with open(log.log, "w") as logfile:
                logfile.write(f"Debug: input\n{input}\n")
                actual_input = [
                    OPJ(mapping_dir, f"RPM_folds_{size_selected}",
                        f"{contrast}", f"{contrast}_{wildcards.small_type}_RPM_folds.txt") for contrast in IP_CONTRASTS]
                logfile.write(f"Gathering RPM folds from:\n{actual_input}\nShould be from:\n{input.fold_results}\n")
                pd.DataFrame({contrast : pd.read_table(
                    OPJ(mapping_dir, f"RPM_folds_{size_selected}",
                        f"{contrast}", f"{contrast}_{wildcards.small_type}_RPM_folds.txt"),
                    index_col=["gene", "cosmid", "name", "small_type"],
                    usecols=["gene", "cosmid", "name", "small_type", "mean_log2_RPM_fold"])["mean_log2_RPM_fold"] for contrast in IP_CONTRASTS}).to_csv(
                        output.all_folds, sep="\t")

#rule gather_RPM_folds:
#    """Gathers RPM folds across contrasts."""
#    input:
#        fold_results = expand(OPJ(
#            mapping_dir, "RPM_by_{{norm}}_folds_%s" % size_selected,
#            "{contrast}", "{contrast}_{{small_type}}_RPM_by_{{norm}}_folds.txt"), contrast=IP_CONTRASTS),
#    output:
#        all_folds = OPJ(
#            mapping_dir, "RPM_by_{norm}_folds_%s" % size_selected, "all", "{small_type}_mean_log2_RPM_by_{norm}_fold.txt"),
#    # wildcard_constraints:
#    #     small_type="si|siu|sisiu|all_si|all_siu|all_sisiu|%s"% "|".join(SMALL_TYPES + JOINED_SMALL_TYPES),
#    log:
#        log = OPJ(log_dir, "gather_RPM_by_{norm}_folds", "{small_type}.log"),
#    benchmark:
#        OPJ(log_dir, "gather_RPM_by_{norm}_folds", "{small_type}_benchmark.txt"),
#    run:
#        with open(log.log, "w") as logfile:
#            logfile.write(f"Debug: input\n{input}\n")
#            actual_input = [
#                OPJ(mapping_dir, f"RPM_by_{wildcards.norm}_folds_{size_selected}",
#                    f"{contrast}", f"{contrast}_{wildcards.small_type}_RPM_by_{wildcards.norm}_folds.txt") for contrast in IP_CONTRASTS]
#            logfile.write(f"Gathering RPM folds from:\n{actual_input}\nShould be from:\n{input.fold_results}\n")
#            pd.DataFrame({contrast : pd.read_table(
#                OPJ(mapping_dir, f"RPM_by_{wildcards.norm}_folds_{size_selected}",
#                    f"{contrast}", f"{contrast}_{wildcards.small_type}_RPM_by_{wildcards.norm}_folds.txt"),
#                index_col=["gene", "cosmid", "name", "small_type"],
#                usecols=["gene", "cosmid", "name", "small_type", "mean_log2_RPM_fold"])["mean_log2_RPM_fold"] for contrast in IP_CONTRASTS}).to_csv(
#                    output.all_folds, sep="\t")

rule gather_RPM_folds:
    """Gathers RPM folds across contrasts."""
    input:
        fold_results = expand(OPJ(
            mapping_dir, "RPM_by_{{norm}}_folds_%s" % size_selected,
            "{contrast}", "{contrast}_{{small_type}}_RPM_by_{{norm}}_folds.txt"), contrast=IP_CONTRASTS),
    output:
        all_folds = OPJ(
            mapping_dir, "RPM_by_{norm}_folds_%s" % size_selected, "all", "{small_type}_mean_log2_RPM_by_{norm}_fold.txt"),
    # wildcard_constraints:
    #     small_type="si|siu|sisiu|all_si|all_siu|all_sisiu|%s"% "|".join(SMALL_TYPES + JOINED_SMALL_TYPES),
    log:
        log = OPJ(log_dir, "gather_RPM_by_{norm}_folds", "{small_type}.log"),
    benchmark:
        OPJ(log_dir, "gather_RPM_by_{norm}_folds", "{small_type}_benchmark.txt"),
    run:
        with open(log.log, "w") as logfile:
            logfile.write(f"Debug: input\n{input}\n")
            actual_input = [
                OPJ(mapping_dir, f"RPM_by_{wildcards.norm}_folds_{size_selected}",
                    f"{contrast}", f"{contrast}_{wildcards.small_type}_RPM_by_{wildcards.norm}_folds.txt") for contrast in IP_CONTRASTS]
            logfile.write(f"Gathering RPM folds from:\n{actual_input}\nShould be from:\n{input.fold_results}\n")
            pd.DataFrame({contrast : pd.read_table(
                OPJ(mapping_dir, f"RPM_by_{wildcards.norm}_folds_{size_selected}",
                    f"{contrast}", f"{contrast}_{wildcards.small_type}_RPM_by_{wildcards.norm}_folds.txt"),
                index_col=["gene", "cosmid", "name", "small_type"],
                usecols=["gene", "cosmid", "name", "small_type", "mean_log2_RPM_fold"])["mean_log2_RPM_fold"] for contrast in IP_CONTRASTS}).to_csv(
                    output.all_folds, sep="\t")


# TODO: update consumers with mapping_type
# TODO: replace counted_type with all subwildcards. Why does it work with cond_name?
# Wildcards in input files cannot be determined from output files:
# 'read_type'
if False:
    rule compute_remapped_RPM_folds:
        input:
            counts_table = source_small_RNA_counts,
            # bowtie2/mapped_C_elegans_klp7co/feature_count/all_18-26_and_nomap_siRNA_on_C_elegans_klp7co_genes_fwd_exon_counts.txt
            #counts_table = rules.gather_feature_counts.output.counts_table,
            #exon_lengths = rules.compute_genes_exon_lengths.output.exon_lengths,
            summary_table = rules.gather_read_counts_summaries.output.summary_table,
            #tags_table = rules.associate_small_type.output.tags_table,
        output:
            fold_results = OPJ(
                mapping_dir, f"RPM_folds_{size_selected}",
                "{contrast}", "remapped", "{contrast}_{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}_RPM_folds.txt"),
        log:
            log = OPJ(log_dir, "compute_RPM_folds", "{contrast}_{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}.log"),
        benchmark:
            OPJ(log_dir, "compute_RPM_folds", "{contrast}_{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}_benchmark.txt"),
        run:
            with open(log.log, "w") as logfile:
                (cond, ref) = CONTRAST2PAIR[wildcards.contrast]
                logfile.write(f"cond: {cond}, ref: {ref}\n")
                column_list = expand("{lib}_{rep}", lib=[ref, cond], rep=REPS)
                logfile.write(f"Reading columns {column_list} from {input.counts_table}\n")
                counts_data = pd.read_table(
                    input.counts_table,
                    usecols=["gene", *column_list],
                    index_col="gene")
                logfile.write(f"Found {len(counts_data.index)} genes\n")
                #logfile.write(f"Reading exon lengths from {exon_lengths_file}\n")
                #exon_lengths = pd.read_table(
                #    input.exon_lengths_file,
                #    index_col="gene")
                #logfile.write(f"Found {len(exon_lengths.index)} genes\n")
                #logfile.write("Determining set of common genes\n")
                #common = counts_data.index.intersection(exon_lengths.index)
                #logfile.write(f"Found {len(common)} common genes\n")
                #logfile.write("Computing counts per kilobase\n")
                #RPK = 1000 * counts_data.loc[common].div(exon_lengths.loc[common]["union_exon_len"], axis="index")
                logfile.write(f"Reading number of non-structural mappers from {input.summary_table}\n")
                norm = pd.read_table(input.summary_table, index_col=0).loc["non_structural"][column_list]
                logfile.write(str(norm))
                logfile.write("Computing counts per million non-structural mappers\n")
                RPM = 1000000 * counts_data / norm
                logfile.write("Computing RPM log2 fold changes\n")
                # Add 0.25 as RPM "pseudo-count" (edgeR does this ? https://www.biostars.org/p/102568/#102604)
                lfc = np.log2(pd.DataFrame(
                    {f"log2({cond}_{rep}/{ref}_{rep})" : (RPM[f"{cond}_{rep}"] + 0.25) / (RPM[f"{ref}_{rep}"] + 0.25) for rep in REPS}))
                # Compute the mean
                lfc = lfc.assign(mean_log2_RPM_fold=lfc.mean(axis=1))
                # Converting gene IDs
                ######################
                with open(OPJ(convert_dir, "wormid2cosmid.pickle"), "rb") as dict_file:
                    lfc = lfc.assign(cosmid=lfc.apply(column_converter(load(dict_file)), axis=1))
                #with open(OPJ(convert_dir, "wormid2name.pickle"), "rb") as dict_file:
                #    lfc = lfc.assign(name=lfc.apply(column_converter(load(dict_file)), axis=1))
                lfc = lfc.assign(name=lfc.apply(column_converter(wormid2name), axis=1))
                #logfile.write("Adding small read type info\n")
                ##pd.concat((counts_data.loc[common], RPM, add_tags_column(lfc, input.tags_table, "small_type")), axis=1).to_csv(output.fold_results, sep="\t")
                #pd.concat((RPM, add_tags_column(lfc, input.tags_table, "small_type")), axis=1).to_csv(output.fold_results, sep="\t")
                pd.concat((RPM, lfc), axis=1).to_csv(output.fold_results, sep="\t")

rule compute_remapped_RPM_folds:
    input:
        counts_table = source_small_RNA_counts,
        # bowtie2/mapped_C_elegans_klp7co/feature_count/all_18-26_and_nomap_siRNA_on_C_elegans_klp7co_genes_fwd_exon_counts.txt
        #counts_table = rules.gather_feature_counts.output.counts_table,
        #exon_lengths = rules.compute_genes_exon_lengths.output.exon_lengths,
        summary_table = rules.gather_read_counts_summaries.output.summary_table,
        #tags_table = rules.associate_small_type.output.tags_table,
    output:
        fold_results = OPJ(
            mapping_dir, "RPM_by_{norm}_folds_%s" % size_selected,
            "{contrast}", "remapped", "{contrast}_{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}_RPM_by_{norm}_folds.txt"),
    log:
        log = OPJ(log_dir, "compute_RPM_by_{norm}_folds", "{contrast}_{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}.log"),
    benchmark:
        OPJ(log_dir, "compute_RPM_by_{norm}_folds", "{contrast}_{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}_benchmark.txt"),
    run:
        with open(log.log, "w") as logfile:
            (cond, ref) = CONTRAST2PAIR[wildcards.contrast]
            logfile.write(f"cond: {cond}, ref: {ref}\n")
            column_list = expand("{lib}_{rep}", lib=[ref, cond], rep=REPS)
            logfile.write(f"Reading columns {column_list} from {input.counts_table}\n")
            counts_data = pd.read_table(
                input.counts_table,
                usecols=["gene", *column_list],
                index_col="gene")
            logfile.write(f"Found {len(counts_data.index)} genes\n")
            #logfile.write(f"Reading exon lengths from {exon_lengths_file}\n")
            #exon_lengths = pd.read_table(
            #    input.exon_lengths_file,
            #    index_col="gene")
            #logfile.write(f"Found {len(exon_lengths.index)} genes\n")
            #logfile.write("Determining set of common genes\n")
            #common = counts_data.index.intersection(exon_lengths.index)
            #logfile.write(f"Found {len(common)} common genes\n")
            #logfile.write("Computing counts per kilobase\n")
            #RPK = 1000 * counts_data.loc[common].div(exon_lengths.loc[common]["union_exon_len"], axis="index")
            logfile.write(f"Reading number of {wildcards.norm} from {input.summary_table}\n")
            norm = pd.read_table(input.summary_table, index_col=0).loc[wildcards.norm][column_list]
            logfile.write(str(norm))
            logfile.write(f"Computing counts per million {wildcards.norm}\n")
            RPM = 1000000 * counts_data / norm
            logfile.write("Computing RPM log2 fold changes\n")
            # Add 0.25 as RPM "pseudo-count" (edgeR does this ? https://www.biostars.org/p/102568/#102604)
            lfc = np.log2(pd.DataFrame(
                {f"log2({cond}_{rep}/{ref}_{rep})" : (RPM[f"{cond}_{rep}"] + 0.25) / (RPM[f"{ref}_{rep}"] + 0.25) for rep in REPS}))
            # Compute the mean
            lfc = lfc.assign(mean_log2_RPM_fold=lfc.mean(axis=1))
            # Converting gene IDs
            ######################
            with open(OPJ(convert_dir, "wormid2cosmid.pickle"), "rb") as dict_file:
                lfc = lfc.assign(cosmid=lfc.apply(column_converter(load(dict_file)), axis=1))
            #with open(OPJ(convert_dir, "wormid2name.pickle"), "rb") as dict_file:
            #    lfc = lfc.assign(name=lfc.apply(column_converter(load(dict_file)), axis=1))
            lfc = lfc.assign(name=lfc.apply(column_converter(wormid2name), axis=1))
            #logfile.write("Adding small read type info\n")
            ##pd.concat((counts_data.loc[common], RPM, add_tags_column(lfc, input.tags_table, "small_type")), axis=1).to_csv(output.fold_results, sep="\t")
            #pd.concat((RPM, add_tags_column(lfc, input.tags_table, "small_type")), axis=1).to_csv(output.fold_results, sep="\t")
            pd.concat((RPM, lfc), axis=1).to_csv(output.fold_results, sep="\t")


if False:
    rule gather_remapped_RPM_folds:
        """Gathers RPM folds across contrasts."""
        input:
            fold_results = expand(OPJ(
                mapping_dir, f"RPM_folds_{size_selected}",
                "{contrast}", "remapped", "{contrast}_{{read_type}}_{{mapping_type}}_{{biotype}}_{{orientation}}_{{feature_type}}_RPM_folds.txt"), contrast=IP_CONTRASTS),
        output:
            all_folds = OPJ(
                mapping_dir, f"RPM_folds_{size_selected}", "all", "remapped", "{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}_mean_log2_RPM_fold.txt"),
        log:
            log = OPJ(log_dir, "gather_RPM_folds", "{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}.log"),
        benchmark:
            OPJ(log_dir, "gather_RPM_folds", "{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}_benchmark.txt"),
        run:
            pd.DataFrame({contrast : pd.read_table(
                # OPJ(mapping_dir, f"RPM_folds_{size_selected}",
                #     f"{contrast}", f"{contrast}_{wildcards.counted_type}_RPM_folds.txt"),
                OPJ(mapping_dir, f"RPM_folds_{size_selected}",
                    f"{contrast}", "remapped", f"{contrast}_{wildcards.read_type}_{wildcards.mapping_type}_{wildcards.biotype}_{wildcards.orientation}_{wildcards.feature_type}_RPM_folds.txt"),
                index_col=["gene", "cosmid", "name"],
                usecols=["gene", "cosmid", "name", "mean_log2_RPM_fold"])["mean_log2_RPM_fold"] for contrast in IP_CONTRASTS}).to_csv(
                    output.all_folds, sep="\t")

rule gather_remapped_RPM_folds:
    """Gathers RPM folds across contrasts."""
    input:
        fold_results = expand(OPJ(
            mapping_dir, "RPM_by_{{norm}}_folds_%s" % size_selected,
            "{contrast}", "remapped", "{contrast}_{{read_type}}_{{mapping_type}}_{{biotype}}_{{orientation}}_{{feature_type}}_RPM_by_{{norm}}_folds.txt"), contrast=IP_CONTRASTS),
    output:
        all_folds = OPJ(
            mapping_dir, "RPM_by_{norm}_folds_%s" % size_selected, "all", "remapped", "{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}_mean_log2_RPM_by_{norm}_fold.txt"),
    log:
        log = OPJ(log_dir, "gather_RPM_by_{norm}_folds", "{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}.log"),
    benchmark:
        OPJ(log_dir, "gather_RPM_by_{norm}_folds", "{read_type}_{mapping_type}_{biotype}_{orientation}_{feature_type}_benchmark.txt"),
    run:
        pd.DataFrame({contrast : pd.read_table(
            # OPJ(mapping_dir, f"RPM_folds_{size_selected}",
            #     f"{contrast}", f"{contrast}_{wildcards.counted_type}_RPM_folds.txt"),
            OPJ(mapping_dir, f"RPM_by_{wildcards.norm}_folds_{size_selected}",
                f"{contrast}", "remapped", f"{contrast}_{wildcards.read_type}_{wildcards.mapping_type}_{wildcards.biotype}_{wildcards.orientation}_{wildcards.feature_type}_RPM_by_{wildcards.norm}_folds.txt"),
            index_col=["gene", "cosmid", "name"],
            usecols=["gene", "cosmid", "name", "mean_log2_RPM_fold"])["mean_log2_RPM_fold"] for contrast in IP_CONTRASTS}).to_csv(
                output.all_folds, sep="\t")


# TODO update consumers of remapped_RPM_folds
# TODO add remapped_RPM_folds
@wc_applied
def source_gathered_folds(wildcards):
    if hasattr(wildcards, "counted_type"):
        return rules.gather_remapped_RPM_folds.output.all_folds
    #elif wildcards.fold_type == "mean_log2_RPM_fold":
    elif wildcards.fold_type in RPM_FOLD_TYPES:
        assert wildcards.fold_type.startswith("mean_log2_RPM_by_")
        assert wildcards.fold_type.endswith("_fold")
        wildcards.norm = wildcards.fold_type[len("mean_log2_RPM_by_"):-len("_fold")]
        return rules.gather_RPM_folds.output.all_folds
    else:
        return rules.gather_DE_folds.output.all_folds


def plot_fold_heatmap(data, gene_colours=None, labels_dict=None):
    """*labels_dict* associate with each category label a pair (colour, count)."""
    if gene_colours is None:
        row_colors = None
    else:
        row_colors = gene_colours.loc[data.index]
    usetex = mpl.rcParams.get("text.usetex", False)
    if usetex:
        data.columns = [texscape(colname) for colname in data.columns]
    try:
        g = sns.clustermap(data, col_cluster=False, yticklabels=False, row_colors=row_colors, cbar_kws={"label" : "log2fold"})
    except RecursionError:
        # https://stackoverflow.com/a/18005236/1878788
        reclim = sys.getrecursionlimit()
        sys.setrecursionlimit(5 * reclim)
        g = sns.clustermap(data, col_cluster=False, yticklabels=False, row_colors=row_colors, cbar_kws={"label" : "log2fold"})
        sys.setrecursionlimit(reclim)
    # Add legend for row colours
    # https://stackoverflow.com/a/46638518/1878788
    #legend_TN = [mpatches.Patch(color=c, label=l) for (c, l) in df[['tissue type','label']].drop_duplicates().values]
    #legend = g.ax.legend()
    # Or https://stackoverflow.com/a/27992943/1878788
    if labels_dict is not None:
        # To find original label from displayed label:
        dislab2label = {}
        label2dislab = {}
        for (label, (colour, count)) in labels_dict.items():
            display_label = f"{label} ({count})"
            if usetex:
                display_label = texscape(display_label)
            dislab2label[display_label] = label
            label2dislab[label] = display_label
            g.ax_col_dendrogram.bar(0, 0, color=colour, label=display_label)
        if "pi" in labels_dict:
            # Assume we are dealing with small_type, try to order the legend entries
            # TODO: this is a hack with no generality
            # How to automaticaly partition list(chain(*small_type_series.values()))
            # in groups of max(map(len, small_type_series.values())), padding with ""s ?
            # TODO: Update this with new small types
            # TODO (09/11/2021): Use third placeholder to add tRF?
            label_order = ["pi", "mi", "", "prot_sisiu", "pseu_sisiu", "", "te_sisiu", "satel_sisiu", "simrep_sisiu"]
            handles, display_labels = g.ax_col_dendrogram.get_legend_handles_labels()
            label2handle = dict(zip([dislab2label[dislab] for dislab in display_labels], handles))
            dummy_handle = mpl.patches.Rectangle((0, 0), 0, 0, edgecolor="none", facecolor="none", fill=False)
            g.ax_col_dendrogram.legend(
                [label2handle.get(label, dummy_handle) for label in label_order],
                [label2dislab.get(label, "") for label in label_order],
                loc="center", ncol=3)
        else:
            g.ax_col_dendrogram.legend(loc="center", ncol=3)
    # Rotate column labels
    plt.setp(g.ax_heatmap.get_xticklabels(), rotation=90)
    # Adjust the subplot to avoid cutting the column labels
    # do this before moving the colour bar (https://stackoverflow.com/a/47352457/1878788)
    plt.subplots_adjust(bottom=0.5)
    # Remove the dendrogram (https://stackoverflow.com/a/42418968/1878788)
    g.ax_row_dendrogram.set_visible(False)
    # Use the dendrogram box to reposition the colour bar
    dendro_box = g.ax_row_dendrogram.get_position()
    dendro_box.x0 = (dendro_box.x0 + 2 * dendro_box.x1) / 3
    g.cax.set_position(dendro_box)
    # Move the ticks and labels to the left (https://stackoverflow.com/a/36939552/1878788)
    g.cax.yaxis.set_ticks_position("left")
    g.cax.yaxis.set_label_position("left")


#class DefaultCounter(defaultdict, Counter):
#    pass

# TODO: Check if errors due to bad data can be caught: dendrogram excessive recursion and
# TypeError in line 1750 of /pasteur/homes/bli/src/bioinfo_utils/small_RNA-seq/small_RNA-seq.snakefile:
# unsupported operand type(s) for -: 'str' and 'int'
#   File "/pasteur/homes/bli/src/bioinfo_utils/small_RNA-seq/small_RNA-seq.snakefile", line 1750, in __rule_make_fold_heatmap
#   File "/home/bli/.local/lib/python3.6/site-packages/pandas/io/parsers.py", line 709, in parser_f
#   File "/home/bli/.local/lib/python3.6/site-packages/pandas/io/parsers.py", line 455, in _read
#   File "/home/bli/.local/lib/python3.6/site-packages/pandas/io/parsers.py", line 1069, in read
#   File "/home/bli/.local/lib/python3.6/site-packages/pandas/io/parsers.py", line 1846, in read
#   File "/home/bli/.local/lib/python3.6/site-packages/pandas/io/parsers.py", line 3221, in _get_empty_meta
rule make_fold_heatmap:
    """With a full list of genes, this rule can use a lot of memory."""
    input:
        all_folds = source_gathered_folds,
    output:
        fold_heatmap = OPJ("figures", "fold_heatmaps", "{small_type}_{fold_type}_heatmap.pdf"),
    log:
        warnings = OPJ(log_dir, "make_fold_heatmap", "{small_type}_{fold_type}.warnings"),
    benchmark:
        OPJ(log_dir, "make_fold_heatmap", "{small_type}_{fold_type}_benchmark.txt"),
    threads: 16  # to limit memory usage, actually
    resources:
        mem_mb=8150
    run:
        with warn_context(log.warnings) as warn:
            usetex = mpl.rcParams.get("text.usetex", False)
            all_folds = pd.read_table(input.all_folds, index_col=["gene"])
            #gene_colours = all_folds.small_type.map(small_type2colour)
            # Faster
            gene_colours = all_folds.apply(small_type_colour_setter, axis=1)
            if usetex:
                gene_colours.name = texscape(f"{wildcards.small_type}_{wildcards.fold_type}")
            else:
                gene_colours.name = f"{wildcards.small_type}_{wildcards.fold_type}"
            #all_folds.drop(["cosmid", "name", "small_type"], axis=1)
            #all_folds[all_folds.columns.difference(["cosmid", "name", "small_type"])]
            # https://github.com/mwaskom/seaborn/issues/1262
            #mpl.use("agg")
            try:
                #labels_dict = merge_with(tuple, small_type2colour, DefaultCounter(all_folds.small_type))
                # https://stackoverflow.com/a/47396625/1878788
                small_type_counts = Counter(small_type2colour.keys())
                small_type_counts.update(all_folds.small_type)
                labels_dict = {small_type : (small_type2colour[small_type], count - 1) for (small_type, count) in small_type_counts.items()}
                save_plot(
                    output.fold_heatmap, plot_fold_heatmap,
                    all_folds.drop(["cosmid", "name", "small_type"], axis=1),
                    gene_colours, labels_dict, tight=False, rasterize=True)
            except ValueError as err:
                if str(err) == "The number of observations cannot be determined on an empty distance matrix.":
                    warn("\n".join([
                        "Got ValueError:",
                        f"{str(err)}",
                        f"No data to plot in {output.fold_heatmap}\n"]))
                    warn("Generating empty file.\n")
                    # Make the file empty
                    open(output.fold_heatmap, "w").close()
                else:
                    print(labels_dict)
                    raise
            #mpl.use("PDF")


#def plot_norm_counts(counts_data, summaries):
#    fig, axes = plt.subplots(nrows=len(SIZE_FACTORS), ncols=1)
#    for ax, normalizer in zip(axes.ravel(), SIZE_FACTORS):
#        if normalizer == "median_ratio_to_pseudo_ref":
#            # Adapted from DESeq paper (doi:10.1186/gb-2010-11-10-r106) but
#            # add pseudo-count to compute the geometric mean, then remove it
#            pseudo_ref = (counts_data + 1).apply(gmean, axis=1) - 1
#            def median_ratio_to_pseudo_ref(col):
#                return (col / pseudo_ref).median()
#            size_factors = counts_data.apply(median_ratio_to_pseudo_ref, axis=0)
#        else:
#            size_factors = summaries.loc[normalizer]
#        by_norm = counts_data / size_factors
#        # TODO: try to plot with semilog x axis
#        np.log10(by_norm[counts_data.prod(axis=1) > 0]).plot.kde(ax=ax, legend=None)
#        ax.set_xlabel("log10(counts)\n(%s)" % normalizer)
#        ax.legend(ncol=len(REPS))


rule test_size_factor:
    input:
        counts_table = source_small_RNA_counts,
        summary_table = rules.gather_read_counts_summaries.output.summary_table,
    output:
        norm_correlations = OPJ("figures", "{small_type}_norm_correlations.pdf"),
        norm_counts_distrib_plot = OPJ("figures", "{small_type}_norm_counts_distrib.pdf"),
    params:
        size_factor_types = TESTED_SIZE_FACTORS,
        norm_corr_plot_title = lambda wildcards: f"Correlation across samples between normalized {wildcards.small_type}RNA counts and size factors.",
        counts_distrib_plot_title = lambda wildcards: f"Normalized {wildcards.small_type}RNA counts distributions\n(size factor: {{}})"
    log:
        warnings = OPJ(log_dir, "test_size_factor", "{small_type}.warnings"),
    benchmark:
        OPJ(log_dir, "test_size_factor", "{small_type}_benchmark.txt"),
    threads: 8  # to limit memory usage, actually
    run:
        with warn_context(log.warnings) as warn:
            counts_data = pd.read_table(
                input.counts_table,
                index_col=0,
                na_filter=False)
            # Integer overflow may happen and result in negative numbers
            # filter_nozero = counts_data.prod(axis=1) > 0
            filter_nozero = counts_data.prod(axis=1) != 0
            if len(counts_data) > 1 and any(filter_nozero):
                summaries = pd.read_table(input.summary_table, index_col=0)
                correlations = pd.concat((
                    size_factor_correlations(counts_data, summaries, normalizer)
                    for normalizer in params.size_factor_types), axis=1)
                correlations.columns = params.size_factor_types
                save_plot(
                    output.norm_correlations, plot_norm_correlations, correlations,
                    title=params.norm_corr_plot_title)
                #correlations.plot.kde()
                # TODO: Normalizations:
                # counts_data / summaries.loc[normalizer]
                # Get an idea of read counts distribution:
                # The filter amounts to counts_data.mean(axis=1) > 4
                #np.log10(counts_data[counts_data.sum(axis=1) > 4 * len(counts_data.columns)] + 1).plot.kde()
                #np.log10(counts_data[counts_data.prod(axis=1) > 0]).plot.kde()
                pp = PdfPages(output.norm_counts_distrib_plot)
                for normalizer in params.size_factor_types:
                    if normalizer == "median_ratio_to_pseudo_ref":
                        ## Adapted from DESeq paper (doi:10.1186/gb-2010-11-10-r106) but
                        ## add pseudo-count to compute the geometric mean, then remove it
                        ##pseudo_ref = (counts_data + 1).apply(gmean, axis=1) - 1
                        ## Ignore lines with zeroes instead:
                        #pseudo_ref = (counts_data[counts_data.prod(axis=1) > 0]).apply(gmean, axis=1)
                        #def median_ratio_to_pseudo_ref(col):
                        #    return (col / pseudo_ref).median()
                        ##size_factors = counts_data.apply(median_ratio_to_pseudo_ref, axis=0)
                        #size_factors = counts_data[counts_data.prod(axis=1) > 0].apply(median_ratio_to_pseudo_ref, axis=0)
                        size_factors = median_ratio_to_pseudo_ref_size_factors(counts_data)
                    else:
                        size_factors = summaries.loc[normalizer]
                    by_norm = counts_data / size_factors
                    assert len(by_norm) * len(by_norm.columns) >= 1
                    data = np.log10(by_norm[filter_nozero])
                    try:
                        xlabel = "log10(normalized counts)"
                        save_plot(pp, plot_counts_distribution, data, xlabel,
                            format="pdf",
                            title=params.counts_distrib_plot_title.format(normalizer))
                    except TypeError as e:
                        if str(e) in NO_DATA_ERRS:
                            warn("\n".join([
                                "Got TypeError:",
                                f"{str(e)}",
                                f"No data to plot for {normalizer}\n"]))
                        else:
                            raise
                    except LinAlgError as e:
                        if str(e) == "singular matrix":
                            warn("\n".join([
                                "Got LinAlgError:", f"{str(e)}",
                                f"Data cannot be plotted for {normalizer}",
                                f"{data}\n"]))
                        else:
                            raise
                    except ValueError as e:
                        if str(e) in {"`dataset` input should have multiple elements.", "array must not contain infs or NaNs"}:
                            warn("\n".join([
                                "Got ValueError:", f"{str(e)}",
                                f"Data cannot be plotted for {normalizer}",
                                f"{data}\n"]))
                        else:
                            raise
                pp.close()
            else:
                # assert len(counts_data) > 1, "Counts data with only one row cannot have its distribution estimated using KDE."
                warn(f"Not enough data to make those files:\n{output.norm_correlations}\n{output.norm_counts_distrib_plot}\n")
                plot_text(output.norm_correlations, "Not enough data.", params.norm_corr_plot_title)
                plot_text(output.norm_counts_distrib_plot, "Not enough data.", params.counts_distrib_plot_title)


rule standardize_counts:
    input:
        #counts_table = rules.gather_small_RNA_counts.output.counts_table,
        counts_table = source_small_RNA_counts,
    output:
        standardized_table = OPJ(annot_counts_dir, "all_{mapped_type}_on_%s" % genome, "{small_type}_{standard}.txt"),
    run:
        counts_data = pd.read_table(
            input.counts_table,
            index_col=0,
            na_filter=False)
        if wildcards.standard == "identity":
            # Doesn't work because we need to transpose
            os.symlink(os.path.abspath(input.counts_table), output.standardized_table)
            #standardized = counts_data.T.assign(
            #    cond_name=pd.Series(COND_NAMES).values).set_index("cond_name")
        else:
            # Potentially interesting reference about feature scaling:
            # http://sebastianraschka.com/Articles/2014_about_feature_scaling.html
            if wildcards.standard == "minmax":
                scaler = preprocessing.MinMaxScaler().fit(counts_data.T)
                #scale = preprocessing.minmax_scale
            elif wildcards.standard == "unit":
                scaler = preprocessing.Normalizer().fit(counts_data.T)
                #scale = preprocessing.normalize
            elif wildcards.standard == "robust":
                scaler = preprocessing.RobustScaler().fit(counts_data.T)
                #scale = preprocessing.robust_scale
            elif wildcards.standard == "zscore":
                scaler = preprocessing.StandardScaler().fit(counts_data.T)
                #scale = preprocessing.scale
            else:
                raise NotImplementedError("Unknown standardization: %s" % wildcards.standard)
            standardized = pd.DataFrame(scaler.transform(counts_data.T)).assign(
                cond_name=pd.Series(COND_NAMES).values).set_index("cond_name")
            # standardized = pd.DataFrame(scale(counts_data.T)).assign(
            #     cond_name=pd.Series(COND_NAMES).values).set_index("cond_name")
            standardized.columns = counts_data.index
            standardized.T.to_csv(output.standardized_table, sep="\t")


# Note that bamCoverage inverses strands:
# https://github.com/fidelram/deepTools/issues/494
def bamcoverage_filter(wildcards):
    if wildcards.orientation == "fwd":
        return "--filterRNAstrand reverse"
    elif wildcards.orientation == "fwd":
        return "--filterRNAstrand forward"
    else:
        return ""


# Note: It seems that running several bamCoverage at the same time (using snakemake -j <n>)
# can result in strange failures, like the following:
# Traceback (most recent call last):
#   File "/home/bli/.local/bin/bamCoverage", line 11, in <module>
#     main(args)
#   File "/home/bli/.local/lib/python3.6/site-packages/deeptools/bamCoverage.py", line 255, in main
#     format=args.outFileFormat, smoothLength=args.smoothLength)
#   File "/home/bli/.local/lib/python3.6/site-packages/deeptools/writeBedGraph.py", line 164, in run
#     chrom_names_and_size, bedgraph_file, out_file_name, True)
#   File "/home/bli/.local/lib/python3.6/site-packages/deeptools/writeBedGraph.py", line 296, in bedGraphToBigWig
#     f = open("{}.sorted".format(_file.name))
# FileNotFoundError: [Errno 2] No such file or directory: '/tmp/tmpsgluqboy.sorted
rule make_bigwig:
    input:
        bam = rules.sam2indexedbam.output.sorted_bam,
    output:
        bigwig = OPJ(mapping_dir, "{lib}_{rep}", "{lib}_{rep}_{read_type}_on_%s_{orientation}.bw" % genome),
    params:
        orient_filter = bamcoverage_filter,
    threads: 8  # to limit memory usage, actually
    log:
        log = OPJ(log_dir, "make_bigwig", "{lib}_{rep}_{read_type}_{orientation}.log"),
        err = OPJ(log_dir, "make_bigwig", "{lib}_{rep}_{read_type}_{orientation}.err"),
    benchmark:
        OPJ(log_dir, "make_bigwig", "{lib}_{rep}_{read_type}_{orientation}_benchmark.txt"),
    run:
        # TODO: make this a function of deeptools version
        no_reads = """Error: The generated bedGraphFile was empty. Please adjust
your deepTools settings and check your input files.
"""
#        no_reads = """[bwClose] There was an error while finishing writing a bigWig file! The output is likely truncated.
#"""
        try:
            shell("""
                cmd="niceload --noswap bamCoverage -b {input.bam} --skipNAs {params.orient_filter} \\
                    -of=bigwig -bs 10 -p={threads} \\
                    -o {output.bigwig} \\
                    1>> {log.log} 2>> {log.err}"
                echo ${{cmd}} >> {log.log}
                eval ${{cmd}} || error_exit "bamCoverage failed"
            """)
        except CalledProcessError as e:
            if last_lines(log.err, 2) == no_reads:
                with open(output.bigwig, "w") as bwfile:
                    bwfile.write("")
            else:
                raise


@wc_applied
def source_norm_file(wildcards):
    if wildcards.norm == "median_ratio_to_pseudo_ref":
        return OPJ(mapping_dir, "annotation",
            f"all_%s_on_{genome}" % size_selected,
            "all_sisiu_median_ratios_to_pseudo_ref.txt")
    else:
        return rules.make_read_counts_summary.output.summary


# TODO: put size factors in summaries?
rule make_normalized_bigwig:
    input:
        bam = rules.sam2indexedbam.output.sorted_bam,
        norm_file = source_norm_file,
        #median_ratios_file = OPJ(mapping_dir, "annotation", f"all_%s_on_{genome}" % size_selected, "all_si_median_ratios_to_pseudo_ref.txt"),
    output:
        bigwig_norm = OPJ(mapping_dir, "{lib}_{rep}", "{lib}_{rep}_{read_type}_on_%s_by_{norm}_{orientation}.bw" % genome),
    params:
        #orient_filter = bamcoverage_filter,
        genome_binned = genome_binned,
    resources:
        mem_mb=2800
    threads: 8  # to limit memory usage, actually
    log:
        log = OPJ(log_dir, "make_normalized_bigwig", "{lib}_{rep}_{read_type}_by_{norm}_{orientation}.log"),
        err = OPJ(log_dir, "make_normalized_bigwig", "{lib}_{rep}_{read_type}_by_{norm}_{orientation}.err"),
        warnings = OPJ(log_dir, "make_normalized_bigwig", "{lib}_{rep}_{read_type}_by_{norm}_{orientation}.warnings"),
    benchmark:
        OPJ(log_dir, "make_normalized_bigwig", "{lib}_{rep}_{read_type}_by_{norm}_{orientation}_benchmark.txt"),
    run:
        with warn_context(log.warnings) as warn:
            if wildcards.norm == "median_ratio_to_pseudo_ref":
                size = float(pd.read_table(
                    input.norm_file, index_col=0, header=None).loc[
                        f"{wildcards.lib}_{wildcards.rep}"])
            else:
                # We normalize by million in order not to have too small values
                size = pd.read_table(input.norm_file).T.loc[wildcards.norm][0] / 1000000
                #scale = 1 / pd.read_table(input.summary, index_col=0).loc[
                #    wildcards.norm_file].loc[f"{wildcards.lib}_{wildcards.rep}"]
            assert size > 0
            no_reads = """Error: The generated bedGraphFile was empty. Please adjust
your deepTools settings and check your input files.
"""
            zero_bytes = """needLargeMem: trying to allocate 0 bytes (limit: 100000000000)
bam2bigwig.sh: bedGraphToBigWig failed
"""
            try:
                # bam2bigwig.sh is installed with libhts
                shell("""
                    niceload --noswap bam2bigwig.sh {input.bam} {params.genome_binned} \\
                        {wildcards.lib}_{wildcards.rep} {wildcards.orientation} F \\
                        %f {output.bigwig_norm} \\
                        > {log.log} 2> {log.err} \\
                        || error_exit "bam2bigwig.sh failed"
                """ % size)
            except CalledProcessError as e:
                if last_lines(log.err, 2) in {no_reads, zero_bytes}:
                    warn(f"{output.bigwig_norm} will be empty.\n")
                    #with open(output.bigwig_norm, "w") as bwfile:
                    #    bwfile.write("")
                    with open(log.err, "a") as errfile:
                        errfile.write("Generating zero-signal bigwig.\n")
                    make_empty_bigwig(output.bigwig_norm, chrom_sizes)
                else:
                    raise


rule merge_bigwig_reps:
    """This rule merges bigwig files by computing a mean across replicates."""
    input:
        expand(OPJ(mapping_dir, "{{lib}}_{rep}", "{{lib}}_{rep}_{{read_type}}_on_%s_by_{{norm}}_{{orientation}}.bw" % genome), rep=REPS),
    output:
        bw = OPJ(mapping_dir, "{lib}_mean", "{lib}_mean_{read_type}_on_%s_by_{norm}_{orientation}.bw" % genome),
    log:
        warnings = OPJ(log_dir, "merge_bigwig_reps", "{lib}_mean_{read_type}_on_%s_by_{norm}_{orientation}.warnings" % genome),
    benchmark:
        OPJ(log_dir, "merge_bigwig_reps", "{lib}_mean_{read_type}_on_%s_by_{norm}_{orientation}_benchmark.txt" % genome),
    resources:
        mem_mb=2700
    threads: 2  # to limit memory usage, actually
    run:
        with warn_context(log.warnings) as warn:
            try:
                bws = [pyBigWig.open(bw_filename) for bw_filename in input]
                #for bw_filename in input:
                #    bws.append(pyBigWig.open(bw_filename))
            except RuntimeError as e:
                warn(str(e))
                warn("Generating empty file.\n")
                # Make the file empty
                # open(output.bw, "w").close()
                make_empty_bigwig(output.bw, chrom_sizes)
            else:
                bw_out = pyBigWig.open(output.bw, "w")
                bw_out.addHeader(list(chrom_sizes.items()))
                for (chrom, chrom_len) in chrom_sizes.items():
                    try:
                        assert all([bw.chroms()[chrom] == chrom_len for bw in bws])
                    except KeyError as e:
                        warn(str(e))
                        warn(f"Chromosome {chrom} might be missing from one of the input files.\n")
                        for filename, bw in zip(input, bws):
                            msg = " ".join([f"{filename}:", *list(bw.chroms().keys())])
                            warn(f"{msg}:\n")
                        #raise
                        warn(f"The bigwig files without {chrom} will be skipped.\n")
                    except AssertionError as e:
                        chrom_lens = []
                        for bw in bws:
                            chrom_lens.append(bw.chroms()[chrom])
                        warn(f"chrom_len for {chrom}: {chrom_len}\nchrom_lens: {chrom_lens}")
                        raise
                    to_use = [bw for bw in bws if chrom in bw.chroms()]
                    if to_use:
                        means = np.nanmean(np.vstack([bw.values(chrom, 0, chrom_len, numpy=True) for bw in to_use]), axis=0)
                    else:
                        means = np.zeros(chrom_len)
                    # bin size is 10
                    bw_out.addEntries(chrom, 0, values=np.nan_to_num(means[0::10]), span=10, step=10)
                bw_out.close()
                for bw in bws:
                    bw.close()


rule make_bigwig_ratios:
    """This rule tries to make a bigwig file displaying the ratio between a condition and a reference, from the corresponding two normalized bigwig files."""
    input:
        cond_bw = OPJ(mapping_dir, "{cond}_{{rep}}", "{cond}_{{rep}}_{{read_type}}_on_%s_by_{{norm}}_{{orientation}}.bw" % genome),
        ref_bw = OPJ(mapping_dir, "{ref}_{{rep}}", "{ref}_{{rep}}_{{read_type}}_on_%s_by_{{norm}}_{{orientation}}.bw" % genome),
    output:
        bw = OPJ(mapping_dir, "{cond}_vs_{ref}_ratio", "{cond}_vs_{ref}_ratio_{read_type}_on_%s_by_{norm}_{orientation}.bw" % genome),
    log:
        warnings = OPJ(log_dir, "make_bigwig_ratios", "{cond}_vs_{ref}_ratio_{read_type}_on_%s_by_{norm}_{orientation}.warnings" % genome),
    benchmark:
        OPJ(log_dir, "make_bigwig_ratios", "{cond}_vs_{ref}_ratio_{read_type}_on_%s_by_{norm}_{orientation}_benchmark.txt" % genome),
    threads: 8  # to limit memory usage, actually
    run:
        with warn_context(log.warnings) as warn:
            try:
                cond_bw = pyBigWig.open(input.cond_bw)
                ref_bw = pyBigWig.open(input.ref_bw)
                #for bw_filename in input:
                #    bws.append(pyBigWig.open(bw_filename))
            except RuntimeError as e:
                warn(str(e))
                warn("Generating empty file.\n")
                # Make the file empty
                # open(output.bw, "w").close()
                make_empty_bigwig(output.bw, chrom_sizes)
            else:
                bw_out = pyBigWig.open(output.bw, "w")
                bw_out.addHeader(list(chrom_sizes.items()))
                for (chrom, chrom_len) in bw_out.chroms().items():
                    assert cond_bw.chroms()[chrom] == chrom_len
                    #ratios = np.divide(cond_bw.values(chrom, 0, chrom_len), ref_bw.values(chrom, 0, chrom_len))
                    ratios = np.log2(np.divide(cond_bw.values(chrom, 0, chrom_len, numpy=True), ref_bw.values(chrom, 0, chrom_len, numpy=True)))
                    # bin size is 10
                    bw_out.addEntries(chrom, 0, values=np.nan_to_num(ratios[0::10]), span=10, step=10)
                bw_out.close()
                for bw in bws:
                    bw.close()


def source_bigwig(wildcards):
    #if not hasattr(wildcards, "rep"):
    #    return rules.merge_bigwig_reps.output.bw
    if wildcards.rep != "mean":
        return rules.make_normalized_bigwig.output.bigwig_norm,
    else:
        return rules.merge_bigwig_reps.output.bw,

# For metagene analyses:
#- Extract transcripts
#- Take only genes with TSS identified, among isoforms of a same gene, take the 3'end closest to the TSS
#- Avoid overlap between gene1 UTR and a gene2 (all biotypes except piRNA and antisense) UTR or TSS based on the 3-primes furthest of the TSS of their gene

# TODO: Do another analysis, less stringent. Do this for "replication-dependent" histone genes. Exclude only if transcripts overlap (after considering only the largest spanning isoform). Or try with just upstream and downstream set to 0: META_MARGIN=0, UNSCALED_INSIDE=0, META_SCALE=500
# meta-profile of data mapped on the transcriptome: we look at 22G RNA that come from transcript templates

def allowed_biotypes(type_set):
    """Returns a set of biotypes that will be considered when building a set of
    non-interfering annotations. The larger the number of biotypes, the more
    likely an annotation of a given biotype will be excluded because of some
    interference. """
    if type_set == "all":
        return {
            "antisense",
            "lincRNA",
            "miRNA",
            "ncRNA",
            "piRNA",
            "protein_coding",
            "pseudogene",
            "rRNA",
            "snoRNA",
            "snRNA",
            "tRNA",
            "DNA_transposons_rmsk",
            "RNA_transposons_rmsk",
            }
    elif type_set == "protein_coding":
        return {"protein_coding", "pseudogene"}
    elif type_set == "protein_coding_TE":
        return {
            "protein_coding",
            "pseudogene",
            "DNA_transposons_rmsk",
            "RNA_transposons_rmsk",
            }
    else:
        raise NotImplementedError("Metagene analyses for %s not implemented." % type_set)


rule extract_transcripts:
    input:
        # We want to get back to the original "protein_coding" annotations,
        # not separated by UTR or CDS
        #gtf = rules.gather_annotations.output.merged_gtf,
        #gtf = OPJ(annot_dir, "genes.gtf"),
        #DNA_transposon_gtf = OPJ(annot_dir, "DNA_transposons_rmsk.gtf"),
        #RNA_transposon_gtf = OPJ(annot_dir, "RNA_transposons_rmsk.gtf"),
        OPJ(annot_dir, "genes.gtf"),
        OPJ(annot_dir, "DNA_transposons_rmsk.gtf"),
        OPJ(annot_dir, "RNA_transposons_rmsk.gtf"),
    output:
        bed = OPJ(local_annot_dir, "transcripts_{type_set}.bed"),
    run:
        with finput(files=input) as gtf_source, open(output.bed, "w") as bed_dest:
            for (chrom, _, bed_type, gtf_start, end,
                 score, strand, _, annot_field) in map(strip_split, gtf_source):
                if bed_type != "transcript":
                    continue
                annots = dict([(k, v.rstrip(";").strip('"')) for (k, v) in [
                    f.split(" ") for f in annot_field[:-1].split("; ")]])
                gene_biotype = annots["gene_biotype"]
                # piRNA should not be highly expressed
                #if gene_biotype in {"miRNA", "piRNA", "antisense"}:
                if gene_biotype not in allowed_biotypes(wildcards.type_set):
                    continue
                transcript_id = annots["transcript_id"]
                gene_id = annots["gene_id"]
                print(chrom, int(gtf_start) - 1, end,
                      gene_id, gene_biotype, strand,
                      sep="\t", file=bed_dest)
    # shell:
    #     """
    #     extract_transcripts_from_gtf.py \\
    #         -g Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes.gtf \\
    #         -o {params.annot_dir} \\
    #         -i "piRNA" "antisense" \\
    #         || error_exit "extract_transcripts failed"
    #     """

#- Extract transcripts
#- Take only genes with TSS identified, among isoforms of a same gene, take the 3'end closest to the TSS
#- Avoid overlap between gene1 UTR and a gene2 (all biotypes except piRNA and antisense) UTR or TSS based on the 3-primes furthest of the TSS of their gene


class Gene(object):
    """This is used to fuse coordinates of transcripts deriving from a same gene.
    *wide_end* corresponds to the furthest-extending 3' end. It will be used to
    determine possible "interferences" (risks of small RNA confusion) between genes."""
    __slots__ = ("gene_id", "chrom", "start", "tight_end", "wide_end", "biotype", "strand")
    def __init__(self, gene_id, chrom, start, end, strand, biotype):
        self.gene_id = gene_id
        self.chrom = chrom
        self.biotype = biotype
        self.strand = strand
        if strand == "+":
            self.start = int(start)
            self.tight_end = int(end)
            self.wide_end = int(end)
        else:
            self.start = int(end)
            self.tight_end = int(start)
            self.wide_end = int(start)

    def add_transcript(self, gene_id, chrom, start, end, strand):
        # assert gene_id == self.gene_id
        assert chrom == self.chrom
        assert strand == self.strand
        if strand == "+":
            self.start = min(int(start), self.start)
            self.tight_end = min(self.tight_end, int(end))
            self.wide_end = max(self.wide_end, int(end))
        else:
            self.start = max(int(end), self.start)
            self.tight_end = max(self.tight_end, int(start))
            self.wide_end = min(self.wide_end, int(start))

    @property
    def left(self):
        if self.strand == "+":
            return self.start
        else:
            return self.wide_end

    @property
    def right(self):
        if self.strand == "+":
            return self.wide_end
        else:
            return self.start

    def too_close_before(self, other, min_dist):
        """Returns True if *other* is strictly less than *min_dist*
        away after *self*."""
        return (other.left - self.right) - 1 < min_dist

    def tight_bed(self):
        if self.strand == "+":
            return "\t".join([
                self.chrom, str(self.start), str(self.tight_end),
                self.gene_id, self.biotype, self.strand])
        else:
            return "\t".join([
                self.chrom, str(self.tight_end), str(self.start),
                self.gene_id, self.biotype, self.strand])


# TODO: make TSS bed file a configurable genome-dependent thing
rule adjust_TSS:
    """Extends transcript coordinates if a new TSS can be found
    in the data provided by Kruesi et al 2013 (for L3 stage)."""
    input:
        in_bed = rules.extract_transcripts.output.bed,
    output:
        out_bed = OPJ(local_annot_dir, "transcripts_{type_set}", "with_TSS.bed"),
    run:
        TSS_dict = defaultdict(set)
        with open("/pasteur/entites/Mhe/Genomes/C_elegans/TSS_annotations/Kruesi_TSS_coding_WT_L3_ce11_sorted.bed", "r") as TSS_bedfile:
            for chrom, bed_start, _, gene_info in map(strip_split, TSS_bedfile):
                # chrI	35384	35385	WBGene00022279|sesn-1@chrI:27595-32482|-1
                # chrI	47149	47150	WBGene00044345|Y48G1C.12@chrI:47472-49819|1
                # chrI	70172	70173	WBGene00000812|csk-1@chrI:71858-81071|1
                # chrI	110690	110691	WBGene00004274|rab-11.1@chrI:108686-110077|-1
                gene_id, _, strand = gene_info.split("|")
                if strand == "1":
                    strand = "+"
                elif strand == "-1":
                    strand = "-"
                else:
                    raise NotImplementedError("Unexpected strand information: %s" % strand)
                TSS_dict[gene_id].add((chrom, bed_start, strand))
        with open(input.in_bed, "r") as in_bedfile, open(output.out_bed, "w") as out_bedfile:
            for (chrom, start, end, gene_id, gene_biotype, strand) in map(strip_split, in_bedfile):
                if gene_id in TSS_dict:
                    # The set should have only one element
                    ((tss_chrom, tss_start, tss_strand),) = TSS_dict[gene_id]
                    assert tss_chrom == "chr%s" % chrom
                    assert tss_strand == strand
                    # TODO: check this is correct
                    if strand == "+":
                        #assert int(tss_start) <= int(start), "%s has a problem:\ntss_start:%s\tstart:%s" % (gene_id, tss_start, start)
                        #if int(tss_start) > int(start):
                        #    print("%s (+) has a problem:\ntss_start:%s\tstart:%s" % (gene_id, tss_start, start))
                        #    continue
                        start = tss_start
                    else:
                        #assert int(tss_start) >= int(end)
                        #if int(tss_start) < int(end):
                        #    print("%s (-) has a problem:\ntss_start:%s\tend:%s" % (gene_id, tss_start, end))
                        #    continue
                        end = tss_start
                print(chrom, start, end, gene_id, gene_biotype, strand, sep="\t", file=out_bedfile)

# TODO: merge also with transposon
rule merge_transcripts:
    """Determine the span of each gene, based on its transcripts spans."""
    input:
        in_bed = rules.adjust_TSS.output.out_bed,
    output:
        out_bed = OPJ(local_annot_dir, "transcripts_{type_set}", "merged_by_gene.bed"),
    run:
        # Use OrderedDict in the hope that this will speed up the sorting
        genes = OrderedDict()
        with open(input.in_bed, "r") as in_bedfile:
            for (chrom, start, end, gene_id, gene_biotype, strand) in map(strip_split, in_bedfile):
                if gene_id in genes:
                    genes[gene_id].add_transcript(gene_id, chrom, start, end, strand)
                else:
                    genes[gene_id] = Gene(gene_id, chrom, start, end, strand, gene_biotype)
        with open(output.out_bed, "w") as out_bedfile:
            for gene in genes.values():
                print(
                    gene.chrom, str(gene.left), str(gene.right),
                    gene.gene_id, gene.biotype, gene.strand,
                    sep="\t", file=out_bedfile)


rule resort_transcript_bed:
    input:
        in_bed = rules.merge_transcripts.output.out_bed,
    output:
        out_bed = OPJ(local_annot_dir, "transcripts_{type_set}", "merged_resorted.bed"),
    shell:
        """
        # This bedops command checks bed format and finds problems like
        # Error on line 36447 in annotations/transcripts_all/merged_by_gene.bed. Genomic end coordinate is less than (or equal to) start coordinate.
        #sort-bed {input.in_bed} > {output.out_bed}
        sort -k1,1 -k2,2n -k3,3n {input.in_bed} > {output.out_bed}
        """


rule filter_out_interfering_transcripts:
    input:
        in_bed = rules.resort_transcript_bed.output.out_bed,
    output:
        out_bed = OPJ(local_annot_dir, "transcripts_{type_set}", "merged_isolated_{min_dist}.bed"),
    run:
        last_chrom = ""
        # Zone occupied by genes previously encountered
        span = Gene("span", "", -int(wildcards.min_dist), -int(wildcards.min_dist), "+", "span")
        previous_gene = span
        with open(input.in_bed, "r") as in_bedfile, open(output.out_bed, "w") as out_bedfile:
            for (chrom, start, end, gene_id, gene_biotype, strand) in map(strip_split, in_bedfile):
                # This happens at the first entrance in the loop because chrom != ""
                if chrom != last_chrom:
                    # see if we have a previous_gene that was not "discarded"
                    if previous_gene.gene_id != "span":
                        print(previous_gene.tight_bed(), sep="\t", file=out_bedfile)
                    # Reset stuff
                    last_chrom = chrom
                    # Zone occupied by genes previously encountered
                    span = Gene("span", chrom, -int(wildcards.min_dist), -int(wildcards.min_dist), "+", "span")
                    previous_gene = span
                this_gene = Gene(gene_id, chrom, start, end, strand, gene_biotype)
                # This code assumes that bed entries are sorted
                # by increasing order of start coordinate, then end coordinate
                if not span.too_close_before(this_gene, int(wildcards.min_dist)):
                    # If nothing is too close before this_gene,
                    # then the previous gene will not be too close
                    # before other genes that come later
                    # we can write it in the output if no gene was too close before it
                    if previous_gene.gene_id != "span":
                        print(previous_gene.tight_bed(), sep="\t", file=out_bedfile)
                    # this_gene will have the possibility to be written next iteration
                    previous_gene = this_gene
                else:
                    # this_gene is too close to the previous ones.
                    # We use this to "discard" this_gene
                    previous_gene = span
                span.add_transcript("span", chrom, start, end, "+")


rule select_genes_for_meta_profile:
    """Creates a bed file for metagene analysis based on
    (TSS being known: not now)
    and length being enough."""
    input:
        in_bed = rules.filter_out_interfering_transcripts.output.out_bed
    output:
        out_bed = OPJ(local_annot_dir, "transcripts_{type_set}", "merged_isolated_{min_dist}_{biotype}_min_{min_len}.bed"),
    run:
        TSS_dict = defaultdict(set)
        with open("/pasteur/entites/Mhe/Genomes/C_elegans/TSS_annotations/Kruesi_TSS_coding_WT_L3_ce11_sorted.bed", "r") as TSS_bedfile:
            for chrom, bed_start, _, gene_info in map(strip_split, TSS_bedfile):
                # chrI	35384	35385	WBGene00022279|sesn-1@chrI:27595-32482|-1
                # chrI	47149	47150	WBGene00044345|Y48G1C.12@chrI:47472-49819|1
                # chrI	70172	70173	WBGene00000812|csk-1@chrI:71858-81071|1
                # chrI	110690	110691	WBGene00004274|rab-11.1@chrI:108686-110077|-1
                gene_id, _, strand = gene_info.split("|")
                if strand == "1":
                    strand = "+"
                elif strand == "-1":
                    strand = "-"
                else:
                    raise NotImplementedError("Unexpected strand information: %s" % strand)
                TSS_dict[gene_id].add((chrom, bed_start, strand))
        with open(input.in_bed, "r") as in_bedfile, open(output.out_bed, "w") as out_bedfile:
            for (chrom, start, end, gene_id, gene_biotype, strand) in map(strip_split, in_bedfile):
                if wildcards.biotype != gene_biotype:
                    continue
                #assert wildcards.biotype == gene_biotype
                #if (gene_biotype == "protein_coding") and (gene_id not in TSS_dict):
                #    continue
                if int(end) - int(start) < META_MIN_LEN:
                    continue
                print(
                    chrom, start, end,
                    gene_id, gene_biotype, strand,
                    sep="\t", file=out_bedfile)


def source_gene_list(wildcards):
    list_filename = OPJ(gene_lists_dir, f"{wildcards.id_list}_ids.txt")
    if list_filename in avail_id_lists:
        return list_filename
    elif wildcards.id_list == "all_genes":
        return exon_lengths_file
    else:
        raise NotImplementedError("Location unknown for gene list %s" % wildcards.id_list)


rule link_gene_lists:
    input:
        source_path = source_gene_list,
    output:
        link = OPJ(local_annot_dir, "gene_lists", "{id_list}_ids.txt"),
    run:
        os.symlink(os.path.abspath(input.source_path), output.link)


# TODO: select those that are detected in the annotation phase (-> present in deseq analyses, first column of results/bowtie2/mapped_C_elegans/annotation/all_18-26_on_C_elegans/te_si_counts.txt)
rule select_gene_list_for_meta_profile:
    """Creates a bed file for metagene analysis based on
    (TSS being known: not now)
    and length being enough."""
    input:
        in_bed = rules.filter_out_interfering_transcripts.output.out_bed,
        id_list = rules.link_gene_lists.output.link,
    output:
        out_bed = OPJ(local_annot_dir, "transcripts_{type_set}", "merged_isolated_{min_dist}_{id_list}.bed"),
    run:
        ###
        # Record known TSS coordinates
        ###
        # Not used
        TSS_dict = defaultdict(set)
        with open("/pasteur/entites/Mhe/Genomes/C_elegans/TSS_annotations/Kruesi_TSS_coding_WT_L3_ce11_sorted.bed", "r") as TSS_bedfile:
            for chrom, bed_start, _, gene_info in map(strip_split, TSS_bedfile):
                # chrI	35384	35385	WBGene00022279|sesn-1@chrI:27595-32482|-1
                # chrI	47149	47150	WBGene00044345|Y48G1C.12@chrI:47472-49819|1
                # chrI	70172	70173	WBGene00000812|csk-1@chrI:71858-81071|1
                # chrI	110690	110691	WBGene00004274|rab-11.1@chrI:108686-110077|-1
                gene_id, _, strand = gene_info.split("|")
                if strand == "1":
                    strand = "+"
                elif strand == "-1":
                    strand = "-"
                else:
                    raise NotImplementedError("Unexpected strand information: %s" % strand)
                TSS_dict[gene_id].add((chrom, bed_start, strand))
        # Establish the set of gene ids to keep
        with open(input.id_list) as gene_list_file:
            gene_set = {strip_split(line)[0] for line in gene_list_file}
        with open(input.in_bed, "r") as in_bedfile, open(output.out_bed, "w") as out_bedfile:
            for (chrom, start, end, gene_id, gene_biotype, strand) in map(strip_split, in_bedfile):
                #if wildcards.biotype != gene_biotype:
                if gene_id not in gene_set:
                    continue
                #assert wildcards.biotype == gene_biotype
                #if (gene_biotype == "protein_coding") and (gene_id not in TSS_dict):
                #    continue
                #if int(end) - int(start) < META_MIN_LEN:
                #    continue
                print(
                    chrom, start, end,
                    gene_id, gene_biotype, strand,
                    sep="\t", file=out_bedfile)


# # TODO: make path to TSS bed file configurable
# rule plot_TSS_profile:
#     input:
#         source_bigwig,
#     output:
#         OPJ("figures", "{lib}_{rep}", "{read_type}_{orientation}_TSS.pdf"),
#     log:
#         plot_TSS_log = OPJ(log_dir, "plot_TSS_profile", "{lib}_{rep}", "{read_type}_{orientation}.log"),
#         plot_TSS_err = OPJ(log_dir, "plot_TSS_profile", "{lib}_{rep}", "{read_type}_{orientation}.err"),
#     threads: 12  # to limit memory usage, actually
#     shell:
#         """
#         tmpdir=$(mktemp -dt "plot_TSS_{wildcards.lib}_{wildcards.treat}_{wildcards.rep}_{wildcards.read_type}_{wildcards.orientation}.XXXXXXXXXX")
#         computeMatrix reference-point -S {input} \\
#             -R /pasteur/entites/Mhe/Genomes/C_elegans/TSS_annotations/Kruesi_TSS_coding_WT_L3_ce11_sorted.bed \\
#             -p 1 \\
#             -out ${{tmpdir}}/tss_profile.gz \\
#             1> {log.plot_TSS_log} \\
#             2> {log.plot_TSS_err} \\
#             || error_exit "computeMatrix failed"
#         plotProfile -m ${{tmpdir}}/tss_profile.gz -out {output} \\
#             1>> {log.plot_TSS_log} \\
#             2>> {log.plot_TSS_err} \\
#             || error_exit "plotProfile failed"
#         rm -rf ${{tmpdir}}
#         """

# In http://dx.doi.org/10.7554/eLife.00808, p. 27:
# -----
# To compare GRO-seq signal across genes, we scaled genes to be the same length,
# allowing us to average the GRO-seq signal across them. To avoid small genes
# that could affect the sensitivity of our analyses, we required that genes be
# ≥1.5 kb in length. These genes were scaled to the same length as follows: the
# 5′ end (1000 bp upstream to 500 bp downstream of the TSS) and the 3′ end (500
# bp upstream to 1000 bp downstream of the WB stop site) were not scaled, and the
# remainder of the gene was scaled to a length of 2 kb. We predicted that leaving
# the ends of the gene unscaled might allow us to better identify any effects
# that occurred at the ends of genes.
# -----


def meta_params(wildcards):
    try:
        biotype = wildcards.biotype
        if biotype == "protein_coding":
            return " ".join([
                "-b %d" % META_MARGIN,
                "--unscaled5prime %d" % UNSCALED_INSIDE,
                "-m %d" % META_SCALE,
                "--unscaled3prime %d" % UNSCALED_INSIDE,
                "-a %d" % META_MARGIN])
        if biotype in {"DNA_transposons_rmsk", "RNA_transposons_rmsk"}:
            return " ".join([
                "-b %d" % META_MARGIN,
                "--unscaled5prime %d" % UNSCALED_INSIDE,
                "-m %d" % META_SCALE,
                "--unscaled3prime %d" % UNSCALED_INSIDE,
                "-a %d" % META_MARGIN])
        else:
            raise NotImplementedError("Metagene analyses for %s not implemented." % biotype)
    except AttributeError:
        # The biotype wildcard is not defined
        # This should be a gene_list based meta profile
        id_list = wildcards.id_list
        list_filename = OPJ(gene_lists_dir, f"{id_list}_ids.txt")
        if list_filename in avail_id_lists:
            return " ".join([
                "-m 500"])
        else:
            raise NotImplementedError("Metagene analyses for %s not implemented." % id_list)

# TODO: Implement this for PRO-seq
# Interference elimination criteria:
# - For reference-point: closeness between TSS or TES (no problem if pseudogene or TE inside a gene)
# TODO: Make profiles around TSS and TES
# TODO: Make heatmaps
# TODO: Make meta_profile for categories of genes (up- or down- regulated, for instance)
# DONE: merge replicates of same {lib}_{treat} (sum bigwig ?) -> plot_biotype_mean_meta_profile
# TODO: overlap libraries on the same graph
# TODO: What is the 3'UTR peak when using 18-26 ? -> some miRNA (how many?)
#rule plot_meta_profile:
#    input:
#        #bigwig = source_bigwig,
#        bigwig = rules.make_bigwig.output.bigwig_norm,
#        bed = rules.select_genes_for_meta_profile.output.out_bed,
#    output:
#        OPJ("figures", "{lib}_{rep}", "{read_type}_{orientation}_on_{type_set}_merged_isolated_{min_dist}_{biotype}_min_{min_len}_meta_profile.pdf"),
#    params:
#        meta_params = meta_params,
#        # before = META_MARGIN,
#        # after = META_MARGIN,
#        # body_length = META_SCALE,
#        # unscaled_5 = UNSCALED_INSIDE,
#        # unscaled_3 = UNSCALED_INSIDE,
#    log:
#        plot_log = OPJ(log_dir, "plot_meta_profile", "{lib}_{rep}", "{read_type}_{orientation}_on_{type_set}_merged_isolated_{min_dist}_{biotype}_min_{min_len}.log"),
#        plot_err = OPJ(log_dir, "plot_meta_profile", "{lib}_{rep}", "{read_type}_{orientation}_on_{type_set}_merged_isolated_{min_dist}_{biotype}_min_{min_len}.err"),
#    threads: 12  # to limit memory usage, actually
#    shell:
#        """
#        tmpdir=$(mktemp -dt "plot_meta_profile_{wildcards.lib}_{wildcards.treat}_{wildcards.rep}_{wildcards.read_type}_{wildcards.orientation}_{wildcards.type_set}_{wildcards.biotype}_{wildcards.min_len}.XXXXXXXXXX")
#        computeMatrix scale-regions -S {input.bigwig} \\
#            -R {input.bed} \\
#            {params.meta_params} \\
#            -p 1 \\
#            -out ${{tmpdir}}/meta_profile.gz \\
#            1> {log.plot_log} \\
#            2> {log.plot_err} \\
#            || error_exit "computeMatrix failed"
#        plotProfile -m ${{tmpdir}}/meta_profile.gz -out {output} \\
#            1>> {log.plot_log} \\
#            2>> {log.plot_err} \\
#            || error_exit "plotProfile failed"
#        rm -rf ${{tmpdir}}
#        """

# TODO: make version with all {lib}_{treat} combinations on the same graph
# rule plot_meta_profile_mean:
#     input:
#         bigwig = rules.merge_bigwig_reps.output.bw,
#         #bw = OPJ(mapping_dir, "{lib}_mean", "{read_type}_on_%s_norm_{orientation}.bw" % genome),
#         bed = rules.select_genes_for_meta_profile.output.out_bed
#     output:
#         OPJ("figures", "{lib}_mean", "{read_type}_{orientation}_on_merged_isolated_%d_{biotype}_min_%d_meta_profile.pdf" % (MIN_DIST, META_MIN_LEN)),
#     params:
#         meta_params = meta_params,
#         # before = META_MARGIN,
#         # after = META_MARGIN,
#         # body_length = META_SCALE,
#         # unscaled_5 = UNSCALED_INSIDE,
#         # unscaled_3 = UNSCALED_INSIDE,
#     log:
#         plot_TSS_log = OPJ(log_dir, "plot_meta_profile", "{lib}_{treat}", "{read_type}_{orientation}_on_merged_isolated_%d_{biotype}_min_%d.log" % (MIN_DIST, META_MIN_LEN)),
#         plot_TSS_err = OPJ(log_dir, "plot_meta_profile", "{lib}_{treat}", "{read_type}_{orientation}_on_merged_isolated_%d_{biotype}_min_%d.err" % (MIN_DIST, META_MIN_LEN)),
#     threads: 12  # to limit memory usage, actually
#     shell:
#         """
#         tmpdir=$(mktemp -dt "plot_meta_profile_{wildcards.lib}_{wildcards.treat}_{wildcards.read_type}_{wildcards.orientation}_{wildcards.biotype}_%d.XXXXXXXXXX")
#         computeMatrix scale-regions -S {input.bigwig} \\
#             -R {input.bed} \\
#             {params.meta_params} \\
#             -p 1 \\
#             -out ${{tmpdir}}/meta_profile.gz \\
#             1> {log.plot_TSS_log} \\
#             2> {log.plot_TSS_err} \\
#             || error_exit "computeMatrix failed"
#         plotProfile -m ${{tmpdir}}/meta_profile.gz -out {output} \\
#             1>> {log.plot_TSS_log} \\
#             2>> {log.plot_TSS_err} \\
#             || error_exit "plotProfile failed"
#         rm -rf ${{tmpdir}}
#         """ % META_MIN_LEN
def get_libs_for_group(wildcards):
    return lib_groups[wildcards.group_type][wildcards.lib_group]


def source_bigwigs(wildcards):
    return expand(
        OPJ(mapping_dir, "{lib}_mean",
            "{lib}_mean_{read_type}_on_%s_by_{norm}_{orientation}.bw" % genome),
        lib=get_libs_for_group(wildcards),
        read_type=[wildcards.read_type], norm=wildcards.norm, orientation=wildcards.orientation)


def make_y_label(wildcards):
    if wildcards.norm == "median_ratio_to_pseudo_ref":
        return "reads scaled to all_sisiu (DESeq way)"
    else:
        return f"reads per million {wildcards.norm}"


rule plot_biotype_mean_meta_profile:
    input:
        #bigwig = rules.merge_bigwig_reps.output.bw,
        #bws = expand(OPJ(mapping_dir, "{lib}_mean", "{lib}_mean_{{read_type}}_on_%s_by_{{norm}}_{{orientation}}.bw" % genome), lib=LIBS),
        #bws = expand(OPJ(mapping_dir, "{lib}_mean", "{lib}_mean_{{read_type}}_on_%s_by_{{norm}}_{{orientation}}.bw" % genome), lib=lib_groups[wildcards.group_type][wildcards.lib_group]),
        bws = source_bigwigs,
        bed = rules.select_genes_for_meta_profile.output.out_bed
    output:
        OPJ("figures", "mean_meta_profiles_meta_scale_{meta_scale}",
            "{read_type}_by_{norm}_{orientation}_on_{type_set}_merged_isolated_{min_dist}_{biotype}_min_{min_len}_{group_type}_{lib_group}_meta_profile.pdf"),
    params:
        meta_params = meta_params,
        #labels = LIBS,
        labels = get_libs_for_group,
        y_label = make_y_label,
        # before = META_MARGIN,
        # after = META_MARGIN,
        # body_length = META_SCALE,
        # unscaled_5 = UNSCALED_INSIDE,
        # unscaled_3 = UNSCALED_INSIDE,
    log:
        log = OPJ(log_dir, "plot_biotype_meta_profile_meta_scale_{meta_scale}", "{read_type}_by_{norm}_{orientation}_on_{type_set}_merged_isolated_{min_dist}_{biotype}_min_{min_len}_{group_type}_{lib_group}.log"),
        err = OPJ(log_dir, "plot_biotype_meta_profile_meta_scale_{meta_scale}", "{read_type}_by_{norm}_{orientation}_on_{type_set}_merged_isolated_{min_dist}_{biotype}_min_{min_len}_{group_type}_{lib_group}.err"),
    threads: 2  # to limit memory usage, actually
    shell:
        """
        tmpdir=$(TMPDIR=/var/tmp mktemp --tmpdir -d "plot_meta_profile_meta_scale_{wildcards.meta_scale}_{wildcards.read_type}_by_{wildcards.norm}_{wildcards.orientation}_{wildcards.type_set}_{wildcards.biotype}_{wildcards.min_len}_{wildcards.group_type}_{wildcards.lib_group}.XXXXXXXXXX")
        echo "tmpdir=\\"${{tmpdir}}\\"" 1> {log.log}
        cmd="computeMatrix scale-regions -S {input.bws} \\
            -R {input.bed} \\
            {params.meta_params} \\
            -p {threads} \\
            -out ${{tmpdir}}/meta_profile.gz"
        echo "${{cmd}}" 1>> {log.log}
        niceload --noswap ${{cmd}} \\
            1>> {log.log} \\
            2> {log.err} \\
            || error_exit "computeMatrix failed"
        cmd="plotProfile -m ${{tmpdir}}/meta_profile.gz -out {output} \\
            -z {wildcards.read_type}_{wildcards.orientation}_on_{wildcards.biotype} \\
            -y \\"{params.y_label}\\" \\
            --samplesLabel {params.labels} \\
            --perGroup \\
            --labelRotation 90"
        echo "${{cmd}}" 1>> {log.log}
        niceload --noswap ${{cmd}} \\
            1>> {log.log} \\
            2>> {log.err} \\
            || error_exit "plotProfile failed"
        rm -rf ${{tmpdir}}
        """


rule plot_gene_list_mean_meta_profile:
    input:
        #bigwig = rules.merge_bigwig_reps.output.bw,
        #bws = expand(OPJ(mapping_dir, "{lib}_mean", "{lib}_mean_{{read_type}}_on_%s_by_{{norm}}_{{orientation}}.bw" % genome), lib=LIBS),
        #bws = expand(OPJ(mapping_dir, "{lib}_mean", "{lib}_mean_{{read_type}}_on_%s_by_{{norm}}_{{orientation}}.bw" % genome), lib=lib_groups[wildcards.group_type][wildcards.lib_group]),
        bws = source_bigwigs,
        bed = rules.select_gene_list_for_meta_profile.output.out_bed
    output:
        OPJ("figures", "mean_meta_profiles_meta_scale_{meta_scale}",
            "{read_type}_by_{norm}_{orientation}_on_{type_set}_merged_isolated_{min_dist}_{id_list}_{group_type}_{lib_group}_meta_profile.pdf"),
    params:
        meta_params = meta_params,
        #labels = LIBS,
        labels = get_libs_for_group,
        y_label = make_y_label,
        # before = META_MARGIN,
        # after = META_MARGIN,
        # body_length = META_SCALE,
        # unscaled_5 = UNSCALED_INSIDE,
        # unscaled_3 = UNSCALED_INSIDE,
    log:
        log = OPJ(log_dir, "plot_gene_list_meta_profile_meta_scale_{meta_scale}", "{read_type}_by_{norm}_{orientation}_on_{type_set}_merged_isolated_{min_dist}_{id_list}_{group_type}_{lib_group}.log"),
        err = OPJ(log_dir, "plot_gene_list_meta_profile_meta_scale_{meta_scale}", "{read_type}_by_{norm}_{orientation}_on_{type_set}_merged_isolated_{min_dist}_{id_list}_{group_type}_{lib_group}.err"),
    #threads: 12  # to limit memory usage, actually
    shell:
        """
        tmpdir=$(TMPDIR=/var/tmp mktemp --tmpdir -d "plot_meta_profile_meta_scale_{wildcards.meta_scale}_{wildcards.read_type}_by_{wildcards.norm}_{wildcards.orientation}_{wildcards.type_set}_{wildcards.id_list}_{wildcards.group_type}_{wildcards.lib_group}.XXXXXXXXXX")
        echo "tmpdir=\\"${{tmpdir}}\\"" 1> {log.log}
        cmd="computeMatrix scale-regions -S {input.bws} \\
            -R {input.bed} \\
            {params.meta_params} \\
            -p 1 \\
            -out ${{tmpdir}}/meta_profile.gz"
        echo "${{cmd}}" 1>> {log.log}
        niceload --noswap ${{cmd}} \\
            1>> {log.log} \\
            2> {log.err} \\
            || error_exit "computeMatrix failed"
        cmd="plotProfile -m ${{tmpdir}}/meta_profile.gz -out {output} \\
            -z {wildcards.read_type}_{wildcards.orientation}_on_{wildcards.id_list} \\
            -y \\"{params.y_label}\\" \\
            --samplesLabel {params.labels} \\
            --perGroup"
        echo "${{cmd}}" 1>> {log.log}
        niceload --noswap ${{cmd}} \\
            1>> {log.log} \\
            2>> {log.err} \\
            || error_exit "plotProfile failed"
        rm -rf ${{tmpdir}}
        """


rule map_perfect_match_piRNAs:
    input:
        pi = annotate_read_output["pi"],
    output:
        sam = temp(OPJ(mapping_dir, "{lib}_{rep}", f"piRNA_perfect_matches_on_{genome}.sam")),
    params:
        tmp_uniques = lambda wildcards: OPJ(
            mapping_dir, "reads",
            f"{wildcards.lib}_{wildcards.rep}_{size_selected}_on_{genome}", "piRNA_uniques.fifo"),
        index = genome_db,
    log:
        map_log = OPJ(log_dir, "map_perfect_match_piRNAs", "{lib}_{rep}.log"),
        map_err = OPJ(log_dir, "map_perfect_match_piRNAs", "{lib}_{rep}.err")
    shell:
        """
    mkfifo {params.tmp_uniques}
    bioawk -c fastx 'length($seq) == %d {{print $seq}}' {input.pi} | uniq | sort | uniq | awk '{{print $1"\\t"$1"\\t"$1}}' > {params.tmp_uniques} &
    # The best possible alignment score in end-to-end mode is 0,
    # which happens when there are no differences between the read and the reference.
    # --score-min L,0,0: we want only perfect matches
    # -a: we want all alignments of a read
    # -L %d -N 0: we want the seed to be the same length of a piRNA, and have no mismatches
    # --no-1mm-upfront: Necessary for the above (see online manual)
    bowtie2 --seed 123 -t -a -L %d -N 0 -i S,1,0.8 --no-1mm-upfront \\
        --score-min L,0,0 --ignore-quals --mm \\
        -x {params.index} --tab5 {params.tmp_uniques} --no-unal -S {output.sam} \\
        1> {log.map_log} 2> {log.map_err}
    rm -f {params.tmp_uniques}
    """ % (PI_MIN, PI_MIN, PI_MIN)


rule make_pi_matches_bed:
    input:
        sam = rules.map_perfect_match_piRNAs.output.sam,
    output:
        bed = OPJ(mapping_dir, "{lib}_{rep}", f"piRNA_perfect_matches_on_{genome}.bed"),
    params:
        tmp_bam = lambda wildcards: OPJ(
            mapping_dir,
            f"{wildcards.lib}_{wildcards.rep}",
            f"piRNA_perfect_matches_on_{genome}.bam"),
    shell:
        """
        mkfifo {params.tmp_bam}
        samtools view -u -F 4 {input.sam} | niceload --mem 500M samtools sort -o {params.tmp_bam} - &
        bedtools bamtobed -i {params.tmp_bam} > {output.bed}
        rm -f {params.tmp_bam}
        """


rule locate_pi_targets:
    input:
        bed_targets = rules.make_pi_matches_bed.output.bed,
        bed_biotype = OPJ(annot_dir, "{biotype}.bed"),
    output:
        bed = OPJ(mapping_dir, "{lib}_{rep}", "piRNA_targets_in_{biotype}.bed"),
    shell:
        """
        # -f 1.0: We want the piRNA match to be entirely inside the annotation
        # -S: We want it to be antisense
        # This automatically filters out self-matches.
        niceload --mem 500M bedtools intersect -a {input.bed_targets} -b {input.bed_biotype} \\
            -f 1.0 -S > {output.bed}
        """


rule plot_pi_targets_profile:
    input:
        #bigwig = source_bigwig,
        bigwig = rules.make_normalized_bigwig.output.bigwig_norm,
        bed = rules.locate_pi_targets.output.bed,
    output:
        figure = OPJ("figures", "{lib}_{rep}", "{read_type}_by_{norm}_{orientation}_pi_targets_in_{biotype}_profile.pdf"),
    params:
        y_label = make_y_label,
    log:
        log = OPJ(log_dir, "plot_pi_targets_profile", "{lib}_{rep}", "{read_type}_by_{norm}_{orientation}_{biotype}.log"),
        err = OPJ(log_dir, "plot_pi_targets_profile", "{lib}_{rep}", "{read_type}_by_{norm}_{orientation}_{biotype}.err"),
        warnings = OPJ(log_dir, "plot_pi_targets_profile", "{lib}_{rep}", "{read_type}_by_{norm}_{orientation}_{biotype}.warnings"),
    threads: 2  # to limit memory usage, actually
    run:
        with warn_context(log.warnings) as warn:
            if os.stat(input.bed).st_size == 0:
                warn(f"No piRNA targets found.\nGenerating dummy figure {output.figure}\n")
                plot_text(output.figure, "No piRNA targets found.")
            else:
                # We should compute -bs, -b, and -a from PI_MIN
                shell("""
tmpdir=$(TMPDIR=/var/tmp mktemp --tmpdir -d "plot_pi_targets_profile_{wildcards.lib}_{wildcards.rep}_{wildcards.read_type}_by_{wildcards.norm}_{wildcards.orientation}_{wildcards.biotype}.XXXXXXXXXX")
echo "tmpdir=\\"${{tmpdir}}\\"" 1> {log.log}
# 501 and 21 are multiples of 3
niceload --noswap computeMatrix scale-regions -S {input.bigwig} \\
    -R {input.bed} \\
    -bs 3 \\
    -b 1002 \\
    -m %d \\
    -a 1002 \\
    -p {threads} \\
    -out ${{tmpdir}}/meta_profile.gz \\
    1>> {log.log} \\
    2> {log.err} \\
    || error_exit "computeMatrix failed"
cmd="plotProfile -m ${{tmpdir}}/meta_profile.gz -out {output.figure} \\
    -y \\"{params.y_label}\\" \\
    --startLabel \\"\\" \\
    --endLabel \\"\\""
echo "${{cmd}}" 1>> {log.log}
niceload --noswap ${{cmd}} \\
    1>> {log.log} \\
    2>> {log.err} \\
    || error_exit "plotProfile failed"
rm -rf ${{tmpdir}}
""" % PI_MIN)


# TODO Make script to generate scatterplots separately
#def plot_scatter(data, x_column, y_column, hue_column="status"):
#    sns.lmplot(
#        x_column, y_columns,
#        data=data, hue=hue_columns,
#        fit_reg=False, scatter_kws={"marker" : ".", "s" : 5})
def plot_counts_scatters(counts_and_res, cols):
    """Alternative to pairplot, in order to avoid histograms on the diagonal."""
    g = sns.PairGrid(counts_and_res, vars=cols, hue="status", size=8)
    #g.map_offdiag(plt.scatter, marker=".")
    g.map_lower(plt.scatter, marker=".")
    g.add_legend()
    #sns.pairplot(counts_and_res, vars=cols, hue="status", markers=".", ax=ax)
    #ax.legend()
    #ax.set_xticks(data.index)
    #ax.set_xticklabels(data.index)
    #ax.set_xlabel("read length")
    #ax.set_ylabel(ylabel)

###################################################
# TODO: check that padj == NA is not considered 0
###################################################

from rpy2.robjects import Formula, StrVector
#from rpy2.rinterface import RRuntimeError
rule small_RNA_differential_expression:
    input:
        counts_table = source_small_RNA_counts,
        summary_table = rules.gather_read_counts_summaries.output.summary_table,
        tags_table = rules.associate_small_type.output.tags_table,
    output:
        deseq_results = OPJ(mapping_dir, "deseq2_%s" % size_selected, "{contrast}", "{contrast}_{small_type}_deseq2.txt"),
        up_genes = OPJ(mapping_dir, "deseq2_%s" % size_selected, "{contrast}", "{contrast}_{small_type}_up_genes.txt"),
        down_genes = OPJ(mapping_dir, "deseq2_%s" % size_selected, "{contrast}", "{contrast}_{small_type}_down_genes.txt"),
        counts_and_res = OPJ(mapping_dir, "deseq2_%s" % size_selected, "{contrast}", "{contrast}_{small_type}_counts_and_res.txt"),
    log:
        warnings = OPJ(log_dir, "small_RNA_differential_expression", "{contrast}_{small_type}.warnings"),
    threads: 4  # to limit memory usage, actually
    run:
        with warn_context(log.warnings) as warn:
            counts_data = pd.read_table(input.counts_table, index_col="gene")
            summaries = pd.read_table(input.summary_table, index_col=0)
            # Running DESeq2
            #################
            formula = Formula("~ lib")
            (cond, ref) = CONTRAST2PAIR[wildcards.contrast]
            if not any(counts_data[f"{ref}_{rep}"].any() for rep in REPS):
                warn(
                    "Reference data is all zero.\nSkipping %s_%s_%s" % (
                        wildcards.contrast, wildcards.orientation, wildcards.small_type))
                for outfile in output:
                    shell(f"echo 'NA' > {outfile}")
            else:
                contrast = StrVector(["lib", cond, ref])
                try:
                    res, size_factors = do_deseq2(COND_NAMES, CONDITIONS, counts_data, formula=formula, contrast=contrast)
                #except RRuntimeError as e:
                except RuntimeError as e:
                    warn(
                        "Probably not enough usable data points to perform DESeq2 analyses:\n%s\nSkipping %s_%s_%s" % (
                            str(e), wildcards.contrast, wildcards.orientation, wildcards.small_type))
                    for outfile in output:
                        shell(f"echo 'NA' > {outfile}")
                else:
                    # Determining fold-change category
                    ###################################
                    set_de_status = status_setter(LFC_CUTOFFS, "log2FoldChange")
                    res = res.assign(status=res.apply(set_de_status, axis=1))
                    # Converting gene IDs
                    ######################
                    with open(OPJ(convert_dir, "wormid2cosmid.pickle"), "rb") as dict_file:
                        res = res.assign(cosmid=res.apply(column_converter(load(dict_file)), axis=1))
                    #with open(OPJ(convert_dir, "wormid2name.pickle"), "rb") as dict_file:
                    #    res = res.assign(name=res.apply(column_converter(load(dict_file)), axis=1))
                    res = res.assign(name=res.apply(column_converter(wormid2name), axis=1))
                    ##########################################
                    # res.to_csv(output.deseq_results, sep="\t", na_rep="NA", decimal=",")
                    res.to_csv(output.deseq_results, sep="\t", na_rep="NA")
                    # Joining counts and DESeq2 results in a same table and determining up- or down- regulation status
                    counts_and_res = counts_data
                    for normalizer in DE_SIZE_FACTORS:
                        if normalizer == "median_ratio_to_pseudo_ref":
                            ## Adapted from DESeq paper (doi:10.1186/gb-2010-11-10-r106) but
                            ## add pseudo-count to compute the geometric mean, then remove it
                            #pseudo_ref = (counts_data + 1).apply(gmean, axis=1) - 1
                            #def median_ratio_to_pseudo_ref(col):
                            #    return (col / pseudo_ref).median()
                            #size_factors = counts_data.apply(median_ratio_to_pseudo_ref, axis=0)
                            size_factors = median_ratio_to_pseudo_ref_size_factors(counts_data)
                        else:
                            size_factors = summaries.loc[normalizer]
                        by_norm = counts_data / size_factors
                        by_norm.columns = by_norm.columns.map(lambda s: "%s_by_%s" % (s, normalizer))
                        counts_and_res = pd.concat((counts_and_res, by_norm), axis=1)
                    counts_and_res = add_tags_column(
                        pd.concat((counts_and_res, res), axis=1),
                        input.tags_table, "small_type")
                    counts_and_res.to_csv(output.counts_and_res, sep="\t", na_rep="NA")
                    # Saving lists of genes gaining or loosing siRNAs
                    up_genes = list(counts_and_res.query(f"status in {UP_STATUSES}").index)
                    down_genes = list(counts_and_res.query(f"status in {DOWN_STATUSES}").index)
                    with open(output.up_genes, "w") as up_file:
                        if up_genes:
                            up_file.write("%s\n" % "\n".join(up_genes))
                        else:
                            up_file.truncate(0)
                    with open(output.down_genes, "w") as down_file:
                        if down_genes:
                            down_file.write("%s\n" % "\n".join(down_genes))
                        else:
                            down_file.truncate(0)


rule gather_DE_folds:
    """Gathers DE folds across contrasts."""
    input:
        fold_results = expand(OPJ(
            mapping_dir, f"deseq2_{size_selected}",
            "{contrast}", "{contrast}_{{small_type}}_counts_and_res.txt"), contrast=DE_CONTRASTS),
    output:
        all_folds = OPJ(
            mapping_dir, f"deseq2_{size_selected}", "all", "{small_type}_{fold_type}.txt"),
    log:
        log = OPJ(log_dir, "gather_DE_folds", "{small_type}_{fold_type}.log"),
    run:
        pd.DataFrame({contrast : pd.read_table(
            OPJ(mapping_dir, f"deseq2_{size_selected}",
                f"{contrast}", f"{contrast}_{wildcards.small_type}_counts_and_res.txt"),
            index_col=["gene", "cosmid", "name", "small_type"],
            usecols=["gene", "cosmid", "name", "small_type", wildcards.fold_type])[wildcards.fold_type] for contrast in DE_CONTRASTS}).to_csv(
                output.all_folds, sep="\t")


# TODO: Make several pages with less vars in PairGrid:
# If the contrast is MUT vs REF, one PairGrid for each treatment.
# If the contrast is between treatment, one PairGrid per genotype.
rule plot_scatterplots:
    input:
        counts_and_res = rules.small_RNA_differential_expression.output.counts_and_res,
    output:
        #pairplot = OPJ(mapping_dir, "deseq2_%s" % size_selected, "{contrast}", "{small_type}_pairplot.pdf"),
        pairplots = OPJ(mapping_dir, "deseq2_%s" % size_selected, "{contrast}", "{contrast}_{small_type}_by_{norm}_pairplots.pdf"),
    run:
        counts_and_res = pd.read_table(input.counts_and_res, index_col="gene")
        (cond, ref) = CONTRAST2PAIR[wildcards.contrast]
        column_list = expand("{lib}_{rep}_by_{norm}", lib=[ref, cond], rep=REPS, norm=[wildcards.norm])
        #if wildcards.contrast == "%s_vs_%s" % (MUT, REF):
        #    for treat in TREATS:
        #        column_lists[treat] = expand("{lib}_%s_{rep}_by_%s" % (treat, NORMALIZER), lib=[REF, MUT], rep=REPS)
        #elif wildcards.contrast in ["%s_vs_%s" % cond_pair for cond_pair in combinations(reversed(TREATS), 2)]:
        #    (cond, ref) = CONTRAST2PAIR[wildcards.contrast]
        #    for lib in LIBS:
        #        column_lists[lib] = expand("%s_{rep}_by_%s" % (lib, NORMALIZER), treat=[ref, cond], rep=REPS)
        #elif wildcards.contrast in ["%s_%s_vs_%s_%s" % (lib2, treat2, lib1, treat1) for (
        #        (lib1, treat1), (lib2, treat2)) in combinations(product(LIBS, TREATS), 2)]:
        #    (cond, ref) = CONTRAST2PAIR[wildcards.contrast]
        #    column_lists[wildcards.contrast] = expand("{lib_treat}_{rep}_by_%s" % NORMALIZER, lib_treat=[ref, cond], rep=REPS)
        #else:
        #    raise NotImplementedError("Unknown contrast: %s" % wildcards.contrast)
        save_plot(output.pairplots, plot_paired_scatters,
            counts_and_res, columns=column_list, hue="status",
            format="pdf",
            title="Sample comparison (normalized %sRNA counts) for %s\n(size factor: %s)" % (wildcards.small_type, wildcards.contrast, wildcards.norm))
        #pp = PdfPages(output.pairplots)
        #for (condition, column_list) in column_lists.items():
        #    save_plot(pp, plot_paired_scatters,
        #        counts_and_res, column_list, "status",
        #        format="pdf",
        #        title="Sample comparison (normalized %sRNA counts) for %s\n(size factor: %s)" % (wildcards.small_type, condition, NORMALIZER))
        #pp.close()
        # save_plot(output.pairplot, sns.pairplot, counts_and_res,
        #     # title="sample comparison", kind="reg", vars=COND_NAMES, hue="status", markers=".")
        #     title="sample comparison (counts per million miRNA)", vars=by_mi.columns, hue="status", markers=".", plot_kws={"log" : True})
        #save_plot(output.pairplots, plot_paired_scatters,
        #    counts_and_res,
        #    title="Sample comparison (normalized %sRNA counts)\n(size factor: %s)" % (wildcards.small_type, NORMALIZER))
        #sns.pairplot(counts_and_res, vars=COND_NAMES, hue="status")


# takes wildcards, gene list name or path
# returns list of wormbase ids
get_id_list = make_id_list_getter(gene_lists_dir, avail_id_lists)
# keys: list names
# values: lists of wormbase ids
# (For None filtering, see https://stackoverflow.com/a/16097112/1878788)
all_id_lists = dict(zip(
    config["gene_lists"],
    filter(None.__ne__, map(get_id_list, config["gene_lists"]))))


#@wc_applied
#def source_fold_data(wildcards):
#    fold_type = wildcards.fold_type
#    if fold_type in {"log2FoldChange", "lfcMLE"}:
#        if hasattr(wildcards, "contrast_type"):
#            return expand(
#                OPJ(mapping_dir,
#                    "deseq2_%s" % size_selected, "{contrast}",
#                    "{contrast}_{{small_type}}_counts_and_res.txt"),
#                contrast=contrasts_dict[wildcards.contrast_type])
#        else:
#            return rules.small_RNA_differential_expression.output.counts_and_res
#    # possibly set wildcard constraints
#    elif fold_type == "mean_log2_RPM_fold":
#        if hasattr(wildcards, "contrast_type"):
#            # We want all contrasts of this type.
#            #https://stackoverflow.com/a/26791923/1878788
#            #print("{0.small_type}".format(wildcards))
#            return [filename.format(wildcards) for filename in expand(
#                OPJ(mapping_dir,
#                    "RPM_folds_%s" % size_selected, "{contrast}",
#                    "{contrast}_{{0.small_type}}_RPM_folds.txt"),
#                contrast=contrasts_dict[wildcards.contrast_type])]
#        elif hasattr(wildcards, "biotypes"):
#            return [filename.format(wildcards) for filename in expand(
#                OPJ(mapping_dir,
#                    "RPM_folds_%s" % size_selected,
#                    "{{0.contrast}}", "remapped",
#                    "{{0.contrast}}_{{0.read_type}}_{{0.mapping_type}}_{biotype}_{{0.orientation}}_transcript_RPM_folds.txt"),
#                biotype=wildcards.biotypes.split("_and_"))]
#        else:
#            return rules.compute_RPM_folds.output.fold_results
#    else:
#        raise NotImplementedError("Unknown fold type: %s" % fold_type)
@wc_applied
def source_fold_data(wildcards):
    fold_type = wildcards.fold_type
    if fold_type in {"log2FoldChange", "lfcMLE"}:
        if hasattr(wildcards, "contrast_type"):
            return expand(
                OPJ(mapping_dir,
                    "deseq2_%s" % size_selected, "{contrast}",
                    "{contrast}_{{small_type}}_counts_and_res.txt"),
                contrast=contrasts_dict[wildcards.contrast_type])
        else:
            return rules.small_RNA_differential_expression.output.counts_and_res
    # 08/06/2023: Take into account new "_by_{norm}" file name element in RPM files
    # possibly set wildcard constraints
    elif fold_type in RPM_FOLD_TYPES:
        assert wildcards.fold_type.startswith("mean_log2_RPM_by_")
        assert wildcards.fold_type.endswith("_fold")
        wildcards.norm = wildcards.fold_type[len("mean_log2_RPM_by_"):-len("_fold")]
        if hasattr(wildcards, "contrast_type"):
            # We want all contrasts of this type.
            #https://stackoverflow.com/a/26791923/1878788
            #print("{0.small_type}".format(wildcards))
            return [filename.format(wildcards) for filename in expand(
                OPJ(mapping_dir,
                    "RPM_by_{{0.norm}}_folds_%s" % size_selected, "{contrast}",
                    "{contrast}_{{0.small_type}}_RPM_by_{{0.norm}}_folds.txt"),
                contrast=contrasts_dict[wildcards.contrast_type])]
        elif hasattr(wildcards, "biotypes"):
            return [filename.format(wildcards) for filename in expand(
                OPJ(mapping_dir,
                    "RPM_by_{{0.norm}}_folds_%s" % size_selected,
                    "{{0.contrast}}", "remapped",
                    "{{0.contrast}}_{{0.read_type}}_{{0.mapping_type}}_{biotype}_{{0.orientation}}_transcript_RPM_by_{{0.norm}}_folds.txt"),
                biotype=wildcards.biotypes.split("_and_"))]
        else:
            return rules.compute_RPM_folds.output.fold_results
    else:
        raise NotImplementedError("Unknown fold type: %s" % fold_type)


def set_id_lists(wildcards):
    gene_list = wildcards.gene_list
    if gene_list == "all_gene_lists":
        return all_id_lists
    else:
        return {gene_list : get_id_list(gene_list)}


# TODO: deal with empty data:
# Empty 'DataFrame': no numeric data to plot
#   File "/home/bli/src/bioinfo_utils/small_RNA-seq/small_RNA-seq.snakefile", line 3827, in __rule_make_gene_list_lfc_boxplots
#   File "/home/bli/.local/lib/python3.6/site-packages/libworkflows/libworkflows.py", line 376, in save_plot
#   File "/home/bli/.local/lib/python3.6/site-packages/libhts/libhts.py", line 432, in plot_boxplots
#   File "/home/bli/.local/lib/python3.6/site-packages/pandas/plotting/_core.py", line 2754, in box
#   File "/home/bli/.local/lib/python3.6/site-packages/pandas/plotting/_core.py", line 2677, in __call__
#   File "/home/bli/.local/lib/python3.6/site-packages/pandas/plotting/_core.py", line 1902, in plot_frame
#   File "/home/bli/.local/lib/python3.6/site-packages/pandas/plotting/_core.py", line 1729, in _plot
#   File "/home/bli/.local/lib/python3.6/site-packages/pandas/plotting/_core.py", line 250, in generate
#   File "/home/bli/.local/lib/python3.6/site-packages/pandas/plotting/_core.py", line 365, in _compute_plot_data
#   File "/home/bli/lib/python3.6/concurrent/futures/thread.py", line 56, in run
rule make_gene_list_lfc_boxplots:
    """Note: This rule may not work properly if some gene lists contain duplicate entries."""
    input:
        data = source_fold_data,
    output:
        boxplots = OPJ("figures", "{contrast}",
            "{contrast}_{small_type}_{fold_type}_{gene_list}_boxplots.pdf")
    log:
        warnings = OPJ(log_dir, "make_gene_list_lfc_boxplots", "{contrast}_{small_type}_{fold_type}_{gene_list}.warnings"),
    params:
        id_lists = set_id_lists,
    run:
        with warn_context(log.warnings) as warn:
            lfcs_dict = {}
            lfc_data = pd.read_table(input.data, index_col="gene")
            for (list_name, id_list) in params.id_lists.items():
                try:
                    selected_rows = lfc_data.loc[lfc_data.index.intersection(id_list)]
                except TypeError as err:
                    print(params.id_lists)
                    print(type(id_list))
                    print(lfc_data.index.intersection(id_list))
                    raise
                if wildcards.fold_type in RPM_FOLD_TYPES:
                    fold_type = "mean_log2_RPM_fold"
                else:
                    fold_type = wildcards.fold_type
                selected_data = selected_rows[fold_type]
                lfcs_dict[list_name] = selected_data
            lfcs = pd.DataFrame(lfcs_dict)
            # lfcs = pd.DataFrame(
            #     {list_name : lfc_data.loc[set(id_list)][wildcards.fold_type] for (
            #         list_name, id_list) in params.id_lists.items()})
            title = f"{wildcards.small_type} {wildcards.fold_type} for {wildcards.contrast}"
            try:
                save_plot(
                    output.boxplots,
                    plot_boxplots,
                    lfcs, wildcards.fold_type,
                    title=title)
            except TypeError as err:
                if str(err) in NO_DATA_ERRS:
                    warn("\n".join([
                        "Got TypeError:",
                        f"{str(err)}",
                        f"No data to plot for {title}\n"]))
                    warn("Generating empty file.\n")
                    # Make the file empty
                    open(output.boxplots, "w").close()
                else:
                    raise


rule make_contrast_lfc_boxplots:
    """For a given gene list, make one box for each contrast."""
    input:
        data = source_fold_data,
    output:
        boxplots = OPJ("figures", "all_{contrast_type}",
            "{contrast_type}_{small_type}_{fold_type}_{gene_list}_boxplots.pdf")
    log:
        warnings = OPJ(log_dir, "make_contrast_lfc_boxplots", "{contrast_type}_{small_type}_{fold_type}_{gene_list}_boxplots.warnings"),
    benchmark:
        OPJ(log_dir, "make_contrast_lfc_boxplots", "{contrast_type}_{small_type}_{fold_type}_{gene_list}_boxplots_benchmark.txt")
    params:
        id_lists = set_id_lists,
    run:
        with warn_context(log.warnings) as warn:
            # TODO: Can there be problems like what happened for RNA-seq
            # where id_list may contain TE family names
            # whereas the read data has an index containing only gene ids?
            lfcs_dict = {}
            for (contrast, filename) in zip(contrasts_dict["ip"], input.data):
                lfc_data = pd.read_table(filename, index_col="gene")
                #print(type(lfc_data))
                for (list_name, id_list) in params.id_lists.items():
                    try:
                        selected_rows = lfc_data.loc[lfc_data.index.intersection(id_list)]
                    except TypeError as err:
                        print(params.id_lists)
                        print(type(id_list))
                        print(lfc_data.index.intersection(id_list))
                        raise
                    #print("selection has type:", type(selected_rows))
                    #print(selected_rows)
                    selected_data = selected_rows["mean_log2_RPM_fold"]
                    #print("column has type:", type(selected_data))
                    #print(selected_data)
                    lfcs_dict[f"{contrast}_{list_name}"] = selected_data
            lfcs = pd.DataFrame(lfcs_dict)
            # lfcs = pd.DataFrame(
            #     {f"{contrast}_{list_name}" : pd.read_table(filename, index_col="gene").loc[
            #         set(id_list)]["mean_log2_RPM_fold"] for (
            #             contrast, filename) in zip(contrasts_dict["ip"], input.data) for (
            #                 list_name, id_list) in params.id_lists.items()})
            title=f"{wildcards.small_type} {wildcards.fold_type} for {wildcards.contrast_type} contrasts"
            try:
                save_plot(
                    output.boxplots,
                    plot_boxplots,
                    lfcs,
                    wildcards.fold_type,
                    title=title)
            except TypeError as err:
                if str(err) in NO_DATA_ERRS:
                    warn("\n".join([
                        "Got TypeError:",
                        f"{str(err)}",
                        f"No data to plot for {title}\n"]))
                    warn("Generating empty file.\n")
                    # Make the file empty
                    open(output.boxplots, "w").close()
                else:
                    raise


rule make_biotype_lfc_boxplots:
    """For a given gene list, make one box for each contrast."""
    input:
        #print_wc,
        data = source_fold_data,
    output:
        boxplots = OPJ("figures", "{contrast}", "by_biotypes",
            "{contrast}_{read_type}_{mapping_type}_{biotypes}_{orientation}_transcript_{fold_type}_{gene_list}_boxplots.pdf")
    benchmark:
        OPJ(log_dir, "make_biotype_lfc_boxplots", "{contrast}_{read_type}_{mapping_type}_{biotypes}_{orientation}_transcript_{fold_type}_{gene_list}_boxplots_benchmark.txt")
    params:
        id_lists = set_id_lists,
    #shell:
    #    """touch {output.boxplots}"""
    run:
        # TODO: test lfcs.shape before trying to plot
        lfcs = pd.DataFrame(
            {f"{biotype}_{list_name}" : pd.read_table(filename, index_col="gene").loc[
                set(id_list)]["mean_log2_RPM_fold"] for (
                    biotype, filename) in zip(wildcards.biotypes.split("_and_"), input.data) for (
                        list_name, id_list) in params.id_lists.items()})
        save_plot(
            output.boxplots,
            plot_boxplots,
            lfcs,
            wildcards.fold_type,
            title=f"{wildcards.read_type}_{wildcards.mapping_type} folds for {wildcards.biotypes} contrasts")


##########################
# Read sequence analyses #
##########################


rule compute_size_distribution:
    input:
        source_fastq
    output:
        OPJ("read_stats", "{lib}_{rep}_{read_type}_size_distribution.txt")
    message:
        "Computing read size distribution for {wildcards.lib}_{wildcards.rep}_{wildcards.read_type}."
    shell:
        """
        zcat {input} | compute_size_distribution {output}
        """


rule plot_size_distribution:
    input:
        rules.compute_size_distribution.output
    output:
        OPJ("figures", "{lib}_{rep}", "{read_type}_size_distribution.pdf")
    message:
        "Plotting size distribution for trimmed {wildcards.lib}_{wildcards.rep}_{wildcards.read_type}."
    run:
        data = pd.read_table(input[0], header=None, names=("size", "count"), index_col=0)
        title = f"read size distribution for {wildcards.lib}_{wildcards.rep}_{wildcards.read_type}"
        plot_histo(output[0], data, title)


def get_type_max_len(wildcards):
    """Returns the maximum length for a "pi", "si", or "mi" read type.
    Otherwise returns MAX_LEN."""
    read_type = wildcards.read_type
    try:
        # ex: "18-26"
        return read_type_max_len[read_type]
    except KeyError:
        try:
            # ex: siuRNA
            return read_type_max_len[read_type[:2]]
        except KeyError:
            try:
                # ex: tRFRNA
                return read_type_max_len[read_type[:-3]]
            except KeyError:
                # ex: nomap_siRNA
                general_type = read_type.split("_")[1]
                return read_type_max_len.get(general_type[:2], int(MAX_LEN))


rule compute_base_composition_along:
    input:
        reads = source_fastq,
        #reads = wc_applied(source_fastq),
        # rules.select_size_range.output.size_selected
    output:
        composition = OPJ(data_dir, "read_stats", "{lib}_{rep}_{read_type}_base_composition_from_{position}.txt"),
        proportions = OPJ(data_dir, "read_stats", "{lib}_{rep}_{read_type}_base_proportions_from_{position}.txt"),
    params:
        max_len = get_type_max_len,
    message:
        "Computing base composition from {wildcards.position} for {wildcards.read_type} reads for {wildcards.lib}_{wildcards.rep}."
    run:
        # "A": 0
        # "C": 1
        # "G": 2
        # "N": 3
        # "T": 4
        letter2index = dict((letter, idx) for (idx, letter) in enumerate(sorted(COMPL)))
        composition = np.zeros((len(COMPL), params.max_len))
        if wildcards.position == "start":
            slicer = slice(None)
        elif wildcards.position == "end":
            slicer = slice(params.max_len - 1, None, -1)
        else:
            raise NotImplementedError("Unknown position: %s" % wildcards.position)
        # Could this be cythonized?
        for (_, sequence, _) in fastx_read(input.reads):
            for (pos, letter) in enumerate(sequence[slicer]):
                composition[letter2index[letter]][pos] += 1
        composition = pd.DataFrame(composition.T)
        composition.index = [pos + 1 for pos in range(params.max_len)]
        composition.index.name = "position"
        composition.columns = sorted(COMPL)
        composition.to_csv(output.composition, sep="\t")
        proportions = (composition.T / composition.sum(axis=1)).T
        proportions.to_csv(output.proportions, sep="\t")


def position2extraction(wildcards):
    position = wildcards.position
    if position == "first":
        return "1"
    elif position == "last":
        return "length($seq)"
    else:
        return position


rule compute_base_composition:
    input:
        reads = source_fastq,
        # rules.select_size_range.output.size_selected
    output:
        data = OPJ(data_dir, "read_stats", "{lib}_{rep}_{read_type}_{position}_base_composition.txt"),
    params:
        extract_pos = position2extraction,
    message:
        "Computing {wildcards.position} base composition for {wildcards.read_type} reads for {wildcards.lib}_{wildcards.rep}."
    shell:
        """
        echo -e "#read_length\\tA\\tC\\tG\\tN\\tT" > {output.data}
        bioawk -c fastx 'BEGIN {{nucls["A"] = "A"; nucls["C"] = "C"; nucls["G"] = "G"; nucls["T"] = "T"; nucls["N"] = "N"}} {{histo[substr($seq, {params.extract_pos}, 1), length($seq)]++; tot[length($seq)]++}} END {{for (l in tot) {{printf "%d", l; for (nucl in nucls) {{printf "\\t%d", histo[nucl,l]}}; printf "\\n"}}}}' {input.reads} \\
            | sort -n -k1 \\
            >> {output.data}
        """


rule compute_base_proportions:
    input:
        reads = source_fastq,
        #rules.select_size_range.output.size_selected
    output:
        data = OPJ(data_dir, "read_stats", "{lib}_{rep}_{read_type}_{position}_base_proportions.txt"),
    params:
        extract_pos = position2extraction,
    message:
        "Computing {wildcards.position} base proportions for {wildcards.read_type} reads for {wildcards.lib}_{wildcards.rep}."
    shell:
        """
        echo -e "#read_length\\tA\\tC\\tG\\tN\\tT" > {output.data}
        # LC_ALL="C" forces the use of dots in numbers (commas complicate further processing)
        LC_ALL="C" bioawk -c fastx 'BEGIN {{nucls["A"] = "A"; nucls["C"] = "C"; nucls["G"] = "G"; nucls["T"] = "T"; nucls["N"] = "N"}} {{histo[substr($seq, {params.extract_pos}, 1), length($seq)]++; tot[length($seq)]++}} END {{for (l in tot) {{printf "%d", l; for (nucl in nucls) {{printf "\\t%f", histo[nucl,l] / tot[l]}}; printf "\\n"}}}}' {input.reads} \\
            | sort -n -k1 \\
            >> {output.data}
        """


# def plot_text(outfile, text, title=None):
#     """This can be used to generate dummy figures in case the data is not suitable."""
#     fig = plt.figure()
#     ax = fig.add_subplot(111)
#     txt = ax.text(0, 0, text)
#     # https://stackoverflow.com/a/21833883/1878788
#     txt.set_clip_on(False)
#     if title is not None:
#         usetex = mpl.rcParams.get("text.usetex", False)
#         if usetex:
#             title = texscape(title)
#         plt.title(title)
#     plt.tight_layout()
#     plt.savefig(outfile)


letter2colour = {"A" : "crimson", "C" : "mediumblue", "G" : "yellow", "T" : "forestgreen", "N" : "black"}
def plot_ACGT(data, xlabel, ylabel):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlim([data.index[0] - 0.5, data.index[-1] + 0.5])
    #ax.set_ylim([0, 100])
    bar_width = 0.125
    letter2legend = dict(zip("ACGTN", "ACGTN"))
    usetex = mpl.rcParams.get("text.usetex", False)
    if usetex:
        xlabel = texscape(xlabel)
        ylabel = texscape(ylabel)
        data.columns = [texscape(colname) for colname in data.columns]
    for (read_len, base_props) in data.iterrows():
        x_shift = -0.25
        for letter, base_count in base_props.iteritems():
            plt.bar(
                read_len + x_shift,
                base_count,
                width=bar_width,
                color=letter2colour[letter],
                label=letter2legend[letter])
            # Avoid redundant legends after this letter
            # has been labeled a first time
            letter2legend[letter] = "_nolegend_"
            x_shift += bar_width
    ax.legend()
    ax.set_xticks(data.index)
    ax.set_xticklabels(data.index)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)


def plot_logo(data, ylabel):
    entropies = -np.sum(data * np.log2(data), axis=1)
    information = 2 - entropies
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlim([data.index[0] - 0.5, data.index[-1] + 0.5])
    ax.set_ylim([0, 2])
    letter2legend = dict(zip("ACGTN", "ACGTN"))
    usetex = mpl.rcParams.get("text.usetex", False)
    if usetex:
        ylabel = texscape(ylabel)
    for (read_len, rel_heights) in data.mul(information, axis=0).iterrows():
        y_base = 0
        for (letter, rel_height) in rel_heights.sort_values().iteritems():
            plt.bar(
                read_len,
                rel_height,
                bottom=y_base,
                align="center",
                width=0.33,
                color=letter2colour[letter],
                label=letter2legend[letter])
            # Avoid redundant legends after this letter
            # has been labeled a first time
            letter2legend[letter] = "_nolegend_"
            y_base += rel_height
    ax.legend()
    ax.set_xticks(data.index)
    ax.set_xticklabels(data.index)
    ax.set_xlabel("read length")
    ax.set_ylabel(ylabel)


# TODO: use the log files
rule plot_base_logo_along:
    input:
        data = rules.compute_base_composition_along.output.proportions,
    output:
        figure = OPJ("figures", "{lib}_{rep}", "{read_type}_base_logo_from_{position}.pdf"),
    message:
        "Plotting base logo from {wildcards.position} for {wildcards.read_type} reads for {wildcards.lib}_{wildcards.rep}."
    log:
        # Currently not used
        #log = OPJ(log_dir, "plot_base_logo", "{lib}_{rep}_{read_type}.log"),
        #err = OPJ(log_dir, "plot_base_logo", "{lib}_{rep}_{read_type}.err"),
        warnings = OPJ(log_dir, "plot_base_logo_along", "{lib}_{rep}_{read_type}_from_{position}.warnings"),
    benchmark:
        OPJ(log_dir, "plot_base_logo_along", "{lib}_{rep}_{read_type}_from_{position}_benchmark.txt"),
    resources:
        mem_mb=2400
    threads: 8  # to limit memory usage, actually
    run:
        with warn_context(log.warnings) as warn:
            data = pd.read_table(input.data, header=0, index_col=0)
            try:
                ylabel = f"information at position from {wildcards.position} (bits)"
                title = f"{wildcards.lib}_{wildcards.rep}_{wildcards.read_type}"
                save_plot(
                    output.figure, plot_logo, data, ylabel, title=title)
            except IndexError as e:
                if str(e) == "index 0 is out of bounds for axis 0 with size 0":
                    warn(f"Got IndexError {str(e)}\nNot enough data to make {output.figure}\n")
                    plot_text(output.figure, "Not enough data.", title)
                else:
                    raise


rule plot_base_logo:
    input:
        data = rules.compute_base_proportions.output.data,
    output:
        figure = OPJ("figures", "{lib}_{rep}", "{read_type}_{position}_base_logo.pdf"),
    message:
        "Plotting {wildcards.position} base logo for {wildcards.read_type} reads for {wildcards.lib}_{wildcards.rep}."
    log:
        # Currently not used
        #log = OPJ(log_dir, "plot_base_logo", "{lib}_{rep}_{read_type}.log"),
        #err = OPJ(log_dir, "plot_base_logo", "{lib}_{rep}_{read_type}.err"),
        warnings = OPJ(log_dir, "plot_base_logo", "{lib}_{rep}_{read_type}_{position}.warnings"),
    benchmark:
        OPJ(log_dir, "plot_base_logo", "{lib}_{rep}_{read_type}_{position}_benchmark.txt"),
    resources:
        mem_mb=4950
    run:
        with warn_context(log.warnings) as warn:
            data = pd.read_table(input.data, header=0, index_col=0)
            try:
                ylabel = f"information at {wildcards.position} position (bits)"
                title = f"{wildcards.lib}_{wildcards.rep}_{wildcards.read_type}"
                save_plot(
                    output.figure, plot_logo, data, ylabel, title=title)
            except IndexError as e:
                if str(e) == "index 0 is out of bounds for axis 0 with size 0":
                    warn(f"Got IndexError {str(e)}\nNot enough data to make {output.figure}\n")
                    plot_text(output.figure, "Not enough data.", title)
                else:
                    raise


rule plot_base_composition_along:
    input:
        data = rules.compute_base_composition_along.output.composition,
    output:
        figure = OPJ("figures", "{lib}_{rep}", "{read_type}_base_composition_from_{position}.pdf"),
    message:
        "Plotting base composition from {wildcards.position} for {wildcards.read_type} reads for {wildcards.lib}_{wildcards.rep}."
    log:
        # Currently not used
        #log = OPJ(log_dir, "plot_base_composition", "{lib}_{rep}_{read_type}.log"),
        #err = OPJ(log_dir, "plot_base_composition", "{lib}_{rep}_{read_type}.err"),
        warnings = OPJ(log_dir, "plot_base_composition_along", "{lib}_{rep}_{read_type}_from_{position}.warnings"),
    benchmark:
        OPJ(log_dir, "plot_base_composition_along", "{lib}_{rep}_{read_type}_from_{position}_benchmark.txt"),
    resources:
        mem_mb=4600
    threads: 4  # to limit memory usage, actually
    run:
        with warn_context(log.warnings) as warn:
            data = pd.read_table(input.data, header=0, index_col=0)
            try:
                ylabel = f"base count at position from {wildcards.position}"
                title = f"{wildcards.lib}_{wildcards.rep}_{wildcards.read_type}"
                save_plot(
                    output.figure, plot_ACGT, data, "position", ylabel, title=title)
            except IndexError as e:
                if str(e) == "index 0 is out of bounds for axis 0 with size 0":
                    warn(f"Got IndexError {str(e)}\nNot enough data to make {output.figure}\n")
                    plot_text(output.figure, "Not enough data.", title)
                else:
                    raise


rule plot_base_composition:
    input:
        data = rules.compute_base_composition.output.data,
    output:
        figure = OPJ("figures", "{lib}_{rep}", "{read_type}_{position}_base_composition.pdf"),
    message:
        "Plotting {wildcards.position} base composition for {wildcards.read_type} reads for {wildcards.lib}_{wildcards.rep}."
    log:
        # Currently not used
        #log = OPJ(log_dir, "plot_base_composition", "{lib}_{rep}_{read_type}.log"),
        #err = OPJ(log_dir, "plot_base_composition", "{lib}_{rep}_{read_type}.err"),
        warnings = OPJ(log_dir, "plot_base_composition", "{lib}_{rep}_{read_type}_{position}.warnings"),
    benchmark:
        OPJ(log_dir, "plot_base_composition", "{lib}_{rep}_{read_type}_{position}_benchmark.txt"),
    resources:
        mem_mb=2650
    run:
        with warn_context(log.warnings) as warn:
            data = pd.read_table(input.data, header=0, index_col=0)
            try:
                ylabel = f"base count at {wildcards.position} position"
                title = f"{wildcards.lib}_{wildcards.rep}_{wildcards.read_type}"
                save_plot(
                    output.figure, plot_ACGT, data, "read_length", ylabel, title=title)
            except IndexError as e:
                if str(e) == "index 0 is out of bounds for axis 0 with size 0":
                    warn(f"Got IndexError {str(e)}\nNot enough data to make {output.figure}\n")
                    plot_text(output.figure, "Not enough data.", title)
                else:
                    raise


def plot_barchart(summary):
    #summary.plot.pie(0, autopct='%.2f', figsize=(6,6))
    usetex = mpl.rcParams.get("text.usetex", False)
    if usetex:
        summary.index = [texscape(colname) for colname in summary.index]
    ax = summary.plot.barh(legend=None)
    ax.set_xlabel("Read counts")


rule plot_summary_barchart:
    input:
        rules.make_read_counts_summary.output,
    output:
        barchart = OPJ("figures", "{lib}_{rep}", "%s_smallRNA_barchart.pdf" % size_selected),
    message:
        "Plotting small RNA barchart for {wildcards.lib}_{wildcards.rep}_%s." % size_selected
    log:
        plot_log = OPJ(log_dir, "plot_summary_barchart", "{lib}_{rep}_%s.log" % size_selected),
        plot_err = OPJ(log_dir, "plot_summary_barchart", "{lib}_{rep}_%s.err" % size_selected)
    benchmark:
        OPJ(log_dir, "plot_summary_barchart", "{lib}_{rep}_%s_benchmark.txt" % size_selected)
    resources:
        mem_mb=2400
    threads: 4  # to limit memory usage, actually
    run:
        name = f"{wildcards.lib}_{wildcards.rep}"
        # summary = pd.read_table(input[0]).T.astype(int).loc[["nomap_siRNA", "ambig_type", "siRNA", "piRNA", "miRNA"]]
        summary = pd.read_table(input[0]).T.astype(int).loc[["nomap_siRNA", "ambig_type", "prot_sisiu_22GRNA", "piRNA", "miRNA", "tRFRNA"]]
        summary.columns = [name]
        save_plot(output.barchart, plot_barchart, summary, title="%s small RNAs" % name)


# TODO: adapt this to the case without treat
#def make_sample_colour_setter(libs=LIBS, treats=TREATS):
#    n_colours = len(libs) * len(treats)
#    palette = sns.color_palette("cubehelix", n_colours)
#    def set_colour(row):
#        """Determines the colour to associate to a given sample corresponding to a
#        row of a DataFrame based on the values of the lib and treat columns."""
#        return palette[libs.index(row["lib"]) * len(treats) + treats.index(row["treat"])]
#    return set_colour
def make_lib2colour(colour_series_dict):
    lib2colour = {}
    base_colours = sns.color_palette("colorblind", len(colour_series_dict))
    for (lib_list, base_colour) in zip(colour_series_dict.values(), base_colours):
        nb_colours = len(lib_list)
        lib2colour.update(
            dict(zip(
                lib_list,
                take_nth(2, chain(
                    sns.light_palette(base_colour, nb_colours + 1)[1:],
                    sns.dark_palette(base_colour, nb_colours + 1, reverse=True)[:-1])))))
    return lib2colour


def make_lib_colour_setter(colour_series_dict):
    lib2colour = make_lib2colour(colour_series_dict)
    def colour_setter(row):
        return tuple(lib2colour[row["lib"]])
    return (colour_setter, lib2colour)


# TODO: Update this with new small types
small_type_series = {
    "pi" : ["pi"],
    "mi" : ["mi"],
    # TODO: We don't know whether this works or not:
    "tRF": ["tRF"],
    "gene" : ["prot_sisiu", "pseu_sisiu", "ri_sisiu"],
    "repeat" : ["te_sisiu", "satel_sisiu", "simrep_sisiu"]}
# small_type_series = {
#     "pi" : ["pi"],
#     "mi" : ["mi"],
#     "gene" : [
#         f"prot_si_{SI_MIN}G", f"pseu_si_{SI_MIN}G",
#         f"prot_siu_{SI_MIN}G", f"pseu_siu_{SI_MIN}G",
#         f"prot_si_{SI_MAX}G", f"pseu_si_{SI_MAX}G",
#         f"prot_siu_{SI_MAX}G", f"pseu_siu_{SI_MAX}G"],
#     "repeat" : [
#         f"te_si_{SI_MIN}G", f"satel_si_{SI_MIN}G", f"simrep_si_{SI_MIN}G",
#         f"te_siu_{SI_MIN}G", f"satel_siu_{SI_MIN}G", f"simrep_siu_{SI_MIN}G",
#         f"te_si_{SI_MAX}G", f"satel_si_{SI_MAX}G", f"simrep_si_{SI_MAX}G",
#         f"te_siu_{SI_MAX}G", f"satel_siu_{SI_MAX}G", f"simrep_siu_{SI_MAX}G"]}


def make_small_type_colour_setter(small_type_series):
    base_colours = sns.color_palette("colorblind", len(small_type_series))
    # series2base_colour = {
    #     "pi" : base_colours[0],
    #     "mi" : base_colours[1],
    #     "gene" : base_colours[2],
    #     "repeat" : base_colours[3]}
    series2base_colour = {
        key : base_colours[i]
        for (i, key) in enumerate(small_type_series.keys())}
    small_type2colour = {}
    for (type_series, small_types) in small_type_series.items():
        nb_colours = len(small_types)
        base_colour = series2base_colour[type_series]
        small_type2colour.update(
            dict(zip(
                small_types,
                take_nth(2, chain(
                    sns.light_palette(base_colour, nb_colours + 1)[1:],
                    sns.dark_palette(base_colour, nb_colours + 1, reverse=True)[:-1])))))
    def colour_setter(row):
        try:
            return tuple(small_type2colour[row["small_type"]])
        except KeyError as e:
            print(row)
            raise
    return (colour_setter, small_type2colour)


(small_type_colour_setter, small_type2colour) = make_small_type_colour_setter(small_type_series)


if genotype_series:
    # TODO: Things may fail due to this not being defined
    (lib_colour_setter, lib2colour) = make_lib_colour_setter(genotype_series)
#def make_sample_plotter(axis, libs=LIBS, treats=TREATS):
def make_sample_plotter(axis):
    #colour_setter = make_sample_colour_setter(libs=LIBS, treats=TREATS)
    def plot_sample(row):
        axis.plot(row[0], row[1], "o", color=lib_colour_setter(row), markersize=10, label=row.name)
    return plot_sample


def plot_pca(pca_with_labels):
    ax = plt.gca()
    sample_plotter = make_sample_plotter(ax)
    pca_with_labels.apply(sample_plotter, axis=1)
    ax.set_xlabel("PC1")
    ax.set_ylabel("PC2")
    ax.legend(ncol=len(REPS))


def plot_components_distrib(components):
    # A bit slow:
    #sns.jointplot(x=0, y=1, data=components.T, kind="kde").set_axis_labels("PC1", "PC2")
    sns.jointplot(x=0, y=1, data=components.T, kind="hex").set_axis_labels("PC1", "PC2")
    #sns.jointplot(x=0, y=1, data=abs_components.abs().T, kind="hex").set_axis_labels("PC1", "PC2") 


def plot_clustermap(data, gene_colours=None, standardization=None):
    #sample_colour_setter = make_sample_colour_setter(libs=LIBS, treats=TREATS)
    #col_colors = COND_COLUMNS.loc[data.columns].apply(sample_colour_setter, axis=1)
    col_colors = COND_COLUMNS.loc[data.columns].apply(lib_colour_setter, axis=1)
    col_colors.name = ""
    if gene_colours is None:
        row_colors = None
    else:
        row_colors = gene_colours.loc[data.index]
    if standardization is None:
        #g = sns.clustermap(data, z_score=0, method="complete", metric="correlation", yticklabels=False)
        g = sns.clustermap(data, z_score=0, method="average", metric="correlation", yticklabels=False, row_colors=row_colors, col_colors=col_colors)
    else:
        if standardization == "unit":
            g = sns.clustermap(data, method="average", metric="euclidean", yticklabels=False, row_colors=row_colors, col_colors=col_colors)
        else:
            g = sns.clustermap(data, method="average", metric="correlation", yticklabels=False, row_colors=row_colors, col_colors=col_colors)
    plt.setp(g.ax_heatmap.get_xticklabels(), rotation=90)
    plt.subplots_adjust(bottom=0.5)
    # plt.show()


rule small_RNA_PCA:
    input:
        #counts_table = rules.gather_small_RNA_counts.output.counts_table,
        standardized_table = rules.standardize_counts.output.standardized_table,
    output:
        pca = OPJ("figures", "{small_type}_{standard}_PCA.pdf"),
        components_distrib = OPJ("figures", "{small_type}_{standard}_PC1_PC2_distrib.pdf"),
    threads: 8  # to limit memory usage, actually
    run:
        #counts_data = pd.read_table(
        #    input.counts_table,
        #    index_col=0,
        #    na_filter=False)
        # Potentially interesting reference about feature scaling:
        # http://sebastianraschka.com/Articles/2014_about_feature_scaling.html
        # Center and reduce, and make genes as columns
        # Differs from the version obtained using StandardScaler
        # because the DataFrame.std uses a non-biased population
        # variance estimator (division by N-1)
        #means = counts_data.mean(axis=1)
        #stds = counts_data.std(axis=1)
        #normalized = ((counts_data.T - means) / stds)
        #normalized.index.names = ["cond_name"]
        series_cond_names = list(concat(genotype_series.values()))
        print(series_cond_names)
        column_list = expand("{lib}_{rep}", lib=series_cond_names, rep=REPS)
        print(column_list)
        standardized = pd.read_table(
            input.standardized_table,
            usecols=["gene", *column_list],
            index_col=0,
            na_filter=False).T
        #pca = pd.DataFrame(PCA(n_components=2).fit_transform(counts)).assign(
        #    cond_name=pd.Series(COND_NAMES).values).set_index("cond_name")
        # Do in two steps in order to have access to the coefficients
        # of the genes in each principal component
        pca_object = PCA(n_components=2).fit(standardized)
        pca_df = pd.DataFrame(pca_object.transform(standardized))
        print(pca_df)
        reduced = pd.DataFrame(pca_object.transform(standardized)).assign(
            cond_name=column_list).set_index("cond_name")
        # Prepend columns classifying in terms of lib and rep
        pca_with_labels = pd.concat((COND_COLUMNS, reduced), axis=1)
        print(pca_with_labels)
        #colour_setter = make_sample_colour_setter()
        #pca_with_labels.plot.scatter(x=0, y=1, s=70, c=pca_with_labels.apply(colour_setter, axis=1))
        save_plot(output.pca, plot_pca, pca_with_labels,
            title="Standardized (%s) %sRNA counts PCA" % (wildcards.standard, wildcards.small_type))
        components = pd.DataFrame(pca_object.components_)
        #components.columns = counts_data.index
        components.columns = standardized.columns
        save_plot(output.components_distrib, plot_components_distrib, components,
            title="Standardized (%s) %sRNA counts PCA" % (wildcards.standard, wildcards.small_type))
        abs_components = components.abs()
        sorted_abs_components_1 = abs_components.sort_values(0, axis=1, ascending=False)
        sorted_abs_components_2 = abs_components.sort_values(1, axis=1, ascending=False)
        # Gather the genes that are affected in at least one of the DESeq2 comparisons
        #gene_selection = set()
        #for gene_list_fname in chain(input.up_genes_lists, input.down_genes_lists):
        #    with open(gene_list_fname, "r") as gene_list_file:
        #        gene_selection.update(map(strip, gene_list_file))
        #top_abs_components_1 = list(sorted_abs_components_1.columns)
        #bot_abs_components_1 = list(reversed(top_abs_components_1))
        #common_genes = np.array([len(set(top_abs_components_1[:N]) & gene_selection) for N in range(len(top_abs_components_1))])
        #rev_common_genes = np.array([len(set(bot_abs_components_1[:N]) & gene_selection) for N in range(len(bot_abs_components_1))])


rule explore_counts:
    input:
        counts_table = source_small_RNA_counts,
        standardized_table = OPJ(mapping_dir, "annotation", f"all_%s_on_{genome}" % size_selected, "{small_type}_zscore.txt"),
        normalized_table = OPJ(mapping_dir, "annotation", f"all_%s_on_{genome}" % size_selected, "{small_type}_unit.txt"),
        summary_table = rules.gather_read_counts_summaries.output.summary_table,
        #standardized_table = rules.standardize_counts.output.standardized_table,
        up_genes_list = OPJ(mapping_dir, "deseq2_%s" % size_selected, "{contrast}", "{small_type}_up_genes.txt"),
        down_genes_list = OPJ(mapping_dir, "deseq2_%s" % size_selected, "{contrast}", "{small_type}_down_genes.txt"),
        deseq_results = OPJ(mapping_dir, "deseq2_%s" % size_selected, "{contrast}", "{small_type}_deseq2.txt"),
    output:
        #pca = OPJ("figures", "{small_type}_{standard}_PCA.pdf"),
        #components_distrib = OPJ("figures", "{small_type}_{standard}_PC1_PC2_distrib.pdf"),
        #clustermap = OPJ("figures", "{contrast}_{small_type}_clustermap.pdf"),
        zscore_clustermap = OPJ("figures", "{contrast}", "{small_type}_zscore_clustermap.pdf"),
        #unit_clustermap = OPJ("figures", "{contrast}_{small_type}_unit_clustermap.pdf"),
    threads: 8  # to limit memory usage, actually
    run:
        counts_data = pd.read_table(
            input.counts_table,
            index_col=0,
            na_filter=False)
        # Gathering count summaries (for normalization)
        ################################################
        summaries = pd.read_table(input.summary_table, index_col=0)
        # TODO: Normalizations:
        # counts_data / summaries.loc[normalizer]
        # Get an idea of read counts distribution:
        #np.log10(counts_data[counts_data.sum(axis=1) > 4 * len(counts_data.columns)] + 1).plot.kde()
        #np.log10(counts_data[counts_data.prod(axis=1) > 0]).plot.kde()
        # Potentially interesting reference about feature scaling:
        # http://sebastianraschka.com/Articles/2014_about_feature_scaling.html
        # Center and reduce, and make genes as columns
        # Differs from the version obtained using StandardScaler
        # because the DataFrame.std uses a non-biased population
        # variance estimator (division by N-1)
        #means = counts_data.mean(axis=1)
        #stds = counts_data.std(axis=1)
        #normalized = ((counts_data.T - means) / stds)
        #normalized.index.names = ["cond_name"]
        #standardized = pd.read_table(
        #    input.standardized_table,
        #    index_col=0,
        #    na_filter=False)
        #pd.cm.coolwarm(colour_normalizer(res["lfcMLE"])[i] * 256)    
        # Gather the genes that are affected in at least one of the DESeq2 comparisons
        gene_selection = set()
        for gene_list_fname in [input.up_genes_list, input.down_genes_list]:
            with open(gene_list_fname, "r") as gene_list_file:
                gene_selection.update(map(strip, gene_list_file))
        fold_type = "log2FoldChange"
        if gene_selection:
            de_res = pd.read_table(
                input.deseq_results,
                index_col=0,
                na_filter=False).loc[gene_selection]
            #lfc = de_res["lfcMLE"]
            lfc = de_res[fold_type]
            max_abs_lfc = max(abs(floor(lfc.quantile(0.25, interpolation="lower"))), abs(ceil(lfc.quantile(0.75, interpolation="higher"))))
            colour_normalizer = Normalize(vmin=-max_abs_lfc, vmax=max_abs_lfc)
            #def normalize_lfc(row):
            #    return colour_normalizer(row["lfcMLE"], clip=True)
            def gene_colour_setter(row):
                return cm.coolwarm(colour_normalizer(row[fold_type], clip=True))
            gene_colours = de_res.apply(gene_colour_setter, axis=1)
            gene_colours.name = f"{wildcards.fold_type}_{wildcards.contrast}"
            #top_abs_components_1 = list(sorted_abs_components_1.columns)
            #bot_abs_components_1 = list(reversed(top_abs_components_1))
            #common_genes = np.array([len(set(top_abs_components_1[:N]) & gene_selection) for N in range(len(top_abs_components_1))])
            #rev_common_genes = np.array([len(set(bot_abs_components_1[:N]) & gene_selection) for N in range(len(bot_abs_components_1))])
            # for clustermap, we work only with the genes that seem
            # significantly affected in at least one of the DESeq2 analyses
            data = counts_data.loc[gene_selection,]
            # Eliminate those with not enough average counts
            data = data[data.mean(axis=1) > 4]
            #save_plot(output.clustermap, plot_clustermap, counts_data.loc[gene_selection,],
            #save_plot(output.clustermap, plot_clustermap, data, gene_colours,
            #    title="Clustering of %sRNA based on z-score of expression level" % wildcards.small_type)
            # Load data standardized so that each gene is a vector of length 1
            # with direction determined by counts repartition across samples.
            zscore_data = pd.read_table(
                input.standardized_table,
                index_col=0,
                na_filter=False).loc[data.index,]
            if len(zscore_data) > 1:
                save_plot(output.zscore_clustermap, plot_clustermap, zscore_data, gene_colours, "zscore",
                    title="Clustering of %sRNA based on standardized (z-score) expression level" % wildcards.small_type)
            else:
                warnings.warn(f"Not enough usable data points to compute clustermap for {wildcards.contrast}_{wildcards.small_type}")
                # Make the file empty
                open(output.zscore_clustermap, "w").close()
            #unit_data = pd.read_table(
            #    input.normalized_table,
            #    index_col=0,
            #    na_filter=False).loc[data.index,]
            #save_plot(output.unit_clustermap, plot_clustermap, unit_data, gene_colours, "unit",
            #    title="Clustering of %sRNA based on normalized (L2) expression level" % wildcards.small_type)
        else:
            warnings.warn(f"No up or down regulated genes for {wildcards.contrast}_{wildcards.small_type}")
            # Make the file empty
            open(output.zscore_clustermap, "w").close()


def try_to_cluster_genes(counts_data):
    from scipy.spatial.distance import pdist, squareform
    from sklearn.cluster import AgglomerativeClustering, DBSCAN
    rscaler = preprocessing.RobustScaler().fit(counts_data.T)
    robust = pd.DataFrame(rscaler.transform(counts_data.T)).assign(
        cond_name=counts_data.columns).set_index("cond_name")
    robust.columns = counts_data.index
    n_clusters = 4
    clusters = AgglomerativeClustering(n_clusters=n_clusters, affinity="precomputed", linkage="average").fit_predict(squareform(pdist(robust.T, metric="correlation")))
    #clusters = DBSCAN(eps=0.05,metric="precomputed").fit_predict(squareform(pdist(robust.T, metric="correlation")))
    clustered = robust.T.assign(cluster=clusters)
    pd.concat(clustered.query("cluster == %d" % n) for n in range(n_clusters))
    sns.heatmap(pd.concat(clustered.query("cluster == %d" % n) for n in range(n_clusters)).drop("cluster", axis=1))

onsuccess:
    print("small RNA-seq analysis finished.")
    cleanup_and_backup(output_dir, config, delete=True)


onerror:
    shell(f"rm -rf {output_dir}_err")
    shell(f"cp -rp {output_dir} {output_dir}_err")
    if upload_on_err:
        cleanup_and_backup(output_dir + "_err", config)
    print("small RNA-seq analysis failed.")

