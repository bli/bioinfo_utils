# Copyright (C) 2020-2023 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Snakefile to process iCLIP data.
"""
import sys
major, minor = sys.version_info[:2]
if major < 3 or (major == 3 and minor < 6):
    sys.exit("Need at least python 3.6\n")


import os
OPJ = os.path.join
from distutils.util import strtobool
from glob import glob
from subprocess import CalledProcessError
from sys import stderr
from yaml import safe_load as yload

from collections import defaultdict
from itertools import product
from cytoolz import valmap

import matplotlib as mpl
# To be able to run the script without a defined $DISPLAY
mpl.use("PDF")
#mpl.rcParams["figure.figsize"] = 2, 4
mpl.rcParams["font.sans-serif"] = [
    "Arial", "Liberation Sans", "Bitstream Vera Sans"]
mpl.rcParams["font.family"] = "sans-serif"
#mpl.rcParams["figure.figsize"] = [16, 30]
import pandas as pd
import matplotlib.pyplot as plt

from idconvert import gene_ids_data_dir
from libhts import make_empty_bigwig, median_ratio_to_pseudo_ref_size_factors, plot_histo
from libworkflows import get_chrom_sizes, cleanup_and_backup
from libworkflows import last_lines, ensure_relative, SHELL_FUNCTIONS, warn_context
from libworkflows import feature_orientation2stranded
from libworkflows import sum_by_family, read_feature_counts, sum_feature_counts
from smincludes import rules as irules
from smwrappers import wrappers_dir

# Define functions to be used in shell portions
shell.prefix(SHELL_FUNCTIONS)

aligner = config["aligner"]
########################
# Genome configuration #
########################
# config["genome_dict"] can be either the path to a genome configuration file
# or a dict
if isinstance(config["genome_dict"], (str, bytes)):
    print(f"loading {config['genome_dict']}", file=stderr)
    with open(config["genome_dict"]) as fh:
        genome_dict = yload(fh)
else:
    genome_dict = config["genome_dict"]
genome = genome_dict["name"]
chrom_sizes = get_chrom_sizes(genome_dict["size"])
chrom_sizes.update(valmap(int, genome_dict.get("extra_chromosomes", {})))
genomelen = sum(chrom_sizes.values())
genome_db = genome_dict["db"][aligner]
# bed file binning the genome in 10nt bins
genome_binned = genome_dict["binned"]
annot_dir = genome_dict["annot_dir"]
# What are the difference between
# OPJ(convert_dir, "wormid2name.pickle") and genome_dict["converter"]?
# /!\ gene_ids_data_dir contains more conversion dicts,
# but is not influenced by genome preparation customization,
# like splitting of miRNAs into 3p and 5p.
# Currently not used
convert_dir = genome_dict.get("convert_dir", gene_ids_data_dir)
# For wormid2name, load in priority the one
# that might contain custom gene names, like for splitted miRNAs
with open(
        genome_dict.get(
            "converter",
            OPJ(convert_dir, "wormid2name.pickle")),
        "rb") as dict_file:
    wormid2name = load(dict_file)
gene_lists_dir = genome_dict["gene_lists_dir"]
avail_id_lists = set(glob(OPJ(gene_lists_dir, "*_ids.txt")))

# One single fastq file with non-demultiplexed data
merged_fastq = config["merged_fastq"]
# How to associate samples to barcodes
barcode_dict = config["barcode_dict"]
BARCODES = list(barcode_dict.keys())
bc_len = len(BARCODES[0])
assert all(len(barcode) == bc_len for barcode in BARCODES), "All barcodes should have the same length."
# qaf_demux parameters
########################
# If the barcode-containing portion of a read has more than MAX_DIFF
# differences with respect to the most likely barcode,
# the read is considered "Unknown"
MAX_DIFF = config.get("max_diff", "3")
# If the most likely barcode has likelihood lower than MIN_PROB,
# the read is considered "Unknown"
MIN_PROB = config.get("min_prob", "0.2")

upload_on_err = strtobool(str(config.get("upload_on_err", "True")))
#output_dir = config["output_dir"]
#workdir: config["output_dir"]
output_dir = os.path.abspath(".")
log_dir = OPJ("logs")
data_dir = OPJ("data")
# Using float in path may be ugly, but maybe the value of MIN_PROB should be indicated somehow
# demux_dir = OPJ(data_dir, f"demultiplexed_{MAX_DIFF}_{MIN_PROB}")
demux_dir = OPJ(data_dir, f"demultiplexed_{MAX_DIFF}")
# Generate the association between sample identifiers and demultiplexed files.
lib2raw = defaultdict(dict)
REPS = set()
for (barcode, lib_info) in barcode_dict.items():
    REPS.add(lib_info["rep"])
    # The demultiplexed files are named by qaf_demux using only demux_dir and barcode
    lib2raw[lib_info["lib"]][lib_info["rep"]] = OPJ(demux_dir, f"{barcode}.fastq.gz")
LIBS = list(lib2raw.keys())
REPS = sorted(REPS)
CONDITIONS = [{
    "lib" : lib,
    "rep" : rep} for rep in REPS for lib in LIBS]
# We use this for various things in order to have always the same library order:
COND_NAMES = ["_".join((
    cond["lib"],
    cond["rep"])) for cond in CONDITIONS]
COND_COLUMNS = pd.DataFrame(CONDITIONS).assign(
    cond_name=pd.Series(COND_NAMES).values).set_index("cond_name")

# Used in particular to correctly orient sequences
LIB_TYPE = config["lib_type"]
#TRIMMERS = ["fastx_clipper"]
TRIMMERS = ["cutadapt"]
ORIENTATIONS = ["fwd", "rev", "all"]
WITH_ADAPT = ["adapt_deduped", "adapt_nodedup"]
POST_TRIMMING = ["noadapt_deduped"] + WITH_ADAPT
# Maybe this could be made configurable
SIZE_RANGES = ["12-18", "21-24", "26-40", "48-52"]
# Note: size is only knowable where the adaptor was found
SIZE_SELECTED = [f"{read_type}_{size_range}" for (read_type, size_range) in product(WITH_ADAPT, SIZE_RANGES)]
TO_REMAP = [f"{to_map}_unmapped" for to_map in POST_TRIMMING + SIZE_SELECTED]
# recommended k-mer length for D. melanogaster is 20
# However, reads shorter than the k-mer length will be ignored.
# http://crac.gforge.inria.fr/documentation/crac/#sec-2
alignment_settings_1 = {
    #"bowtie2": "-L 6 -i S,1,0.8 -N 0",
    "bowtie2": {
        # Settings chosen after benchmarking, but may be dataset-dependend
        "12-18": "--end-to-end -L 12 -i S,1,0.5 -N 1",
        "21-24": "--local -L 14 -i S,1,0.5 -N 1",
        "26-40": "--local -L 15 -i S,1,0.5 -N 1",
        "48-52": "--local -L 16 -i S,1,0.5 -N 1",
        "noadapt": "--local -L 17 -i S,1,0.5 -N 1",
        # What is used for sRNA-seq
        "default": "-L 6 -i S,1,0.8 -N 0",
        },
    # Small RNA-seq parameters may not be compatible with --local
    #"bowtie2": "--local -L 6 -i S,1,0.8 -N 0",
    "crac": {
        "12-18": "-k 20 --stranded --use-x-in-cigar",
        "21-24": "-k 20 --stranded --use-x-in-cigar",
        "26-40": "-k 20 --stranded --use-x-in-cigar",
        "48-52": "-k 20 --stranded --use-x-in-cigar",
        "noadapt": "-k 20 --stranded --use-x-in-cigar",
        "default": "-k 20 --stranded --use-x-in-cigar",
        }
    }
alignment_settings_2 = {
    #"bowtie2": "-L 6 -i S,1,0.8 -N 0",
    "bowtie2": {
        # Settings chosen after benchmarking, but may be dataset-dependend
        "12-18": "--end-to-end -L 12 -i S,1,0.5 -N 1",
        "21-24": "--end-to-end -L 13 -i S,1,0.5 -N 1",
        "26-40": "--end-to-end -L 19 -i S,1,0.5 -N 1",
        "48-52": "--end-to-end -L 16 -i S,1,0.5 -N 1",
        "noadapt": "--end-to-end -L 22 -i S,1,0.5 -N 1",
        # What is used for sRNA-seq
        "default": "-L 6 -i S,1,0.8 -N 0",
        },
    # Small RNA-seq parameters may not be compatible with --local
    #"bowtie2": "--local -L 6 -i S,1,0.8 -N 0",
    "crac": {
        "12-18": "-k 20 --stranded --use-x-in-cigar",
        "21-24": "-k 20 --stranded --use-x-in-cigar",
        "26-40": "-k 20 --stranded --use-x-in-cigar",
        "48-52": "-k 20 --stranded --use-x-in-cigar",
        "noadapt": "-k 20 --stranded --use-x-in-cigar",
        "default": "-k 20 --stranded --use-x-in-cigar",
        }
    }
alignment_settings = alignment_settings_1
# Lower stringency settings, to remap the unmapped
realignment_settings = {
    # Try with almost-default settings
    "bowtie2": "-N 1",
    # Allow more mismatches in the seed
    # Reduce minimal mismatch and gap open penalties
    #"bowtie2": "--local -L 6 -i S,1,0.8 -N 1 --mp 6,1 --rdg 4,3",
    # TODO: Find how to be less stringent with crac
    "crac": "-k 20 --stranded --use-x-in-cigar"}
#test_alignment_settings = {
#    "bowtie2": {
#        "": "-L 6 -i S,1,0.8 -N 1",
#        "": "-L 6 -i S,1,0.8 -N 1",
#        "": "-L 6 -i S,1,0.8 -N 1",
#        "": "-L 6 -i S,1,0.8 -N 1",
#        "": "-L 6 -i S,1,0.8 -N 1",
#        "": "-L 6 -i S,1,0.8 -N 1"}
#    }


# For compatibility with trim_and_dedup as used in PRO-seq pipeline
lib2adapt = defaultdict(lambda: config["adapter"])
MAX_ADAPT_ERROR_RATE = config["max_adapt_error_rate"]

COUNT_BIOTYPES = ["protein_coding", "DNA_transposons_rmsk_families", "RNA_transposons_rmsk_families"]
SIZE_FACTORS = ["protein_coding", "median_ratio_to_pseudo_ref"]
assert set(SIZE_FACTORS).issubset(set(COUNT_BIOTYPES) | {"median_ratio_to_pseudo_ref"})
NORM_TYPES = ["protein_coding", "median_ratio_to_pseudo_ref"]
assert set(NORM_TYPES).issubset(set(SIZE_FACTORS))

wildcard_constraints:
    lib="|".join(LIBS),
    rep="\d+",
    orientation="|".join(ORIENTATIONS),
    norm="|".join(SIZE_FACTORS),
    #size_range="\d+-\d+"

preprocessing = [
    ## Will be pulled in as dependencies of other needed results:
    # expand(OPJ(demux_dir, "{barcode}.fastq.gz"), barcode=BARCODES),
    # expand(OPJ(data_dir, "{lib}_{rep}.fastq.gz"), lib=LIBS, rep=REPS),
    # expand(OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_{read_type}.fastq.gz"), trimmer=TRIMMERS, lib=LIBS, rep=REPS, read_type=POST_TRIMMING + SIZE_SELECTED),
    ##
    expand(OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_{read_type}_fastqc.html"), trimmer=TRIMMERS, lib=LIBS, rep=REPS, read_type=POST_TRIMMING + SIZE_SELECTED),
    expand(OPJ(data_dir, "trimmed_{trimmer}", "read_stats", "{lib}_{rep}", "{read_type}_size_distribution.pdf"), trimmer=TRIMMERS, lib=LIBS, rep=REPS, read_type=POST_TRIMMING),
]

mapping = [
    ## Will be pulled in as dependencies of other needed results:
    # expand(OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_{read_type}_on_%s_sorted.bam" % genome), trimmer=TRIMMERS, lib=LIBS, rep=REPS, read_type=POST_TRIMMING + SIZE_SELECTED),
    ##
    expand(
        OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_{read_type}_on_%s_samtools_stats.txt" % genome),
        trimmer=TRIMMERS, lib=LIBS, rep=REPS,
        read_type=POST_TRIMMING + SIZE_SELECTED + TO_REMAP),
]

counting = [
    ## Will be pulled in as dependencies of other needed results:
    # expand(OPJ("{trimmer}", aligner, "mapped_%s" % genome, "feature_count", "{lib}_{rep}_{read_type}_on_%s" % genome, "{biotype}_{orientation}_counts.txt"), trimmer=TRIMMERS, lib=LIBS, rep=REPS, read_type=POST_TRIMMING + SIZE_SELECTED, biotype=COUNT_BIOTYPES, orientation=ORIENTATIONS),
    ##
    expand(OPJ("{trimmer}", aligner, f"mapped_{genome}", "feature_count", "summaries", "all_{read_type}_on_%s_{orientation}_counts.txt" % genome),
        trimmer=TRIMMERS, read_type=POST_TRIMMING + SIZE_SELECTED + ["deduped"], orientation=ORIENTATIONS),
    expand(OPJ("{trimmer}", aligner, f"mapped_{genome}", "feature_count", "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_counts.txt"),
        trimmer=TRIMMERS, read_type=POST_TRIMMING + SIZE_SELECTED + ["deduped"], biotype=COUNT_BIOTYPES, orientation=ORIENTATIONS),
    expand(OPJ("{trimmer}", aligner, f"mapped_{genome}", "{lib}_{rep}_{read_type}_on_%s_by_{norm}_{orientation}.bw" % genome),
        trimmer=TRIMMERS, lib=LIBS, rep=REPS, read_type=POST_TRIMMING + SIZE_SELECTED + ["deduped"], norm=NORM_TYPES, orientation=["all"]),
    expand(OPJ("{trimmer}", aligner, f"mapped_{genome}", "feature_count", "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_RPM.txt"),
        trimmer=TRIMMERS, read_type=["deduped"], biotype=COUNT_BIOTYPES, orientation=ORIENTATIONS),
    expand(OPJ("{trimmer}", aligner, f"mapped_{genome}", "feature_count", "{lib}_mean_{read_type}_on_%s" % genome, "{lib}_mean_{biotype}_{orientation}_TPM.txt"),
        trimmer=TRIMMERS, lib=LIBS, read_type=["deduped"], biotype=["protein_coding"], orientation=ORIENTATIONS),
]

localrules: all, link_raw_data

#TODO:
# - Plot histogram of read type counts at successive processing steps
# - Remap unmapped with less stringency to check if we are too stringent
# (- remove deduplication step ?)
# - map and featureCount rev/fwd: fwd -> mRNA, rev -> smallRNA
# - map with CRAC, detect chimera and crosslink-induced sequencing errors
# - find cross-link sites on genes: should be 5' of antisense reads
# (otherwise, we expect mismatches at the cross-link sites: distribution of mismatch positions in the reads)
# see also https://genomebiology.biomedcentral.com/articles/10.1186/s13059-016-1130-x
rule all:
    """This top rule is used to drive the whole workflow by taking as input its final products."""
    input:
        preprocessing,
        mapping,
        counting,


#################
# Preprocessing #
#################
rule demultiplex:
    input:
        fq_in = merged_fastq,
    output:
        expand(OPJ(demux_dir, "{barcode}.fastq.gz"), barcode=BARCODES),
    params:
        demux_dir = demux_dir,
        bc_start = config["bc_start"],
        barcodes = " -b ".join(BARCODES),
        max_diff = MAX_DIFF,
        min_prob = MIN_PROB
    log:
        err = OPJ(log_dir, "demultiplex.err")
    benchmark:
        OPJ(log_dir, "demultiplex_benchmark.txt")
    shell:
        # qaf_demux should be available here: https://gitlab.pasteur.fr/bli/qaf_demux
        """
        qaf_demux \\
            -i {input.fq_in} -g -o {params.demux_dir} \\
            -s {params.bc_start} -b {params.barcodes} -m {params.max_diff} \\
            2> {log.err} || error_exit "qaf_demux failed"
        """


include: ensure_relative(irules["link_raw_data"], workflow.basedir)


rule trim_and_dedup:
    """The adaptor is trimmed, then reads are treated in two groups depending
    on whether the adapter was found or not. For each group the reads are
    sorted, deduplicated, and the random k-mers that helped identify
    PCR duplicates are removed at both ends"""
    input:
        rules.link_raw_data.output,
    output:
        noadapt = OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_noadapt_deduped.fastq.gz"),
        adapt = OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_adapt_deduped.fastq.gz"),
        adapt_nodedup = OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_adapt_nodedup.fastq.gz"),
        nb_raw =  OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_nb_raw.txt"),
        nb_adapt =  OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_nb_adapt.txt"),
        nb_adapt_deduped =  OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_nb_adapt_deduped.txt"),
        nb_noadapt =  OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_nb_noadapt.txt"),
        nb_noadapt_deduped =  OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_nb_noadapt_deduped.txt"),
    params:
        adapter = lambda wildcards : lib2adapt[wildcards.lib],
        max_adapt_error_rate = MAX_ADAPT_ERROR_RATE,
        process_type = "iCLIP",
        trim5 = 8,
        trim3 = 4,
    threads: 8 # Actually, to avoid too much IO
    message:
        "Trimming adaptor from raw data using {wildcards.trimmer}, deduplicating reads, and removing 5' and 3' random n-mers for {wildcards.lib}_{wildcards.rep}."
    benchmark:
        OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_trim_benchmark.txt")
    log:
        trim = OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_trim.log"),
        log = OPJ(log_dir, "{trimmer}", "trim_and_dedup", "{lib}_{rep}.log"),
        err = OPJ(log_dir, "{trimmer}", "trim_and_dedup", "{lib}_{rep}.err"),
    run:
        shell_commands = """
MAX_ERROR_RATE="{params.max_adapt_error_rate}" THREADS="{threads}" {params.process_type}_trim_and_dedup.sh {wildcards.trimmer} {input} \\
    {params.adapter} {params.trim5} {params.trim3} \\
    {output.adapt} {output.noadapt} {output.adapt_nodedup} {log.trim} \\
    {output.nb_raw} {output.nb_adapt} {output.nb_adapt_deduped} \\
    {output.nb_noadapt} {output.nb_noadapt_deduped} 1> {log.log} 2> {log.err}
"""
        shell(shell_commands)


def source_trimmed_fastq(wildcards):
    """Determine the fastq file corresponding to a given read type."""
    # remove size range
    read_type = "_".join(wildcards.read_type.split("_")[:-1])
    if read_type == "adapt_deduped":
        return rules.trim_and_dedup.output.adapt
    elif read_type == "noadapt_deduped":
        return rules.trim_and_dedup.output.noadapt
    elif read_type == "adapt_nodedup":
        return rules.trim_and_dedup.output.adapt_nodedup
    else:
        raise NotImplementedError("Unknown read type: %s" % read_type)


def source_fastq(wildcards):
    """Determine the fastq file corresponding to a given read type."""
    read_type = wildcards.read_type
    if read_type == "raw":
        return rules.link_raw_data.output
    elif read_type in SIZE_SELECTED:
        return rules.select_size_range.output.selected
    elif read_type == "adapt_deduped":
        return rules.trim_and_dedup.output.adapt
    elif read_type == "noadapt_deduped":
        return rules.trim_and_dedup.output.noadapt
    elif read_type == "adapt_nodedup":
        return rules.trim_and_dedup.output.adapt_nodedup
    elif read_type.endswith("unmapped"):
        return rules.map_on_genome.output.nomap_fastq
    else:
        raise NotImplementedError("Unknown read type: %s" % read_type)


def awk_size_filter(wildcards):
    """Returns the bioawk filter to select reads of type *wildcards.read_type*."""
    size_range = wildcards.read_type.split("_")[-1]
    (min_len, max_len) = size_range.split("-")
    return f"{min_len} <= length($seq) && length($seq) <= {max_len}"


rule select_size_range:
    """Select (and count) reads in the correct size range."""
    input:
        source_trimmed_fastq,
    output:
        selected = OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_{read_type}.fastq.gz"),
        nb_selected = OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_nb_{read_type}.txt"),
    wildcard_constraints:
        read_type = "|".join(SIZE_SELECTED)
    params:
        awk_filter = awk_size_filter,
    message:
        "Selecting {wildcards.read_type} for {wildcards.lib}_{wildcards.rep}_{wildcards.read_type}."
    shell:
        """
        bioawk -c fastx '{params.awk_filter} {{print "@"$name" "$4"\\n"$seq"\\n+\\n"$qual}}' {input} \\
            | tee >(count_fastq_reads {output.nb_selected}) \\
            | gzip > {output.selected}
        """


# for with_size_selection in [True, False]:
#     if with_size_selection:
#         rule do_fastqc_on_size_selected:
#             input:
#                 fastq = source_fastq
#             output:
#                 fastqc_out = OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_{read_type}_{size_range}_fastqc.html")
#             shell:
#                 """
#                 fastqc {input.fastq}
#                 """
#     else:
#         rule do_fastqc:
#             input:
#                 fastq = source_fastq
#             output:
#                 fastqc_out = OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_{read_type}_fastqc.html")
#             shell:
#                 """
#                 fastqc {input.fastq}
#                 """

rule do_fastqc:
    input:
        fastq = source_fastq,
    output:
        fastqc_out = OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_{read_type}_fastqc.html")
    shell:
        """
        fastqc {input.fastq}
        """
# rule do_fastqc_on_trimmed:
#     input:
#         fastq = source_fastq,
#     output:
#         fastqc_out = OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_{read_type}_fastqc.html")
#     shell:
#         """
#         fastqc {input.fastq}
#         """
# 
# 
# rule do_fastqc_on_size_selected:
#     input:
#         fastq = source_fastq,
#         #fastq = rules.select_size_range.output.selected,
#     output:
#         fastqc_out = OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_{read_type}_{size_range}_fastqc.html")
#     shell:
#         """
#         fastqc {input.fastq}
#         """


rule compute_size_distribution:
    input:
        source_fastq
    output:
        OPJ(data_dir, "trimmed_{trimmer}", "read_stats", "{lib}_{rep}", "{read_type}_size_distribution.txt"),
    message:
        "Computing read size distribution for {wildcards.lib}_{wildcards.rep}_{wildcards.read_type}."
    shell:
        """
        zcat {input} | compute_size_distribution {output}
        """


rule plot_size_distribution:
    input:
        rules.compute_size_distribution.output
    output:
        OPJ(data_dir, "trimmed_{trimmer}", "read_stats", "{lib}_{rep}", "{read_type}_size_distribution.{fig_format}")
    message:
        "Plotting size distribution for trimmed {wildcards.lib}_{wildcards.rep}_{wildcards.read_type}."
    run:
        data = pd.read_table(input[0], header=None, names=("size", "count"), index_col=0)
        title = f"read size distribution for {wildcards.lib}_{wildcards.rep}_{wildcards.read_type}"
        plot_histo(output[0], data, title)


#WITH_ADAPT = ["adapt_deduped", "adapt_nodedup"]
#POST_TRIMMING = ["noadapt_deduped"] + WITH_ADAPT
#SIZE_RANGES = ["12-18", "21-24", "26-40", "48-52"]
#SIZE_SELECTED = [f"{read_type}_{size_range}" for (read_type, size_range) in product(WITH_ADAPT, SIZE_RANGES)]
size_range_re = re.compile(".*_(\d+-\d+)")
def set_alignment_settings(wildcards):
    # Determine whether there is a size range, extract it or use a default one for the alignment settings
    # size_range = wildcards.read_type.split("_")[-1]
    m = size_range_re.match(wildcards.read_type)
    if m:
        (size_range,) = m.groups()
    else:
        if wildcards.read_type.startswith("noadapt_deduped"):
            size_range = "noadapt"
        else:
            size_range = "default"
    return alignment_settings[aligner][size_range]


def set_realignment_settings(wildcards):
    return realignment_settings[aligner]

def generate_test_alignment_settings(wildcards):
    settings_to_test = []
    if aligner in {"bowtie2", "hisat2"}:
        m = size_range_re.match(wildcards.read_type)
        if m:
            (size_range,) = m.groups()
            max_seed_len = min(23, int(size_range.split("-")[-1]))
        else:
            max_seed_len = 23
        for mode in  ["--end-to-end", "--local"]:
            for seed_len in range(6, max_seed_len + 1):
                # Can't pickle generator object (problem to transmit to wrapper)
                #yield f"{mode} -L {seed_len} -i S,1,0.5 -N 1"
                settings_to_test.append(f"{mode} -L {seed_len} -i S,1,0.5 -N 1")
    else:
        raise NotImplementedError(
            f"Mapping parameter exploration not implemented for {aligner}.")
    return settings_to_test

###########
# Mapping #
###########
rule explore_mapping_parameters:
    input:
        fastq = source_fastq,
    output:
        mapping_params = OPJ("{trimmer}", aligner, "mapping_tests", "{lib}_{rep}_{read_type}_on_%s_mapping_params.txt" % genome),
    params:
        aligner = aligner,
        index = genome_db,
        settings = generate_test_alignment_settings,
        # To be set inside the wrapper
        fastq = "",
        rates_record = "",
        these_settings = "",
    message:
        "Exploring mapping parameters on a sample of {wildcards.lib}_{wildcards.rep}_{wildcards.read_type} on %s." % genome
    log:
        log = OPJ(log_dir, "{trimmer}", aligner, "explore_mapping_parameters_{read_type}", "{lib}_{rep}.log"),
        err = OPJ(log_dir, "{trimmer}", aligner, "explore_mapping_parameters_{read_type}", "{lib}_{rep}.err"),
    threads: 12
    wrapper:
        f"file://{wrappers_dir}/explore_mapping_parameters"


def get_test_alignment_settings(wildcards, input):
    with open(input.mapping_params_file) as fh:
        return fh.readline().split("\t")[0]


rule map_on_genome:
    input:
        # fastq = OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_{read_type}.fastq.gz"),
        fastq = source_fastq,
        mapping_params_file = rules.explore_mapping_parameters.output.mapping_params,
    output:
        # sam files take a lot of space
        sam = temp(OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_{read_type}_on_%s.sam" % genome)),
        nomap_fastq = OPJ("{trimmer}", aligner, "info_mapping_%s" % genome, "{lib}_{rep}_{read_type}_unmapped_on_%s.fastq.gz" % genome),
    wildcard_constraints:
        read_type = "|".join(POST_TRIMMING + SIZE_SELECTED)
        # read_type = "|".join(["noadapt_deduped"] + SIZE_SELECTED)
    params:
        aligner = aligner,
        index = genome_db,
        #settings = alignment_settings[aligner],
        #settings = set_alignment_settings,
        settings = get_test_alignment_settings,
    message:
        "Mapping {wildcards.lib}_{wildcards.rep}_{wildcards.read_type} on %s." % genome
    log:
        log = OPJ(log_dir, "{trimmer}", aligner, "map_{read_type}_on_genome", "{lib}_{rep}.log"),
        err = OPJ(log_dir, "{trimmer}", aligner, "map_{read_type}_on_genome", "{lib}_{rep}.err"),
    threads: 12
    wrapper:
        f"file://{wrappers_dir}/map_on_genome"


rule remap_on_genome:
    input:
        # fastq = OPJ(data_dir, "trimmed_{trimmer}", "{lib}_{rep}_{read_type}.fastq.gz"),
        #fastq = rules.map_on_genome.output.nomap_fastq,
        fastq = OPJ("{trimmer}", aligner, "info_mapping_%s" % genome, "{lib}_{rep}_{read_type}_unmapped_on_%s.fastq.gz" % genome),
    output:
        # sam files take a lot of space
        sam = temp(OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_{read_type}_unmapped_on_%s.sam" % genome)),
        nomap_fastq = OPJ("{trimmer}", aligner, "info_mapping_%s" % genome, "{lib}_{rep}_{read_type}_unmapped_unmapped_on_%s.fastq.gz" % genome),
    wildcard_constraints:
        read_type = "|".join(POST_TRIMMING + SIZE_SELECTED)
        # read_type = "|".join(["noadapt_deduped"] + SIZE_SELECTED)
    #wildcard_constraints:
    #    read_type = "|".join([f"{to_map}_unmapped" for to_map in POST_TRIMMING + SIZE_SELECTED])
    params:
        aligner = aligner,
        index = genome_db,
        #settings = realignment_settings[aligner],
        settings = set_realignment_settings,
    message:
        "Re-mapping unmapped {wildcards.lib}_{wildcards.rep}_{wildcards.read_type} on %s." % genome
    log:
        log = OPJ(log_dir, "{trimmer}", aligner, "remap_{read_type}_unmapped_on_genome", "{lib}_{rep}.log"),
        err = OPJ(log_dir, "{trimmer}", aligner, "remap_{read_type}_unmapped_on_genome", "{lib}_{rep}.err"),
    threads: 12
    wrapper:
        f"file://{wrappers_dir}/map_on_genome"


rule sam2indexedbam:
    input:
        sam = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_{read_type}_on_%s.sam" % genome),
    output:
        sorted_bam = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_{read_type}_on_%s_sorted.bam" % genome),
        index = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_{read_type}_on_%s_sorted.bam.bai" % genome),
    wildcard_constraints:
        read_type = "|".join(POST_TRIMMING + SIZE_SELECTED + TO_REMAP)
        # read_type = "|".join(["noadapt_deduped"] + SIZE_SELECTED + TO_REMAP)
    message:
        "Sorting and indexing sam file for {wildcards.lib}_{wildcards.rep}_{wildcards.read_type}."
    log:
        log = OPJ(log_dir, "{trimmer}", "sam2indexedbam", "{lib}_{rep}_{read_type}.log"),
        err = OPJ(log_dir, "{trimmer}", "sam2indexedbam", "{lib}_{rep}_{read_type}.err"),
    threads:
        8
    resources:
        mem_mb=4100
    wrapper:
        f"file://{wrappers_dir}/sam2indexedbam"


rule merge_mapped_and_remapped_bams:
    """This rule fuses the sorted bam files corresponding to the mapping
    of the reads in a category, and the re-mapping of those of the same category
    that did not map in the first place."""
    input:
        mapped_sorted_bam = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_{read_type}_on_%s_sorted.bam" % genome),
        remapped_sorted_bam = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_{read_type}_unmapped_on_%s_sorted.bam" % genome),
    output:
        sorted_bam = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_{read_type}_mapped_and_remapped_on_%s_sorted.bam" % genome),
        bai = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_{read_type}_mapped_and_remapped_on_%s_sorted.bam.bai" % genome),
    #wildcard_constraints:
    #    read_type = "|".join(SIZE_SELECTED)
    message:
        "Fusing sorted bam files for {wildcards.lib}_{wildcards.rep}_{wildcards.read_type}"
    log:
        log = OPJ(log_dir, "{trimmer}", "merge_mapped_and_remapped_bams", "{lib}_{rep}_{read_type}.log"),
        err = OPJ(log_dir, "{trimmer}", "merge_mapped_and_remapped_bams", "{lib}_{rep}_{read_type}.err"),
    shell:
        """
        samtools merge -c {output.sorted_bam} {input.mapped_sorted_bam} {input.remapped_sorted_bam} 1> {log.log} 2> {log.err}
        indexed=""
        while [ ! ${{indexed}} ]
        do
            samtools index {output.sorted_bam} && indexed="OK"
            if [ ! ${{indexed}} ]
            then
                rm -f {output.bai}
                echo "Indexing failed. Retrying" 1>&2
            fi
        done 1>> {log.log} 2>> {log.err}
        """


rule merge_size_ranges_bams:
    """This rule fuses the sorted bam files corresponding to the mapping
    of the reads belonging to different size ranges."""
    input:
        sorted_bams = expand(
            OPJ("{{trimmer}}", aligner, "mapped_%s" % genome, "{{lib}}_{{rep}}_{{read_type}}_{size_range}_mapped_and_remapped_on_%s_sorted.bam" % genome),
            size_range=SIZE_RANGES),
    output:
        sorted_bam = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_{read_type}_all_sizes_on_%s_sorted.bam" % genome),
        bai = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_{read_type}_all_sizes_on_%s_sorted.bam.bai" % genome),
    wildcard_constraints:
        read_type = "|".join(WITH_ADAPT)
    message:
        "Fusing sorted bam files for {wildcards.lib}_{wildcards.rep}_{wildcards.read_type}"
    log:
        log = OPJ(log_dir, "{trimmer}", "merge_size_ranges_bams", "{lib}_{rep}_{read_type}.log"),
        err = OPJ(log_dir, "{trimmer}", "merge_size_ranges_bams", "{lib}_{rep}_{read_type}.err"),
    shell:
        """
        samtools merge -c {output.sorted_bam} {input.sorted_bams} 1> {log.log} 2> {log.err}
        indexed=""
        while [ ! ${{indexed}} ]
        do
            samtools index {output.sorted_bam} && indexed="OK"
            if [ ! ${{indexed}} ]
            then
                rm -f {output.bai}
                echo "Indexing failed. Retrying" 1>&2
            fi
        done 1>> {log.log} 2>> {log.err}
        """


rule fuse_bams:
    """This rule fuses the two sorted bam files corresponding to the mapping
    of the reads containing the adaptor or not."""
    input:
        noadapt_sorted_bam = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_noadapt_deduped_on_%s_sorted.bam" % genome),
        adapt_sorted_bam = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_adapt_deduped_all_sizes_on_%s_sorted.bam" % genome),
    output:
        sorted_bam = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_deduped_on_%s_sorted.bam" % genome),
        bai = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_deduped_on_%s_sorted.bam.bai" % genome),
    message:
        "Fusing sorted bam files for {wildcards.lib}_{wildcards.rep}_deduped"
    log:
        log = OPJ(log_dir, "{trimmer}", "fuse_bams", "{lib}_{rep}_deduped.log"),
        err = OPJ(log_dir, "{trimmer}", "fuse_bams", "{lib}_{rep}_deduped.err"),
    shell:
        """
        samtools merge -c {output.sorted_bam} {input.noadapt_sorted_bam} {input.adapt_sorted_bam} 1> {log.log} 2> {log.err}
        indexed=""
        while [ ! ${{indexed}} ]
        do
            samtools index {output.sorted_bam} && indexed="OK"
            if [ ! ${{indexed}} ]
            then
                rm -f {output.bai}
                echo "Indexing failed. Retrying" 1>&2
            fi
        done 1>> {log.log} 2>> {log.err}
        """


rule compute_mapping_stats:
    input:
        # sorted_bam = rules.sam2indexedbam.output.sorted_bam,
        sorted_bam = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_{read_type}_on_%s_sorted.bam" % genome),
    output:
        stats = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_{read_type}_on_%s_samtools_stats.txt" % genome),
    shell:
        """samtools stats {input.sorted_bam} > {output.stats}"""


def biotype2annot(wildcards):
    if wildcards.biotype.endswith("_rmsk_families"):
        biotype = wildcards.biotype[:-9]
    else:
        biotype = wildcards.biotype
    return OPJ(annot_dir, f"{biotype}.gtf")


rule feature_count_reads:
    input:
        sorted_bam = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_{read_type}_on_%s_sorted.bam" % genome),
        bai = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "{lib}_{rep}_{read_type}_on_%s_sorted.bam.bai" % genome),
    output:
        counts = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "feature_count", "{lib}_{rep}_{read_type}_on_%s" % genome, "{biotype}_{orientation}_counts.txt"),
        counts_converted = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "feature_count", "{lib}_{rep}_{read_type}_on_%s" % genome, "{biotype}_{orientation}_counts_gene_names.txt"),
    params:
        stranded = feature_orientation2stranded(LIB_TYPE),
        annot = biotype2annot,
    message:
        "Counting {wildcards.orientation} {wildcards.biotype} {wildcards.read_type} reads for {wildcards.lib}_{wildcards.rep} with featureCounts."
    log:
        log = OPJ(log_dir, "{trimmer}", "feature_count_reads", "{lib}_{rep}_{read_type}_{biotype}_{orientation}.log"),
        err = OPJ(log_dir, "{trimmer}", "feature_count_reads", "{lib}_{rep}_{read_type}_{biotype}_{orientation}.err")
    shell:
        """
        tmpdir=$(mktemp -dt "feature_{wildcards.lib}_{wildcards.rep}_{wildcards.read_type}_{wildcards.biotype}_{wildcards.orientation}.XXXXXXXXXX")
        cmd="featureCounts -a {params.annot} -o {output.counts} -t transcript -g "gene_id" -O -M --primary -s {params.stranded} --fracOverlap 0 --tmpDir ${{tmpdir}} {input.sorted_bam}"
        featureCounts -v 2> {log.log}
        echo ${{cmd}} 1>> {log.log}
        eval ${{cmd}} 1>> {log.log} 2> {log.err} || error_exit "featureCounts failed"
        rm -rf ${{tmpdir}}
        cat {output.counts} | wormid2name > {output.counts_converted}
        """


rule summarize_feature_counts:
    """For a given library, compute the total counts for each biotype and write this in a summary table."""
    input:
        biotype_counts_files = expand(OPJ("{{trimmer}}", aligner, "mapped_%s" % genome, "feature_count", "{{lib}}_{{rep}}_{{read_type}}_on_%s" % genome, "{biotype}_{{orientation}}_counts.txt"), biotype=COUNT_BIOTYPES),
    output:
        summary = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "feature_count", "summaries", "{lib}_{rep}_{read_type}_on_%s_{orientation}_counts.txt" % genome),
    run:
        sum_counter = sum_feature_counts
        with open(output.summary, "w") as summary_file:
            header = "\t".join(COUNT_BIOTYPES)
            summary_file.write("%s\n" % header)
            sums = "\t".join((str(sum_counter(counts_file)) for counts_file in input.biotype_counts_files))
            summary_file.write("%s\n" % sums)


rule gather_read_counts_summaries:
    """Gather read count summaries across libraries: lib_rep -> all."""
    input:
        summary_tables = expand(OPJ("{{trimmer}}", aligner, "mapped_%s" % genome, "feature_count", "summaries",
            "{lib}_{rep}_{{read_type}}_on_%s_{{orientation}}_counts.txt" % genome), lib=LIBS, rep=REPS),
    output:
        summary_table = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "feature_count", "summaries",
            "all_{read_type}_on_%s_{orientation}_counts.txt" % genome),
    run:
        summary_files = (OPJ(
            wildcards.trimmer,
            aligner,
            "mapped_%s" % genome,
            "feature_count",
            "summaries",
            f"{cond_name}_{wildcards.read_type}_on_%s_{wildcards.orientation}_counts.txt" % genome) for cond_name in COND_NAMES)
        summaries = pd.concat(
            (pd.read_table(summary_file).T.astype(int) for summary_file in summary_files),
            axis=1)
        summaries.columns = COND_NAMES
        summaries.to_csv(output.summary_table, sep="\t")


rule gather_counts:
    """For a given biotype, gather counts from all libraries in one table."""
    input:
        counts_tables = expand(OPJ("{{trimmer}}", aligner, "mapped_%s" % genome, "feature_count",
            "{lib}_{rep}_{{read_type}}_on_%s" % genome, "{{biotype}}_{{orientation}}_counts.txt"), lib=LIBS, rep=REPS),
    output:
        counts_table = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "feature_count",
            "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_counts.txt"),
    # wildcard_constraints:
    #     # Avoid ambiguity with join_all_counts
    #     biotype = "|".join(COUNT_BIOTYPES)
    run:
        # Gathering the counts data
        ############################
        counts_files = (OPJ(
            wildcards.trimmer,
            aligner,
            "mapped_%s" % genome,
            "feature_count",
            f"{cond_name}_{wildcards.read_type}_on_%s" % genome,
            f"{wildcards.biotype}_{wildcards.orientation}_counts.txt") for cond_name in COND_NAMES)
        # if wildcards.counter == "htseq_count":
        #     counts_data = pd.concat(
        #         map(read_htseq_counts, counts_files),
        #         axis=1).fillna(0).astype(int)
        # elif wildcards.counter == "intersect_count":
        #     counts_data = pd.concat(
        #         map(read_intersect_counts, counts_files),
        #         axis=1).fillna(0).astype(int)
        # elif wildcards.counter == "feature_count":
        #     counts_data = pd.concat(
        #         map(read_feature_counts, counts_files),
        #         axis=1).fillna(0).astype(int)
        # else:
        #     raise NotImplementedError(f"{wilcards.counter} not handled (yet?)")
        counts_data = pd.concat(
            map(read_feature_counts, counts_files),
            axis=1).fillna(0).astype(int)
        counts_data.columns = COND_NAMES
        # Simple_repeat|Simple_repeat|(TTTTTTG)n:1
        # Simple_repeat|Simple_repeat|(TTTTTTG)n:2
        # Simple_repeat|Simple_repeat|(TTTTTTG)n:3
        # Simple_repeat|Simple_repeat|(TTTTTTG)n:4
        # -> Simple_repeat|Simple_repeat|(TTTTTTG)n
        if wildcards.biotype.endswith("_rmsk_families"):
            counts_data = sum_by_family(counts_data)
        counts_data.index.names = ["gene"]
        counts_data.to_csv(output.counts_table, sep="\t")


rule compute_RPK:
    """For a given biotype, compute the corresponding RPK value (reads per kilobase)."""
    input:
        # TODO: Why wildcards seems to be None?
        #counts_data = rules.gather_counts.output.counts_table,
        counts_data = OPJ("{trimmer}", aligner, f"mapped_{genome}", "feature_count",
            "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_counts.txt"),
    output:
        rpk_file = OPJ("{trimmer}", aligner, f"mapped_{genome}", "feature_count",
            "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_RPK.txt"),
    params:
        feature_lengths_file = OPJ(annot_dir, "union_exon_lengths.txt"),
    wrapper:
        f"file://{wrappers_dir}/compute_RPK"


rule compute_sum_million_RPK:
    input:
        rpk_file = rules.compute_RPK.output.rpk_file,
    output:
        sum_rpk_file = OPJ("{trimmer}", aligner, f"mapped_{genome}", "feature_count",
            "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_sum_million_RPK.txt"),
    run:
        sum_rpk = pd.read_table(
            input.rpk_file,
            index_col=0).sum()
        (sum_rpk / 1000000).to_csv(output.sum_rpk_file, sep="\t")


# Compute TPM using total number of mappers divided by genome length
# (not sum of RPK across biotypes: some mappers may not be counted)
# No, doesn't work: mappers / genome length not really comparable
# Needs to be done on all_types
rule compute_TPM:
    """For a given biotype, compute the corresponding TPM value (reads per kilobase per million mappers)."""
    input:
        rpk_file = rules.compute_RPK.output.rpk_file
    output:
        tpm_file = OPJ("{trimmer}", aligner, f"mapped_{genome}", "feature_count",
            "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_TPM.txt"),
    # The sum must be done over all counted features
    wildcard_constraints:
        biotype = "|".join(["protein_coding"])
    # run:
    #     rpk = pd.read_table(input.rpk_file, index_col="gene")
    #     tpm = 1000000 * rpk / rpk.sum()
    #     tpm.to_csv(output.tpm_file, sep="\t")
    wrapper:
        f"file://{wrappers_dir}/compute_TPM"


# Useful to compute translation efficiency in the Ribo-seq pipeline
rule compute_mean_TPM:
    input:
        all_tmp_file = rules.compute_TPM.output.tpm_file
    output:
        tpm_file = OPJ("{trimmer}", aligner, f"mapped_{genome}", "feature_count",
            "{lib}_mean_{read_type}_on_%s" % genome, "{lib}_mean_{biotype}_{orientation}_TPM.txt"),
    wildcard_constraints:
        biotype = "|".join(["protein_coding"])
    run:
        tpm = pd.read_table(
            input.all_tmp_file, index_col="gene",
            usecols=["gene", *[f"{wildcards.lib}_{rep}" for rep in REPS]])
        tpm_mean = tpm.mean(axis=1)
        # This is a Series
        tpm_mean.name = wildcards.lib
        tpm_mean.to_csv(output.tpm_file, sep="\t", header=True)


rule compute_RPM:
    input:
        counts_data = OPJ("{trimmer}", aligner, f"mapped_{genome}", "feature_count",
            "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_counts.txt"),
        #summary_table = rules.gather_read_counts_summaries.output.summary_table,
        summary_table = OPJ(
            "{trimmer}", aligner, f"mapped_{genome}", "feature_count", "summaries",
            "all_{read_type}_on_%s_fwd_counts.txt" % genome),
    output:
        rpm_file = OPJ("{trimmer}", aligner, f"mapped_{genome}", "feature_count",
            "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_RPM.txt"),
    run:
        # Reading column counts from {input.counts_table}
        counts_data = pd.read_table(
            input.counts_data,
            index_col="gene")
        # Reading number of protein_coding fwd mappers from {input.summary_table}
        norm = pd.read_table(input.summary_table, index_col=0).loc["protein_coding"]
        # Computing counts per million protein_coding fwd mappers
        RPM = 1000000 * counts_data / norm
        RPM.to_csv(output.rpm_file, sep="\t")

# TODO: add other steps found in RNA-seq pipeline ?

rule compute_median_ratio_to_pseudo_ref_size_factors:
    input:
        counts_table = rules.gather_counts.output.counts_table,
    output:
        median_ratios_file = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "feature_count", "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_median_ratios_to_pseudo_ref.txt"),
    run:
        counts_data = pd.read_table(
            input.counts_table,
            index_col=0,
            na_filter=False)
        # http://stackoverflow.com/a/21320592/1878788
        #median_ratios = pd.DataFrame(median_ratio_to_pseudo_ref_size_factors(counts_data)).T
        #median_ratios.index.names = ["median_ratio_to_pseudo_ref"]
        # Easier to grep when not transposed, actually:
        median_ratios = median_ratio_to_pseudo_ref_size_factors(counts_data)
        print(median_ratios)
        median_ratios.to_csv(output.median_ratios_file, sep="\t")


def source_norm_file(wildcards):
    if wildcards.norm == "median_ratio_to_pseudo_ref":
        return OPJ(f"{wildcards.trimmer}", aligner, f"mapped_{genome}", "feature_count", f"all_{wildcards.read_type}_on_%s" % genome, "protein_coding_fwd_median_ratios_to_pseudo_ref.txt")
    else:
        return rules.summarize_feature_counts.output.summary


rule make_normalized_bigwig:
    input:
        bam = rules.sam2indexedbam.output.sorted_bam,
        #bam = rules.fuse_bams.output.sorted_bam,
        # TODO: use sourcing function based on norm
        norm_file = source_norm_file,
        #size_factor_file = rules.compute_coverage.output.coverage
        #median_ratios_file = OPJ("{trimmer}", aligner, "mapped_%s" % genome, "feature_count", "all_{read_type}_on_%s" % genome, "protein_coding_fwd_median_ratios_to_pseudo_ref.txt"),
        # TODO: compute this
        #scale_factor_file = OPJ(aligner, "mapped_%s" % genome, "annotation", "all_%s_on_%s" % (size_selected, genome), "pisimi_median_ratios_to_pseudo_ref.txt"),
    output:
        bigwig_norm = OPJ("{trimmer}", aligner, f"mapped_{genome}", "{lib}_{rep}_{read_type}_on_%s_by_{norm}_{orientation}.bw" % genome),
    #params:
    #    orient_filter = bamcoverage_filter,
    threads: 12  # to limit memory usage, actually
    benchmark:
        OPJ(log_dir, "{trimmer}", "make_normalized_bigwig", "{lib}_{rep}_{read_type}_by_{norm}_{orientation}_benchmark.txt")
    params:
        genome_binned = genome_binned,
    log:
        log = OPJ(log_dir, "{trimmer}", "make_normalized_bigwig", "{lib}_{rep}_{read_type}_by_{norm}_{orientation}.log"),
        err = OPJ(log_dir, "{trimmer}", "make_normalized_bigwig", "{lib}_{rep}_{read_type}_by_{norm}_{orientation}.err"),
    run:
        if wildcards.norm == "median_ratio_to_pseudo_ref":
            size = float(pd.read_table(
                input.norm_file, index_col=0, header=None).loc[
                    f"{wildcards.lib}_{wildcards.rep}"])
        else:
            # We normalize by million in order not to have too small values
            size = pd.read_table(input.norm_file).T.loc[wildcards.norm][0] / 1000000
            #scale = 1 / pd.read_table(input.summary, index_col=0).loc[
            #    wildcards.norm_file].loc[f"{wildcards.lib}_{wildcards.rep}"]
        assert size >= 0, f"{size} is not positive"
        if size == 0:
            make_empty_bigwig(output.bigwig_norm, chrom_sizes)
        else:
            # TODO: make this a function of deeptools version
            no_reads = """Error: The generated bedGraphFile was empty. Please adjust
    your deepTools settings and check your input files.
    """
            zero_bytes = """needLargeMem: trying to allocate 0 bytes (limit: 100000000000)
    bam2bigwig.sh: bedGraphToBigWig failed
    """
            try:
                # bam2bigwig.sh should be installed with libhts
                shell("""
                    bam2bigwig.sh {input.bam} {params.genome_binned} \\
                        {wildcards.lib}_{wildcards.rep} {wildcards.orientation} %s \\
                        %f {output.bigwig_norm} \\
                        > {log.log} 2> {log.err} \\
                        || error_exit "bam2bigwig.sh failed"
                    """ % (LIB_TYPE[-1], size))
            except CalledProcessError as e:
                if last_lines(log.err, 2) in {no_reads, zero_bytes}:
                    make_empty_bigwig(output.bigwig_norm, chrom_sizes)
                    #with open(output.bigwig_norm, "w") as bwfile:
                    #    bwfile.write("")
                else:
                    raise


onsuccess:
    print("iCLIP data analysis finished.")
    cleanup_and_backup(output_dir, config, delete=True)

onerror:
    shell(f"rm -rf {output_dir}_err")
    shell(f"cp -rp {output_dir} {output_dir}_err")
    if upload_on_err:
        cleanup_and_backup(output_dir + "_err", config)
    print("iCLIP data analysis failed.")

