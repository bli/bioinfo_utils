/*open Batteries*/
/* Function ignoring the "stream count" argument
(whatever that means) and wrapping lines in option.
*/
/*
let get_one_line = (_: int): option(string) => {
    /* Wrap a line in option, or handle exception
    to return None when stdin is exhausted */
    try (Some(input_line(stdin))) {
        | End_of_file => None
    };
};
*/
/*
/* Create a stream of lines from the above function */
let lines = Stream.from(get_one_line);

/* Process the lines from the above stream */
Stream.iter(print_endline, lines);
*/

type fastq = {
  name: string,
  seq: string,
  qual: string
};

let print_fq = ({name, seq, qual}: fastq): unit =>
  switch (name.[0]) {
  | '@' =>
    print_endline(name);
    print_endline(seq);
    print_endline("+");
    print_endline(qual);
  | _ => failwith("Wrong name: " ++ name);
  };

let best_quals = (q1: string, q2:string): string => {
  let char_pairs = BatEnum.combine((
    BatString.enum(q1),
    BatString.enum(q2)));
  let max_chars = BatEnum.map(
    BatPervasives.uncurry(max),
    char_pairs);
  BatString.of_enum(max_chars);
};

/*
print_endline("---------------");
print_fq({
  name: "@SRR3993973.15350 F352I-7B3B0B:114:C5VFYACXX:4:1101:10423:4424 length=50",
  seq: "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
  qual: best_quals(
    "BBBFFFFFFFFFBFIIFFFFFFBFFFFFFFFFBFFFFBB7BFFFFFFFFB",
    "BBBFFFFFFFFFFIIIFFFFFFFFFFFFFFFBFFFFB70''77'''000'")});
*/

/*
let read_fq = (_: int): option(fastq) => {
  switch (get_one_line(1)) {
  | Some(name) => {
    switch (get_one_line(1)) {
    | Some(seq) => {
      switch (get_one_line(1)) {
      | Some(_) => {
        switch (get_one_line(1)) {
        | Some(qual) => Some({name: name, seq: seq, qual: qual});
        | None => None
        };
      };
      | None => None
      };
    };
    | None => None
    };
  };
  | None => None
  };
};
*/
/*
let read_fq2 = (_: int): option(fastq) => {
  let (l1, l2, _, l4) = (get_one_line(1), get_one_line(1), get_one_line(1), get_one_line(1));
  switch (l1) {
  | None => None
  | Some(name) => {
    switch (l2) {
    | None => None
    | Some(seq) => {
      switch (l4) {
      | None => None
      | Some(qual) => Some({name: name, seq: seq, qual: qual})
      };
      }
    };
    }
  };
};
*/
/*
let fqs = Stream.from(read_fq2);
*/
/*
Stream.iter(print_fq, fqs);
*/

let lines: BatEnum.t(string) = BatIO.lines_of(BatIO.stdin);
/*
BatEnum.iter(print_endline, lines);
*/

let read_fq = (): fastq => {
  let l1 = BatEnum.get(lines);
  let l2 = BatEnum.get(lines);
  BatEnum.drop(1, lines);
  let l4 = BatEnum.get(lines);
  switch ((l1, l2, l4)) {
  | (Some(name), Some(seq), Some(qual)) => {name: name, seq: seq, qual: qual}
  | _ => raise(BatEnum.No_more_elements)
  };
};

let fqs = BatEnum.from(read_fq);
/*
BatEnum.iter(print_fq, fqs);
*/

let rec fuse_init_fqs = (): fastq => {
  let fq1 = BatEnum.get(fqs);
  switch (fq1) {
  | None => raise(BatEnum.No_more_elements)
  | Some({name: n1, seq: s1, qual: q1}) =>
    switch (BatEnum.peek(fqs)) {
    | Some({name: _, seq: s2, qual: q2}) when (s2 == s1) =>
      let fused = {name: n1, seq: s1, qual: best_quals(q1, q2)};
      /* replace the next fastq by fused */
      BatEnum.junk(fqs);
      BatEnum.push(fqs, fused);
      /* recurse with the new fused as next fastq */
      fuse_init_fqs();
    | _ =>
      /* the next is either non existent or has a different seq */
      {name: n1, seq: s1, qual: q1}
    };
  };
};

let fused_fqs = BatEnum.from(fuse_init_fqs);
BatEnum.iter(print_fq, fused_fqs);
