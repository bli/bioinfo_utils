(* open Stdio *)
(* open Batteries *)
(* Function ignoring the "stream count" argument
(whatever that means) and wrapping lines in option.
*)
(*
let get_one_line = (_: int): option(string) => {
    /* Wrap a line in option, or handle exception
    to return None when stdin is exhausted */
    try (Some(input_line(stdin))) {
        | End_of_file => None
    };
};
*)
(*
(* Create a stream of lines from the above function *)
let lines = Stream.from(get_one_line);

(* Process the lines from the above stream *)
Stream.iter(print_endline, lines);
*)

type fastq = {
  name: string;
  seq: string;
  qual: string
}

(*
let print_fq ({name; seq; qual}: fastq): unit =
  match String.get name 0 with
  | '@' -> (
    print_endline name;
    print_endline seq;
    print_endline "+";
    print_endline qual)
  | _ -> failwith ("Wrong name: " ^ name)
;;
*)

let print_fq ({name; seq; qual}: fastq): unit =
  match name.[0] with
  | '@' -> print_endline (String.concat "\n" [name; seq; "+"; qual])
  | _ -> failwith ("Wrong name: " ^ name)


(*
let fq1 = {
  name = "@SRR3993973.15350 F352I-7B3B0B:114:C5VFYACXX:4:1101:10423:4424 length=50";
  seq = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
  qual = "BBBFFFFFFFFFBFIIFFFFFFBFFFFFFFFFBFFFFBB7BFFFFFFFFB"} in
print_fq fq1;;
*)

(*
let q1 = "BBBFFFFFFFFFBFIIFFFFFFBFFFFFFFFFBFFFFBB7BFFFFFFFFB"
and q2 = "BBBFFFFFFFFFFIIIFFFFFFFFFFFFFFFBFFFFB70''77'''000'"
;;

let q1_enum = BatString.enum q1
and q2_enum = BatString.enum q2
;;

let char_pairs = BatEnum.combine (q1_enum, q2_enum);;
*)

let best_quals (q1: string) (q2:string): string =
  let char_pairs =
    BatEnum.combine ((BatString.enum q1), (BatString.enum q2)) in
  let max_chars =
    BatEnum.map (BatPervasives.uncurry max) char_pairs in
  BatString.of_enum max_chars


(*
print_fq {
  name = "@SRR3993973.15350 F352I-7B3B0B:114:C5VFYACXX:4:1101:10423:4424 length=50";
  seq = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
  qual = (best_quals
    "BBBFFFFFFFFFBFIIFFFFFFBFFFFFFFFFBFFFFBB7BFFFFFFFFB"
    "BBBFFFFFFFFFFIIIFFFFFFFFFFFFFFFBFFFFB70''77'''000'")}
*)

(*
let main () =
  let lines: string BatEnum.t = BatIO.lines_of BatIO.stdin in
  (*
  BatEnum.iter print_endline lines;;
  *)

  let read_fq (): fastq =
    let l1 = BatEnum.get lines in
    let l2 = BatEnum.get lines in
    let _ = BatEnum.drop 1 lines in
    let l4 = BatEnum.get lines in
    match (l1, l2, l4) with
    | (Some(name), Some(seq), Some(qual)) -> {name; seq; qual}
    | _ -> raise BatEnum.No_more_elements
  in

  let fqs = BatEnum.from(read_fq) in

  (*
  BatEnum.iter print_fq fqs;;
  *)

  let rec fuse_init_fqs (): fastq =
    let fq1 = BatEnum.get fqs in (
    match fq1 with
    | None -> raise BatEnum.No_more_elements;
    | Some {name = n1; seq = s1; qual = q1} -> (
      match BatEnum.peek fqs with
      | Some {name = _; seq = s2; qual = q2} when s2 = s1 -> (
        let fused = {name = n1; seq = s1; qual = best_quals q1 q2} in (
          (* replace the next fastq by fused *)
          BatEnum.junk fqs;
          BatEnum.push fqs fused;
          (* recurse with the new fused as next fastq *)
          fuse_init_fqs ()
          )
        )
      | _ -> (
        (* the next is either non existent or has a different seq *)
        {name = n1; seq = s1; qual = q1})
      )
    )
  in

  let fused_fqs = BatEnum.from fuse_init_fqs in

  BatEnum.iter print_fq fused_fqs
*)


type line_group =
  | Oneline of string
  | Twolines of string * string
  | Threelines of string * string * string
  | Fourlines of string * string * string * string

let make_fq = function
  | Fourlines (l1, l2, _, l4) -> {name=l1; seq=l2; qual=l4}
  | _ -> failwith "Not enough lines for a fastq record"

(* seems to work
let main () =
  let four_line_groups =
    let accumulate_four_lines (acc: line_group list) (line: string) =
      match acc with
      | [] -> [Oneline line]
      | (Oneline l1) :: tl -> (Twolines (l1, line)) :: tl
      | (Twolines (l1, l2)) :: tl -> (Threelines (l1, l2, line)) :: tl
      | (Threelines (l1, l2, l3)) :: tl -> (Fourlines (l1, l2, l3, line)) :: tl
      | (Fourlines _) :: _ -> (Oneline line) :: acc
    in
    BatLazyList.of_list (Stdio.In_channel.fold_lines Stdio.In_channel.stdin ~init:[] ~f:accumulate_four_lines)
  in
  let fqs = BatLazyList.map make_fq four_line_groups in
  BatLazyList.iter print_fq fqs
*)

let main () =
  let module LL = BatLazyList in
  let four_line_groups =
    let accumulate_four_lines (acc: line_group list) (line: string) =
      match acc with
      | [] -> [Oneline line]
      | (Oneline l1) :: tl -> (Twolines (l1, line)) :: tl
      | (Twolines (l1, l2)) :: tl -> (Threelines (l1, l2, line)) :: tl
      | (Threelines (l1, l2, l3)) :: tl -> (Fourlines (l1, l2, l3, line)) :: tl
      | (Fourlines _) :: _ -> (Oneline line) :: acc
    in
    LL.of_list (Stdio.In_channel.fold_lines Stdio.In_channel.stdin ~init:[] ~f:accumulate_four_lines)
  in
  let fqs = LL.map make_fq four_line_groups in
  let fused_fqs: fastq LL.t =
    let fuse_fqs (fq_ll: fastq LL.t) (fq: fastq): fastq LL.t =
      (*
      match LL.get fq_ll with
      | None -> LL.cons fq fq_ll
      | Some (fq2, tl) ->
          if fq2.seq = fq.seq
          then  LL.cons {name = fq.name; seq = fq2.seq; qual = best_quals fq2.qual fq.qual} tl
          else LL.cons fq fq_ll
      *)
      match fq_ll with
      | lazy LL.Nil -> LL.cons fq fq_ll
      | lazy (LL.Cons (fq2, tl)) ->
          if fq2.seq = fq.seq
          then  LL.cons {name = fq.name; seq = fq2.seq; qual = best_quals fq2.qual fq.qual} tl
          else LL.cons fq fq_ll
    in LL.fold_left fuse_fqs LL.nil fqs
  in
  LL.iter print_fq fused_fqs

let () = main ()
