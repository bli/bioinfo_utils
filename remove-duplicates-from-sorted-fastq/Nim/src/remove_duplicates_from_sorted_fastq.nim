from strutils import join
from sequtils import zip

# https://stackoverflow.com/a/35697846
template toClosure(inlineIterator): auto =
  ## Wrap an inline iterator in a first-class closure iterator.
  iterator closureIterator: type(inlineIterator) {.closure.} =
    for elem in inlineIterator:
      yield elem
  closureIterator

type
  Nucleotides = distinct string
  Qualities = distinct string
  #Nucleotides = string
  #Qualities = string
  Fastq = tuple
    name: string
    nucls: Nucleotides
    quals: Qualities

# proc `==` (ns, ms: Nucleotides): bool =
#   string(ns) == string(ms)
# https://nim-by-example.github.io/types/distinct/
proc `==` (ns, ms: Nucleotides): bool {.borrow.}

proc makeFastq(name, nucls, quals: string): Fastq =
  result = (name: name, nucls: nucls.Nucleotides, quals: quals.Qualities)

proc bestQuals(quals1, quals2: string): string =
  let N = min(quals1.len, quals2.len)
  result = newStringOfCap(N)
  for pair in zip(quals1, quals2):
    result.add(chr(max(ord(pair.a), ord(pair.b))))

proc bestQuals(quals1, quals2: Qualities): Qualities =
  result = bestQuals(string(quals1), string(quals2)).Qualities

proc fuseFastq(rec1, rec2: Fastq): Fastq =
  result = (name: rec1.name, nucls: rec1.nucls, quals: bestQuals(rec1.quals, rec2.quals))

proc `$` (record: Fastq): string =
  result = join([
    record.name,
    string(record.nucls),
    "+",
    string(record.quals)], "\n")

iterator parseFastqs(input: File): Fastq =
  var
    nameLine: string
    nucLine: string
    quaLine: string
  while not input.endOfFile:
    nameLine = input.readLine()
    nucLine = input.readLine()
    discard input.readLine()
    quaLine = input.readLine()
    yield makeFastq(nameLine, nucLine, quaLine)

proc deduplicate(inputFqs: iterator) =
  var
    record: Fastq
  record = (name: "", nucls: "".Nucleotides, quals: "".Qualities)
  for fastq in inputFqs():
    if record.nucls != fastq.nucls:
      if record.name != "":
        echo $record
      record = fastq
    else:
      record = fuseFastq(record, fastq)
      continue
  if record.name != "":
    echo $record

when isMainModule:
  let inputFqs = toClosure(parseFastqs(stdin))
  deduplicate(inputFqs)
