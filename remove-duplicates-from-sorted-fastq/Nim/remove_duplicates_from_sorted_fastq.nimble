# Package

version       = "0.1.0"
author        = "Blaise Li"
description   = "This program removes duplicate fastq entries from sequence-sorted fastq."
license       = "MIT"
srcDir        = "src"
bin           = @["remove_duplicates_from_sorted_fastq"]

# Dependencies

requires "nim >= 0.17.3"
