{-# LANGUAGE OverloadedStrings #-}
module Main where

-- For more efficient text manipulation
import qualified Data.ByteString.Lazy.Char8 as C

-- Type synonyms for fields in a fastq record
type Name = C.ByteString
type Nucleotides = C.ByteString
type Qualities = C.ByteString

-- New type for a fastq record
data Fastq = Fastq Name Nucleotides Qualities

-- Turn a fastq record into text.
formatFastq :: Fastq -> C.ByteString
formatFastq (Fastq n s q) =
    C.unlines [n, s, "+", q]

{-
Parse lines four by four, ignoring the third with "_".
We use pattern-matching and recursion to achieve this.
-}
getFastqs :: [C.ByteString] -> [Fastq]
getFastqs [] = []
getFastqs (l1 : l2 : _ : l4 : ls) =
    Fastq l1 l2 l4 : getFastqs ls

{-
We assume that the records come in sorted by sequence.
Successive records having the same sequence are fused,
arbitrarilly keeping the first name, and taking the
highest quality at each position.
-}
fuseFastqs :: [Fastq] -> [Fastq]
fuseFastqs [] = []
fuseFastqs [f] = [f]
fuseFastqs (f1@(Fastq n s1 q1) :
            f2@(Fastq _ s2 q2) :
            fs)
    | s1 == s2 =
        fuseFastqs (Fastq n s1 (bestquals q1 q2) : fs)
    | otherwise =
        f1 : fuseFastqs (f2 : fs)

{-
For each position, we want to keep the highest quality
among the records that had the same sequence.
In fastq format, qualities are integers represented as
ascii characters. Using max on Char does what we want.
This seems to work on Bytestring elements too.
-}
bestquals :: Qualities -> Qualities -> Qualities
bestquals q1 q2 = C.pack (C.zipWith max q1 q2)

-- Compose functions into a "pipeline".
processLines :: [C.ByteString] -> [C.ByteString]
processLines =
    map formatFastq . fuseFastqs . getFastqs

{-
C.lines decomposes the text into lines which are
processed by processLines to remove duplicates.
C.concat "flattens" the resulting list of
text-formatted fastq records into a single text.
C.interact deals with the IO interaction, i.e. taking
text from the outside world and returning text to it.
-}
main :: IO ()
main = C.interact (C.concat . processLines . C.lines)
