#!/bin/bash -l

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

function try_load_gmp_module
{
    (module --version 2> /dev/null && module load gmp) || :
}

if [ $(id -u) = 0 ]
then
    # We don't want things to be installed in root's home directory.
    install="install --local-bin-path /usr/local/bin"
else
    install="install"
fi

config_file="${HOME}/.stack/config.yaml"

stack --version 2> /dev/null \
    || (echo "loading stack" && module load stack/1.4.0)

if [ "${config_file}" ]
then
    stack exec -- ghc --version > /dev/null \
        || (try_load_gmp_module \
            && stack setup \
            && INCLUDE_PATH=$(echo ${CMAKE_INCLUDE_PATH} | tr ":" ",") \
            && LIBRARY_PATH=$(echo ${CMAKE_LIBRARY_PATH} | tr ":" ",") \
            && include_config="extra-include-dirs: [${INCLUDE_PATH}]" \
            && libdir_config="extra-lib-dirs: [${LIBRARY_PATH}]" \
            && echo "" >> ${config_file} \
            && echo ${include_config} >> ${config_file} \
            && echo ${libdir_config} >> ${config_file}) \
        || error_exit "stack setup failed"
    make_stack_options () {
        stack_options=""
    }
else
    stack exec -- ghc --version > /dev/null \
        || (try_load_gmp_module && stack setup) \
        || error_exit "stack setup failed"
    # https://unix.stackexchange.com/a/60690/55127
    # Delay variable evaluation in order to adapt to loaded libraries
    make_stack_options () {
        INCLUDE_PATH=$(echo ${CMAKE_INCLUDE_PATH} | tr ":" ",") \
        LIBRARY_PATH=$(echo ${CMAKE_LIBRARY_PATH} | tr ":" ",") \
        stack_options="--extra-include-dirs=${INCLUDE_PATH} --extra-lib-dirs=${LIBRARY_PATH}"
    }
fi

# cd ${HOME} to ignore local project stack config
hlint --version 2> /dev/null \
    || (try_load_gmp_module && (cd ${HOME} && make_stack_options && stack ${stack_options} ${install} hlint)) \
    || error_exit "hlint install failed"
scan --version 2> /dev/null \
    || (try_load_gmp_module && (cd ${HOME} && make_stack_options && stack ${stack_options} ${install} scan)) \
    || error_exit "scan install failed"
doctest --version 2> /dev/null \
    || (try_load_gmp_module && (cd ${HOME} && make_stack_options && stack ${stack_options} ${install} doctest)) \
    || error_exit "doctest install failed"

make_stack_options
stack ${stack_options} build --exec "hlint src/Main.hs" --exec "scan -j False src/Main.hs" --exec "doctest src/Main.hs"

stack ${install}
