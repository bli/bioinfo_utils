#!/bin/sh
# Extracts the most abundant sequences from a fastq of fasta file (the file can be gzipped)
# Outputs those reads in fasta format, the most abundant first, with their count as comment
# Usage: fastq2most_abundant.sh <fastq file> <number of top most abundant sequences wanted>

# Extract the sequence
# Sort and count
# Find the most abundant
# Format as fasta
bioawk -c fastx '{print $seq}' ${1} \
    | sort | uniq -c \
    | sort -nr | head -${2} \
    | mawk '{print ">"NR" ("$1")\n"$2}'

exit 0
