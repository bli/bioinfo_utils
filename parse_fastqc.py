#!/usr/bin/env python3

import sys

with open(sys.argv[1], "r") as fqc:
    line = fqc.readline()
    assert line.startswith("##FastQC")
    while not line.startswith(">>Overrepresented sequences"):
        line = fqc.readline()
    result = line.strip().split("\t")[1]
    if result == "fail":
        seq, count, percent, source = fqc.readline().strip().split("\t")
        assert seq == "#Sequence"
        #cumul_percent = 0
        line = fqc.readline()
        order = 0
        while not line.startswith(">>END_MODULE"):
            seq, _, percent, _ = line.strip().split("\t")
            order += 1
            #cumul_percent += float(percent)
            print(">Over_represented_%d (%s)\n%s" % (order, percent, seq))
            line = fqc.readline()
    else:
        assert fqc.readline().startswith(">>END_MODULE")

sys.exit(0)
