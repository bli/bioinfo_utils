This program is used to trim T-tails from reads in fastq format.

This program needs to be compiled and installed using the stack Haskell package
manager. See the `install.sh` script.
