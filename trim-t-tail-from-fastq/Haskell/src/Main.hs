{-# LANGUAGE OverloadedStrings #-}
module Main where

-- For more efficient text manipulation
-- import qualified Data.ByteString.Lazy.Char8 as C
import qualified Data.ByteString.Char8 as C

-- Type synonyms for fields in a fastq record
type Name = C.ByteString
type Nucleotides = C.ByteString
type Qualities = C.ByteString

-- New type for a fastq record
data Fastq = Fastq Name Nucleotides Qualities

-- Turn a fastq record into text.
formatFastq :: Fastq -> C.ByteString
formatFastq (Fastq n s q) =
    C.unlines [n, s, "+", q]

{-
Parse lines four by four, ignoring the third with "_".
We use pattern-matching and recursion to achieve this.
-}
getFastqs :: [C.ByteString] -> [Fastq]
getFastqs [] = []
getFastqs (l1 : l2 : _ : l4 : ls) =
    Fastq l1 l2 l4 : getFastqs ls


trimTtails :: [Fastq] -> [Fastq]
trimTtails [] = []
trimTtails (Fastq n s q : fs) =
    let (s', q') = zipTrim s q
    in Fastq n s' q' : trimTtails fs


zipTrim :: Nucleotides -> Qualities -> (Nucleotides, Qualities)
{-
We don't check that the qualities have the same length
as the sequences, but they should.
-}
zipTrim s q
    | C.null s = (C.empty, C.empty)
    | C.head (last $ C.group s) == 'T' =
        C.unzip (C.zip (C.concat $ init $ C.group s) q)
    | otherwise = (s, q)


-- Compose functions into a "pipeline".
processLines :: [C.ByteString] -> [C.ByteString]
processLines =
    map formatFastq . trimTtails . getFastqs

{-
C.lines decomposes the text into lines which are
processed by processLines to remove duplicates.
C.concat "flattens" the resulting list of
text-formatted fastq records into a single text.
C.interact deals with the IO interaction, i.e. taking
text from the outside world and returning text to it.
-}
main :: IO ()
main = C.interact (C.concat . processLines . C.lines)
