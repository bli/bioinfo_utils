#!/usr/bin/env python3
# vim: set fileencoding=<utf-8> :
"""This script takes a genome (or any set of sequences) as fasta format and
another fasta file containing small RNA sequences (or any set of small
requences) and searches for matches of these sequences in the genome. It also
takes matching directives describing where mismatches are allowed. The matches
will be written to the standard output in bed format."""

import argparse
import os
import sys
# from gatb import Bank
from mappy import fastx_read
import regex as re

OPJ = os.path.join
DNA_COMPL_TABLE = str.maketrans("ACGTRYSWKMBDHVN", "TGCAYRWSMKVHDBN")


def reverse_complement(seq):
    """Faster than Bio.Seq.reverse_complement."""
    return seq.translate(DNA_COMPL_TABLE)[::-1]


def num_matches(seq1, seq2):
    return str(sum(1 if letter1 == letter2 else 0
               for (letter1, letter2) in zip(seq1, seq2)))


def mm_constraint(max_mm):
    """Returns the regexp element indicating that up to *max_mm* mismatches
    are allowed on a preceding pattern."""
    if max_mm:
        return "{s<=%d}" % max_mm
    else:
        return ""


def regexpart_maker(start, end, max_mm=0):
    """Returns a function generating a portion of regexp pattern
    when called on a sequence."""
    if end == 0:
        # We don't want `-0`
        end = None

    def regexpart(seq):
        return f"({seq[start:end]}){mm_constraint(max_mm)}"
    return regexpart


def parse_directives(directives, reverse=False):
    """starts and ends are expected to be 1-based and inclusive."""
    regex_part_makers = []
    prev_end = None
    for [start, end, max_mm] in sorted([
            [int(elem) if elem else 0 for elem in directive.split(":")]
            for directive in directives]):
        if prev_end is None and start > 1:
            # We need to fill the gap
            if reverse:
                regex_part_makers.append(
                    regexpart_maker(-(start - 1), 0, max_mm))
            else:
                regex_part_makers.append(
                    regexpart_maker(0, start - 1, max_mm))
        if prev_end is not None and start > prev_end + 1:
            # We need to fill the gap
            if reverse:
                regex_part_makers.append(
                    regexpart_maker(-start, -(prev_end + 1), max_mm))
            else:
                regex_part_makers.append(
                    regexpart_maker(prev_end + 1, start, max_mm))
        if reverse:
            regex_part_makers.append(
                regexpart_maker(-end, -(start - 1), max_mm))
        else:
            regex_part_makers.append(regexpart_maker(start - 1, end, max_mm))
        prev_end = end
    if reverse:
        return list(reversed(regex_part_makers))
    else:
        return regex_part_makers


class FakeMatcher:
    def finditer(self, s, overlapped=False):
        return
        yield

def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-g", "--genome",
        required=True,
        help="Path to a genome in fasta (or fasta.gz) file.")
    parser.add_argument(
        "-s", "--sequences",
        required=True,
        help="Path to a sequences fasta file (fastq is actually also allowed, "
        "as well as gzipped versions of fasta and fastq).")
    parser.add_argument(
        "-f", "--forward_only",
        action="store_true",
        help="If set, matches antisense to the genomic sequences "
        "will not be reported.")
    parser.add_argument(
        "-r", "--reverse_only",
        action="store_true",
        help="If set, matches sense to the genomic sequences "
        "will not be reported.")
    parser.add_argument(
        "-m", "--matching_directives",
        nargs="*",
        help="Space-separated list of matching directives.\n"
        "A matching directive consists in a start:end:max_mm code "
        "where *start* and *end* correspond to 1-based inclusive positions "
        "in the small sequences between which at most *max_mm* mismatches "
        "are allowed.\n"
        "Leaving *max_mm* empty is the same as disallowing mismatches.\n"
        "*start* and/or *end* can be left empty, to indicate that "
        "the described range extends to the extremity of the small sequence.\n"
        "The start:end ranges should not overlap.\n"
        "Regions not included in matching directives are assumed to require "
        "perfect match.")
    args = parser.parse_args()
    if not args.reverse_only:
        rpms_fwd = parse_directives(args.matching_directives)
    else:
        rpms_fwd = []
    if not args.forward_only:
        rpms_rev = parse_directives(args.matching_directives, reverse=True)
    else:
        rpms_rev = []

    def make_matchers(seq):
        """Generates the functions to find matches of the forward and
        reverse-complement of *seq*. When called on a sequence, these
        functions will return an iterator on match objects."""
        # print(seq, file=sys.stderr)
        if rpms_fwd:
            regexp_fwd = re.compile("".join(rpm(seq) for rpm in rpms_fwd))
        else:
            regexp_fwd = FakeMatcher()
        # print(regexp_fwd.pattern, file=sys.stderr)
        if rpms_rev:
            regexp_rev = re.compile("".join(
                rpm(reverse_complement(seq)) for rpm in rpms_rev))
        else:
            regexp_rev = FakeMatcher()
        # print(regexp_rev.pattern, file=sys.stderr)
        return (regexp_fwd.finditer, regexp_rev.finditer)
    matchers = []
    # for record in Bank(args.sequences):
    #     seq_name = record.comment.decode("utf-8")
    #     seq_fwd = record.sequence.decode("utf-8")
    for (seq_name, seq_fwd, _) in fastx_read(args.sequences):
        seq_rev = reverse_complement(seq_fwd)
        matchers.append((
            seq_name, seq_fwd, seq_rev, make_matchers(seq_fwd)))
    # chromosomes = Bank(args.genome)
    # for chrom in chromosomes:
    #     chrom_name = chrom.comment.decode("utf-8")
    #     chrom_seq = chrom.sequence.decode("utf-8")
    for (chrom_name, chrom_seq, _) in fastx_read(args.genome):
        for (seq_name, seq_fwd, seq_rev, (match_fwd, match_rev)) in matchers:
            for match in match_fwd(chrom_seq, overlapped=True):
                score = num_matches(match.group(), seq_fwd)
                print(
                    chrom_name, match.start(), match.end(),
                    seq_name, score, "+", sep="\t")
            for match in match_rev(chrom_seq, overlapped=True):
                score = num_matches(match.group(), seq_rev)
                print(
                    chrom_name, match.start(), match.end(),
                    seq_name, score, "-", sep="\t")
    return 0


if __name__ == "__main__":
    sys.exit(main())
