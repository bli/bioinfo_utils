#!/bin/bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

lib=${1}
report_dir="${lib}_fastqc"

if [ ! -e ${report_dir}.zip -a ! -e ${report_dir} ]
then
    cmd="fastqc ${lib}.fastq.gz"
    nice -n 19 ionice -c2 -n7 ${cmd} || error_exit "${cmd} failed"
fi

if [ ! -e ${report_dir} ]
then
    unzip ${report_dir}.zip
fi

parse_fastqc.py ${report_dir}/fastqc_data.txt \
    > ${report_dir}/${lib}_overrepresented.fasta \
    || error_exit "fastqc result parsing failed for ${lib}"

exit 0
