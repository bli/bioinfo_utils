#!/usr/bin/env python3
# vim: set fileencoding=<utf-8> :
"""
Generating files for codon-optimized klp-7
"""

import sys
from collections import OrderedDict
from mappy import fastx_read

# https://docs.pyfilesystem.org/en/latest/guide.html
from fs.osfs import OSFS as FS
#from fs.tempfs import TempFS as FS

# TODO: introns are the same as the original
# Case usage contains intron-exon information,
# the GTAAA at the end of 5UTR is exon in another isoform
# The exons were determined programatically to optimize codon usage
# with manual adjustments (small caps) made by Meetali Singh
fasta_transgene_caps = """\
>5UTR
gttattattttcagGTAAA
>exon_1
ATGCTTGTCGTCGGAATGCACGTCGAGATCAAGCGCTCTGACGGACGCATCCACGGAGCCGTCATCGCCGAGGTCAAGTCTAACGGACGCTTCATGGTCGAGTGGTACGAGAAG
>intron_2
gtgagtcgcatccttcgagaagAccatcactcgtcacagaatcatgcatttcggatgtgatgccgaaccaaaaatcatgcatttggcattgtgtactttctttcttctcttttttctcttgcaatctttcacattttcag
>exon_2
GGAGAGACCAAGGGAAAGGAGTCTTCTCTTGAGGAGCTTCTTACCCTTAACCCATCTCTTCAAGCCCCAAAGCCAACCCCACCACCACAACCACCACAAAAGACCCTTCAAGCCTCTACCGCCGTCAACCGCCAAAACGGAATCCACGCCCAATCTATGATCCTTGACGACGAGGACACCTTCCTTCTTGACCACATCAACATGATCCCAGCCAAGG
>intron_3
gtttgttggagagttctggtttaatatgaaacaaaaattgattttcag
>exon_3
GAGCCTCTGCCAACAAGCGCCCAACCGGAGCCTTCACCCAACACGTCCCAGTCCGCATGGCCCCACCATCTGAGAAGCCACTTCCAACCCGCCGtGCtCCATCgCCAAAGGAGGACGCCGCCCCAGCCCCAAAGTCTACCCGCACCACCGCCGCCTTCAAGCCAGACCTTGACTCTACCGCCATCACCGTCCCAAAGCACACtGCtCGtCGtACtGTCGTCGTCGCtCCAGCtCCAGCtCCAGTCCCAGCtCCACCAATCGCtAACATGACtTCTCAACGcGCCCCATCtCCAGTCGCCCGCGTCCCATCTCCAAAGAACGTCCCACGCTCTTACCCACAACAAGACGTCCAACCATCTAACGGAAACTTCGCCTTCGCCGAGATGATCCGCAACTACCGCGCCCAAATCGACTACCGCCCACTTTCTATGTTCGACGGAGTCAACGAGAACCGCATCTCTGTCTGCGTCCGCAAGCGCCCACTTAACAAGAAGGAGCTTACCAAGGCCGAGGTCGACGTCATCACCATCCCATCTCGCGACATCACCATCCTTCACCAACCACAAACCCGCGTCGACCTTACCAAGTACCTTGACAACCAAAAGTTCCGCTTCGACTACTCTTTCGACGAGTACGCCAACAACGAGCTTGTCTACCG
>intron_4
gttggtttttttaatttttcagttttcgtttttaaaaatgatattttcag
>exon_4
CTTCACCGCCGCCCCACTTGTCAAGACCGTCTTCGACAACGGAACCGCCACCTGCTTCGCCTACGGACAAACCGGATCTGGAAAGACCCACACCATGGGAGGtGAtTTCTCgGGAAAGAAGCAAAACGCCTCTATGGGAATCTACGCCCTTACCGCCCGCGACGTCTTCCGCATGCTTGAGCAACCACAATACCGCCGCAAGGACCTTTCTGTCCACTGCGCCTTCTTCGAGATCTACGGAACCAAGACCTACGACCTTCTTAACGACAAGGCCGAGCTTCGCGTCCTTGAGGACAAGATGCAAAAGGTCCAAGTCGTCGGACTTAAGGAGGAGCAAGCCCACAACGAGCAAGACGTCCTTGAGCTTATCAACAAGGGAACCCTTGTCCGCACCGCCGGAACCACCTCTGCCAACGCCAACTCTTCTCGCTCTCACGCCATCTTCCAAATCATCCTTCGCCA
>intron_5
gttagtttttgtttttacgtgaagattgctttaaaattaattattttcag
>exon_5
AGGAAAGAAGGTCTGGGGAAAGTTCTCTCTTATCGACCTTGCCGGAAACGAGCGCGGACAAGACACCCGCGAGTGCGACCGCGACACCCGCAAGGAGGGAGCCAACATCAACACCTCTCTTCTTGCCCTTAAGGAGTGCATCCGCGGAATGGCCCGCAACTCTTCTCACGTCCCATTCCGCCAATCTAAGCTTACCATGGTCCTTCGCGACTCTTTCATCGGAGAGAAGTCTCGCACCGTCATGATCTCTATGATCTCTCCAGGAATCTCTTCTTCTGACCACACCCTTAACACCCTTCGCTACGCCGACCG
>intron_6
gtaggaattttatttattttttgctaaaaaaaattcacatttttcag
>exon_6
CGTCAAGGAGATGGGAACCGACGGATCTGGAGAGGCCACCCCAATCCGCGACGAGGAGCTTTTCCTTCCACCATCTGCCGACAAGTCTGACGAGGAGTACGatGaGATGGTCGAGAAGCAAGAGCACCGtCGtGTCGCtGTCGACCACGTCCGCAACCTTAAGGACATGTCTGAGAAGATCATCCGCGAGACCACCATCGTCCTTTCTAACGAGCCATCTGCCGCCCAAAAGGCCGAGTGCCTTGCCAAGCTTGACCAACTTGCCCAAATCGTCTCTAACACCCGCGTCGCCGTCGAGAACGTCTGA
>3UTR
ttgctgattcccgcgacgatatgctac
"""

# All capitals version
fasta_transgene = """\
>5UTR
GTTATTATTTTCAGGTAAA
>exon_1
ATGCTTGTCGTCGGAATGCACGTCGAGATCAAGCGCTCTGACGGACGCATCCACGGAGCCGTCATCGCCGAGGTCAAGTCTAACGGACGCTTCATGGTCGAGTGGTACGAGAAG
>intron_2
GTGAGTCGCATCCTTCGAGAAGACCATCACTCGTCACAGAATCATGCATTTCGGATGTGATGCCGAACCAAAAATCATGCATTTGGCATTGTGTACTTTCTTTCTTCTCTTTTTTCTCTTGCAATCTTTCACATTTTCAG
>exon_2
GGAGAGACCAAGGGAAAGGAGTCTTCTCTTGAGGAGCTTCTTACCCTTAACCCATCTCTTCAAGCCCCAAAGCCAACCCCACCACCACAACCACCACAAAAGACCCTTCAAGCCTCTACCGCCGTCAACCGCCAAAACGGAATCCACGCCCAATCTATGATCCTTGACGACGAGGACACCTTCCTTCTTGACCACATCAACATGATCCCAGCCAAGG
>intron_3
GTTTGTTGGAGAGTTCTGGTTTAATATGAAACAAAAATTGATTTTCAG
>exon_3
GAGCCTCTGCCAACAAGCGCCCAACCGGAGCCTTCACCCAACACGTCCCAGTCCGCATGGCCCCACCATCTGAGAAGCCACTTCCAACCCGCCGTGCTCCATCGCCAAAGGAGGACGCCGCCCCAGCCCCAAAGTCTACCCGCACCACCGCCGCCTTCAAGCCAGACCTTGACTCTACCGCCATCACCGTCCCAAAGCACACTGCTCGTCGTACTGTCGTCGTCGCTCCAGCTCCAGCTCCAGTCCCAGCTCCACCAATCGCTAACATGACTTCTCAACGCGCCCCATCTCCAGTCGCCCGCGTCCCATCTCCAAAGAACGTCCCACGCTCTTACCCACAACAAGACGTCCAACCATCTAACGGAAACTTCGCCTTCGCCGAGATGATCCGCAACTACCGCGCCCAAATCGACTACCGCCCACTTTCTATGTTCGACGGAGTCAACGAGAACCGCATCTCTGTCTGCGTCCGCAAGCGCCCACTTAACAAGAAGGAGCTTACCAAGGCCGAGGTCGACGTCATCACCATCCCATCTCGCGACATCACCATCCTTCACCAACCACAAACCCGCGTCGACCTTACCAAGTACCTTGACAACCAAAAGTTCCGCTTCGACTACTCTTTCGACGAGTACGCCAACAACGAGCTTGTCTACCG
>intron_4
GTTGGTTTTTTTAATTTTTCAGTTTTCGTTTTTAAAAATGATATTTTCAG
>exon_4
CTTCACCGCCGCCCCACTTGTCAAGACCGTCTTCGACAACGGAACCGCCACCTGCTTCGCCTACGGACAAACCGGATCTGGAAAGACCCACACCATGGGAGGTGATTTCTCGGGAAAGAAGCAAAACGCCTCTATGGGAATCTACGCCCTTACCGCCCGCGACGTCTTCCGCATGCTTGAGCAACCACAATACCGCCGCAAGGACCTTTCTGTCCACTGCGCCTTCTTCGAGATCTACGGAACCAAGACCTACGACCTTCTTAACGACAAGGCCGAGCTTCGCGTCCTTGAGGACAAGATGCAAAAGGTCCAAGTCGTCGGACTTAAGGAGGAGCAAGCCCACAACGAGCAAGACGTCCTTGAGCTTATCAACAAGGGAACCCTTGTCCGCACCGCCGGAACCACCTCTGCCAACGCCAACTCTTCTCGCTCTCACGCCATCTTCCAAATCATCCTTCGCCA
>intron_5
GTTAGTTTTTGTTTTTACGTGAAGATTGCTTTAAAATTAATTATTTTCAG
>exon_5
AGGAAAGAAGGTCTGGGGAAAGTTCTCTCTTATCGACCTTGCCGGAAACGAGCGCGGACAAGACACCCGCGAGTGCGACCGCGACACCCGCAAGGAGGGAGCCAACATCAACACCTCTCTTCTTGCCCTTAAGGAGTGCATCCGCGGAATGGCCCGCAACTCTTCTCACGTCCCATTCCGCCAATCTAAGCTTACCATGGTCCTTCGCGACTCTTTCATCGGAGAGAAGTCTCGCACCGTCATGATCTCTATGATCTCTCCAGGAATCTCTTCTTCTGACCACACCCTTAACACCCTTCGCTACGCCGACCG
>intron_6
GTAGGAATTTTATTTATTTTTTGCTAAAAAAAATTCACATTTTTCAG
>exon_6
CGTCAAGGAGATGGGAACCGACGGATCTGGAGAGGCCACCCCAATCCGCGACGAGGAGCTTTTCCTTCCACCATCTGCCGACAAGTCTGACGAGGAGTACGATGAGATGGTCGAGAAGCAAGAGCACCGTCGTGTCGCTGTCGACCACGTCCGCAACCTTAAGGACATGTCTGAGAAGATCATCCGCGAGACCACCATCGTCCTTTCTAACGAGCCATCTGCCGCCCAAAAGGCCGAGTGCCTTGCCAAGCTTGACCAACTTGCCCAAATCGTCTCTAACACCCGCGTCGCCGTCGAGAACGTCTGA
>3UTR
TTGCTGATTCCCGCGACGATATGCTAC
"""
# TODO: check translation of exons is the same as original

klp7co_transcript_parts = ["3UTR", "exon_1", "intron_2", "exon_2", "intron_3", "exon_3", "intron_4", "exon_4", "intron_5", "exon_5", "intron_6", "exon_6", "3UTR"]


def main():
    """Main function of the program."""
    with FS("/pasteur/entites/Mhe/Genomes/transgenes") as fs:
        with fs.open("klp7co_transgene_parts.fa", "w") as transgene_file:
            transgene_file.write(fasta_transgene)

        sequence_parts = OrderedDict((seq_name, seq) for (seq_name, seq, _) in fastx_read(transgene_file.name))
        klp7co_seq = "".join(sequence_parts[seq_name] for seq_name in klp7co_transcript_parts)
        with fs.open("klp7co_transcript.fa", "w") as transcript_file:
            transcript_file.write(f">klp7co\n{klp7co_seq}\n")
        with fs.open("klp7co.gtf", "w") as transcript_gtf_file:
            annotations = "gene_id \"klp7co\"; transcript_id \"klp7co\"; gene_biotype \"protein_coding\";"
            transcript_gtf_file.write("\t".join([
                "klp7co", "local", "transcript", "1", str(len(klp7co_seq)),
                "0", "+", ".", annotations]))
            transcript_gtf_file.write("\n")
            with fs.open("klp7co_transgene.gtf", "w") as transgene_gtf_file:
                last_pos_in_transgene = 0
                last_pos_in_transcript = 0
                exon_number = 0
                for (seq_name, seq) in sequence_parts.items():
                    start_in_transgene = last_pos_in_transgene + 1
                    end_in_transgene = last_pos_in_transgene + len(seq)
                    # if seq_name == "mex5_promoter":
                    #     annotations = "gene_id \"mex5\"; transcript_id \"mex5\"; gene_biotype \"transgene\";"
                    #     transgene_gtf_file.write("\t".join([
                    #         "klp7co_transgene", "local", "UTR", str(start_in_transgene), str(end_in_transgene),
                    #         "0", "+", ".", annotations]))
                    #     transgene_gtf_file.write("\n")
                    if seq_name == "5UTR":
                        annotations = "gene_id \"klp7co\"; transcript_id \"klp7co\"; gene_biotype \"transgene\";"
                        transgene_gtf_file.write("\t".join([
                            "klp7co_transgene", "local", "UTR", str(start_in_transgene), str(end_in_transgene),
                            "0", "+", ".", annotations]))
                        transgene_gtf_file.write("\n")
                    elif seq_name.startswith("exon"):
                        start_in_transcript = last_pos_in_transcript + 1
                        end_in_transcript = last_pos_in_transcript + len(seq)
                        exon_number += 1
                        annotations = f"exon_id \"klp7co.e{exon_number}\"; exon_number \"{exon_number}\"; gene_id \"klp7co\"; transcript_id \"klp7co\"; gene_biotype \"transgene\";"
                        transgene_gtf_file.write("\t".join([
                            "klp7co_transgene", "local", "exon", str(start_in_transgene), str(end_in_transgene),
                            "0", "+", ".", annotations]))
                        transgene_gtf_file.write("\n")
                        annotations = f"exon_id \"klp7co.e{exon_number}\"; exon_number \"{exon_number}\"; gene_id \"klp7co\"; transcript_id \"klp7co\"; gene_biotype \"protein_coding\";"
                        transcript_gtf_file.write("\t".join([
                            "klp7co", "local", "exon", str(start_in_transcript), str(end_in_transcript),
                            "0", "+", ".", annotations]))
                        transcript_gtf_file.write("\n")
                        last_pos_in_transcript = end_in_transcript
                    elif seq_name.startswith("intron"):
                        start_in_transcript = last_pos_in_transcript + 1
                        end_in_transcript = last_pos_in_transcript + len(seq)
                        annotations = "gene_id \"klp7co\"; transcript_id \"klp7co\"; gene_biotype \"transgene\";"
                        transgene_gtf_file.write("\t".join([
                            "klp7co_transgene", "local", "intron", str(start_in_transgene), str(end_in_transgene),
                            "0", "+", ".", annotations]))
                        transgene_gtf_file.write("\n")
                        last_pos_in_transcript = end_in_transcript
                    # elif seq_name == "his11":
                    #     annotations = "gene_id \"his11\"; transcript_id \"his11\"; gene_biotype \"transgene\";"
                    #     transgene_gtf_file.write("\t".join([
                    #         "klp7co_transgene", "local", "transcript", str(start_in_transgene), str(end_in_transgene),
                    #         "0", "+", ".", annotations]))
                    #     transgene_gtf_file.write("\n")
                    # elif seq_name == "tbb2_3UTR":
                    #     annotations = "gene_id \"tbb2\"; transcript_id \"tbb2\"; gene_biotype \"transgene\";"
                    #     transgene_gtf_file.write("\t".join([
                    #         "klp7co_transgene", "local", "UTR", str(start_in_transgene), str(end_in_transgene),
                    #         "0", "+", ".", annotations]))
                    #     transgene_gtf_file.write("\n")
                    elif seq_name == "3UTR":
                        annotations = "gene_id \"klp7co\"; transcript_id \"klp7co\"; gene_biotype \"transgene\";"
                        transgene_gtf_file.write("\t".join([
                            "klp7co_transgene", "local", "UTR", str(start_in_transgene), str(end_in_transgene),
                            "0", "+", ".", annotations]))
                        transgene_gtf_file.write("\n")
                    else:
                        raise ValueError(f"{seq_name} does not belong to the transgene.")
                    last_pos_in_transgene = end_in_transgene
    return 0


if __name__ == "__main__":
    sys.exit(main())
