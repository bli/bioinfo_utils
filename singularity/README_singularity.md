Running the pipelines using singularity
=======================================

This folder contains a recipe to build a singularity container for the
workflows and their dependencies.

To build the container run the following command:

    sudo singularity build run_pipeline run_pipeline.def


Of course, you need singularity, and administrative rights to do that (that's
part of how singularity works).

This folder also contain a shell script wrapper and aliases that will run one
of the workflows using the container. The built container should be present in
the same directory as the wrapper and aliases.

Each alias will run a specific pipeline, depending on its name.
There may be several aliases for a same pipeline.
Here are the aliases available for each pipeline: 

* sRNA-seq
    - `run_sRNA-seq_pipeline`
    - `run_small_RNA-seq_pipeline`
* GRO-seq
    - `run_GRO-seq_pipeline`
    - `run_PRO-seq_pipeline`
* RNA-seq
    - `run_RNA-seq_pipeline`
* Degradome-seq
    - `run_Degradome-seq_pipeline`
* Ribo-seq
    - `run_Ribo-seq_pipeline`
* iCLIP (which has a special "non-seq" name)
    - `run_iCLIP-seq_pipeline`
    - `run_iCLIP_pipeline`

### TODO

The container takes a long time to build. Maybe it should be split into smaller
bits, used by a larger one.
