#!/bin/bash -l
# Usage: workflows_shell.sh [-v|--version|--licence]
# Run an interactive shell in the same container that is used for the workflows.
#
# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename "${0}")

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

# https://stackoverflow.com/a/1638397/1878788
# Absolute path to this script
SCRIPT=$(readlink -f "${0}")
# Absolute path this script is in
BASEDIR=$(dirname "${SCRIPT}")
container="${BASEDIR}/run_pipeline"
# If we are on this machine, then the pipeline will be run without sbatch
[[ ${DEFAULT_HOSTNAME} ]] || DEFAULT_HOSTNAME="pisa"

# Do we have singularity?
singularity --version 2> /dev/null && have_singularity=1

if [ ! ${have_singularity} ]
then
    install_doc="https://sylabs.io/guides/3.4/user-guide/quick_start.html#quick-installation-steps"
    # Do we have an environment modules system?
    module --version 2> /dev/null && have_modules=1
    if [ ${have_modules} ]
    then
        module load apptainer || error_exit "singularity is needed to run the pipelines (see ${install_doc})"
    else
        error_exit "singularity is needed to run the pipelines (see ${install_doc})"
    fi
fi

if [ ! -e ${container} ]
then
    if [ -e ${BASEDIR}/run_pipeline.def ]
    then
        echo "The container was not found. Trying to build it. This may take quite some time and requires sudoer's rights."
        sudo singularity build ${BASEDIR}/run_pipeline ${BASEDIR}/run_pipeline.def || error_exit "The container could not be built."
    else
        error_exit "The container was not found, nor a definition file to build it."
    fi
fi

case ${1} in
    "--version" | "-v")
        singularity run ${container} -v
        ;;
    "--licence")
        singularity run ${container} --licence
        ;;
    *)
        # -B /pasteur will mount /pasteur in the container
        # so that it finds the Genome configuration and gene lists
        # that are expected to be in a specific location there.
        [[ $(hostname) = ${DEFAULT_HOSTNAME} ]] && SINGULARITYENV_USER=${USER} singularity shell -B /pasteur -B /local -B /run/shm:/run/shm ${container} $@ || APPTAINERENV_USER=${USER} apptainer shell -B /opt/hpc/slurm -B /var/run/munge -B /pasteur -B /local ${container} $@
        ;;
esac
