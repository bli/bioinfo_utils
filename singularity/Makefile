VERSION := $(shell git log | head -1 | cut -d " " -f2)

ifeq ($(PREFIX),)
	PREFIX := /opt/bioinfo_utils
endif

ifeq ($(TEST_PREFIX),)
	TEST_PREFIX := /opt/test_bioinfo_utils
endif

all: run_pipeline

install: run_pipeline run_pipeline.sh workflows_shell.sh wrap_in_container.sh cluster_config.json
	sudo install -d $(PREFIX)_$(VERSION)/bin
	sudo install run_pipeline $(PREFIX)_$(VERSION)/bin/.
	sudo install run_pipeline.sh $(PREFIX)_$(VERSION)/bin/.
	sudo install workflows_shell.sh $(PREFIX)_$(VERSION)/bin/.
	sudo install wrap_in_container.sh $(PREFIX)_$(VERSION)/bin/.
	sudo install cluster_config.json $(PREFIX)_$(VERSION)/bin/.
	for datatype in sRNA-seq small_RNA-seq RNA-seq GRO-seq PRO-seq Degradome-seq Ribo-seq iCLIP-seq iCLIP; \
	do \
	    sudo ln -sf $(PREFIX)_$(VERSION)/bin/run_pipeline.sh $(PREFIX)_$(VERSION)/bin/run_$${datatype}_pipeline; \
	done
	sudo ln -sfn $(PREFIX)_$(VERSION) $(PREFIX)

# To test before replacing "prod" version in $(PREFIX)
install_test: run_pipeline run_pipeline.sh workflows_shell.sh wrap_in_container.sh cluster_config.json
	sudo install -d $(TEST_PREFIX)_$(VERSION)/bin
	sudo install run_pipeline $(TEST_PREFIX)_$(VERSION)/bin/.
	sudo install run_pipeline.sh $(TEST_PREFIX)_$(VERSION)/bin/.
	sudo install workflows_shell.sh $(TEST_PREFIX)_$(VERSION)/bin/.
	sudo install wrap_in_container.sh $(TEST_PREFIX)_$(VERSION)/bin/.
	sudo install cluster_config.json $(TEST_PREFIX)_$(VERSION)/bin/.
	for datatype in sRNA-seq small_RNA-seq RNA-seq GRO-seq PRO-seq Degradome-seq Ribo-seq iCLIP-seq iCLIP; \
	do \
	    sudo ln -sf $(TEST_PREFIX)_$(VERSION)/bin/run_pipeline.sh $(TEST_PREFIX)_$(VERSION)/bin/run_$${datatype}_pipeline; \
	done
	sudo ln -sfn $(TEST_PREFIX)_$(VERSION) $(TEST_PREFIX)

# TODO: Bind home on temporary directory at build time in order to decrease "pollution" risks?
# Should depend on the rest of the repository.
run_pipeline: run_pipeline.def workflows_base.sif
	sh -c "yes | sudo singularity build run_pipeline run_pipeline.def | tee build.err 2>&1"

workflows_base.sif: workflows_base.def
	sh -c "yes | sudo singularity build workflows_base.sif workflows_base.def | tee build_base.err 2>&1"
