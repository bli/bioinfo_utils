#!/usr/bin/env python3
# vim: set fileencoding=<utf-8> :
"""This script parses tab-separated input and converts the fields using the
dictionary contained in the pickle file provided as first argument.
Input is taken in the file given as second argument or in stdin."""

from sys import argv, stdin, stdout
# To store dictionaries
from pickle import load


def strip_split(text):
    return text.strip().split("\t")


input_filename = None
if len(argv) == 2:
    pickle_filename = argv[1]
elif len(argv) == 3:
    pickle_filename = argv[1]
    input_filename = argv[2]
elif len(argv) == 1:
    pickle_filename = "/pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes_id2name.pickle"
else:
    exit(__doc__)

with open(pickle_filename, "rb") as pickle_file:
    id2name = load(pickle_file)

if input_filename is None or input_filename == "-":
    input_source = stdin
else:
    input_source = open(input_filename, "r")

for fields in map(strip_split, input_source):
    print(*[id2name.get(field, field) for field in fields], sep="\t", file=stdout)

if input_filename is not None:
    input_source.close()


exit(0)
