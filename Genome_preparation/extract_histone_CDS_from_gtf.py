#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script extracts information about histone genes from an Ensembl gtf
annotation file."""

from __future__ import print_function

import sys

# To store dictionaries
from cPickle import dump, HIGHEST_PROTOCOL


#UTR5 = 0
#UTR3 = 0
#UTR5 = 40
#UTR3 = 40
UTR5 = 40
UTR3 = 200

in_gtf = sys.argv[1]
protein_names = set(sys.argv[2:])


# To check that it is sorted
chromosomes = [None]
start = 0
with open(in_gtf, "r") as gtf:
    for line in gtf:
        fields = line.strip().split("\t")
        chrom = fields[0]
        if chrom != chromosomes[-1]:
            assert chrom not in chromosomes, "Chromosomes do not seem to be grouped."
            chromosomes.append(chrom)
            start = 0
        else:
            assert int(fields[3]) >= start, "Positions do not seem to be sorted."
        start = int(fields[3])
        if fields[2] == "CDS":
            annots = dict([(k, v.strip('"')) for (k, v) in [f.split(" ") for f in fields[8].split("; ")[:-1]]])
            protein_id = annots["protein_id"]
            #if gene_name.startswith("his-"):
            if protein_id in protein_names:
                gene_name = annots["gene_name"]
                end = int(fields[4])
                score = fields[5]
                strand = fields[6]
                exon = annots["exon_number"]
                gene_id = annots["gene_id"]
                if exon != "1":
                    continue
                if strand == "+":
                    print(chrom, start - 1 - UTR5, end + UTR3, "::".join([protein_id, exon, gene_id, gene_name]), score, strand, sep="\t")
                else:
                    print(chrom, start - 1 - UTR3, end + UTR5, "::".join([protein_id, exon, gene_id, gene_name]), score, strand, sep="\t")

sys.exit(0)
