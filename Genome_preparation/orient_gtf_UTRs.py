#!/usr/bin/env python3
# vim: set fileencoding=<utf-8> :
"""This script reads a gtf file and extracts and orients UTRs."""

import argparse
import os
import sys
import warnings
from re import sub
from collections import defaultdict, namedtuple

from cytoolz import valmap


OPJ = os.path.join


def formatwarning(message, category, filename, lineno, line):
    """Format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)


warnings.formatwarning = formatwarning


def split_info(text):
    """Split a gtf annotation item in a (key, value) pair."""
    return text.split(" ")


def unquote(text):
    """Remove double quotes from a gtf annotation value."""
    return text.strip("\"")


def parse_annots(annot_string):
    """Parse the content of a gtf annotation string into a dict."""
    annot_dict = dict(map(split_info, annot_string.rstrip(";").split("; ")))
    return valmap(unquote, annot_dict)


UTR = namedtuple("UTR", ["chrom", "start", "end", "strand", "gtf_line"])


class Transcript:
    """A transcript with its coordinates and UTRs."""
    def __init__(self):
        """An init without argument enables defaultdict."""
        self._coords_set = False
        self.chrom = None
        self.start = None
        self.end = None
        self.strand = None
        self.annots = None
        self.utrs = {"unchecked": [], "5UTR": [], "3UTR": []}

    def set_annots(self, annots):
        """Used when the real transcript is encountered."""
        if self.annots is not None:
            msg = "Transcript annotations should not change."
            assert self.annots == annots, msg
        else:
            self.annots = annots

    def set_coords(self, chrom, start, end, strand):
        """Used when the real transcript is encountered."""
        if self._coords_set:
            raise AttributeError(
                "Transcript coordinates can only be set once.")
        self.chrom = chrom
        self.start = int(start)
        self.end = int(end)
        self.strand = strand
        self._coords_set = True

    def add_utr(self, chrom, start, end, strand, gtf_line):
        """Used when an UTR is encountered."""
        self.utrs["unchecked"].append(
            UTR(chrom, int(start), int(end), strand, gtf_line))

    def check_utrs(self):
        """Assign UTRs to the correct type (3' or 5')."""
        msg = "Cannot check UTRs when transcript coordinates are not known."
        assert self._coords_set, msg
        while self.utrs["unchecked"]:
            utr = self.utrs["unchecked"].pop()
            assert self.start <= utr.start, "UTR starts too early."
            assert self.end >= utr.end, "UTR ends too late."
            msg = "UTR and transcript orientation differ."
            assert utr.strand == self.strand, msg
            if (
                    (self.strand == "+" and utr.start == self.start)
                    or (self.strand == "-" and utr.end == self.end)):
                self.utrs["5UTR"].append(
                    UTR(utr.chrom, utr.start, utr.end, utr.strand, utr.gtf_line))
            elif (
                    (self.strand == "+" and utr.end == self.end)
                    or (self.strand == "-" and utr.start == self.start)):
                self.utrs["3UTR"].append(
                    UTR(utr.chrom, utr.start, utr.end, utr.strand, utr.gtf_line))
            else:
                warnings.warn("The following UTR is neither 5' nor 3'.\n"
                              f"{utr.gtf_line}\n"
                              "Discarding it.\n")


def write_UTRs_to_files(transcripts, UTR5_filename, UTR3_filename):
    """
    Classify UTRs and write their gtf representation in the corresponding file.
    """
    with open(UTR5_filename, "w") as UTR5_file, \
            open(UTR3_filename, "w") as UTR3_file:
        for transcript in transcripts.values():
            transcript.check_utrs()
            UTR5_file.writelines(sub(
                "UTR", "transcript",
                utr.gtf_line, count=1) for utr in transcript.utrs["5UTR"])
            UTR3_file.writelines(sub(
                "UTR", "transcript",
                utr.gtf_line, count=1) for utr in transcript.utrs["3UTR"])


def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-a", "--annot_dir",
        required=True,
        help="Directory in which to read annotations "
        "and write the resulting file.")
    args = parser.parse_args()
    annot_dir = args.annot_dir
    transcripts = defaultdict(Transcript)
    with open(OPJ(annot_dir, "genes.gtf"), "r") as genes_gtf:
        for line in genes_gtf:
            (
                chrom, _, feature_type, start, end,
                _, strand, _, annot_string) = line.strip().split("\t")
            if feature_type not in {"transcript", "UTR"}:
                continue
            annot_dict = parse_annots(annot_string)
            # print(annot_dict)
            transcript_id = annot_dict["transcript_id"]
            if feature_type == "transcript":
                transcripts[transcript_id].set_annots(annot_dict)
                transcripts[transcript_id].set_coords(
                    chrom, start, end, strand)
            elif feature_type == "UTR":
                transcripts[transcript_id].add_utr(
                    chrom, start, end, strand, line)
            else:
                raise ValueError(f"{feature_type} should not be found here.")
    write_UTRs_to_files(
        transcripts,
        OPJ(annot_dir, "protein_coding_5UTR.gtf"),
        OPJ(annot_dir, "protein_coding_3UTR.gtf"))
    return 0


if __name__ == "__main__":
    sys.exit(main())
