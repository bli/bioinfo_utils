#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script parses an xml file about transposons downloaded from Wormbase
and extracts the association between cosmid-derived IDs, WBTransposon*
identifiers and transposon families."""

from __future__ import print_function

import sys

from string import strip

from bs4 import BeautifulSoup

in_xml = sys.argv[1]

with open(in_xml, "r") as xml_file:
    xml_structure = BeautifulSoup(xml_file.read(), "html.parser")

for transposon_info in xml_structure.find_all("transposon"):
    info_items = list(transposon_info.children)
    try:
        cosmid_id = transposon_info.molecular_info.corresponding_cds.cds.contents[0]
        print(cosmid_id, end="\t")
    except AttributeError:
        print(".", end="\t")
    transposon_id = strip(info_items[0])
    print(transposon_id, end="\t")
    try:
        old_name = transposon_info.origin.member_of.old_name.text.contents[0]
    except AttributeError:
        old_name = None
    try:
        transposon_family = transposon_info.origin.member_of.transposon_family.contents[0]
        print(transposon_family)
    except AttributeError:
        print("UNKNOWN")


sys.exit(0)
