#!/usr/bin/env pypy
# vim: set fileencoding=<utf-8> :
"""This script converts an Ensembl gtf annotation file into a bed file."""

#from __future__ import print_function

#from sys import argv, stdout, exit
from sys import exit
import fileinput

#WRITE = stdout.write

from itertools import imap
from string import split, strip

JOIN = "\t".join

def strip_split(text):
    return split(strip(text), "\t")



#with open(in_gtf, "r") as gtf:
for chrom, _, _, start, end, score, strand, _, annot_field in imap(strip_split, fileinput.input()):
    # TODO: The value for the last pair may have an extra ";"
    # This only works if gene_id is not the last key
    annots = dict([(k, v.strip('"')) for (k, v) in [f.split(" ") for f in annot_field[:-1].split("; ")]])
    #biotype = annots["gene_biotype"]
    gene_id = annots["gene_id"]
    print JOIN([chrom, str(int(start)-1), end, gene_id, score, strand])
    #print(chrom, int(start)-1, end, gene_id, score, strand, sep="\t")

exit(0)
