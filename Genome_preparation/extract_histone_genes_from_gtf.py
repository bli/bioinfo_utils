#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script extracts information about histon genes from an Ensembl gtf
annotation file."""

from __future__ import print_function

from os.path import splitext
import sys
from collections import defaultdict

# To store dictionaries
from cPickle import dump, HIGHEST_PROTOCOL


in_gtf = sys.argv[1]
base, ext = splitext(in_gtf)


max_lengths = defaultdict(int)
# To check that it is sorted
chromosomes = [None]
start = 0
with open(in_gtf, "r") as gtf:
    for line in gtf:
        fields = line.strip().split("\t")
        chrom = fields[0]
        if chrom != chromosomes[-1]:
            assert chrom not in chromosomes, "Chromosomes do not seem to be grouped."
            chromosomes.append(chrom)
            start = 0
        else:
            assert int(fields[3]) >= start, "Positions do not seem to be sorted."
        start = int(fields[3])
        if fields[2] == "transcript":
            annots = dict([(k, v.strip('"')) for (k, v) in [f.split(" ") for f in fields[8].split("; ")[:-1]]])
            if "exon_number" not in annots:
                if annots["gene_biotype"] != "protein_coding":
                    continue
                gene_name = annots["gene_name"]
                if gene_name.startswith("his-"):
                    print(line, end="")

sys.exit(0)
