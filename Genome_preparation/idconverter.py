#!/usr/bin/env python3
# vim: set fileencoding=<utf-8> :
"""This script parses tab-separated input and converts the fields using the
dictionary contained in the pickle file provided as first argument.
Input is taken in the file given as second argument or in stdin."""

# from __future__ import print_function
from sys import argv, exit, stdin, stdout

# from string import split, strip
# from itertools import imap

SPLIT = str.split
STRIP = str.strip
def strip_split(text):
    return SPLIT(STRIP(text), "\t")

# To store dictionaries
# from cPickle import load
from pickle import load


input_filename = None
if len(argv) == 2:
    pickle_filename = argv[1]
elif len(argv) == 3:
    pickle_filename = argv[1]
    input_filename = argv[2]
else:
    exit(__doc__)

with open(pickle_filename, "rb") as pickle_file:
    converter = load(pickle_file)

if input_filename is None or input_filename == "-":
    input_source = stdin
else:
    input_source = open(input_filename, "r")

# for fields in imap(strip_split, input_source):
for fields in map(strip_split, input_source):
    print(*[converter.get(field, field) for field in fields], sep="\t", file=stdout)

if input_filename is not None:
    input_source.close()


exit(0)
