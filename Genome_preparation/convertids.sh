#!/bin/bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

genome_dir=${HOME}/Genomes
genome="C_elegans"
dict_dir="${genome_dir}/${genome}/Wormbase/WS253/geneIDs"
# This script can be called from various symbolic links.
# The name of the link determines which dictionary to load.
dict=${dict_dir}/${PROGNAME}.pickle

if [ ${1} ]
then
    cmd="idconverter.py $dict ${1}"
else
    cmd="idconverter.py $dict < /dev/stdin"
fi
eval ${cmd} || error_exit "${cmd} failed"

exit 0
