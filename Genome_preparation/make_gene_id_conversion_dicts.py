#!/usr/bin/env python3
# vim: set fileencoding=<utf-8> :
"""This script pickles python dictionaries converting between various kinds of
gene identifiers, provided as the tab-separated columns of the file given as
first argument."""

# from __future__ import print_function
# from string import split, strip
# from itertools import imap
# To store dictionaries
# from cPickle import dump, HIGHEST_PROTOCOL
from pickle import dump, HIGHEST_PROTOCOL
import sys
import os.path
OPB = os.path.basename
OPD = os.path.dirname
OPJ = os.path.join
SPLIT = str.split
STRIP = str.strip

def strip_split(text):
    """This function strips a line and splits it into fields according to
    tabulations."""
    return SPLIT(STRIP(text), "\t")


def main():
    """Main function of the script."""
    cosmid2name = {}
    name2cosmid = {}

    wormid2name = {}
    name2wormid = {}

    cosmid2wormid = {}
    wormid2cosmid = {}


    with open(sys.argv[1], "r") as id_file:
        dict_dir = OPD(id_file.name)
        # for (cosmid, wormid, name) in imap(strip_split, id_file):
        for (cosmid, wormid, name) in map(strip_split, id_file):
            if name != "." and cosmid != ".":
                cosmid2name[cosmid] = name
                name2cosmid[name] = cosmid
            if name != "." and wormid != ".":
                wormid2name[wormid] = name
                name2wormid[name] = wormid
            if wormid != "." and cosmid != ".":
                cosmid2wormid[cosmid] = wormid
                wormid2cosmid[wormid] = cosmid


    with open(OPJ(dict_dir, "cosmid2name.pickle"), "wb") as pickle_file:
        print("Storing cosmid2name in %s" % pickle_file.name)
        dump(cosmid2name, pickle_file, HIGHEST_PROTOCOL)
    with open(OPJ(dict_dir, "name2cosmid.pickle"), "wb") as pickle_file:
        print("Storing name2cosmid in %s" % pickle_file.name)
        dump(name2cosmid, pickle_file, HIGHEST_PROTOCOL)

    with open(OPJ(dict_dir, "wormid2name.pickle"), "wb") as pickle_file:
        print("Storing wormid2name in %s" % pickle_file.name)
        dump(wormid2name, pickle_file, HIGHEST_PROTOCOL)
    with open(OPJ(dict_dir, "name2wormid.pickle"), "wb") as pickle_file:
        print("Storing name2wormid in %s" % pickle_file.name)
        dump(name2wormid, pickle_file, HIGHEST_PROTOCOL)

    with open(OPJ(dict_dir, "cosmid2wormid.pickle"), "wb") as pickle_file:
        print("Storing cosmid2wormid in %s" % pickle_file.name)
        dump(cosmid2wormid, pickle_file, HIGHEST_PROTOCOL)
    with open(OPJ(dict_dir, "wormid2cosmid.pickle"), "wb") as pickle_file:
        print("Storing wormid2cosmid in %s" % pickle_file.name)
        dump(wormid2cosmid, pickle_file, HIGHEST_PROTOCOL)

    return 0

sys.exit(main())
