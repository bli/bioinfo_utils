#!/usr/bin/env pypy
# vim: set fileencoding=<utf-8> :
"""This script reads a bed input and merges successive entries that have the same name.
The merged result spans the original entries."""

#from __future__ import print_function

#from sys import argv, stdout, exit
from sys import exit
import fileinput

#WRITE = stdout.write

from itertools import imap
from string import split, strip

JOIN = "\t".join

def strip_split(text):
    return split(strip(text), "\t")



#with open(in_gtf, "r") as gtf:
c_chrom, c_name, c_score, c_strand = None, None, None, None
for chrom, start, end, name, score, strand in imap(strip_split, fileinput.input()):
    if name != c_name:
        if c_name is not None:
            print JOIN([c_chrom, str(c_start - 1), str(c_end), c_name, c_score, c_strand])
        c_chrom, c_start, c_end, c_name, c_score, c_strand = chrom, int(start), int(end), name, score, strand
    else:
        c_entry = JOIN([c_chrom, c_score, c_strand])
        entry = JOIN([chrom, score, strand])
        assert entry == c_entry, "Impossible to merge %s with %s" % (entry, c_entry)
        # chromosome, name, score and strand are the same.
        # Update start and end
        c_start, c_end = min(c_start, int(start)), max(c_end, int(end))
print JOIN([c_chrom, str(c_start - 1), str(c_end), c_name, c_score, c_strand])

exit(0)
