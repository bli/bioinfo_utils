#!/bin/bash
# Usage: gff_merge_transcripts.sh <gff>

# To avoid error_exit when output piped to head
# (together with trap in merge_bedtools and to_gff)
shopt -s lastpipe

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

error_exit()
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


get_gene_id()
{
    # $1: file in which to output gene IDs
    cut -f9 | gtf2attr "gene_id" > ${1} \
        || error_exit "get_gene_id failed"
}

get_bed_coord()
{
    # $1: file in which to output the coordinates
    bioawk -c gff '{print $seqname,$start-1,$end}' > ${1} \
        || error_exit "get_bed_coord failed"
}

get_scorestrand()
{
    # $1: file in which to output the score and strands
    cut -f6,7 > ${1} || error_exit "get_scorestrand failed"
}

merge_bedtools()
{
    # https://bioinformatics.stackexchange.com/q/2265/292
    # -g 1,4,5,6 assumes that for a given name,
    # chromosome, score and strand are always the same
    trap '' PIPE && exec \
    bedtools groupby -g 1,4,5,6 -c 2,3 -o min,max -i "stdin" \
        | mawk -v OFS="\t" '{print $1,$5,$6,$2,$3,$4}' \
        | sort-bed - \
        || error_exit "merge_bedtools failed"
}

setup()
{
    workdir=$(mktemp -d)
    gene_ids="${workdir}/gene_ids.tsv"
    mkfifo ${gene_ids}

    bed_coords="${workdir}/bed_coords.tsv"
    mkfifo ${bed_coords}

    scorestrands="${workdir}/bed_scorestrands.tsv"
    mkfifo ${scorestrands}
}

setup

cleanup()
{
    rm -rf ${workdir}
}

#trap '' PIPE

## https://superuser.com/a/747905/207872
#if [ $# -ge 1 -a -f "${1}" ]
#then
#    gff_in="${1}"
#    cat ${gff_in} \
#        | tee >(get_gene_id ${gene_ids}) \
#        | tee >(get_bed_coord ${bed_coords}) \
#        | get_scorestrand ${scorestrands} \
#        || { cleanup && error_exit "gff info extraction failed"; } &
#else
#    # https://unix.stackexchange.com/a/387940/55127
#    exec 3<&0
#    cat - <&3 \
#        | tee >(get_gene_id ${gene_ids}) \
#        | tee >(get_bed_coord ${bed_coords}) \
#        | get_scorestrand ${scorestrands} \
#        || { cleanup && error_exit "gff info extraction failed"; } &
#fi
# https://unix.stackexchange.com/a/387940/55127
# ens.forum.informatique.shell:5159
# Le fd 0 (l'entrée standard) est dupliqué sur le fd 3 (en lecture,
# mais ça n'a aucune importance). Ils sont alors complètement équivalents
# en ce qui concerne les lectures qui sont faites dessus.
exec 3<&0
# 1. le shell fait sa bêtise de fermer le fd 0 ;
# 2. on copie le fd 3 sur le fd 0 (ce qui a pour effet de le dé-fermer,
#    puisque 3 était une copie) ;
# 3. on lance cat.
<&3 cat $1 \
    | tee >(get_gene_id ${gene_ids}) \
    | tee >(get_bed_coord ${bed_coords}) \
    | get_scorestrand ${scorestrands} \
    || { cleanup && error_exit "gff info extraction failed"; } &
#bed_merged="${workdir}/bed_merged.bed"
#bed_with_gene_ids="${workdir}/bed_with_gene_ids.bed"
#mkfifo ${bed_with_gene_ids}

#paste ${bed_coords} ${gene_ids} ${scorestrands} > ${bed_with_gene_ids} || cleanup && error_exit "pasting info failed"


#first_sigpipe_handler()
#{
#    trap '' PIPE
#}
#
#trap first_sigpipe_handler PIPE

#paste ${bed_coords} ${gene_ids} ${scorestrands} | bedtools groupby -g 4 -c 5 -o first || cleanup && error_exit "pasting and grouping info failed"
#trap '' PIPE && exec \
paste ${bed_coords} ${gene_ids} ${scorestrands} \
    | LC_ALL=C sort -k4 \
    | merge_bedtools \
    || { cleanup && error_exit "merging on gene_id failed"; }


#paste ${bed_coords} ${gene_ids} ${scorestrands} \
#    | sort -k4 \
#    | merge_bedtools \
#    | tee ${bed_merged} \
#    | to_gff \
#    || { cleanup && error_exit "merging on gene_id failed"; }
#paste ${bed_coords} ${gene_ids} ${scorestrands} \
#    | merge_to_gff \
#    || { cleanup && error_exit "merging on gene_id failed"; }

#cleanup

exit 0
