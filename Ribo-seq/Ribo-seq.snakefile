# Copyright (C) 2020-2023 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Snakefile to analyse Ribo-seq data.

TODO: Some figures and summaries may be overridden when changing the mapper. The mapper name should be added to their path.
"""
import sys
major, minor = sys.version_info[:2]
if major < 3 or (major == 3 and minor < 6):
    sys.exit("Need at least python 3.6\n")


# Bias removal: only consider after 15-th codon and before -5-th codon of CDS
# TODO: compute TPM of RPF and "normalize" by TPM of transcripts from RNA-seq data (translation efficiency (TE))
# graph: x: RNA-seq TPM, y: log2(Ribo_TPM/RNA_TPM)
# or y: log2folds of deseq2 normalized values
# Other option: use scikit-ribo? (more detailed analysis)
# Done: define RPF as size-selected (likely 28-30), that map in sense orientation on protein-coding genes

# Done: extract RPF reads
# bedtools intersect \
#     -a results_Ribo-seq_test_2/bowtie2/mapped_C_elegans/WT_1/28-30_on_C_elegans_sorted.bam \
#     -b /pasteur/homes/bli/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding_merged.bed \
#     -wa -u -f 1.0 \
#     | samtools view | bioawk -c sam '{print "@"$qname"\n"$seq"\n+\n"$qual}' > /tmp/bioawk.fq
# 
# mkfifo /tmp/test.bam
# bedtools intersect \
#     -a results_Ribo-seq_test_2/bowtie2/mapped_C_elegans/WT_1/28-30_on_C_elegans_sorted.bam \
#     -b /pasteur/homes/bli/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding_merged.bed \
#     -wa -u -f 1.0  > /tmp/test.bam &
# bedtools bamtofastq -i /tmp/test.bam -fq /tmp/bamtofastq.fq
# rm /tmp/test.bam

# Total number of "non-structural" (mapped - (fwd-(t,sn,sno,r-RNA))) to compute RPKM
# Quick-and-dirty: use gene span (TES - TSS) -> done for repeats
# Better: use length of union of exons for each gene -> done for genes
# Ideal: use transcriptome-based per-isoform computation
# Actually, we don't want a normalization by length. We deal with small RNAs, not transcripts


# Possibly filter out on RPKM
# Then, compute folds of RP(K)M IP/input (for a given experiment, i.-e. REP)
# and give list of genes sorted by such folds -> see /Gene_lists/csr1_prot_si_supertargets*

# Exploratory:
# Heatmap (fold)
# genes | experiment

# Then either:
# - take the genes (above log-fold threshold) common across replicates
# - look at irreproducible discovery rate (http://dx.doi.org/doi:10.1214%2F11-AOAS466)
# -> define CSR-1-loaded

# For metagene: see --metagene option of computeMatrix
# Retrieve gtf info after filtering out interfering genes based on merged bed


import os
OPJ = os.path.join
from distutils.util import strtobool
from glob import glob
from gzip import open as gzopen
from re import sub
from pickle import dump, load
from yaml import safe_load as yload
from fileinput import input as finput
from shutil import copyfile
from sys import stderr
from subprocess import Popen, PIPE, CalledProcessError
# Warning: "legacy" and unsafe (https://docs.python.org/3/library/subprocess.html#legacy-shell-invocation-functions)
from subprocess import getoutput
# Useful data structures
from collections import OrderedDict as od
from collections import defaultdict, Counter

# Useful for functional style
from itertools import chain, combinations, product, repeat, starmap
from functools import reduce
from operator import attrgetter
from operator import or_ as union
from cytoolz import concat, merge_with, take_nth, valmap


def concat_lists(lists):
    return list(concat(lists))


import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)


warnings.formatwarning = formatwarning


# from gatb import Bank
from mappy import fastx_read
# To parse SAM format
import pysam
import pyBigWig

# To compute correlation coefficient
# from scipy.stats.stats import pearsonr
# To catch errors when plotting KDE
from scipy.linalg import LinAlgError
# For data processing and displaying
from sklearn import preprocessing
from sklearn.decomposition import PCA
import matplotlib as mpl
# To be able to run the script without a defined $DISPLAY
# https://github.com/mwaskom/seaborn/issues/1262
#mpl.use("agg")
mpl.use("PDF")
#mpl.rcParams["figure.figsize"] = 2, 4
mpl.rcParams["font.sans-serif"] = [
    "Arial", "Liberation Sans", "Bitstream Vera Sans"]
mpl.rcParams["font.family"] = "sans-serif"
#mpl.rcParams["figure.figsize"] = [16, 30]

from matplotlib import cm
from matplotlib.colors import Normalize

from matplotlib import numpy as np
from math import ceil, floor
from matplotlib.backends.backend_pdf import PdfPages
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
# import seaborn.apionly as sns
# import husl
# predefined seaborn graphical parameters
sns.set_context("talk")

#from libsmallrna import PI_MIN, PI_MAX, SI_MIN, SI_MAX
# Do this outside the workflow
#from libhts import gtf_2_genes_exon_lengths, repeat_bed_2_lengths
from idconvert import gene_ids_data_dir
from libdeseq import do_deseq2
from libhts import status_setter
from libhts import median_ratio_to_pseudo_ref_size_factors, size_factor_correlations
from libhts import plot_paired_scatters, plot_norm_correlations, plot_counts_distribution, plot_boxplots, plot_histo
from libreads import ReadStats, plot_base_proportions_along_read
from libriboseq import load_codon_goodness, make_fq_filter
from libworkflows import texscape, wc_applied, ensure_relative, cleanup_and_backup
from libworkflows import get_chrom_sizes, column_converter, make_id_list_getter
from libworkflows import read_int_from_file, strip_split, file_len, last_lines, plot_text, save_plot, SHELL_FUNCTIONS
from libworkflows import sum_by_family, read_feature_counts, sum_feature_counts, sum_htseq_counts, warn_context
from smincludes import rules as irules
from smwrappers import wrappers_dir

from pyseqlogo.pyseqlogo import draw_logo

NO_DATA_ERRS = [
    "Empty 'DataFrame': no numeric data to plot",
    "no numeric data to plot"]


alignment_settings = {"bowtie2": "-L 6 -i S,1,0.8 -N 0"}

# Positions in small RNA sequences for which to analyse nucleotide distribution
#POSITIONS = ["first", "last"]
POSITIONS = config["positions"]
# Orientations of small RNAs with respect to an annotated feature orientation.
# "fwd" and "rev" restrict feature quantification to sense or antisense reads.
ORIENTATIONS = config["orientations"]
# small RNA types on which to run DESeq2
DE_TYPES = config["de_types"]
# small RNA types on which to compute IP/input RPM folds
IP_TYPES = ["pisimi", "siu", "prot_si"]
#IP_TYPES = config["ip_types"]
# Cutoffs in log fold change
LFC_CUTOFFS = [0.5, 1, 2]
UP_STATUSES = [f"up{cutoff}" for cutoff in LFC_CUTOFFS]
DOWN_STATUSES = [f"down{cutoff}" for cutoff in LFC_CUTOFFS]
#STANDARDS = ["zscore", "robust", "minmax", "unit"]
# hexbin jointplot for principal components crashes on MemoryError for PCA without standardization
#STANDARDS = ["robust", "identity"]
STANDARDS = ["robust"]

COMPL = {"A" : "T", "C" : "G", "G" : "C", "T" : "A", "N" : "N"}

# Possible feature ID conversions
ID_TYPES = ["name", "cosmid"]

#########################
# Reading configuration #
#########################
# key: library name
# value: 3' adapter sequence
lib2adapt = config["lib2adapt"]
trim5 = config["trim5"]
trim3 = config["trim3"]
# key: library name
# value: path to raw data
lib2raw = config["lib2raw"]
LIBS = list(lib2raw.keys())
# Libraries for which we have matching RNA-seq data
# so that translation efficiency can be computed
EFF_LIBS = list(config["transcriptome_TPM"].keys())
# What type of "efficency" are we computing? Translation efficiency.
[this_TPM, ref_TPM, eff_name] = ["Ribo_TPM", "RNA_TPM", "TE"]
#REF=config["WT"]
#MUT=config["mutant"]
# Used to associate colours to libraries
# Or to make separate plots per series
# key: series name
# value: list of libraries
series_dict = config["series"]
SERIES_TYPES = list(series_dict.keys())
genotype_series = series_dict["genotype_series"]
dt_series = series_dict.get("dt_series", {})
merged_series = merge_with(concat_lists, *series_dict.values())
ALL_SERIES = list(merged_series.keys())
#all_libs_in series = concat_lists(merge_with(concat_lists, *series_dict.values()).values())
REPS = config["replicates"]
DE_COND_PAIRS = config["de_cond_pairs"]
msg = "\n".join([
    "Some contrats do not use known library names.",
    "Contrasts:"
    ", ".join([f"({cond}, {ref})" for (cond, ref) in DE_COND_PAIRS])])
assert all([cond in LIBS and ref in LIBS for (cond, ref) in DE_COND_PAIRS]), msg
DT_COND_PAIRS = config["dt_cond_pairs"]
msg = "\n".join([
    "Some contrats do not use known library names.",
    "Contrasts:"
    ", ".join([f"({cond}, {ref})" for (cond, ref) in DT_COND_PAIRS])])
assert all([cond in LIBS and ref in LIBS for (cond, ref) in DT_COND_PAIRS]), ""
COND_PAIRS = DE_COND_PAIRS + DT_COND_PAIRS
DE_CONTRASTS = [f"{cond1}_vs_{cond2}" for [cond1, cond2] in DE_COND_PAIRS]
DT_CONTRASTS = [f"{cond1}_vs_{cond2}" for [cond1, cond2] in DT_COND_PAIRS]
contrasts_dict = {"de" : DE_CONTRASTS, "dt" : DT_CONTRASTS}
CONTRASTS = DE_CONTRASTS + DT_CONTRASTS
CONTRAST2PAIR = dict(zip(CONTRASTS, COND_PAIRS))
MIN_LEN = config["min_len"]
MAX_LEN = config["max_len"]
size_selected = "%s-%s" % (MIN_LEN, MAX_LEN)
# Which gtf annotations to use when filtering data in order to extract RPF reads?
RPF_BIOTYPES = config.get("rpf_biotypes", ["protein_coding"])
RPF_SUBTYPES = [
    "RPF29",
    "RPF29badA", "RPF29goodA", "RPF29badP", "RPF29goodP",
    "RPF29badAbadP", "RPF29badAgoodP", "RPF29goodAbadP", "RPF29goodAgoodP"]
read_type_max_len = {
    size_selected: int(MAX_LEN),
    "RPF": int(MAX_LEN)}
read_type_max_len.update(
    {rpf_subtype: 29 for rpf_subtype in RPF_SUBTYPES})
read_type_min_len = {
    size_selected: int(MIN_LEN),
    "RPF": int(MIN_LEN)}
read_type_min_len.update(
    {rpf_subtype: 29 for rpf_subtype in RPF_SUBTYPES})
READ_TYPES_FOR_COMPOSITION = [
    size_selected, "RPF",
    "RPF29badA", "RPF29goodA", "RPF29badP", "RPF29goodP",
    "RPF29badAbadP", "RPF29badAgoodP", "RPF29goodAbadP", "RPF29goodAgoodP"]
READ_TYPES_FOR_MAPPING = [
    size_selected, "RPF",
    "RPF29badA", "RPF29goodA", "RPF29badP", "RPF29goodP",
    "RPF29badAbadP", "RPF29badAgoodP", "RPF29goodAbadP", "RPF29goodAgoodP"]
READ_TYPES_FOR_COUNTING = [
    "RPF",
    "RPF29badA", "RPF29goodA", "RPF29badP", "RPF29goodP",
    "RPF29badAbadP", "RPF29badAgoodP", "RPF29goodAbadP", "RPF29goodAgoodP"]

# Types of annotation features, as defined in the "gene_biotype"
# GTF attribute sections of the annotation files.
COUNT_BIOTYPES = config["count_biotypes"]
ANNOT_BIOTYPES = config["annot_biotypes"]
DE_BIOTYPES = [
    "protein_coding",
    "protein_coding_CDS",
    "protein_coding_UTR",
    "protein_coding_5UTR",
    "protein_coding_3UTR",
    "pseudogene",
    "DNA_transposons_rmsk",
    "RNA_transposons_rmsk"]
# cf Gu et al. (2009), supplement:
# -----
# To compare 22G-RNAs derived from a gene, transposon, or pseudogene between two
# samples, each sample was normalized using the total number of reads less structural RNAs, i.e.
# sense small RNA reads likely derived from degraded ncRNAs, tRNAs, snoRNAs, rRNAs,
# snRNAs, and scRNAs. Degradation products of structural RNAs map to the sense strand, with a
# poorly defined size profile and 1 st nucleotide distribution. At least 25 22G-RNA reads per million,
# nonstructural reads in one of the two samples was arbitrarily chosen as a cutoff for comparison
# analyses. A change of 2-fold or more between samples was chosen as an enrichment threshold.
# Because some 21U-RNAs or miRNAs overlap with protein coding genes, reads derived from
# miRNA loci within a window of ± 4 nt and all the known 21U-RNAs were filtered out prior to
# comparison analysis.
# -----
# And Germano Cecere, about scRNA:
# -----
# Its an old nomenclature and in anycase there is only one of this annotated scRNAs
# (small cytoplasmic RNA genes).
# https://www.ncbi.nlm.nih.gov/books/NBK19701/table/genestructure_table2/?report=objectonly
# Don't even pay attention to this
# -----
STRUCTURAL_BIOTYPES = ["tRNA", "snRNA", "snoRNA", "rRNA", "ncRNA"]
BIOTYPES = COUNT_BIOTYPES
GENE_LISTS = config["gene_lists"]
BOXPLOT_GENE_LISTS = config["boxplot_gene_lists"]
#BOXPLOT_GENE_LISTS = [
#    "all_genes",
#    "replication_dependent_octamer_histone",
#    "piRNA_dependent_prot_si_down4",
#    "csr1_prot_si_supertargets_48hph",
#    "spermatogenic_Ortiz_2014", "oogenic_Ortiz_2014"]
aligner = config["aligner"]
########################
# Genome configuration #
########################
# config["genome_dict"] can be either the path to a genome configuration file
# or a dict
if isinstance(config["genome_dict"], (str, bytes)):
    print(f"loading {config['genome_dict']}", file=stderr)
    with open(config["genome_dict"]) as fh:
        genome_dict = yload(fh)
else:
    genome_dict = config["genome_dict"]
genome = genome_dict["name"]
genome_version = genome_dict.get("version", "WBcel235")
chrom_sizes = get_chrom_sizes(genome_dict["size"])
chrom_sizes.update(valmap(int, genome_dict.get("extra_chromosomes", {})))
genomelen = sum(chrom_sizes.values())
genome_db = genome_dict["db"][aligner]
# bed file binning the genome in 10nt bins
genome_binned = genome_dict["binned"]
annot_dir = genome_dict["annot_dir"]
exon_lengths_file = OPJ(annot_dir, "union_exon_lengths.txt"),
# What are the difference between
# OPJ(convert_dir, "wormid2name.pickle") and genome_dict["converter"]?
# /!\ gene_ids_data_dir contains more conversion dicts,
# but is not influenced by genome preparation customization,
# like splitting of miRNAs into 3p and 5p.
convert_dir = genome_dict.get("convert_dir", gene_ids_data_dir)
# For wormid2name, load in priority the one
# that might contain custom gene names, like for splitted miRNAs
with open(
        genome_dict.get(
            "converter",
            OPJ(convert_dir, "wormid2name.pickle")),
        "rb") as dict_file:
    wormid2name = load(dict_file)
gene_lists_dir = genome_dict["gene_lists_dir"]
avail_id_lists = set(glob(OPJ(gene_lists_dir, "*_ids.txt")))
index = genome_db

codon_goodness = load_codon_goodness(genome_dict.get("codon_goodness_table", None))
rpf_subtype2filter = {
    rpf_subtype: make_fq_filter(rpf_subtype, codon_goodness)
        for rpf_subtype in RPF_SUBTYPES}


upload_on_err = strtobool(str(config.get("upload_on_err", "True")))
#output_dir = config["output_dir"]
#workdir: config["output_dir"]
output_dir = os.path.abspath(".")
local_annot_dir = config.get("local_annot_dir", OPJ("annotations"))
log_dir = config.get("log_dir", OPJ("logs"))
data_dir = config.get("data_dir", OPJ("data"))

counter = "feature_count"
counts_dir = OPJ(aligner, f"mapped_{genome}", counter)
# Used to skip some genotype x treatment x replicate number combinations
# when some of them were not sequenced
forbidden = {frozenset(wc_comb.items()) for wc_comb in config["missing"]}
CONDITIONS = [{
    "lib" : lib,
    "rep" : rep} for rep in REPS for lib in LIBS]
# We use this for various things in order to have always the same library order:
COND_NAMES = ["_".join((
    cond["lib"],
    cond["rep"])) for cond in CONDITIONS]
COND_COLUMNS = pd.DataFrame(CONDITIONS).assign(
    cond_name=pd.Series(COND_NAMES).values).set_index("cond_name")
#SIZE_FACTORS = ["raw", "deduped", size_selected, "mapped", "siRNA", "miRNA"]
#SIZE_FACTORS = [size_selected, "mapped", "miRNA"]
#TESTED_SIZE_FACTORS = ["mapped", "non_structural", "siRNA", "miRNA", "median_ratio_to_pseudo_ref"]
TESTED_SIZE_FACTORS = ["mapped", "non_structural", "siRNA", "miRNA", "RPF"]
#SIZE_FACTORS = ["mapped", "miRNA", "median_ratio_to_pseudo_ref"]
# "median_ratio_to_pseudo_ref" is a size factor adapted from
# the method described in the DESeq paper, but with addition
# and then substraction of a pseudocount, in order to deal with zero counts.
# This seems to perform well (see "test_size_factor" results).
#DE_SIZE_FACTORS = ["non_structural", "median_ratio_to_pseudo_ref"]
DE_SIZE_FACTORS = ["non_structural"]
#SIZE_FACTORS = ["non_structural", "median_ratio_to_pseudo_ref"]
SIZE_FACTORS = ["non_structural", "RPF"]
NORMALIZER = "median_ratio_to_pseudo_ref"

# For metagene analyses
#META_MARGIN = 300
META_MARGIN = 0
META_SCALE = 500
#UNSCALED_INSIDE = 500
UNSCALED_INSIDE = 0
#META_MIN_LEN = 1000
META_MIN_LEN = 2 * UNSCALED_INSIDE
MIN_DIST = 2 * META_MARGIN


def add_dataframes(df1, df2):
    return df1.add(df2, fill_value=0)


def sum_dataframes(dfs):
    return reduce(add_dataframes, dfs)


def sum_counts(fname):
    p = Popen(
        ['awk', '$1 ~ /^piRNA$|^miRNA$|^pseudogene$|^satellites_rmsk$|^simple_repeats_rmsk$|^protein_coding_|^.NA_transposons_rmsk$/ {sum += $2} END {print sum}', fname],
        stdout=PIPE,
        stderr=PIPE)
    result, err = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    try:
        return int(result.strip().split()[0])
    except IndexError:
        warnings.warn(f"No counts in {fname}\n")
        return 0

def sum_te_counts(fname):
    p = Popen(
        ['awk', '$1 !~ /WBGene/ {sum += $2} END {print sum}', fname],
        stdout=PIPE,
        stderr=PIPE)
    result, err = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    return int(result.strip().split()[0])


# Limit risks of ambiguity by imposing replicates to be numbers
# and restricting possible forms of some other wildcards
wildcard_constraints:
    lib="|".join(LIBS),
    #treat="|".join(TREATS),
    rep="\d+",
    min_dist="\d+",
    min_len="\d+",
    #max_len="\d+",
    biotype="|".join(set(["genes", "alltypes"] + COUNT_BIOTYPES + ANNOT_BIOTYPES)),
    id_list="|".join(GENE_LISTS),
    type_set="|".join(["all", "protein_coding", "protein_coding_TE"]),
    feature_type="|".join(["transcript", "exon"]),
    read_type="|".join([
        "raw", "trimmed", "deduped", f"{size_selected}",
        "mapped", "RPF", *RPF_SUBTYPES]),
    standard="zscore|robust|minmax|unit|identity",
    orientation="all|fwd|rev",
    contrast="|".join(CONTRASTS),
    norm="|".join(TESTED_SIZE_FACTORS),
    series="|".join(ALL_SERIES),
    series_type="|".join(SERIES_TYPES),
    fold_type="|".join(["mean_log2_RPM_fold", "log2FoldChange", "lfcMLE"]),

# Define functions to be used in shell portions
shell.prefix(SHELL_FUNCTIONS)


###################################
# Preparing the input of rule all #
###################################

bigwig_files = [
    # individual libraries
    expand(
        OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}",
            "{lib}_{rep}_{read_type}_on_%s_by_{norm}_{orientation}_transcript.bw" % genome),
        lib=LIBS, rep=REPS, read_type=READ_TYPES_FOR_MAPPING, norm=SIZE_FACTORS, orientation=["all"]),
    # means of replicates
    expand(
        OPJ(aligner, f"mapped_{genome}", "{lib}_mean",
            "{lib}_mean_{read_type}_on_%s_by_{norm}_{orientation}_transcript.bw" % genome),
        lib=LIBS, read_type=READ_TYPES_FOR_MAPPING, norm=SIZE_FACTORS, orientation=["all"]),
    # methods
    expand(
        OPJ(aligner, f"mapped_{genome}", "{lib}_mean", "methods",
            "{lib}_mean_{read_type}_on_%s_by_{norm}_{orientation}_transcript_methods.txt" % genome),
        lib=LIBS, read_type=READ_TYPES_FOR_MAPPING, norm=SIZE_FACTORS, orientation=["all"]),
    ]

counts_files = [
    expand(
        OPJ(counts_dir, "summaries",
            "{lib}_{rep}_{read_type}_on_%s_{orientation}_transcript_counts.txt" % genome),
        lib=LIBS, rep=REPS, read_type=READ_TYPES_FOR_COUNTING, orientation=ORIENTATIONS),
    # TPM is computed on all biotypes simultaneously
    expand(
        OPJ(counts_dir,
            "{lib}_mean_{read_type}_on_%s" % genome, "{lib}_mean_{biotype}_{orientation}_transcript_TPM.txt"),
        lib=LIBS, read_type=READ_TYPES_FOR_COUNTING, biotype=["alltypes"], orientation=ORIENTATIONS),
    expand(
        OPJ(counts_dir,
            "{lib}_mean_{read_type}_on_%s" % genome, "{lib}_mean_{biotype}_{orientation}_exon_TPM.txt"),
        lib=LIBS, read_type=READ_TYPES_FOR_COUNTING, biotype=["genes"], orientation=ORIENTATIONS),
    # expand(
    #     OPJ(counts_dir,
    #         "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_TPM.txt"),
    #     read_type=READ_TYPES_FOR_COUNTING, biotype=["alltypes"], orientation=ORIENTATIONS),
    expand(
        OPJ(counts_dir,
            "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_transcript_RPK.txt"),
        read_type=READ_TYPES_FOR_COUNTING, biotype=BIOTYPES, orientation=ORIENTATIONS),
    expand(
        OPJ(counts_dir,
            "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_transcript_counts.txt"),
        read_type=READ_TYPES_FOR_COUNTING, biotype=BIOTYPES, orientation=ORIENTATIONS),
    # expand(
    #     OPJ(counts_dir,
    #         "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_RPK.txt"),
    #     read_type=READ_TYPES_FOR_COUNTING, biotype=BIOTYPES + ["alltypes"], orientation=ORIENTATIONS),
    # expand(
    #     OPJ(counts_dir,
    #         "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_counts.txt"),
    #     read_type=READ_TYPES_FOR_COUNTING, biotype=BIOTYPES + ["alltypes"], orientation=ORIENTATIONS),
    ]

meta_profiles = [
        #expand(OPJ(local_annot_dir, "transcripts_{type_set}", "merged_isolated_{min_dist}.bed"), type_set=["all", "protein_coding", "protein_coding_TE"], min_dist="0 5 10 25 50 100 250 500 1000 2500 5000 10000".split()),
        #expand(OPJ(local_annot_dir, "transcripts_{type_set}", "merged_isolated_{min_dist}_{biotype}_min_{min_len}.bed"), type_set=["all", "protein_coding", "protein_coding_TE"], min_dist="0 5 10 25 50 100 250 500 1000 2500 5000 10000".split(), biotype=["protein_coding"], min_len=[str(META_MIN_LEN)]),
        # TODO: check how to adapt to dt_series instead of ip_series
        expand(
            OPJ("figures", "mean_meta_profiles_meta_scale_{meta_scale}",
                "{read_type}_by_{norm}_{orientation}_on_{type_set}_merged_isolated_{min_dist}_{biotype}_min_{min_len}_{series_type}_{series}_meta_profile.pdf"),
            meta_scale= [str(META_SCALE)], read_type=READ_TYPES_FOR_MAPPING,
            norm=SIZE_FACTORS, orientation=["all"], type_set=["protein_coding_TE"], min_dist=[str(MIN_DIST)],
            biotype=["protein_coding", "DNA_transposons_rmsk", "RNA_transposons_rmsk"], min_len=[str(META_MIN_LEN)],
            series_type=["dt_series"], series=list(dt_series.keys())),
        expand(
            OPJ("figures", "mean_meta_profiles_meta_scale_{meta_scale}",
                "{read_type}_by_{norm}_{orientation}_on_{type_set}_merged_isolated_{min_dist}_{biotype}_min_{min_len}_{series_type}_{series}_meta_profile.pdf"),
            meta_scale= [str(META_SCALE)], read_type=READ_TYPES_FOR_MAPPING,
            norm=SIZE_FACTORS, orientation=["all"], type_set=["protein_coding"], min_dist=[str(MIN_DIST)],
            biotype=["protein_coding"], min_len=[str(META_MIN_LEN)],
            series_type=["dt_series"], series=list(dt_series.keys())),
        expand(
            OPJ("figures", "mean_meta_profiles_meta_scale_{meta_scale}",
                "{read_type}_by_{norm}_{orientation}_on_{type_set}_merged_isolated_{min_dist}_{id_list}_{series_type}_{series}_meta_profile.pdf"),
            meta_scale=[str(META_SCALE)], read_type=READ_TYPES_FOR_MAPPING,
            norm=SIZE_FACTORS, orientation=["all"], type_set=["protein_coding_TE"], min_dist=["0"], id_list=GENE_LISTS,
            series_type=["dt_series"], series=list(dt_series.keys())),
        ## TODO: Resolve issue with bedtools
        # expand(
        #     OPJ("figures", "{lib}_{rep}",
        #         "{read_type}_by_{norm}_{orientation}_pi_targets_in_{biotype}_profile.pdf"),
        #     lib=LIBS, rep=REPS, read_type=READ_TYPES_FOR_MAPPING,
        #     norm=SIZE_FACTORS, orientation=["all"], biotype=["protein_coding"]),
    ]

read_graphs = [
    # Plots
    expand(
        OPJ("figures", "{lib}_{rep}", "{read_type}_base_proportions_along.pdf"),
        lib=LIBS, rep=REPS, read_type=READ_TYPES_FOR_COMPOSITION),
    expand(
        OPJ("figures", "{lib}_{rep}", "{read_type}_base_composition_from_{position}.pdf"),
        lib=LIBS, rep=REPS, read_type=READ_TYPES_FOR_COMPOSITION, position=["start", "end"]),
    expand(
        OPJ("figures", "{lib}_{rep}", "{read_type}_seqlogo_from_{position}.pdf"),
        lib=LIBS, rep=REPS, read_type=READ_TYPES_FOR_COMPOSITION, position=["start"]),
    expand(
        OPJ("figures", "{lib}_{rep}", "{read_type}_base_logo_from_{position}.pdf"),
        lib=LIBS, rep=REPS, read_type=READ_TYPES_FOR_COMPOSITION, position=["start", "end"]),
    expand(
        OPJ("figures", "{lib}_{rep}", "{read_type}_{position}_base_composition.pdf"),
        lib=LIBS, rep=REPS, read_type=READ_TYPES_FOR_COMPOSITION, position=POSITIONS),
    expand(
        OPJ("figures", "{lib}_{rep}", "{read_type}_{position}_base_logo.pdf"),
        lib=LIBS, rep=REPS, read_type=READ_TYPES_FOR_COMPOSITION, position=POSITIONS),
    expand(
        OPJ("figures", "{lib}_{rep}", "{read_type}_size_distribution.pdf"),
        lib=LIBS, rep=REPS, read_type=["trimmed"]),
    # Not relevant in Ribo-seq?
    #expand(
    #    OPJ("figures", "{lib}_{rep}", f"{size_selected}_smallRNA_barchart.pdf"),
    #    lib=LIBS, rep=REPS),
    expand(
        OPJ("figures", "{lib}_{rep}", "nb_transcript_reads.pdf"),
        lib=LIBS, rep=REPS),
    ]

# TODO: check what can be done with "dt" (differential translation, a.k.a. translation efficiency difference)

localrules: all, link_raw_data

rule all:
    input:
        expand(
            OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "{read_type}_on_%s_nb_mapped.txt" % genome),
            lib=LIBS, rep=REPS, read_type=[size_selected]),
        expand(
            OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "{read_type}_on_%s_coverage.txt" % genome),
            lib=LIBS, rep=REPS, read_type=[size_selected]),
        # In read_graphs
        #expand(
        #    OPJ("figures", "{lib}_{rep}", "nb_reads.pdf"),
        #    lib=LIBS, rep=REPS),
        read_graphs,
        counts_files,
        bigwig_files,
        # translation_efficiency
        expand(
            OPJ(counts_dir, "{lib}_mean_{read_type}_on_%s" % genome, "{lib}_{biotype}_{orientation}_transcript_%s.txt" % eff_name),
            lib=EFF_LIBS, read_type=["RPF"], biotype=["alltypes", "protein_coding"], orientation=["fwd"]),
        # DESeq2 results
        expand(
            OPJ(
                counts_dir,
                "deseq2_{read_type}",
                "{contrast}", "{orientation}_{biotype}_transcript", "{contrast}_counts_and_res.txt"),
            read_type=[size_selected], contrast=DE_CONTRASTS, orientation=ORIENTATIONS, biotype=DE_BIOTYPES),
        # We are not supposed to have data outside protein-coding genes for RPF
        # (June 2020: Now we could, depending on RPF_BIOTYPES)
        expand(
            OPJ(
                counts_dir,
                "deseq2_{read_type}",
                "{contrast}", "{orientation}_{biotype}_transcript", "{contrast}_counts_and_res.txt"),
            read_type=["RPF"], contrast=DE_CONTRASTS, orientation=ORIENTATIONS, biotype=["alltypes", "protein_coding"]),
            #read_type=["RPF"], contrast=DE_CONTRASTS, orientation=ORIENTATIONS, biotype=RPF_BIOTYPES),
        expand(
            OPJ(
                counts_dir,
                "diff_%s_{read_type}" % eff_name,
                "{contrast}", "{orientation}_{biotype}_transcript", "{contrast}_diff_%s.txt" % eff_name),
            read_type=["RPF"], contrast=DT_CONTRASTS, orientation=["fwd"], biotype=["alltypes", "protein_coding"]),
            #read_type=["RPF"], contrast=DT_CONTRASTS, orientation=["fwd"], biotype=["alltypes", *RPF_BIOTYPES]),

include: ensure_relative(irules["link_raw_data"], workflow.basedir)


# rule trim_and_dedup:
#     input:
#         rules.link_raw_data.output,
#         #OPJ(data_dir, "{lib}_{rep}.fastq.gz"),
#     params:
#         adapter = lambda wildcards : lib2adapt[wildcards.lib],
#         trim5 = trim5,
#         trim3 = trim3,
#     output:
#         trimmed = OPJ(data_dir, "trimmed", "{lib}_{rep}_trimmed.fastq.gz"),
#         nb_raw =  OPJ(data_dir, "trimmed", "{lib}_{rep}_nb_raw.txt"),
#         nb_trimmed =  OPJ(data_dir, "trimmed", "{lib}_{rep}_nb_trimmed.txt"),
#         nb_deduped =  OPJ(data_dir, "trimmed", "{lib}_{rep}_nb_deduped.txt"),
#     #threads: 2
#     message:
#         "Trimming adaptor from raw data, deduplicating reads, removing random 5' {trim5}-mers and 3' {trim3}-mers for {wildcards.lib}_{wildcards.rep}."
#     benchmark:
#         OPJ(log_dir, "trim_and_dedup", "{lib}_{rep}_benchmark.txt")
#     log:
#         cutadapt = OPJ(log_dir, "cutadapt", "{lib}_{rep}.log"),
#         trim_and_dedup = OPJ(log_dir, "trim_and_dedup", "{lib}_{rep}.log"),
#     shell:
#         """
#         zcat {input} \\
#             | tee >(count_fastq_reads {output.nb_raw}) \\
#             | cutadapt -a {params.adapter} --discard-untrimmed - 2> {log.cutadapt} \\
#             | tee >(count_fastq_reads {output.nb_trimmed}) \\
#             | dedup \\
#             | tee >(count_fastq_reads {output.nb_deduped}) \\
#             | trim_random_nt {params.trim5} {params.trim3}  2>> {log.cutadapt} \\
#             | gzip > {output.trimmed} \\
#             2> {log.trim_and_dedup}
#         """


rule trim:
    input:
        rules.link_raw_data.output,
        #OPJ(data_dir, "{lib}_{rep}.fastq.gz"),
    params:
        adapter = lambda wildcards : lib2adapt[wildcards.lib],
        trim5 = trim5,
        trim3 = trim3,
    output:
        trimmed = OPJ(data_dir, "trimmed", "{lib}_{rep}_trimmed.fastq.gz"),
        nb_raw =  OPJ(data_dir, "trimmed", "{lib}_{rep}_nb_raw.txt"),
        nb_trimmed =  OPJ(data_dir, "trimmed", "{lib}_{rep}_nb_trimmed.txt"),
        methods = OPJ(data_dir, "trimmed", "methods", "{lib}_{rep}_trimmed_methods.txt"),
    #threads: 2
    message:
        "Trimming adaptor from raw data, removing random 5' {trim5}-mers and 3' {trim3}-mers for {wildcards.lib}_{wildcards.rep}."
    benchmark:
        OPJ(log_dir, "trim", "{lib}_{rep}_benchmark.txt")
    log:
        cutadapt = OPJ(log_dir, "cutadapt", "{lib}_{rep}.log"),
        trim = OPJ(log_dir, "trim", "{lib}_{rep}.log"),
    shell:
        """
        zcat {input} \\
            | tee >(count_fastq_reads {output.nb_raw}) \\
            | cutadapt -a {params.adapter} --discard-untrimmed - 2> {log.cutadapt} \\
            | tee >(count_fastq_reads {output.nb_trimmed}) \\
            | trim_random_nt {params.trim5} {params.trim3}  2>> {log.cutadapt} \\
            | gzip > {output.trimmed} \\
            2> {log.trim}
        v=$(cutadapt --version)
        echo "# {rule}" > {output.methods}
        echo "The 3' adaptor ({params.adapter}) was trimmed from the raw reads using cutadapt (version ${{v}})." >> {output.methods}
        echo "The 5' and 3' UMIs were removed from the trimmed reads using cutadapt (version ${{v}}) with options -u {params.trim5} and -u -{params.trim3}." >> {output.methods}
        """


def awk_size_filter(wildcards):
    """Returns the bioawk filter to select reads of size from MIN_LEN to MAX_LEN."""
    return "%s <= length($seq) && length($seq) <= %s" % (MIN_LEN, MAX_LEN)


rule select_size_range:
    """Select (and count) reads in the correct size range."""
    input:
        # rules.trim_and_dedup.output.trimmed
        trimmed = rules.trim.output.trimmed,
        methods = rules.trim.output.methods,
    output:
        selected = OPJ(data_dir, "trimmed", "{lib}_{rep}_%s.fastq.gz" % size_selected),
        nb_selected = OPJ(data_dir, "trimmed", "{lib}_{rep}_nb_%s.txt" % size_selected),
        methods = OPJ(data_dir, "trimmed", "methods", "{lib}_{rep}_%s_methods.txt" % size_selected),
    params:
        awk_filter = awk_size_filter,
    message:
        "Selecting reads size %s for {wildcards.lib}_{wildcards.rep}." % size_selected
    shell:
        """
        bioawk -c fastx '{params.awk_filter} {{print "@"$name" "$4"\\n"$seq"\\n+\\n"$qual}}' {input.trimmed} \\
            | tee >(count_fastq_reads {output.nb_selected}) \\
            | gzip > {output.selected}
        cat {input.methods} > {output.methods}
        echo "# {rule}" >> {output.methods}
        v=$(bioawk --version | bioawk '{{print $3}}')
        gitv=$(grep -A 1 bioawk /usr/local/share/doc/program_versions.txt | tail -1)
        echo "After removing UMIs, the reads from %s to %s nt were selected using bioawk version ${{v}} (git commit ${{gitv}})." >> {output.methods}
        """ % (MIN_LEN, MAX_LEN)


@wc_applied
def source_fastq(wildcards):
    """Determine the fastq file corresponding to a given read type."""
    read_type = wildcards.read_type
    if read_type == "raw":
        return rules.link_raw_data.output
    elif read_type == "trimmed":
        # return rules.trim_and_dedup.output.trimmed
        return rules.trim.output.trimmed
    elif read_type == size_selected:
        return rules.select_size_range.output.selected
    elif read_type == "nomap":
        return rules.map_on_genome.output.nomap_fastq
    elif read_type == "RPF":
        return rules.extract_RPF.output.rpf
    elif read_type.startswith("RPF29"):
        # AttributeError: 'Wildcards' object has no attribute 'rpf_subtype'
        # return rules.extract_RPF_subtype.output.sub_rpf
        return OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "{read_type}_on_%s_fwd_on_rpf_biotypes.fastq.gz" % genome)
    else:
        raise NotImplementedError("Unknown read type: %s" % read_type)


@wc_applied
def source_fastq_methods(wildcards):
    """Determine the fastq file corresponding to a given read type."""
    read_type = wildcards.read_type
    if read_type == "raw":
        return []
        # return rules.link_raw_data.output.methods
    elif read_type == "trimmed":
        return rules.trim.output.methods
    elif read_type == size_selected:
        return rules.select_size_range.output.methods
    elif read_type == "nomap":
        return rules.map_on_genome.output.methods
    elif read_type == "RPF":
        return rules.extract_RPF.output.methods
    elif read_type.startswith("RPF29"):
        # AttributeError: 'Wildcards' object has no attribute 'rpf_subtype'
        # return rules.extract_RPF_subtype.output.methods
        return OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "methods", "{read_type}_on_%s_fwd_on_rpf_biotypes_methods.txt" % genome),
    else:
        raise NotImplementedError("Unknown read type: %s" % read_type)


# ValueError in inspect.signature
# get_read_type = attrgetter("read_type")
def get_read_type(wildcards):
    return wildcards.read_type


rule map_on_genome:
    input:
        # fastq = rules.select_size_range.output.selected,
        fastq = source_fastq,
        methods = source_fastq_methods,
    output:
        sam = temp(OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "{read_type}_on_%s.sam" % genome)),
        methods = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "methods", "{read_type}_on_%s_methods.txt" % genome),
        nomap_fastq = OPJ(aligner, f"not_mapped_{genome}", "{lib}_{rep}_{read_type}_unmapped_on_%s.fastq.gz" % genome),
    params:
        aligner = aligner,
        index = index,
        settings = alignment_settings[aligner],
        # for methods
        genome_name = f"{genome} genome ({genome_version})",
        read_name = get_read_type,
    message:
        "Mapping {wildcards.lib}_{wildcards.rep}_{wildcards.read_type} on {params.genome_name}."
    benchmark:
        OPJ(log_dir, "map_on_genome", "{lib}_{rep}_{read_type}_benchmark.txt")
    log:
        log = OPJ(log_dir, "map_on_genome", "{lib}_{rep}_{read_type}.log"),
        err = OPJ(log_dir, "map_on_genome", "{lib}_{rep}_{read_type}.err")
    threads:
        8
    wrapper:
        f"file://{wrappers_dir}/map_on_genome"


def source_sam(wildcards):
    if hasattr(wildcards, "read_type"):
        return OPJ(
            aligner, f"mapped_{genome}",
            f"{wildcards.lib}_{wildcards.rep}",
            f"{wildcards.read_type}_on_{genome}.sam")
    else:
        return OPJ(
            aligner, f"mapped_{genome}",
            f"{wildcards.lib}_{wildcards.rep}",
            f"{size_selected}_on_{genome}.sam")

def source_sam_methods(wildcards):
    if hasattr(wildcards, "read_type"):
        return OPJ(
            aligner, f"mapped_{genome}",
            f"{wildcards.lib}_{wildcards.rep}", "methods",
            f"{wildcards.read_type}_on_{genome}_methods.txt")
    else:
        return OPJ(
            aligner, f"mapped_{genome}",
            f"{wildcards.lib}_{wildcards.rep}", "methods",
            f"{size_selected}_on_{genome}_methods.txt")


rule sam2indexedbam:
    input:
        sam = source_sam,
    output:
        sorted_bam = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "{read_type}_on_%s_sorted.bam" % genome),
        index = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "{read_type}_on_%s_sorted.bam.bai" % genome),
    message:
        "Sorting and indexing sam file for {wildcards.lib}_{wildcards.rep}_{wildcards.read_type}."
    benchmark:
        OPJ(log_dir, "sam2indexedbam", "{lib}_{rep}_{read_type}_benchmark.txt"),
    log:
        log = OPJ(log_dir, "sam2indexedbam", "{lib}_{rep}_{read_type}.log"),
        err = OPJ(log_dir, "sam2indexedbam", "{lib}_{rep}_{read_type}.err"),
    threads:
        8
    resources:
        mem_mb=4100
    wrapper:
        f"file://{wrappers_dir}/sam2indexedbam"


rule compute_mapping_stats:
    input:
        sorted_bam = rules.sam2indexedbam.output.sorted_bam,
    output:
        stats = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "{read_type}_on_%s_samtools_stats.txt" % genome),
    shell:
        """samtools stats {input.sorted_bam} > {output.stats}"""


rule get_nb_mapped:
    """Extracts the number of mapped reads from samtools stats results."""
    input:
        stats = rules.compute_mapping_stats.output.stats,
    output:
        nb_mapped = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "{read_type}_on_%s_nb_mapped.txt" % genome),
    shell:
        """samstats2mapped {input.stats} {output.nb_mapped}"""


rule compute_coverage:
    input:
        sorted_bam = rules.sam2indexedbam.output.sorted_bam,
    output:
        coverage = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "{read_type}_on_%s_coverage.txt" % genome),
    params:
        genomelen = genomelen,
    shell:
        """
        bases=$(samtools depth {input.sorted_bam} | awk '{{sum += $3}} END {{print sum}}') || error_exit "samtools depth failed"
        python3 -c "print(${{bases}} / {params.genomelen})" > {output.coverage}
        """


# rule filter_protein_coding_bam:
#     """We only count as RPF (ribosome protected fragments) those reads that map
#     on protein coding genes, in sense direction. We therefore miss translation
#     on "non-coding" regions."""
#     input:
#         sorted_bam = OPJ(
#             aligner, f"mapped_{genome}", "{lib}_{rep}",
#             f"{size_selected}_on_{genome}_sorted.bam"),
#         protein_gtf = OPJ(annot_dir, "protein_coding.gtf")
#     output:
#         protein_bam = temp(OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", f"{size_selected}_on_{genome}_fwd_on_protein_coding.bam")),
#         protein_bai = temp(OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", f"{size_selected}_on_{genome}_fwd_on_protein_coding.bam.bai")),
#     shell:
#         """
#         bedtools intersect -a {input.sorted_bam} -b {input.protein_gtf} -wa -u -f 1.0 -s \\
#             > {output.protein_bam}
#         samtools index {output.protein_bam}
#         """


rule extract_RPF:
    """
    We only count as RPF (ribosome protected fragments) those reads that map
    on RPF_BIOTYPES (by default protein coding genes), in sense direction. If
    we want translation on "non-coding" regions, config["rpf_biotypes"] should
    be set accordingly.
    """
    input:
        sorted_bam = OPJ(
            aligner, f"mapped_{genome}", "{lib}_{rep}",
            f"{size_selected}_on_{genome}_sorted.bam"),
        gtfs = [OPJ(annot_dir, f"{biotype}.gtf") for biotype in RPF_BIOTYPES],
        methods = OPJ(
            aligner, f"mapped_{genome}", "{lib}_{rep}", "methods",
            f"{size_selected}_on_{genome}_methods.txt"),
    output:
        rpf = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", f"{size_selected}_on_{genome}_fwd_on_rpf_biotypes.fastq.gz"),
        methods = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "methods", f"{size_selected}_on_{genome}_fwd_on_rpf_biotypes_methods.txt"),
    params:
        tmp_filtered_bam = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", f"{size_selected}_on_{genome}_fwd_on_rpf_biotypes.bam"),
    shell:
        """
        mkfifo {params.tmp_filtered_bam}
        bedtools intersect -a {input.sorted_bam} -b {input.gtfs} -wa -u -f 1.0 -s \\
            > {params.tmp_filtered_bam} &
        samtools fastq {params.tmp_filtered_bam} \\
            | gzip > {output.rpf}
        rm -f {params.tmp_filtered_bam}
        cp {input.methods} {output.methods}
        echo "# {rule}" >> {output.methods}
        sv=$(samtools --version | head -1 | awk '{{print $2}}')
        bv=$(bedtools --version | awk '{{print $2}}')
        echo "Reads mapping on sense orientation on annotated protein coding genes were considered as Ribosome-protected fragments (RPF). Such reads were extracted from mapping results using samtools ${{sv}} and bedtools ${{bv}} and re-mapped on the genome." >> {output.methods}
        """
# rule extract_RPF:
#     """Version using intermediate bam file (untested)"""
#     input:
#         protein_bam = OPJ(
#             aligner, f"mapped_{genome}", "{lib}_{rep}",
#             f"{size_selected}_on_{genome}_fwd_on_protein_coding.bam"),
#         protein_bai = OPJ(
#             aligner, f"mapped_{genome}", "{lib}_{rep}",
#             f"{size_selected}_on_{genome}_fwd_on_protein_coding.bam.bai"),
#     output:
#         rpf = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", f"{size_selected}_on_{genome}_fwd_on_protein_coding.fastq.gz")
#     shell:
#         """
#         samtools fastq {input.protein_bam} > {output.rpf}
#         """


# Use bam or RPF ? Maybe simpler to use RPF
rule extract_RPF_subtype:
    input:
        #rpf = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", f"{size_selected}_on_{genome}_fwd_on_rpf_biotypes.fastq.gz")
        #methods = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "methods", f"{size_selected}_on_{genome}_fwd_on_rpf_biotypes_methods.txt")
        rpf = rules.extract_RPF.output.rpf,
        methods = rules.extract_RPF.output.methods,
        # protein_bam = OPJ(
        #     aligner, f"mapped_{genome}", "{lib}_{rep}",
        #     f"{size_selected}_on_{genome}_fwd_on_protein_coding.bam"),
        # protein_bai = OPJ(
        #     aligner, f"mapped_{genome}", "{lib}_{rep}",
        #     f"{size_selected}_on_{genome}_fwd_on_protein_coding.bam.bai"),
    output:
        sub_rpf = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "{rpf_subtype}_on_%s_fwd_on_rpf_biotypes.fastq.gz" % genome),
        methods = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "methods", "{rpf_subtype}_on_%s_fwd_on_rpf_biotypes_methods.txt" % genome),
    log:
        log = OPJ(log_dir, "extract_RPF_subtype","{lib}_{rep}", "{rpf_subtype}_on_%s_fwd_on_rpf_biotypes.log" % genome),
        warnings = OPJ(log_dir, "extract_RPF_subtype","{lib}_{rep}", "{rpf_subtype}_on_%s_fwd_on_rpf_biotypes.warnings" % genome),
    run:
        with warn_context(log.warnings) as warn, open(log.log, "w") as logfile:
            logfile.write(f"Extracting {wildcards.rpf_subtype} from {input.rpf}\n")
            FQ_TEMPLATE = "@%s\n%s\n+\n%s\n"
            # FQ_TEMPLATE_2 = b"@%s\n%s\n+\n%s\n"
            # Select the appropriate filtering function
            nb_written = 0
            filter_fun = rpf_subtype2filter[wildcards.rpf_subtype]
            with gzopen(output.sub_rpf, "wt") as fastq_out:
                for (name, sequence, qualities) in filter(filter_fun, fastx_read(input.rpf)):
                        # try:
                        #     fastq = FQ_TEMPLATE % (name, sequence, qualities)
                        # except Exception as e1:
                        #     warn(str(e1) + "\n")
                        #     warn(f"{type(name)}, {type(sequence)}, {type(qualities)}\n")
                        #     try:
                        #         fastq = FQ_TEMPLATE_2 % (name, sequence, qualities)
                        #     except Exception as e2:
                        #         warn(str(e2) + "\n")
                        #         fastq = f"@{name}\n{sequence}\n+\n{qualities}\n"
                        # warn(f"{type(fastq)}\n")
                        # fastq_out.write(fastq)
                        fastq_out.write(FQ_TEMPLATE % (name, sequence, qualities))
                        nb_written += 1
                        # fastq_out.write(f"@{name}\n{sequence}\n+\n{qualities}\n")
            logfile.write(f"{nb_written} reads written in {output.sub_rpf}\n")
            copyfile(input.methods, output.methods)
            with open(output.methods, "a") as methods:
                methods.write(f"# {rule}\n{filter_fun.__doc__}\n")


def feature_orientation2stranded(wildcards):
    orientation = wildcards.orientation
    if orientation == "fwd":
        return 1
    elif orientation == "rev":
        return 2
    elif orientation == "all":
        return 0
    else:
        exit("Orientation is to be among \"fwd\", \"rev\" and \"all\".")


def source_bams(wildcards):
    """We can count from several bam files with feature_count."""
    read_types = wildcards.read_type.split("_and_")
    sorted_bams = [OPJ(
        aligner, f"mapped_{genome}", f"{wildcards.lib}_{wildcards.rep}",
        f"{read_type}_on_{genome}_sorted.bam") for read_type in read_types]
    return sorted_bams


rule feature_count_reads:
    input:
        source_bams,
    output:
        counts = OPJ(
            counts_dir,
            "{lib}_{rep}_{read_type}_on_%s" % genome, "{biotype}_{orientation}_{feature_type}_counts.txt"),
    params:
        stranded = feature_orientation2stranded,
        annot = lambda wildcards: OPJ(annot_dir, f"{wildcards.biotype}.gtf"),
        gene_id_name = genome_dict.get("gene_id_name", "gene_id"),
        tmpdir_prefix = lambda wildcards: f"feature_{wildcards.lib}_{wildcards.rep}_{wildcards.read_type}_{wildcards.biotype}_{wildcards.orientation}_{wildcards.feature_type}.XXXXXXXXXX",
    message:
        "Counting {wildcards.orientation} {wildcards.biotype} {wildcards.feature_type} reads for {wildcards.lib}_{wildcards.rep}_{wildcards.read_type} with featureCounts."
    benchmark:
        OPJ(log_dir, "feature_count_reads", "{lib}_{rep}_{read_type}_{biotype}_{orientation}_{feature_type}_benchmark.txt"),
    log:
        log = OPJ(log_dir, "feature_count_reads", "{lib}_{rep}_{read_type}_{biotype}_{orientation}_{feature_type}.log"),
        err = OPJ(log_dir, "feature_count_reads", "{lib}_{rep}_{read_type}_{biotype}_{orientation}_{feature_type}.err"),
    threads: 8
    wrapper:
        f"file://{wrappers_dir}/feature_count_reads"
    # shell:
    #     """
    #     tmpdir=$(TMPDIR=/var/tmp mktemp --tmpdir -d "feature_{wildcards.lib}_{wildcards.rep}_{wildcards.read_type}_{wildcards.biotype}_{wildcards.orientation}.XXXXXXXXXX")
    #     cmd="featureCounts \\
    #         -a {params.annot} -o {output.counts} \\
    #         -t {wildcards.feature_type} -g {params.gene_id_name} \\
    #         -O -s {params.stranded} --fracOverlap 1 \\
    #         --tmpDir ${{tmpdir}} {input}"
    #     featureCounts -v 2> {log.log}
    #     echo ${{cmd}} 1>> {log.log}
    #     eval ${{cmd}} 1>> {log.log} 2> {log.err} || error_exit "featureCounts failed"
    #     rm -rf ${{tmpdir}}
    #     """


rule summarize_feature_counts:
    """For a given library, compute the total counts for each biotype and write this in a summary table."""
    input:
        expand(
            OPJ(counts_dir,
                "{{lib}}_{{rep}}_{{read_type}}_on_%s" % genome, "{biotype}_{{orientation}}_{{feature_type}}_counts.txt"),
            biotype=COUNT_BIOTYPES),
    output:
        summary = OPJ(counts_dir, "summaries",
            "{lib}_{rep}_{read_type}_on_%s_{orientation}_{feature_type}_counts.txt" % genome)
    run:
        with open(output.summary, "w") as summary_file:
            header = "\t".join(COUNT_BIOTYPES)
            summary_file.write("#biotypes\t%s\n" % header)
            sums = "\t".join((str(sum_feature_counts(counts_file, nb_bams=len(wildcards.read_type.split("_and_")))) for counts_file in input))
            summary_file.write(f"%s_%s_%s\t%s\n" % (wildcards.lib, wildcards.rep, wildcards.orientation, sums))


rule count_non_structural_mappers:
    input:
        # nb_mapped is the total number of size-selected that mapped
        #nb_mapped = rules.get_nb_mapped.output.nb_mapped,
        nb_mapped = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", f"{size_selected}_on_{genome}_nb_mapped.txt"),
        # structural are fwd to STRUCTURAL_BIOTYPES
        summary = OPJ(
            counts_dir, "summaries",
            "{lib}_{rep}_%s_on_%s_fwd_transcript_counts.txt" % (size_selected, genome)),
    output:
        nb_non_structural = OPJ(
            counts_dir, "summaries",
            "{lib}_{rep}_nb_non_structural.txt"),
    run:
        nb_mapped = read_int_from_file(input.nb_mapped)
        structural = pd.read_table(input.summary, index_col=0).loc[:, STRUCTURAL_BIOTYPES].sum(axis=1)[0]
        with open(output.nb_non_structural, "w") as out_file:
            out_file.write("%d\n" % (nb_mapped - structural))


rule count_RPF:
    input:
        summary_table = OPJ(counts_dir, "summaries",
            "{lib}_{rep}_%s_on_%s_fwd_{feature_type}_counts.txt" % (size_selected, genome))
    output:
        nb_RPF = OPJ(counts_dir, "summaries",
            "{lib}_{rep}_%s_on_%s_fwd_{feature_type}_nb_RPF.txt" % (size_selected, genome))
    run:
        # nb_RPF = pd.read_table(input.summary_table, index_col=0)["protein_coding"][f"{wildcards.lib}_{wildcards.rep}_fwd"]
        # June 2020: replacing protein_coding by RPF_BIOTYPES
        # WARNING: If overlapping biotypes are used, the counts will be wrong
        nb_RPF = sum(
            pd.read_table(input.summary_table, index_col=0)[biotype][f"{wildcards.lib}_{wildcards.rep}_fwd"]
            for biotype in RPF_BIOTYPES)
        with open(output.nb_RPF, "w") as nb_RPF_file:
            nb_RPF_file.write(f"{nb_RPF}\n")


@wc_applied
def source_counts(wildcards):
    """Determines from which rule the gathered counts should be sourced."""
    if wildcards.biotype == "alltypes":
        return rules.join_all_counts.output.counts_table
    else:
        # "Directly" from the counts gathered across libraries
        return rules.gather_counts.output.counts_table


rule plot_read_numbers:
    input:
        # nb_raw = rules.trim_and_dedup.output.nb_raw,
        nb_raw = rules.trim.output.nb_raw,
        # nb_deduped = rules.trim_and_dedup.output.nb_deduped,
        # nb_trimmed = rules.trim_and_dedup.output.nb_trimmed,
        nb_trimmed = rules.trim.output.nb_trimmed,
        nb_selected = rules.select_size_range.output.nb_selected,
        nb_mapped = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", f"{size_selected}_on_{genome}_nb_mapped.txt"),
        nb_RPF = rules.count_RPF.output.nb_RPF,
    output:
        plot = OPJ("figures", "{lib}_{rep}", "nb_{feature_type}_reads.pdf"),
    message:
        "Plotting read numbers for {wildcards.lib}_{wildcards.rep}."
    # Currently not used
    #log:
    #    log = OPJ(log_dir, "plot_read_numbers", "{lib}_{rep}.log"),
    #    err = OPJ(log_dir, "plot_read_numbers", "{lib}_{rep}.err")
    run:
        name = f"{wildcards.lib}_{wildcards.rep}"
        summary = pd.Series({
            "nb_raw" : read_int_from_file(input.nb_raw),
            # "nb_deduped" : read_int_from_file(input.nb_deduped),
            "nb_trimmed" : read_int_from_file(input.nb_trimmed),
            "nb_selected" : read_int_from_file(input.nb_selected),
            "nb_mapped" : read_int_from_file(input.nb_mapped),
            "nb_RPF" : read_int_from_file(input.nb_RPF)})
        # Chose column order:
        #summary = summary[["nb_raw", "nb_deduped", "nb_trimmed", "nb_selected", "nb_mapped", "nb_RPF"]]
        summary = summary[["nb_raw", "nb_trimmed", "nb_selected", "nb_mapped", "nb_RPF"]]
        save_plot(output.plot, plot_barchart, summary, title="%s reads" % name)


# Generate report with
# - raw
# - trimmed
# - deduplicated
# - size-selected
# - mapped
# - ambiguous
# - si, pi, mi
# - all_si ((22G-26G)(T*) that are not mi or pi)
rule make_read_counts_summary:
    input:
        # nb_raw = rules.trim_and_dedup.output.nb_raw,
        nb_raw = rules.trim.output.nb_raw,
        # nb_deduped = rules.trim_and_dedup.output.nb_deduped,
        # nb_trimmed = rules.trim_and_dedup.output.nb_trimmed,
        nb_trimmed = rules.trim.output.nb_trimmed,
        nb_size_selected = rules.select_size_range.output.nb_selected,
        nb_mapped = rules.get_nb_mapped.output.nb_mapped,
        nb_non_structural = rules.count_non_structural_mappers.output.nb_non_structural,
        nb_RPF = rules.count_RPF.output.nb_RPF,
    output:
        summary = OPJ(
            aligner, "summaries",
            "{lib}_{rep}_{read_type}_on_%s_{feature_type}_read_counts.txt" % genome),
    #threads: 8  # to limit memory usage, actually
    run:
        with open(output.summary, "w") as summary_file:
            # summary_file.write("%s\n" % "\t".join([
            #     "raw", "trimmed", "deduped", "%s" % size_selected,
            #     "mapped", "non_structural", "RPF"]))
            summary_file.write("%s\n" % "\t".join([
                "raw", "trimmed", "%s" % size_selected,
                "mapped", "non_structural", "RPF"]))
            summary_file.write("%d" % read_int_from_file(input.nb_raw))
            summary_file.write("\t")
            summary_file.write("%d" % read_int_from_file(input.nb_trimmed))
            summary_file.write("\t")
            # summary_file.write("%d" % read_int_from_file(input.nb_deduped))
            # summary_file.write("\t")
            summary_file.write("%d" % read_int_from_file(input.nb_size_selected))
            summary_file.write("\t")
            summary_file.write("%d" % read_int_from_file(input.nb_mapped))
            summary_file.write("\t")
            summary_file.write("%d" % read_int_from_file(input.nb_non_structural))
            summary_file.write("\t")
            summary_file.write("%d" % read_int_from_file(input.nb_RPF))
            summary_file.write("\n")


rule gather_read_counts_summaries:
    input:
        summary_tables = expand(OPJ(
            aligner, "summaries",
            "{name}_%s_on_%s_{{feature_type}}_read_counts.txt" % (size_selected, genome)), name=COND_NAMES),
    output:
        summary_table = OPJ(
            aligner, "summaries",
            "all_%s_on_%s_{feature_type}_read_counts.txt" % (size_selected, genome)),
    run:
        summary_files = (OPJ(
            aligner, "summaries",
            f"{cond_name}_{size_selected}_on_{genome}_{wildcards.feature_type}_read_counts.txt") for cond_name in COND_NAMES)
        summaries = pd.concat((pd.read_table(summary_file).T.astype(int) for summary_file in summary_files), axis=1)
        summaries.columns = COND_NAMES
        summaries.to_csv(output.summary_table, sep="\t")


from rpy2.robjects import Formula, StrVector
#from rpy2.rinterface import RRuntimeError
rule differential_expression:
    input:
        counts_table = source_counts,
        summary_table = rules.gather_read_counts_summaries.output.summary_table,
    output:
        deseq_results = OPJ(
            counts_dir,
            "deseq2_{read_type}",
            "{contrast}", "{orientation}_{biotype}_{feature_type}", "{contrast}_deseq2.txt"),
        up_genes = OPJ(
            counts_dir,
            "deseq2_{read_type}",
            "{contrast}", "{orientation}_{biotype}_{feature_type}", "{contrast}_up_genes.txt"),
        down_genes = OPJ(
            counts_dir,
            "deseq2_{read_type}",
            "{contrast}", "{orientation}_{biotype}_{feature_type}", "{contrast}_down_genes.txt"),
        counts_and_res = OPJ(
            counts_dir,
            "deseq2_{read_type}",
            "{contrast}", "{orientation}_{biotype}_{feature_type}", "{contrast}_counts_and_res.txt"),
    threads: 4  # to limit memory usage, actually
    run:
        counts_data = pd.read_table(input.counts_table, index_col="gene")
        summaries = pd.read_table(input.summary_table, index_col=0)
        # Running DESeq2
        #################
        formula = Formula("~ lib")
        (cond, ref) = CONTRAST2PAIR[wildcards.contrast]
        if not any(counts_data[f"{ref}_{rep}"].any() for rep in REPS):
            warnings.warn(
                "Reference data is all zero.\nSkipping %s_%s_%s_%s" % (
                    wildcards.read_type, wildcards.contrast, wildcards.orientation, wildcards.biotype))
            for outfile in output:
                shell(f"echo 'NA' > {outfile}")
        else:
            contrast = StrVector(["lib", cond, ref])
            try:
                res, size_factors = do_deseq2(COND_NAMES, CONDITIONS, counts_data, formula=formula, contrast=contrast)
            #except RRuntimeError as e:
            except RuntimeError as e:
                warnings.warn(
                    "Probably not enough usable data points to perform DESeq2 analyses:\n%s\nSkipping %s_%s_%s_%s" % (
                        str(e), wildcards.read_type, wildcards.contrast, wildcards.orientation, wildcards.biotype))
                for outfile in output:
                    shell(f"echo 'NA' > {outfile}")
            else:
                # Determining fold-change category
                ###################################
                set_de_status = status_setter(LFC_CUTOFFS, "log2FoldChange")
                #counts_and_res = add_tags_column(pd.concat((counts_and_res, res), axis=1).assign(status=res.apply(set_de_status, axis=1)), input.tags_table, "small_type")
                res = res.assign(status=res.apply(set_de_status, axis=1))
                # Converting gene IDs
                ######################
                with open(OPJ(convert_dir, "wormid2cosmid.pickle"), "rb") as dict_file:
                    res = res.assign(cosmid=res.apply(column_converter(load(dict_file)), axis=1))
                #with open(OPJ(convert_dir, "wormid2name.pickle"), "rb") as dict_file:
                #    res = res.assign(name=res.apply(column_converter(load(dict_file)), axis=1))
                res = res.assign(name=res.apply(column_converter(wormid2name), axis=1))
                # Just to see if column_converter works also with named column, and not just index:
                # with open(OPJ(convert_dir, "cosmid2name.pickle"), "rb") as dict_file:
                #     res = res.assign(name=res.apply(column_converter(load(dict_file), "cosmid"), axis=1))
                ##########################################
                # res.to_csv(output.deseq_results, sep="\t", na_rep="NA", decimal=",")
                res.to_csv(output.deseq_results, sep="\t", na_rep="NA")
                # Joining counts and DESeq2 results in a same table and determining up- or down- regulation status
                counts_and_res = counts_data
                for normalizer in SIZE_FACTORS:
                    if normalizer == "median_ratio_to_pseudo_ref":
                        ## Adapted from DESeq paper (doi:10.1186/gb-2010-11-10-r106) but
                        ## add pseudo-count to compute the geometric mean, then remove it
                        #pseudo_ref = (counts_data + 1).apply(gmean, axis=1) - 1
                        #def median_ratio_to_pseudo_ref(col):
                        #    return (col / pseudo_ref).median()
                        #size_factors = counts_data.apply(median_ratio_to_pseudo_ref, axis=0)
                        size_factors = median_ratio_to_pseudo_ref_size_factors(counts_data)
                    else:
                        #raise NotImplementedError(f"{normalizer} normalization not implemented")
                        size_factors = summaries.loc[normalizer]
                    by_norm = counts_data / size_factors
                    by_norm.columns = by_norm.columns.map(lambda s: "%s_by_%s" % (s, normalizer))
                    counts_and_res = pd.concat((counts_and_res, by_norm), axis=1)
                #counts_and_res = add_tags_column(pd.concat((counts_and_res, res), axis=1).assign(status=res.apply(set_de_status, axis=1)), input.tags_table, "small_type")
                counts_and_res = pd.concat((counts_and_res, res), axis=1)
                counts_and_res.to_csv(output.counts_and_res, sep="\t", na_rep="NA")
                # Saving lists of genes gaining or loosing siRNAs
                # diff_expressed = res.query("padj < 0.05")
                # up_genes = list(diff_expressed.query("log2FoldChange >= 1").index)
                # down_genes = list(diff_expressed.query("log2FoldChange <= -1").index)
                up_genes = list(counts_and_res.query(f"status in {UP_STATUSES}").index)
                down_genes = list(counts_and_res.query(f"status in {DOWN_STATUSES}").index)
                #up_genes = list(counts_and_res.query("status == 'up'").index)
                #down_genes = list(counts_and_res.query("status == 'down'").index)
                with open(output.up_genes, "w") as up_file:
                    if up_genes:
                        up_file.write("%s\n" % "\n".join(up_genes))
                    else:
                        up_file.truncate(0)
                with open(output.down_genes, "w") as down_file:
                    if down_genes:
                        down_file.write("%s\n" % "\n".join(down_genes))
                    else:
                        down_file.truncate(0)


rule gather_counts:
    """For a given biotype, gather counts from all libraries in one table."""
    input:
        counts_tables = expand(OPJ(
            counts_dir,
            "{lib}_{rep}_{{read_type}}_on_%s" % genome,
            "{{biotype}}_{{orientation}}_{{feature_type}}_counts.txt"), lib=LIBS, rep=REPS),
    output:
        counts_table = OPJ(
            counts_dir,
            "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_{feature_type}_counts.txt"),
    wildcard_constraints:
        # Avoid ambiguity with join_all_counts
        biotype = "|".join(BIOTYPES + ["genes"])
    run:
        # Gathering the counts data
        ############################
        #counts_files = (OPJ(
        counts_files = [OPJ(
            counts_dir,
            f"{cond_name}_{wildcards.read_type}_on_%s" % genome,
            f"{wildcards.biotype}_{wildcards.orientation}_{wildcards.feature_type}_counts.txt") for cond_name in COND_NAMES]
        counts_data = pd.concat(
            map(read_feature_counts, counts_files),
            axis=1).fillna(0).astype(int)
        counts_data.columns = COND_NAMES
        # Simple_repeat|Simple_repeat|(TTTTTTG)n:1
        # Simple_repeat|Simple_repeat|(TTTTTTG)n:2
        # Simple_repeat|Simple_repeat|(TTTTTTG)n:3
        # Simple_repeat|Simple_repeat|(TTTTTTG)n:4
        # -> Simple_repeat|Simple_repeat|(TTTTTTG)n
        if wildcards.biotype.endswith("_rmsk_families"):
            counts_data = sum_by_family(counts_data)
        counts_data.index.names = ["gene"]
        counts_data.to_csv(output.counts_table, na_rep="NA", sep="\t")


def make_tag_association(dfs, tag):
    """Associates a tag "tag" to the union of the indices of dataframes *dfs*."""
    idx = reduce(union, (df.index for df in dfs))
    return pd.DataFrame(list(zip(idx, repeat(tag)))).set_index(0)


rule associate_biotype:
    """This rule uses the read count matrices to associate a biotype to each gene identifier."""
    input:
        counts_tables = expand(OPJ(
            counts_dir,
            "all_{{read_type}}_on_%s" % genome,
            "{biotype}_all_{{feature_type}}_counts.txt"), biotype=[b_name for b_name in BIOTYPES if not b_name.startswith("protein_coding_")]),
    output:
        tags_table = OPJ(counts_dir, "all_{read_type}_on_%s" % genome, "{feature_type}_id2tags.txt"),
    run:
        biotype2tags_tables = {}
        for biotype in BIOTYPES:
            if biotype.startswith("protein_coding_"):
                continue
            biotype2tags_tables[biotype] = make_tag_association(
                (pd.read_table(OPJ(
                    counts_dir,
                    f"all_{wildcards.read_type}_on_{genome}",
                    f"{biotype}_all_{wildcards.feature_type}_counts.txt"), index_col="gene"),), biotype)
        tags_table = pd.concat(chain(biotype2tags_tables.values()))
        tags_table.index.names = ["gene"]
        tags_table.columns = ["biotype"]
        tags_table.to_csv(output.tags_table, sep="\t")


def add_tags_column(data, tags_table, tag_name):
    """Adds a column *tag_name* to *data* based on the DataFrame *tag_table*
    associating tags to row names."""
    # Why "inner" ?
    df = pd.concat((data, pd.read_table(tags_table, index_col=0)), join="inner", axis=1)
    df.columns = (*data.columns, tag_name)
    return df


rule join_all_counts:
    """Concatenate counts for all biotypes into "alltypes"."""
    input:
        counts_tables = expand(OPJ(
            counts_dir,
            "all_{{read_type}}_on_%s" % genome,
            "{biotype}_{{orientation}}_{{feature_type}}_counts.txt"), biotype=[b_name for b_name in BIOTYPES if not b_name.startswith("protein_coding_")]),
    output:
        counts_table = OPJ(counts_dir,
        "all_{read_type}_on_%s" % genome, "alltypes_{orientation}_{feature_type}_counts.txt"),
    run:
        # !! drop_duplicates() is wrong: doesn't consider the index (at least in pandas < 1.0.0),
        # so it eliminates rows having the same data pattern. For instance [0, 0, ...] (wich eliminates a lot of entries)
        # counts_data = pd.concat((pd.read_table(table, index_col="gene") for table in input.counts_tables)).drop_duplicates()
        counts_data = pd.concat((pd.read_table(table, index_col="gene") for table in input.counts_tables))
        assert len(counts_data.index.unique()) == len(counts_data.index), "Some genes appear several times in the counts table."
        counts_data.index.names = ["gene"]
        counts_data.to_csv(output.counts_table, sep="\t")


rule compute_RPK:
    """For a given biotype, compute the corresponding RPK value (reads per kilobase)."""
    input:
        #counts_data = rules.gather_counts.output.counts_table,
        counts_table = source_counts,
        #counts_data = OPJ(counts_dir,
        #    "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_counts.txt"),
    output:
        rpk_file = OPJ(counts_dir,
            "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_{feature_type}_RPK.txt"),
    params:
        # TODO: use union of CDS exons instead
        feature_lengths_file = OPJ(annot_dir, "union_exon_lengths.txt"),
    run:
        counts_data = pd.read_table(input.counts_table, index_col="gene")
        feature_lengths = pd.read_table(params.feature_lengths_file, index_col="gene")
        common = counts_data.index.intersection(feature_lengths.index)
        rpk = 1000 * counts_data.loc[common].div(feature_lengths.loc[common]["union_exon_len"], axis="index")
        rpk.to_csv(output.rpk_file, sep="\t")


rule compute_TPM:
    """For a given biotype, compute the corresponding TPM value (reads per kilobase per million mappers)."""
    input:
        rpk_file = rules.compute_RPK.output.rpk_file
    output:
        tpm_file = OPJ(counts_dir,
            "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_{feature_type}_TPM.txt"),
    # The sum must be done over all counted features
    wildcard_constraints:
        biotype = "|".join(["alltypes", "protein_coding", "genes"]),
    run:
        rpk = pd.read_table(input.rpk_file, index_col="gene")
        tpm = 1000000 * rpk / rpk.sum()
        tpm.to_csv(output.tpm_file, sep="\t")


# TODO: Is it better to compute the mean and then the fold of the means?
# Here, the matching between Ribo-seq and RNA-seq replicates is arbitrary.
# We don't even always have the same number of replicates,
# so we work with means across replicates.
rule compute_mean_TPM:
    input:
        all_tmp_file = rules.compute_TPM.output.tpm_file
        # TODO: Find out what needs these TPMs:
        # Missing input files for rule compute_mean_TPM:
        # bowtie2/mapped_C_elegans_klp7co/feature_count/all_RPF_on_C_elegans_klp7co/klp7co_fwd_TPM.txt
    output:
        tpm_file = OPJ(counts_dir,
            "{lib}_mean_{read_type}_on_%s" % genome, "{lib}_mean_{biotype}_{orientation}_{feature_type}_TPM.txt"),
    run:
        tpm = pd.read_table(
            input.all_tmp_file, index_col="gene",
            usecols=["gene", *[f"{wildcards.lib}_{rep}" for rep in REPS]])
        tpm_mean = tpm.mean(axis=1)
        # This is a Series
        tpm_mean.name = wildcards.lib
        tpm_mean.to_csv(output.tpm_file, sep="\t", header=True)


# TODO: Do we use the correct length in compute_RPK ?
# TODO: check TE definition
rule compute_efficiency:
    """This is actually a TPM fold between library types."""
    input:
        mean_tpm = rules.compute_mean_TPM.output.tpm_file,
        tags_table = rules.associate_biotype.output.tags_table,
    output:
        eff_file = OPJ(counts_dir,
            "{lib}_mean_{read_type}_on_%s" % genome, "{lib}_{biotype}_{orientation}_{feature_type}_%s.txt" % eff_name),
    run:
        mean_tpm = pd.read_table(input.mean_tpm, index_col="gene")
        transcriptome_tpm = pd.read_table(config["transcriptome_TPM"][wildcards.lib], index_col="gene")
        transcriptome_tpm.columns = mean_tpm.columns
        lfc = np.log2((mean_tpm + 1) / (transcriptome_tpm + 1))
        # TE_mut = (ribo_tpm_mut / transcritome_tpm_mut)
        # TE_WT = (ribo_tpm_WT / transcritome_tpm_WT)
        # log2(TE_mut / TE_WT) = log2(TE_mut) - log2(TE_WT)
        tpm_and_eff = pd.concat([mean_tpm, transcriptome_tpm, lfc], axis=1)
        tpm_and_eff.columns = [this_TPM, ref_TPM, eff_name]
        tpm_and_eff.index.name = "gene"
        # Converting gene IDs
        ######################
        with open(OPJ(convert_dir, "wormid2cosmid.pickle"), "rb") as dict_file:
            tpm_and_eff = tpm_and_eff.assign(cosmid=tpm_and_eff.apply(
                column_converter(load(dict_file)), axis=1))
        #with open(OPJ(convert_dir, "wormid2name.pickle"), "rb") as dict_file:
        #    tpm_and_eff = tpm_and_eff.assign(name=tpm_and_eff.apply(
        #        column_converter(load(dict_file)), axis=1))
        tpm_and_eff = tpm_and_eff.assign(name=tpm_and_eff.apply(
            column_converter(wormid2name), axis=1))
        tpm_and_eff = add_tags_column(tpm_and_eff, input.tags_table, "biotype")
        tpm_and_eff.to_csv(output.eff_file, sep="\t", na_rep="NA")


rule gather_efficiency:
    input:
        eff_files = expand(OPJ(
            counts_dir,
            "{lib}_mean_{{read_type}}_on_%s" % genome,
            "{lib}_{{biotype}}_{{orientation}}_{{feature_type}}_%s.txt" % eff_name), lib=EFF_LIBS),
        #tags_table = rules.associate_biotype.output.tags_table,
    output:
        eff_file = OPJ(
            counts_dir,
            "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_{feature_type}_%s.txt" % eff_name),
    run:
        eff_data = pd.concat(
            [pd.read_table(eff_file, index_col="gene", usecols=["gene", eff_name]) for eff_file in input.eff_files],
            axis=1)
        eff_data.columns = [f"{lib}_{eff_name}" for lib in EFF_LIBS]
        eff_data.index.name = "gene"
        # Converting gene IDs
        ######################
        with open(OPJ(convert_dir, "wormid2cosmid.pickle"), "rb") as dict_file:
            eff_data = eff_data.assign(cosmid=eff_data.apply(
                column_converter(load(dict_file)), axis=1))
        #with open(OPJ(convert_dir, "wormid2name.pickle"), "rb") as dict_file:
        #    eff_data = eff_data.assign(name=eff_data.apply(
        #        column_converter(load(dict_file)), axis=1))
        eff_data = eff_data.assign(name=eff_data.apply(
            column_converter(wormid2name), axis=1))
        #eff_data = add_tags_column(eff_data, input.tags_table, "biotype")
        eff_data.to_csv(output.eff_file, sep="\t", na_rep="NA")


rule compute_efficiency_difference:
    input:
        eff_file = rules.gather_efficiency.output.eff_file,
    output:
        diff_eff_file = OPJ(counts_dir, "diff_%s_{read_type}" % eff_name,
            "{contrast}", "{orientation}_{biotype}_{feature_type}", "{contrast}_diff_%s.txt" % eff_name),
    run:
        eff_data = pd.read_table(input.eff_file, index_col="gene")
        (cond, ref) = CONTRAST2PAIR[wildcards.contrast]
        # TE_mut = (ribo_tpm_mut / transcritome_tpm_mut)
        # TE_WT = (ribo_tpm_WT / transcritome_tpm_WT)
        # log2(TE_mut / TE_WT) = log2(TE_mut) - log2(TE_WT)
        diff_eff = eff_data[f"{cond}_{eff_name}"] - eff_data[f"{ref}_{eff_name}"]
        diff_eff.to_csv(output.diff_eff_file, sep="\t", na_rep="NA")


rule compute_RPM:
    input:
        counts_table = source_counts,
        summary_table = rules.gather_read_counts_summaries.output.summary_table,
        tags_table = rules.associate_biotype.output.tags_table,
    output:
        rpm_file = OPJ(
            counts_dir,
            "all_{read_type}_on_%s" % genome, "{biotype}_{orientation}_{feature_type}_RPM_{norm}.txt"),
    log:
        log = OPJ(log_dir, "compute_RPM_{norm}", "{read_type}_{biotype}_{orientation}_{feature_type}.log"),
    run:
        with open(log.log, "w") as logfile:
            logfile.write(f"Reading column counts from {input.counts_table}\n")
            counts_data = pd.read_table(
                input.counts_table,
                index_col="gene")
            logfile.write(f"Reading number of {wildcards.norm} from {input.summary_table}\n")
            norm = pd.read_table(input.summary_table, index_col=0).loc[wildcards.norm]
            logfile.write(str(norm))
            logfile.write("Computing counts per million {wildcards.norm} mappers\n")
            RPM = 1000000 * counts_data / norm
            #RPM.to_csv(output.rpm_file, sep="\t")
            add_tags_column(RPM, input.tags_table, "biotype").to_csv(output.rpm_file, sep="\t", na_rep="NA")


def source_quantif(wildcards):
    """Determines from which rule the gathered counts should be sourced."""
    if wildcards.quantif_type == "counts":
        return rules.gather_counts.output.counts_table
    elif wildcards.quantif_type == "RPK":
        return rules.compute_RPK.output.rpk_file
    elif wildcards.quantif_type == "TPM":
        return rules.compute_TPM.output.tpm_file
    elif wildcards.quantif_type == "RPM":
        return rules.compute_RPM.output.rpm_file
    else:
        raise NotImplementedError("%s is not yet handeled." % wildcards.quantif_type)


@wc_applied
def source_norm_file(wildcards):
    return rules.make_read_counts_summary.output.summary


# TODO: put size factors in summaries?
rule make_normalized_bigwig:
    input:
        bam = rules.sam2indexedbam.output.sorted_bam,
        norm_file = source_norm_file,
        #median_ratios_file = OPJ(aligner, f"mapped_{genome}", "annotation", f"all_{size_selected}_on_{genome}", "all_si_median_ratios_to_pseudo_ref.txt"),
        methods = source_sam_methods,
        # methods = OPJ(
        #     aligner, f"mapped_{genome}", "{lib}_{rep}", "methods",
        #     f"{size_selected}_on_{genome}_methods.txt"),
    output:
        bigwig_norm = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "{lib}_{rep}_{read_type}_on_%s_by_{norm}_{orientation}_{feature_type}.bw" % genome),
        methods = OPJ(aligner, f"mapped_{genome}", "{lib}_{rep}", "methods", "{lib}_{rep}_{read_type}_on_%s_by_{norm}_{orientation}_{feature_type}_methods.txt" % genome),
    params:
        genome_binned = genome_binned,
    threads: 8  # to limit memory usage, actually
    log:
        log = OPJ(log_dir, "make_normalized_bigwig", "{lib}_{rep}_{read_type}_by_{norm}_{orientation}_{feature_type}.log"),
        err = OPJ(log_dir, "make_normalized_bigwig", "{lib}_{rep}_{read_type}_by_{norm}_{orientation}_{feature_type}.err"),
        warnings = OPJ(log_dir, "make_normalized_bigwig", "{lib}_{rep}_{read_type}_by_{norm}_{orientation}_{feature_type}.warnings"),
    run:
        with warn_context(log.warnings) as warn:
            copyfile(input.methods, output.methods)
            # TODO: do something for the methods regarding the source of normalization?
            if wildcards.norm == "median_ratio_to_pseudo_ref":
                size = float(pd.read_table(
                    input.norm_file, index_col=0, header=None).loc[
                        f"{wildcards.lib}_{wildcards.rep}"])
                norm_methods = "Read counts in the bigwig file were normalized using a factor determined similarly as in DESeq2 (<insert ref>)."
            else:
                # We normalize by million in order not to have too small values
                size = pd.read_table(input.norm_file).T.loc[wildcards.norm][0] / 1000000
                #scale = 1 / pd.read_table(input.summary, index_col=0).loc[
                #    wildcards.norm_file].loc[f"{wildcards.lib}_{wildcards.rep}"]
                norm_methods = f"Read counts in the bigwig file were normalized by million {wildcards.norm}."
            assert size > 0
            no_reads = """needLargeMem: trying to allocate 0 bytes (limit: 100000000000)
bam2bigwig.sh: bedGraphToBigWig failed
"""
            try:
                shell("""
                    bam2bigwig.sh {input.bam} {params.genome_binned} \\
                        {wildcards.lib}_{wildcards.rep} {wildcards.orientation} F \\
                        %f {output.bigwig_norm} \\
                        > {log.log} 2> {log.err} \\
                        || error_exit "bam2bigwig.sh failed"
                """ % size)
                with open(output.methods, "a") as methods:
                    (bedtools_v,) = re.match(r"bedtools\s+(.*)", getoutput("bedtools --version")).groups()
                    (bedops_v,) = re.match(r".*version:\s+(.*)\s+.*", getoutput("bedops --version | grep 'version'")).groups()
                    (bg2bw_v,) = re.match(r".*version:\s+([^)]+).*", getoutput("bedGraphToBigWig")).groups()
                    methods.write(f"# {rule}\nThe resulting alignment was used to generate a bigwig file with a custom bash script using bedtools (version {bedtools_v}), bedops (version {bedops_v}) and bedGraphToBigWig (version {bg2bw_v}).\n{norm_methods}\n")
            except CalledProcessError as e:
                if last_lines(log.err, 2) == no_reads:
                    warn(f"{output.bigwig_norm} will be empty.\n")
                    #with open(output.bigwig_norm, "w") as bwfile:
                    #    bwfile.write("")
                    with open(log.err, "a") as errfile:
                        errfile.write("Generating zero-signal bigwig.")
                    bw_out = pyBigWig.open(output.bigwig_norm, "w")
                    bw_out.addHeader(list(chrom_sizes.items()))
                    # for (chrom, chrom_len) in bw_out.chroms().items():
                    for (chrom, chrom_len) in chrom_sizes.items():
                        bw_out.addEntries(chrom, 0, values=np.nan_to_num(np.zeros(chrom_len)[0::10]), span=10, step=10)
                    bw_out.close()
                    with open(output.methods, "a") as methods:
                        methods.write(f"# {rule}\nAn empty bigwig file was generated for {output.bigwig_norm} due to the following issue:\n{str(e)}.\n")
                else:
                    raise
        #scale = 1 / size
        #assert scale > 0
        ## TODO: make this a function of deeptools version
#        no_reads = """Error: The generated bedGraphFile was empty. Please adjust
#your deepTools settings and check your input files.
#"""
#        no_reads = """[bwClose] There was an error while finishing writing a bigWig file! The output is likely truncated.
#"""
        #try:
        #    shell("""
        #        cmd="bamCoverage -b {input.bam} {params.orient_filter} \\
        #            -of=bigwig -bs 10 -p={threads} \\
        #            --scaleFactor %f -o {output.bigwig_norm} \\
        #            1>> {log.log} 2>> {log.err}"
        #        > {log.err}
        #        echo ${{cmd}} > {log.log}
        #        eval ${{cmd}} || error_exit "bamCoverage failed"
        #    """ % scale)
        #except CalledProcessError as e:
        #    if last_lines(log.err, 2) == no_reads:
        #        with open(output.bigwig_norm, "w") as bwfile:
        #            bwfile.write("")
        #    else:
        #        raise


rule merge_bigwig_reps:
    """This rule merges bigwig files by computing a mean across replicates."""
    input:
        bws = expand(OPJ(aligner, f"mapped_{genome}", "{{lib}}_{rep}", "{{lib}}_{rep}_{{read_type}}_on_%s_by_{{norm}}_{{orientation}}_{{feature_type}}.bw" % genome), rep=REPS),
        methods = expand(OPJ(aligner, f"mapped_{genome}", "{{lib}}_{rep}", "methods", "{{lib}}_{rep}_{{read_type}}_on_%s_by_{{norm}}_{{orientation}}_{{feature_type}}_methods.txt" % genome), rep=REPS),
    output:
        bw = OPJ(aligner, f"mapped_{genome}", "{lib}_mean", "{lib}_mean_{read_type}_on_%s_by_{norm}_{orientation}_{feature_type}.bw" % genome),
        methods = OPJ(aligner, f"mapped_{genome}", "{lib}_mean", "methods", "{lib}_mean_{read_type}_on_%s_by_{norm}_{orientation}_{feature_type}_methods.txt" % genome),
    log:
        warnings = OPJ(log_dir, "merge_bigwig_reps", "{lib}_mean_{read_type}_on_%s_by_{norm}_{orientation}_{feature_type}.warnings" % genome),
    threads: 2  # to limit memory usage, actually
    run:
        with warn_context(log.warnings) as warn:
            try:
                bws = [pyBigWig.open(bw_filename) for bw_filename in input.bws]
                #for bw_filename in input:
                #    bws.append(pyBigWig.open(bw_filename))
            except RuntimeError as e:
                warn(str(e))
                warn("Generating empty file.\n")
                # Make the file empty
                open(output.bw, "w").close()
                if hasattr(output, "methods"):
                    in_methods = " ".join(input.methods)
                    shell(f"cat {in_methods} > {output.methods}")
                    with open(output.methods, "a") as methods:
                        methods.write(f"# {rule}\nAn empty bigwig file was generated for {output.bw} due to the following issue:\n{str(e)}\n")
            else:
                bw_out = pyBigWig.open(output.bw, "w")
                bw_out.addHeader(list(chrom_sizes.items()))
                for (chrom, chrom_len) in chrom_sizes.items():
                    try:
                        assert all([bw.chroms()[chrom] == chrom_len for bw in bws])
                    except KeyError as e:
                        warn(str(e))
                        warn(f"Chromosome {chrom} might be missing from one of the input files.\n")
                        for filename, bw in zip(input.bws, bws):
                            msg = " ".join([f"{filename}:", *list(bw.chroms().keys())])
                            warn(f"{msg}:\n")
                        #raise
                        warn(f"The bigwig files without {chrom} will be skipped.\n")
                    to_use = [bw for bw in bws if chrom in bw.chroms()]
                    if to_use:
                        means = np.nanmean(np.vstack([bw.values(chrom, 0, chrom_len, numpy=True) for bw in to_use]), axis=0)
                    else:
                        means = np.zeros(chrom_len)
                    # bin size is 10
                    bw_out.addEntries(chrom, 0, values=np.nan_to_num(means[0::10]), span=10, step=10)
                bw_out.close()
                for bw in bws:
                    bw.close()
                if hasattr(output, "methods"):
                    in_methods = " ".join(input.methods)
                    shell(f"cat {in_methods} > {output.methods}")
                    with open(output.methods, "a") as methods:
                        methods.write(f"# {rule}\nA bigwig file averaging the signals from individual replicates was generated using pyBigWig {pyBigWig.__version__} and numpy {np.__version__}.\n")


rule make_bigwig_ratios:
    """This rule tries to make a bigwig file displaying the ratio between a condition and a reference, from the corresponding two normalized bigwig files."""
    input:
        cond_bw = OPJ(aligner, f"mapped_{genome}", "{cond}_{{rep}}", "{cond}_{{rep}}_{{read_type}}_on_%s_by_{{norm}}_{{orientation}}_{{feature_type}}.bw" % genome),
        ref_bw = OPJ(aligner, f"mapped_{genome}", "{ref}_{{rep}}", "{ref}_{{rep}}_{{read_type}}_on_%s_by_{{norm}}_{{orientation}}_{{feature_type}}.bw" % genome),
    output:
        bw = OPJ(aligner, f"mapped_{genome}", "{cond}_vs_{ref}_ratio", "{cond}_vs_{ref}_ratio_{read_type}_on_%s_by_{norm}_{orientation}_{feature_type}.bw" % genome),
    log:
        warnings = OPJ(log_dir, "make_bigwig_ratios", "{cond}_vs_{ref}_ratio_{read_type}_on_%s_by_{norm}_{orientation}_{feature_type}.warnings" % genome),
    threads: 8  # to limit memory usage, actually
    run:
        with warn_context(log.warnings) as warn:
            try:
                cond_bw = pyBigWig.open(input.cond_bw)
                ref_bw = pyBigWig.open(input.ref_bw)
                #for bw_filename in input:
                #    bws.append(pyBigWig.open(bw_filename))
            except RuntimeError as e:
                warn(str(e))
                warn("Generating empty file.\n")
                # Make the file empty
                open(output.bw, "w").close()
            else:
                bw_out = pyBigWig.open(output.bw, "w")
                bw_out.addHeader(list(chrom_sizes.items()))
                # for (chrom, chrom_len) in bw_out.chroms().items():
                for (chrom, chrom_len) in chrom_sizes.items():
                    assert cond_bw.chroms()[chrom] == chrom_len
                    #ratios = np.divide(cond_bw.values(chrom, 0, chrom_len), ref_bw.values(chrom, 0, chrom_len))
                    ratios = np.log2(np.divide(cond_bw.values(chrom, 0, chrom_len, numpy=True), ref_bw.values(chrom, 0, chrom_len, numpy=True)))
                    # bin size is 10
                    bw_out.addEntries(chrom, 0, values=np.nan_to_num(ratios[0::10]), span=10, step=10)
                bw_out.close()
                for bw in bws:
                    bw.close()


##########################
# Read sequence analyses #
##########################


# TODO: check and activate
rule compute_size_distribution:
    input:
        source_fastq
    output:
        OPJ("read_stats", "{lib}_{rep}_{read_type}_size_distribution.txt")
    message:
        "Computing read size distribution for {wildcards.lib}_{wildcards.rep}_{wildcards.read_type}."
    shell:
        """
        zcat {input} | compute_size_distribution {output}
        """


rule plot_size_distribution:
    input:
        rules.compute_size_distribution.output
    output:
        OPJ("figures", "{lib}_{rep}", "{read_type}_size_distribution.pdf"),
    message:
        "Plotting size distribution for {wildcards.lib}_{wildcards.rep}_{wildcards.read_type}."
    run:
        data = pd.read_table(input[0], header=None, names=("size", "count"), index_col=0)
        title = f"read size distribution for {wildcards.lib}_{wildcards.rep}_{wildcards.read_type}"
        plot_histo(output[0], data, title)


def get_type_max_len(wildcards):
    """Returns the maximum length for a "pi", "si", or "mi" read type.
    Otherwise returns 0."""
    read_type = wildcards.read_type
    try:
        # ex: "18-26"
        return read_type_max_len[read_type]
    except KeyError:
        try:
            # ex: siuRNA
            return read_type_max_len[read_type[:2]]
        except KeyError:
            # ex: nomap_siRNA
            general_type = read_type.split("_")[1]
            return read_type_max_len.get(general_type[:2], 0)


rule compute_base_composition_along:
    input:
        reads = source_fastq,
        # rules.select_size_range.output.size_selected
    output:
        composition = OPJ(data_dir, "read_stats", "{lib}_{rep}_{read_type}_base_composition_from_{position}.txt"),
        proportions = OPJ(data_dir, "read_stats", "{lib}_{rep}_{read_type}_base_proportions_from_{position}.txt"),
    params:
        max_len = get_type_max_len,
    message:
        "Computing base composition from {wildcards.position} for {wildcards.read_type} reads for {wildcards.lib}_{wildcards.rep}."
    run:
        # "A": 0
        # "C": 1
        # "G": 2
        # "N": 3
        # "T": 4
        letter2index = dict((letter, idx) for (idx, letter) in enumerate(sorted(COMPL)))
        composition = np.zeros((len(COMPL), params.max_len))
        if wildcards.position == "start":
            slicer = slice(None)
        elif wildcards.position == "end":
            slicer = slice(params.max_len - 1, None, -1)
        else:
            raise NotImplementedError("Unknown position: %s" % wildcards.position)
        # Could this be cythonized?
        for (_, sequence, _) in fastx_read(input.reads):
            for (pos, letter) in enumerate(sequence[slicer]):
                composition[letter2index[letter]][pos] += 1
        composition = pd.DataFrame(composition.T)
        composition.index = [pos + 1 for pos in range(params.max_len)]
        composition.index.name = "position"
        composition.columns = sorted(COMPL)
        composition.to_csv(output.composition, sep="\t")
        proportions = (composition.T / composition.sum(axis=1)).T
        proportions.to_csv(output.proportions, sep="\t")


def position2extraction(wildcards):
    position = wildcards.position
    if position == "first":
        return "1"
    elif position == "last":
        return "length($seq)"
    else:
        return position


rule compute_base_composition:
    input:
        reads = source_fastq,
        # rules.select_size_range.output.size_selected
    output:
        data = OPJ(data_dir, "read_stats", "{lib}_{rep}_{read_type}_{position}_base_composition.txt"),
    params:
        extract_pos = position2extraction,
    message:
        "Computing {wildcards.position} base composition for {wildcards.read_type} reads for {wildcards.lib}_{wildcards.rep}."
    shell:
        """
        echo -e "#read_length\\tA\\tC\\tG\\tN\\tT" > {output.data}
        bioawk -c fastx 'BEGIN {{nucls["A"] = "A"; nucls["C"] = "C"; nucls["G"] = "G"; nucls["T"] = "T"; nucls["N"] = "N"}} {{histo[substr($seq, {params.extract_pos}, 1), length($seq)]++; tot[length($seq)]++}} END {{for (l in tot) {{printf "%d", l; for (nucl in nucls) {{printf "\\t%d", histo[nucl,l]}}; printf "\\n"}}}}' {input.reads} \\
            | sort -n -k1 \\
            >> {output.data}
        """


rule compute_base_proportions:
    input:
        reads = source_fastq,
        #rules.select_size_range.output.size_selected
    output:
        data = OPJ(data_dir, "read_stats", "{lib}_{rep}_{read_type}_{position}_base_proportions.txt"),
    params:
        extract_pos = position2extraction,
    message:
        "Computing {wildcards.position} base proportions for {wildcards.read_type} reads for {wildcards.lib}_{wildcards.rep}."
    shell:
        """
        echo -e "#read_length\\tA\\tC\\tG\\tN\\tT" > {output.data}
        # LC_ALL="C" forces the use of dots in numbers (commas complicate further processing)
        LC_ALL="C" bioawk -c fastx 'BEGIN {{nucls["A"] = "A"; nucls["C"] = "C"; nucls["G"] = "G"; nucls["T"] = "T"; nucls["N"] = "N"}} {{histo[substr($seq, {params.extract_pos}, 1), length($seq)]++; tot[length($seq)]++}} END {{for (l in tot) {{printf "%d", l; for (nucl in nucls) {{printf "\\t%f", histo[nucl,l] / tot[l]}}; printf "\\n"}}}}' {input.reads} \\
            | sort -n -k1 \\
            >> {output.data}
        """


rule plot_base_proportions:
    input:
        reads = source_fastq,
    output:
        base_props_along = OPJ("figures", "{lib}_{rep}", "{read_type}_base_proportions_along.pdf"),
        used_data = directory(OPJ("figures", "{lib}_{rep}", "{read_type}_base_proportions_along_data")),
    params:
        max_len = lambda wildcards: read_type_max_len[wildcards.read_type],
        min_len = lambda wildcards: read_type_min_len[wildcards.read_type],
    message:
        "Exploring composition for {wildcards.read_type} reads for {wildcards.lib}_{wildcards.rep}."
    threads: 1
    run:
        rs = ReadStats(input.reads,
            alphabet="ACGT",
            min_len=params.min_len, max_len=params.max_len,
            threads=threads)
        (_, _, _, plotted_data) = plot_base_proportions_along_read(
            [rs.proportions_along_from_start, rs.proportions_along_from_end],
            data_labels=["from read start", "from read end"],
            alphabet="ACGT",
            fig_path=output.base_props_along,
            title="Base proportions along read length")
        data_dir = Path(output.used_data)
        data_dir.mkdir(parents=False, exist_ok=False)
        for (label, data) in plotted_data.items():
            fname = label.replace(" ", "_")
            data.to_csv(
                data_dir.joinpath(f"{fname}.csv"),
                sep="\t")


# def plot_text(outfile, text, title=None):
#     """This can be used to generate dummy figures in case the data is not suitable."""
#     fig = plt.figure()
#     ax = fig.add_subplot(111)
#     txt = ax.text(0, 0, text)
#     # https://stackoverflow.com/a/21833883/1878788
#     txt.set_clip_on(False)
#     if title is not None:
#         plt.title(title)
#     plt.tight_layout()
#     plt.savefig(outfile)


letter2colour = {"A" : "crimson", "C" : "mediumblue", "G" : "yellow", "T" : "forestgreen", "N" : "black"}
def plot_ACGT(data, xlabel, ylabel):
    fig = plt.figure(figsize=(12,8))
    ax = fig.add_subplot(111)
    ax.set_xlim([data.index[0] - 0.5, data.index[-1] + 0.5])
    #ax.set_ylim([0, 100])
    bar_width = 0.125
    letter2legend = dict(zip("ACGTN", "ACGTN"))
    usetex = mpl.rcParams.get("text.usetex", False)
    if usetex:
        xlabel = sub("_", r"\_", xlabel)
        ylabel = sub("_", r"\_", ylabel)
        data.columns = [texscape(colname) for colname in data.columns]
    for (read_len, base_props) in data.iterrows():
        x_shift = -0.25
        for letter, base_count in base_props.iteritems():
            plt.bar(
                read_len + x_shift,
                base_count,
                width=bar_width,
                color=letter2colour[letter],
                label=letter2legend[letter])
            # Avoid redundant legends after this letter
            # has been labeled a first time
            letter2legend[letter] = "_nolegend_"
            x_shift += bar_width
    ax.legend()
    ax.set_xticks(data.index)
    ax.set_xticklabels(data.index)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)


def plot_logo(data, ylabel):
    entropies = -np.sum(data * np.log2(data), axis=1)
    information = 2 - entropies
    fig = plt.figure(figsize=(12,8))
    ax = fig.add_subplot(111)
    ax.set_xlim([data.index[0] - 0.5, data.index[-1] + 0.5])
    ax.set_ylim([0, 2])
    letter2legend = dict(zip("ACGTN", "ACGTN"))
    usetex = mpl.rcParams.get("text.usetex", False)
    if usetex:
        ylabel = sub("_", r"\_", ylabel)
    for (read_len, rel_heights) in data.mul(information, axis=0).iterrows():
        y_base = 0
        for (letter, rel_height) in rel_heights.sort_values().iteritems():
            plt.bar(
                read_len,
                rel_height,
                bottom=y_base,
                align="center",
                width=0.33,
                color=letter2colour[letter],
                label=letter2legend[letter])
            # Avoid redundant legends after this letter
            # has been labeled a first time
            letter2legend[letter] = "_nolegend_"
            y_base += rel_height
    ax.legend()
    ax.set_xticks(data.index)
    ax.set_xticklabels(data.index)
    ax.set_xlabel("read length")
    ax.set_ylabel(ylabel)


# TODO: use the log files
rule plot_base_logo_along:
    input:
        data = rules.compute_base_composition_along.output.proportions,
    output:
        figure = OPJ("figures", "{lib}_{rep}", "{read_type}_base_logo_from_{position}.pdf"),
    message:
        "Plotting base logo from {wildcards.position} for {wildcards.read_type} reads for {wildcards.lib}_{wildcards.rep}."
    log:
        # Currently not used
        #log = OPJ(log_dir, "plot_base_logo", "{lib}_{rep}_{read_type}.log"),
        #err = OPJ(log_dir, "plot_base_logo", "{lib}_{rep}_{read_type}.err"),
        warnings = OPJ(log_dir, "plot_base_logo_along", "{lib}_{rep}_{read_type}_from_{position}.warnings"),
    run:
        with warn_context(log.warnings) as warn:
            data = pd.read_table(input.data, header=0, index_col=0)
            try:
                ylabel = f"information at position from {wildcards.position} (bits)"
                title = f"{wildcards.lib}_{wildcards.rep}_{wildcards.read_type}"
                save_plot(
                    output.figure, plot_logo, data, ylabel, title=title)
            except IndexError as e:
                if str(e) == "index 0 is out of bounds for axis 0 with size 0":
                    warn(f"Got IndexError {str(e)}\nNot enough data to make {output.figure}\n")
                    plot_text(output.figure, "Not enough data.", title)
                else:
                    raise


rule plot_base_logo:
    input:
        data = rules.compute_base_proportions.output.data,
    output:
        figure = OPJ("figures", "{lib}_{rep}", "{read_type}_{position}_base_logo.pdf"),
    message:
        "Plotting {wildcards.position} base logo for {wildcards.read_type} reads for {wildcards.lib}_{wildcards.rep}."
    log:
        # Currently not used
        #log = OPJ(log_dir, "plot_base_logo", "{lib}_{rep}_{read_type}_{position}.log"),
        #err = OPJ(log_dir, "plot_base_logo", "{lib}_{rep}_{read_type}_{position}.err"),
        warnings = OPJ(log_dir, "plot_base_logo", "{lib}_{rep}_{read_type}_{position}.warnings"),
    run:
        with warn_context(log.warnings) as warn:
            data = pd.read_table(input.data, header=0, index_col=0)
            try:
                ylabel = f"information at {wildcards.position} position (bits)"
                title = f"{wildcards.lib}_{wildcards.rep}_{wildcards.read_type}"
                save_plot(
                    output.figure, plot_logo, data, ylabel, title=title)
            except IndexError as e:
                if str(e) == "index 0 is out of bounds for axis 0 with size 0":
                    warn(f"Got IndexError {str(e)}\nNot enough data to make {output.figure}\n")
                    plot_text(output.figure, "Not enough data.", title)
                else:
                    raise


rule plot_base_composition_along:
    input:
        data = rules.compute_base_composition_along.output.composition,
    output:
        figure = OPJ("figures", "{lib}_{rep}", "{read_type}_base_composition_from_{position}.pdf"),
        seqlogo = OPJ("figures", "{lib}_{rep}", "{read_type}_seqlogo_from_{position}.pdf"),
    message:
        "Plotting base composition from {wildcards.position} for {wildcards.read_type} reads for {wildcards.lib}_{wildcards.rep}."
    log:
        # Currently not used
        #log = OPJ(log_dir, "plot_base_composition", "{lib}_{rep}_{read_type}.log"),
        #err = OPJ(log_dir, "plot_base_composition", "{lib}_{rep}_{read_type}.err"),
        warnings = OPJ(log_dir, "plot_base_composition_along", "{lib}_{rep}_{read_type}_from_{position}.warnings"),
    run:
        with warn_context(log.warnings) as warn:
            data = pd.read_table(input.data, header=0, index_col=0)
            try:
                ylabel = f"base count at position from {wildcards.position}"
                title = f"{wildcards.lib}_{wildcards.rep}_{wildcards.read_type}"
                save_plot(
                    output.figure, plot_ACGT, data, "position", ylabel, title=title)
                mpl.use("Agg")
                fig, axes = draw_logo(data[["A", "C", "G", "T"]].to_dict(orient="list"), data_type="counts")
                plt.savefig(output.seqlogo, bbox_inches="tight")
                mpl.use("PDF")
            except IndexError as e:
                if str(e) == "index 0 is out of bounds for axis 0 with size 0":
                    warn(f"Got IndexError {str(e)}\nNot enough data to make {output.figure}\n")
                    plot_text(output.figure, "Not enough data.", title)
                    plot_text(output.seqlogo, "Not enough data.", title)
                else:
                    raise


rule plot_base_composition:
    input:
        data = rules.compute_base_composition.output.data,
    output:
        figure = OPJ("figures", "{lib}_{rep}", "{read_type}_{position}_base_composition.pdf"),
    message:
        "Plotting {wildcards.position} base composition for {wildcards.read_type} reads for {wildcards.lib}_{wildcards.rep}."
    log:
        # Currently not used
        #log = OPJ(log_dir, "plot_base_composition", "{lib}_{rep}_{read_type}_{position}.log"),
        #err = OPJ(log_dir, "plot_base_composition", "{lib}_{rep}_{read_type}_{position}.err"),
        warnings = OPJ(log_dir, "plot_base_composition", "{lib}_{rep}_{read_type}_{position}.warnings"),
    run:
        with warn_context(log.warnings) as warn:
            data = pd.read_table(input.data, header=0, index_col=0)
            try:
                ylabel = f"base count at {wildcards.position} position"
                title = f"{wildcards.lib}_{wildcards.rep}_{wildcards.read_type}"
                save_plot(
                    output.figure, plot_ACGT, data, "read_length", ylabel, title=title)
            except IndexError as e:
                if str(e) == "index 0 is out of bounds for axis 0 with size 0":
                    warn(f"Got IndexError {str(e)}\nNot enough data to make {output.figure}\n")
                    plot_text(output.figure, "Not enough data.", title)
                else:
                    raise


def plot_barchart(summary):
    #summary.plot.pie(0, autopct='%.2f', figsize=(6,6))
    usetex = mpl.rcParams.get("text.usetex", False)
    if usetex:
        summary.index = [texscape(colname) for colname in summary.index]
    ax = summary.plot.barh(legend=None)
    ax.set_xlabel("Read counts")


rule plot_summary_barchart:
    input:
        rules.make_read_counts_summary.output
    output:
        barchart = OPJ("figures", "{lib}_{rep}", "%s_smallRNA_barchart.pdf" % size_selected),
    message:
        "Plotting small RNA barchart for {wildcards.lib}_{wildcards.rep}_%s." % size_selected
    log:
        plot_log = OPJ(log_dir, "plot_summary_barchart", "{lib}_{rep}_%s.log" % size_selected),
        plot_err = OPJ(log_dir, "plot_summary_barchart", "{lib}_{rep}_%s.err" % size_selected)
    run:
        name = f"{wildcards.lib}_{wildcards.rep}"
        summary = pd.read_table(input[0]).T.astype(int).loc[["nomap_siRNA", "ambig_type", "siRNA", "piRNA", "miRNA"]]
        summary.columns = [name]
        save_plot(output.barchart, plot_barchart, summary, title="%s small RNAs" % name)

onsuccess:
    print("small RNA-seq analysis finished.")
    cleanup_and_backup(output_dir, config, delete=True)


onerror:
    shell(f"rm -rf {output_dir}_err")
    shell(f"cp -rp {output_dir} {output_dir}_err")
    if upload_on_err:
        cleanup_and_backup(output_dir + "_err", config)
    print("small RNA-seq analysis failed.")

