Trim 5' 4nt
(trim low quality 3')
Map
Compute distance distribution of 5' ends with respect to 22G (only those degradome reads within +- 20 nt of a given 22G), where 22G can be input, IP, possibly restricted to belong to a given list of genes.
See Shen et al 2018, fig 2
Take 22G from 44hph CSR1 IP analysis: for instance results_csr1_ADH_IP/bowtie2/mapped_C_elegans/WT_44hph_IP_1/prot_sisiuRNA_on_C_elegans_sorted.bam, either individual small RNA-seq replicates, or fused bam files

Should comparison of different degradome conditions be with the same 22G bam file, or with 22G bam files of their corresponding conditions?
-> Probably better to use the WT as a reference for where 22G are expected to act, and use it for both conditions of the degradome library.

Then, we will have to compare WT and CSR1-ADH degradome


Test data: /pasteur/entites/Mhe/Illumina/miniseq_sequencing_results/180711_MN00481_0020_A000H2HKCG/Alignment_1/20180712_052723/Fastq/

Look at piPipes (http://dx.doi.org/10.1093/bioinformatics/btu647)


Try distance distributioon on filtered genes:
csr1_prot_si_supertargets_44hph_ids.txt (>= 200RPKM lfc(IP/input) >= 2 csr1IP_44hph)
csr1_prot_si_supertargets_44hph_top200IP_ids (>= 200RPKM lfc(IP/input) >= 2 csr1IP_44hph, top 200 by IP RPM)

Try with
csr1ADH_vs_WT_all_alltypes_up_genes_non_spermatogenic_ids.txt
csr1ADH_vs_WT_all_alltypes_up_genes_ids.txt
csr1ADH_vs_WT_all_alltypes_up_genes_spermatogenic_ids.txt

    FILTERS="csr1_prot_si_targets_44hph csr1_prot_si_supertargets_44hph csr1_prot_si_supertargets_44hph_top200IP csr1_prot_si_supertargets_44hph_top200lfc csr1ADH_vs_WT_all_alltypes_up_genes csr1ADH_vs_WT_all_alltypes_up_genes_spermatogenic csr1ADH_vs_WT_all_alltypes_up_genes_non_spermatogenic"
    LIBS="WT ADH"
    for filter in ${FILTERS}
    do
        for lib in ${LIBS}
        do
            echo ${lib}_${filter} 1>&2
            time ~/src/bioinfo_utils/Degradome-seq/degradome_metaprofile.py \
                -s /pasteur/entites/Mhe/bli/small_RNA-seq_analyses/results_csr1_ADH_IP/bowtie2/mapped_C_elegans/WT_44hph_IP_1/prot_sisiuRNA_on_C_elegans_sorted.bam \
                -d /pasteur/entites/Mhe/bli/Degradome-seq_analyses/results_csr1ADH_44hph/bowtie2/mapped_C_elegans/${lib}_44hph_1_on_C_elegans_sorted.bam \
                -g /pasteur/entites/Mhe/bli/Gene_lists/${filter}_ids.txt \
                -a /pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding.gtf \
                > /pasteur/entites/Mhe/bli/Degradome-seq_analyses/test_data/distances_${filter}_${lib}.txt
        done
    done 2> /pasteur/entites/Mhe/bli/Degradome-seq_analyses/test_data/tests.log


Issue: peaks of sRNA are too broad
==================================

Bedgraph giving number of 5' ends in sRNA and Degradome, for visual inspection
Coverage correlation, possibly filtering by gene ?

Similarly to TE:
Degradation efficiency: for a given gene: ratio of Degradome TPM / RNA TPM, should decrease in ADH for genes that are targeted.

See if CLIP reads are more suited (not all IP reads will be acting on the mRNA), using sense pairs (for mRNA reads) and antisense (sRNA reads) separately. (fix mapping parameters for iCLIP first, try mapping tests with -N 0, different parameters for each size range category)
